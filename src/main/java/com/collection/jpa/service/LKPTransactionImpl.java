package com.collection.jpa.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.TypedQuery;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.collection.jpa.CustomUserDetails;
import com.collection.jpa.dao.DelegationBucketRepository;
import com.collection.jpa.dao.SKPCTransactionRepository;
import com.collection.jpa.dao.LKPTransactionRepository;
import com.collection.jpa.dao.PrintActivityRepository;
import com.collection.jpa.dto.LkpCriteria;
import com.collection.jpa.dto.BucketDetailDTO;
import com.collection.jpa.dto.LkpHeaderDTO;
import com.collection.jpa.dto.LkpPageDTO;
import com.collection.jpa.dto.LkpDataDTO;
import com.collection.jpa.dto.LkpCyclePageDTO;
import com.collection.jpa.dto.LkpPrintDTO;
import com.collection.jpa.dto.LkpPrintPageDTO;
import com.collection.jpa.dto.MemberCriteria;
import com.collection.jpa.dto.CollectionMemberDTO;
import com.collection.jpa.dto.LkpTeamPageDTO;
import com.collection.jpa.dto.PrintCriteria;
import com.collection.jpa.dto.ReclusteringDTO;
import com.collection.jpa.dto.SpecialDelegationDTO;
import com.collection.jpa.dto.StaffCriteria;
import com.collection.jpa.dto.TeamCriteria;
import com.collection.jpa.dto.TeamDTO;
import com.collection.jpa.entity.Bucket;
import com.collection.jpa.entity.CollectionTeam;
import com.collection.jpa.entity.DelegationBucket;
import com.collection.jpa.entity.Employee;
import com.collection.jpa.entity.LKPEntryStatus;
import com.collection.jpa.entity.SKPCTransaction;
import com.collection.jpa.entity.Status;
import com.collection.jpa.entity.LKPTransaction;
import com.collection.jpa.entity.PrintActivity;
import com.collection.jpa.util.Page;
import com.collection.jpa.util.ServiceResult;


@Repository
@Transactional(readOnly = true)
public class LKPTransactionImpl implements LKPTransactionService {
	
	Date currentDate = new Date();

	@Autowired
	private LKPTransactionRepository repository;
	
	@Autowired
	private PrintActivityRepository printrepository;

	@Autowired
	private SKPCTransactionRepository skpcrepository;

	@Autowired
	private DelegationBucketRepository bucketrepository;

	@Autowired
	private QueryImpl queryService;

	@Autowired
	private CollectionTeamImpl teamService;

	@Autowired
	private EmployeeImpl employeeService;


    private List<LkpPrintDTO> getPrintList(List<LkpDataDTO> printList) {
		List<LkpPrintDTO> lkpList = new ArrayList<LkpPrintDTO>();

		for (LkpDataDTO m: printList) {
			LkpPrintDTO lkp = new LkpPrintDTO();
			lkp.setId(m.getId());
			lkp.setTeamId(m.getTeamId());
			lkp.setSupervisorId(m.getSupervisorId());
			lkp.setContractNo(m.getContractNo());
			lkp.setContractDate(m.getContractDate());
			lkp.setCustName(m.getCustName());
			lkp.setCustBillingAddress(m.getCustBillingAddress());
			lkp.setPhone1(m.getPhone1());
			lkp.setPhone2(m.getPhone2());
			lkp.setLeastInstallmentNo(m.getLeastInstallmentNo());
			lkp.setLastInstallmentNo(m.getLastInstallmentNo());
			lkp.setTopInstallment(m.getTopInstallment());
			lkp.setLeastDueDate(m.getLeastDueDate());
			lkp.setLastDueDate(m.getLastDueDate());
			lkp.setOverdue(m.getOverdue());
			lkp.setOutstandingPrincipalAmnt(m.getOutstandingPrincipalAmnt());
			lkp.setOutstandingInterestAmnt(m.getOutstandingInterestAmnt());
			lkp.setMonthlyInstallment(m.getMonthlyInstallment());
			lkp.setBusinessUnit(m.getBusinessUnit());
			lkp.setBrand(m.getBrand());
			lkp.setModel(m.getModel());
			lkp.setColor(m.getColor());
			lkp.setFrameNo(m.getFrameNo());
			lkp.setEngineNo(m.getEngineNo());
			lkp.setPoliceNo(m.getPoliceNo());
			lkp.setStnk(m.getStnk());
			lkp.setPrintNo(m.getPrintNo());
			lkp.setPrintDate(m.getPrintDate());
			List<BucketDetailDTO> theDetails = queryService.bucketDetail(m.getContractNo(), m.getLeastInstallmentNo(), m.getLastInstallmentNo()).getResultList();
			lkp.setDetails(theDetails);

			lkpList.add(lkp);
		}

		return lkpList;
	}

    @PreAuthorize("hasRole('ROLE_SUPERADMIN') or hasRole('ROLE_COLLECTION_HEAD') or hasRole('ROLE_COLLECTION_SPV')")
    public Page<LkpCyclePageDTO> pageManualAssignment(Pageable pageable) {
        CustomUserDetails user = (CustomUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        LkpCriteria lkpcriteria = new LkpCriteria();
        lkpcriteria.setOfficeCode(user.getOfficeCode());
        
        if (user.getJobCode().equals("COLLSPV")) {
            TeamCriteria teamcriteria = new TeamCriteria();
            teamcriteria.setSpvId(user.getEmplId());
            List<TeamDTO> teams = queryService.collectionTeam(teamcriteria).getResultList();
            List<Integer> cycleIdList = new ArrayList<Integer>();
            for (TeamDTO team : teams) {
            	cycleIdList.add(team.getCycleId());
            }
        	lkpcriteria.setCycleIdList(cycleIdList);
    	}

        TypedQuery<LkpCyclePageDTO> query = queryService.lkpManualAssignment(lkpcriteria);
		if (query == null) {
    		return new Page<LkpCyclePageDTO>();
        }
		return (Page<LkpCyclePageDTO>) new Page<LkpCyclePageDTO>(query, pageable);
	}
	
    @PreAuthorize("hasRole('ROLE_SUPERADMIN') or hasRole('ROLE_COLLECTION_HEAD') or hasRole('ROLE_COLLECTION_SPV')")
	public Page<LkpTeamPageDTO> pageClustering(Pageable pageable) {
        CustomUserDetails user = (CustomUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

    	LkpCriteria lkpcriteria = new LkpCriteria();
        lkpcriteria.setOfficeCode(user.getOfficeCode());

        if (user.getJobCode().equals("COLLSPV")) {
            TeamCriteria teamcriteria = new TeamCriteria();
            teamcriteria.setSpvId(user.getEmplId());
            List<TeamDTO> teams = queryService.collectionTeam(teamcriteria).getResultList();
            List<Integer> teamIdList = new ArrayList<Integer>();
            for (TeamDTO team : teams) {
                teamIdList.add(team.getId());
            }
        	lkpcriteria.setTeamIdList(teamIdList);
    	}
    	
		TypedQuery<LkpTeamPageDTO> query = queryService.lkpClustering(lkpcriteria);
		if (query == null) {
    		return new Page<LkpTeamPageDTO>();
        }
		return (Page<LkpTeamPageDTO>) new Page<LkpTeamPageDTO>(query, pageable);
	}
	
    @Transactional
	@Modifying(clearAutomatically = true)
    @PreAuthorize("hasRole('ROLE_COLLECTION_SPV')")
	public ServiceResult assign(LKPTransaction persisted) {
        CustomUserDetails user = (CustomUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        if (!(user.getJobCode().equals("COLLSPV"))) {
			return new ServiceResult(ServiceResult.MessageType.ERROR, "Not authorized user", null);
        }
        
        if (persisted == null || persisted.getId() == null) {
    		return new ServiceResult(ServiceResult.MessageType.ERROR, "Contract not defined", null);
    	}

        LKPTransaction lkp = repository.findOne(persisted.getId());
        if (lkp == null) {
    		return new ServiceResult(ServiceResult.MessageType.ERROR, "Contract not found", null);
        }

        if (lkp.getAssignedTo() != null && lkp.getTeam() != null) {
    		return new ServiceResult(ServiceResult.MessageType.ERROR, "Contract already assigned (to another team)", lkp.getId());
    	}

        if (persisted.getSupervisor() == null || persisted.getSupervisor().getId() != user.getEmplId()) {
			return new ServiceResult(ServiceResult.MessageType.ERROR, "Not valid Supervisor", null);
    	}
    	
        if (persisted.getTeam() == null || persisted.getTeam().getId() == null) {
			return new ServiceResult(ServiceResult.MessageType.ERROR, "Please specify team", null);
        }
        TeamCriteria criteria = new TeamCriteria();
        criteria.setId(persisted.getTeam().getId());
        criteria.setOfficeId(user.getOfficeId());
        criteria.setSpvId(user.getEmplId());
        List<TeamDTO> teamList = queryService.collectionTeam(criteria).getResultList();
        if (teamList.isEmpty()) {
			return new ServiceResult(ServiceResult.MessageType.ERROR, "Not valid Supervisor", null);
        }
    	CollectionTeam team = teamService.findOne(teamList.get(0).getId());
    	Employee spv = employeeService.findOne(persisted.getSupervisor().getId());

    	if (persisted.getAssignedTo() == null || persisted.getAssignedTo().getId() == null) {
    		return new ServiceResult(ServiceResult.MessageType.ERROR, "Collector has not been assigned", null);
    	}
    	
    	Employee empl = employeeService.findOne(persisted.getAssignedTo().getId());
    	if (empl == null) {
    		return new ServiceResult(ServiceResult.MessageType.ERROR, "Employee not found", null);
    	}

    	if (queryService.hasMaxLoadReached(empl.getId())) {
    		return new ServiceResult(ServiceResult.MessageType.ERROR, "Has reached the maximum load", lkp.getId());
    	}
    	
    	lkp.setAssignedTo(empl);
    	lkp.setTeam(team);
    	lkp.setSupervisor(spv);
    	
    	try {
    		repository.save(lkp);
		} catch (Exception excp) {
			return new ServiceResult(ServiceResult.MessageType.ERROR, "Contract assignment failed", null);
		}
    	return new ServiceResult(ServiceResult.MessageType.SUCCESS, String.format("Contract #%s successfully assigned", lkp.getBucket().getContractNo()), lkp.getId());
	}

    @PreAuthorize("hasRole('ROLE_COLLECTION_SPV')")
	public List<LkpPrintDTO> listPrint(StaffCriteria criteria) {
        CustomUserDetails user = (CustomUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        if (!user.getJobCode().equals("COLLSPV")) {
        	return null;
    	}

    	if (criteria == null || criteria.getStaffId() == null) {
    		return null;
    	}

    	LkpCriteria lkpcriteria = new LkpCriteria();
        lkpcriteria.setOfficeCode(user.getOfficeCode());
    	lkpcriteria.setSupervisorId(user.getEmplId());
        lkpcriteria.setAssignedTo(criteria.getStaffId());
        lkpcriteria.setPrintId(0);

        TypedQuery<LkpDataDTO> query = queryService.lkpPrint(lkpcriteria);
		if (query == null) {
			return null;
        }

        List<LkpDataDTO> printList = query.getResultList();
        System.out.println(printList);
		return getPrintList(printList);
    }

    @PreAuthorize("hasRole('ROLE_COLLECTION_SPV')")
	public LkpHeaderDTO headerPrint(StaffCriteria criteria) {
        CustomUserDetails user = (CustomUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        if (!user.getJobCode().equals("COLLSPV")) {
        	return null;
    	}

    	if (criteria == null || criteria.getStaffId() == null) {
    		return null;
    	}

    	MemberCriteria membercriteria = new MemberCriteria();
        if (user.getJobCode().equals("COLLSPV")) {
        	membercriteria.setSupervisorId(user.getEmplId());
    	}
    	membercriteria.setEmplId(criteria.getStaffId());
    	System.out.println("membercriteria: " + membercriteria);
    	TypedQuery<CollectionMemberDTO> query = queryService.collectionMember(membercriteria);
		
    	CollectionMemberDTO member = new CollectionMemberDTO();
		try {
			member = query.getSingleResult();
		} catch (Exception excp) {
		}

    	if (member.getEmplId() == null) {
    		return null;
    	}

        LkpHeaderDTO header = new LkpHeaderDTO();
		header.setTeamId(member.getTeamId());
		header.setTeamName(member.getTeamName());
		header.setEmployeeId(member.getEmplId());
		header.setEmployeeRegNo(member.getEmplRegNo());
		header.setEmployeeName(member.getEmplName());

		return header;
	}

	@Transactional
	@Modifying(clearAutomatically = true)
    @PreAuthorize("hasRole('ROLE_COLLECTION_SPV')")
	public ServiceResult setPrinted(LkpHeaderDTO header, List<LkpPrintDTO> listData) {
        CustomUserDetails user = (CustomUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        if (!user.getJobCode().equals("COLLSPV")) {
			return new ServiceResult(ServiceResult.MessageType.ERROR, "Not authorized user", null);
    	}

        if (listData.size() == 0) {
        	return new ServiceResult(ServiceResult.MessageType.INFO, "No LKP to print", null);
        }

		PrintActivity print = new PrintActivity();
		print.setTeamId(header.getTeamId());
		print.setEmployeeId(header.getEmployeeId());
        print.setPrintDate(header.getPrintDate());
        print.setPrintNo(header.getPrintNo());
        print.setType("C");

        try {
            PrintActivity result = printrepository.save(print);
            Integer printId = result.getId();
            
            List<Integer> theList = new ArrayList<Integer>();
            for (LkpPrintDTO m: listData) {
                theList.add(m.getId());
            }
            
            queryService.lkpPrinted(theList, printId);
            
		} catch (Exception excp) {
	        return new ServiceResult(ServiceResult.MessageType.ERROR, "Failed to update print activity", null);
		}

        return new ServiceResult(ServiceResult.MessageType.SUCCESS, "Print activity successfully updated", null);
	}
	
    @PreAuthorize("hasRole('ROLE_SUPERADMIN') or hasRole('ROLE_COLLECTION_HEAD') or hasRole('ROLE_COLLECTION_SPV')")
	public Page<PrintActivity> pageReprint(StaffCriteria criteria, Pageable pageable) {
    	if (criteria == null || criteria.getStaffId() == null) {
			return new Page<PrintActivity>();
    	}

    	PrintCriteria printcriteria = new PrintCriteria();
    	printcriteria.setStaffId(criteria.getStaffId());
    	printcriteria.setPrintType("C");
		TypedQuery<PrintActivity> query = queryService.printActivity(printcriteria);
		if (query == null) {
			return new Page<PrintActivity>();
        }
		return (Page<PrintActivity>) new Page<PrintActivity>(query, pageable);
	}

    @PreAuthorize("hasRole('ROLE_SUPERADMIN') or hasRole('ROLE_COLLECTION_HEAD') or hasRole('ROLE_COLLECTION_SPV')")
	public Page<LkpPrintPageDTO> pageReprintDetail(PrintCriteria criteria, Pageable pageable) {
    	if (criteria == null || (criteria.getPrintId() == null && criteria.getPrintNo() == null)) {
    		return new Page<LkpPrintPageDTO>();
    	}

        CustomUserDetails user = (CustomUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

    	LkpCriteria lkpcriteria = new LkpCriteria();
        lkpcriteria.setOfficeCode(user.getOfficeCode());

        if (user.getJobCode().equals("COLLSPV")) {
        	lkpcriteria.setSupervisorId(user.getEmplId());
    	}

        lkpcriteria.setPrintId(criteria.getPrintId());
		lkpcriteria.setPrintNo(criteria.getPrintNo());

        TypedQuery<LkpPrintPageDTO> query = queryService.lkpPrintPage(lkpcriteria);
		if (query == null) {
    		return new Page<LkpPrintPageDTO>();
        }

		return (Page<LkpPrintPageDTO>) new Page<LkpPrintPageDTO>(query, pageable);
	}

    @PreAuthorize("hasRole('ROLE_SUPERADMIN') or hasRole('ROLE_COLLECTION_HEAD') or hasRole('ROLE_COLLECTION_SPV')")
	public List<LkpPrintDTO> listReprint(PrintCriteria criteria) {
    	if (criteria == null || (criteria.getPrintId() == null && criteria.getPrintNo() == null)) {
    		return null;
    	}

        CustomUserDetails user = (CustomUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

    	LkpCriteria lkpcriteria = new LkpCriteria();
        if (user.getJobCode().equals("COLLSPV")) {
        	lkpcriteria.setSupervisorId(user.getEmplId());
    	}
        if (criteria.getPrintId() == null || criteria.getPrintId() == 0) {
        	lkpcriteria.setPrintId(-1); // reprint
        }
        else {
        	lkpcriteria.setPrintId(criteria.getPrintId());
        }
    	lkpcriteria.setPrintNo(criteria.getPrintNo());

    	System.out.println("criteria: " + lkpcriteria);
        TypedQuery<LkpDataDTO> query = queryService.lkpPrint(lkpcriteria);
		if (query == null) {
			return null;
        }

        List<LkpDataDTO> printList = query.getResultList();
		return getPrintList(printList);
	}

    @PreAuthorize("hasRole('ROLE_SUPERADMIN') or hasRole('ROLE_COLLECTION_HEAD') or hasRole('ROLE_COLLECTION_ADMIN')")
	public LkpPageDTO contract(LkpCriteria criteria) {
		LkpPageDTO result = new LkpPageDTO();
		if (criteria == null || criteria.getContractNo() == null) {
			return result;
        }

        CustomUserDetails user = (CustomUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        criteria.setOfficeCode(user.getOfficeCode());
        criteria.setAssignedTo(-1);
        criteria.setPrintId(-1);
		criteria.setCodeEntryStatus("null");
		System.out.println("LKP Criteria: " + criteria);
        TypedQuery<LkpPageDTO> query = queryService.lkpPage(criteria);
		try {
			result = query.getSingleResult();
		} catch (Exception excp) {
		}
		return result;
	}

    @Transactional
	@Modifying(clearAutomatically = true)
    @PreAuthorize("hasRole('ROLE_COLLECTION_ADMIN')")
	public ServiceResult entry(LKPTransaction persisted) {
        CustomUserDetails user = (CustomUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        if (!(user.getJobCode().equals("COLLADM"))) {
			return new ServiceResult(ServiceResult.MessageType.ERROR, "Not authorized user", null);
        }
        
    	if (persisted == null || persisted.getId() == null) {
    		return new ServiceResult(ServiceResult.MessageType.ERROR, "Contract not defined", null);
    	}

    	if (persisted.getLkpEntryStatus() == null) {
    		return new ServiceResult(ServiceResult.MessageType.ERROR, "Entry status not defined", null);
    	}
    	
    	LKPTransaction lkp = repository.findOne(persisted.getId());

    	if (lkp == null) {
    		return new ServiceResult(ServiceResult.MessageType.ERROR, "Contract not found", null);
    	}

    	if (lkp.getAssignedTo() == null || lkp.getTeam() == null || lkp.getTeam().getId() == null) {
    		return new ServiceResult(ServiceResult.MessageType.ERROR, "Contract has not been assigned yet", lkp.getId());
    	}
    	
    	if (lkp.getPrintActivity() == null) {
    		return new ServiceResult(ServiceResult.MessageType.ERROR, "Contract has not been printed yet", lkp.getId());
    	}

    	if (lkp.getLkpEntryStatus() != null) {
    		return new ServiceResult(ServiceResult.MessageType.ERROR, "Contract already done", lkp.getId());
    	}

    	lkp.setLkpEntryStatus(persisted.getLkpEntryStatus());
    	lkp.setDateRequired(persisted.getDateRequired());
    	lkp.setStrComment(persisted.getStrComment());   
    	
    	try {
    		repository.save(lkp);
		} catch (Exception excp) {
    		return new ServiceResult(ServiceResult.MessageType.ERROR, "Contract result entry failed", null);
		}
    	return new ServiceResult(ServiceResult.MessageType.SUCCESS, String.format("Result entry for contract #%s succeed", lkp.getBucket().getContractNo()), lkp.getId());
    }

    @PreAuthorize("hasRole('ROLE_SUPERADMIN') or hasRole('ROLE_COLLECTION_HEAD') or hasRole('ROLE_COLLECTION_SPV')")
	public Page<LkpPrintPageDTO> pageReclustering(StaffCriteria criteria, Pageable pageable) {
    	if (criteria == null || criteria.getStaffId() == null) {
    		return new Page<LkpPrintPageDTO>();
    	}

        CustomUserDetails user = (CustomUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

    	LkpCriteria lkpcriteria = new LkpCriteria();
        lkpcriteria.setOfficeCode(user.getOfficeCode());
    	lkpcriteria.setAssignedTo(criteria.getStaffId());
    	lkpcriteria.setReclustering(true);

        if (user.getJobCode().equals("COLLSPV")) {
        	lkpcriteria.setSupervisorId(user.getEmplId());
    	}

    	TypedQuery<LkpPrintPageDTO> query = queryService.lkpPrintPage(lkpcriteria);
		if (query == null) {
    		return new Page<LkpPrintPageDTO>();
        }
		return (Page<LkpPrintPageDTO>) new Page<LkpPrintPageDTO>(query, pageable);
	}
	
    @Transactional
	@Modifying(clearAutomatically = true)
    @PreAuthorize("hasRole('ROLE_COLLECTION_SPV')")
	public ServiceResult recluster(ReclusteringDTO data) {
        CustomUserDetails user = (CustomUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        if (!(user.getJobCode().equals("COLLSPV"))) {
			return new ServiceResult(ServiceResult.MessageType.ERROR, "Not authorized user", null);
        }

    	if (data == null || data.getLkpId() == null) {
    		return new ServiceResult(ServiceResult.MessageType.ERROR, "Contract not defined", null);
    	}

    	LkpCriteria lkpcriteria = new LkpCriteria();
        lkpcriteria.setOfficeCode(user.getOfficeCode());
    	lkpcriteria.setId(data.getLkpId());
    	lkpcriteria.setReclustering(true);

        if (user.getJobCode().equals("COLLSPV")) {
        	lkpcriteria.setSupervisorId(user.getEmplId());
        }

        TypedQuery<LkpPageDTO> query = queryService.lkpPage(lkpcriteria);
		if (query == null) {
    		return new ServiceResult(ServiceResult.MessageType.ERROR, "Contract not found", null);
        }
		
        LKPTransaction oldLkp = repository.findOne(data.getLkpId());

    	LKPEntryStatus status = new LKPEntryStatus();
    	status.setCode("NEW");
    	oldLkp.setLkpEntryStatus(status);
    	
    	LKPTransaction newLkp = new LKPTransaction();
    	newLkp.setBucket(oldLkp.getBucket());
    	newLkp.setRunDate(new Date());
    	newLkp.setTeam(oldLkp.getTeam());
    	newLkp.setSupervisor(oldLkp.getSupervisor());
    	newLkp.setOwnerId(oldLkp.getOwnerId());
    	
    	try {
    		repository.save(oldLkp);
    		repository.save(newLkp);
		} catch (Exception excp) {
			return new ServiceResult(ServiceResult.MessageType.ERROR, "Create new assignment for the contract failed", null);
		}
    	return new ServiceResult(ServiceResult.MessageType.SUCCESS, String.format("Contract #%s new assignment succeed", newLkp.getBucket().getContractNo()), newLkp.getId());
	}

    @PreAuthorize("hasRole('ROLE_SUPERADMIN') or hasRole('ROLE_COLLECTION_HEAD') or hasRole('ROLE_COLLECTION_SPV')")
	public Page<LkpPageDTO> pageSpecialDelegation(Pageable pageable) {
        CustomUserDetails user = (CustomUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

    	LkpCriteria lkpcriteria = new LkpCriteria();
        lkpcriteria.setOfficeCode(user.getOfficeCode());
		lkpcriteria.setPrintId(0);

        if (user.getJobCode().equals("COLLSPV")) {
        	lkpcriteria.setSupervisorId(user.getEmplId());
        }

        TypedQuery<LkpPageDTO> query = queryService.lkpPage(lkpcriteria);
		if (query == null) {
    		return new Page<LkpPageDTO>();
        }

		return (Page<LkpPageDTO>) new Page<LkpPageDTO>(query, pageable);
	}

    @Transactional
	@Modifying(clearAutomatically = true)
    @PreAuthorize("hasRole('ROLE_COLLECTION_SPV')")
	public ServiceResult delegate(SpecialDelegationDTO data) {
        CustomUserDetails user = (CustomUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        if (!(user.getJobCode().equals("COLLSPV"))) {
			return new ServiceResult(ServiceResult.MessageType.ERROR, "Not authorized user", null);
        }

        if (data == null || data.getContractId() == null) {
    		return new ServiceResult(ServiceResult.MessageType.ERROR, "Contract not defined", null);
    	}

    	LkpCriteria lkpcriteria = new LkpCriteria();
        lkpcriteria.setOfficeCode(user.getOfficeCode());
    	lkpcriteria.setId(data.getContractId());
		lkpcriteria.setPrintId(0);

        if (user.getJobCode().equals("COLLSPV")) {
        	lkpcriteria.setSupervisorId(user.getEmplId());
        }

        TypedQuery<LkpPageDTO> query = queryService.lkpPage(lkpcriteria);
		if (query == null) {
    		return new ServiceResult(ServiceResult.MessageType.ERROR, "Contract not found", null);
        }

		LkpPageDTO lkp = new LkpPageDTO();
		try {
			lkp = query.getSingleResult();
		} catch (Exception excp) {
    		return new ServiceResult(ServiceResult.MessageType.ERROR, "Contract not found", null);
		}
		
        LKPTransaction oldLkp = repository.findOne(data.getContractId());
    	Bucket bucket = oldLkp.getBucket();

    	SKPCTransaction skpc = new SKPCTransaction();
    	skpc.setBucket(bucket);
    	skpc.setRunDate(currentDate);
    	skpc.setStatus(new Status(0));

    	DelegationBucket delbucket = new DelegationBucket();
    	delbucket.setMemoNo(data.getMemoNo());
    	delbucket.setMemoDate(data.getMemoDate());
    	delbucket.setRunDate(currentDate);
    	delbucket.setContractNo(bucket.getContractNo());
    	delbucket.setContractDate(bucket.getContractDate());
    	delbucket.setOfficeCode(bucket.getOfficeCode());
    	delbucket.setTopInstallment(bucket.getTopInstallment());
    	delbucket.setOutstandingPrincipalAmnt(bucket.getOutstandingPrincipalAmnt());
    	delbucket.setOutstandingInterestAmnt(bucket.getOutstandingInterestAmnt());
    	delbucket.setMonthlyInstallment(bucket.getMonthlyInstallment());
    	delbucket.setCustName(bucket.getCustName());
    	delbucket.setCustAddress(bucket.getCustAddress());
    	delbucket.setCustBillingAddress(bucket.getCustBillingAddress());
    	delbucket.setZipcode(bucket.getZipcode());
    	delbucket.setSubZipcode(bucket.getSubZipcode());
    	delbucket.setPhone1(bucket.getPhone1());
    	delbucket.setPhone2(bucket.getPhone2());
    	delbucket.setBusinessUnit(bucket.getBusinessUnit());
    	delbucket.setLeastDueDate(bucket.getLeastDueDate());
    	delbucket.setLeastInstallmentNo(bucket.getLeastInstallmentNo());
    	delbucket.setLastDueDate(bucket.getLastDueDate());
    	delbucket.setLastInstallmentNo(bucket.getLastInstallmentNo());
    	
    	try {
        	bucketrepository.save(delbucket);
        	skpcrepository.save(skpc);
        	queryService.lkpDelete(oldLkp.getBucket().getContractNo());
		} catch (Exception excp) {
    		return new ServiceResult(ServiceResult.MessageType.ERROR, "Contract not found", null);
		}
		
    	return new ServiceResult(ServiceResult.MessageType.SUCCESS, String.format("Contract #%s has successfully delegated", delbucket.getContractNo()), delbucket.getId());
	}

}
