package com.collection.jpa.service;

import java.util.List;

import com.collection.jpa.entity.IdCard;


public interface IdCardService {

	List<IdCard> findAll();

	IdCard insert(IdCard persisted);
	
//	IdCard save(IdCard persisted);

}
