package com.collection.jpa.service;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.collection.jpa.dao.ZoneRepository;
import com.collection.jpa.dto.ZoneCriteria;
import com.collection.jpa.entity.Zone;
import com.collection.jpa.util.Page;
//import com.collection.jpa.util.ServiceResult;


@Repository
@Transactional(readOnly = true)
public class ZoneImpl implements ZoneService {
	
	@PersistenceContext
	private EntityManager entityManager;
	
	@Autowired
	private ZoneRepository repository;
	
	public Page<Zone> findAll(ZoneCriteria criteria, Pageable pageable) {
		final List<Predicate> predicates = new ArrayList<Predicate>();

		CriteriaBuilder cBuilder = entityManager.getCriteriaBuilder();
		CriteriaQuery<Zone> cQuery = cBuilder.createQuery(Zone.class);
		Root<Zone> root = cQuery.from(Zone.class);

		if(criteria.getOfficeId()!=null) {
            predicates.add(cBuilder.equal(root.get("office").get("id"), criteria.getOfficeId()));
        }
        if(criteria.getCode()!=null) {
            predicates.add(cBuilder.equal(root.get("code"), criteria.getCode()));
        }
        if(criteria.getName()!=null) {
            predicates.add(cBuilder.like(cBuilder.lower(root.get("name")), "%"+criteria.getName().toLowerCase()+"%"));
        }

		cQuery.select(root).where(cBuilder.and(predicates.toArray(new Predicate[predicates.size()])));

		TypedQuery<Zone> query = entityManager.createQuery(cQuery);
		
		return (Page<Zone>) new Page<Zone>(query, pageable);
	}
	
	public Zone getOne(Integer zoneId) {
		return repository.getOne(zoneId);
	}
	
/*	@Override
    @Transactional
	public ServiceResult insert(Zone persisted) {
		return repository.save(persisted);
	}
	
    @Transactional
	@Modifying(clearAutomatically = true)
	public ServiceResult update(Zone persisted) {
		return repository.save(persisted);
	}

    @Transactional
	public ServiceResult delete(Integer zoneId) {
		repository.delete(zoneId);
	}
*/	
}
