package com.collection.jpa.service;

import java.util.List;

import org.springframework.data.domain.Pageable;

import com.collection.jpa.dto.ChangeSpvParamDTO;
import com.collection.jpa.dto.TeamCriteria;
import com.collection.jpa.dto.TeamDTO;
import com.collection.jpa.entity.RemedialTeam;
import com.collection.jpa.util.Page;
import com.collection.jpa.util.ServiceResult;


public interface RemedialTeamService {
	
	RemedialTeam findOne(Integer id);

	List<TeamDTO> list();

	Page<TeamDTO> page(TeamCriteria criteria, Pageable pageable);
	
	TeamDTO content(TeamCriteria criteria);
	
	ServiceResult changeSpv(ChangeSpvParamDTO data);
	
	ServiceResult setup(RemedialTeam persisted);
}
