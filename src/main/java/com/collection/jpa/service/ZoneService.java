package com.collection.jpa.service;

import org.springframework.data.domain.Pageable;

import com.collection.jpa.dto.ZoneCriteria;
import com.collection.jpa.entity.Zone;
import com.collection.jpa.util.Page;
//import com.collection.jpa.util.ServiceResult;


public interface ZoneService {

	Page<Zone> findAll(ZoneCriteria criteria, Pageable pageable);

	Zone getOne(Integer id);
	
/*	ServiceResult insert(Zone persisted);
	
	ServiceResult update(Zone persisted);
	
	ServiceResult delete(Integer zoneId);
*/	
}
