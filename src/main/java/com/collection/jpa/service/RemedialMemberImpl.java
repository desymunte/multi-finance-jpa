package com.collection.jpa.service;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.persistence.TypedQuery;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.collection.jpa.CustomUserDetails;
import com.collection.jpa.dao.RemedialMemberRepository;
import com.collection.jpa.dao.RemedialTeamRepository;
import com.collection.jpa.dto.RemedialMemberDTO;
import com.collection.jpa.dto.TeamCriteria;
import com.collection.jpa.dto.TeamDTO;
import com.collection.jpa.dto.MemberCriteria;
import com.collection.jpa.entity.RemedialMember;
import com.collection.jpa.entity.RemedialTeam;
import com.collection.jpa.entity.RemedialWorkingArea;
import com.collection.jpa.util.Page;
import com.collection.jpa.util.ServiceResult;


@Repository
@Transactional(readOnly = true)
public class RemedialMemberImpl implements RemedialMemberService {
	
	@Autowired
	private RemedialMemberRepository repository;
	
	@Autowired
	private RemedialTeamRepository teamrepository;
	
	@Autowired
	private QueryImpl queryService;
	
	@Autowired
	private RemedialWorkingAreaImpl implWorkingArea;
	

    @PreAuthorize("hasRole('ROLE_SUPERADMIN') or hasRole('ROLE_REMEDIAL_HEAD') or hasRole('ROLE_REMEDIAL_SPV')")
	public Page<RemedialMemberDTO> page(MemberCriteria criteria, Pageable pageable) {
        CustomUserDetails user = (CustomUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        if (criteria == null) {
        	return new Page<RemedialMemberDTO>();
        }
        criteria.setOfficeId(user.getOfficeId());

        if (user.getJobCode().equals("REMSPV")) {
            TeamCriteria teamcriteria = new TeamCriteria();
            teamcriteria.setSpvId(user.getEmplId());
            List<TeamDTO> teams = queryService.remedialTeam(teamcriteria).getResultList();
            List<Integer> teamIdList = new ArrayList<Integer>();
            for (TeamDTO team : teams) {
                teamIdList.add(team.getId());
            }
        	criteria.setTeamIdList(teamIdList);
    	}

        TypedQuery<RemedialMemberDTO> query = queryService.remedialMember(criteria);
        
		return (Page<RemedialMemberDTO>) new Page<RemedialMemberDTO>(query, pageable);
	}

	@Transactional
    @PreAuthorize("hasRole('ROLE_REMEDIAL_HEAD') or hasRole('ROLE_REMEDIAL_SPV')")
	public ServiceResult insert(RemedialMember persisted) {
        CustomUserDetails user = (CustomUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

    	if (persisted == null ||
    		persisted.getId() != null ||
    		persisted.getRemedialTeam().getId() == null ||
    		persisted.getEmployee().getId() == null) {
    		return new ServiceResult(ServiceResult.MessageType.ERROR, "Invalid Remedial Member data", null);
    	}
    	
    	RemedialTeam team = teamrepository.findOne(persisted.getRemedialTeam().getId());
    	System.out.println("team" + persisted.getRemedialTeam().getId());
    	System.out.println("supervisor" + team.getSupervisor().getId());
    	System.out.println("user" + user.getEmplId());
    	if (team == null ||
    		team.getSupervisor().getId() != user.getEmplId()) {
    		return new ServiceResult(ServiceResult.MessageType.ERROR, "Invalid Remedial Team data", null);
    	}

    	MemberCriteria criteria = new MemberCriteria(); 
    	criteria.setEmplId(persisted.getEmployee().getId());
		TypedQuery<RemedialMemberDTO> query = queryService.remedialMember(criteria);
		List<RemedialMemberDTO> results = query.getResultList();
		boolean exist = !results.isEmpty();
		if (exist) {
			return new ServiceResult(ServiceResult.MessageType.ERROR, "Staff already assigned as a Collector", null);
		}
		
		RemedialMember member = new RemedialMember();
		member.setRemedialTeam(team);
		member.setEmployee(persisted.getEmployee());
		member.setDailyLoad(persisted.getDailyLoad());
		member.setMonthlyLoad(persisted.getMonthlyLoad());

		for (Iterator<RemedialWorkingArea> itr = persisted.getRemedialWorkingAreas().iterator(); itr.hasNext();) { 
			RemedialWorkingArea area = itr.next(); 
			member.addRemedialWorkingArea(area);
    	}

		try {
			repository.save(member);
		} catch (Exception excp) {
    		return new ServiceResult(ServiceResult.MessageType.ERROR, "Fail to add Remedial Member", null);
		}

		return new ServiceResult(ServiceResult.MessageType.SUCCESS, "New Remedial Member successfully added", member.getId());
	}
	
	@Transactional
	@PreAuthorize("hasRole('ROLE_REMEDIAL_HEAD') or hasRole('ROLE_REMEDIAL_SPV')")
	public ServiceResult delete(RemedialMember persisted) {
		implWorkingArea.deleteList(persisted.getId());
		repository.delete(persisted);
		System.out.println("REMOVE: " + persisted);
		return new ServiceResult(ServiceResult.MessageType.SUCCESS, "Member Remedial has been deleted", persisted.getId());
		
	}
	
//    @PreAuthorize("hasRole('ROLE_REMEDIAL_SPV')")
//	public MemberDTO member(StaffCriteria criteria) {
//        CustomUserDetails user = (CustomUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
//
//    	MemberDTO result = new MemberDTO();
//
//    	if (criteria.getStaffId() != null) {
//            MemberCriteria newcriteria = new MemberCriteria(); 
//            newcriteria.setEmplId(criteria.getStaffId());
//            newcriteria.setSupervisorId(user.getEmplId());
//
//        	TypedQuery<MemberDTO> query = queryService.remedialMember(newcriteria);
//
//        	try {
//    			result = query.getSingleResult();
//    		} catch (Exception excp) {
//    		}
//        }
//
//    	return result;
//	}

}
