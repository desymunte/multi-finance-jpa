package com.collection.jpa.service;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.collection.jpa.dao.IdCardRepository;
import com.collection.jpa.entity.IdCard;


@Repository
@Transactional(readOnly = true)
public class IdCardImpl implements IdCardService {
	
	@PersistenceContext
	private EntityManager entityManager;
	
	@Autowired
	private IdCardRepository repository;
	
	@Override
	public List<IdCard> findAll() {
		return repository.findAll();
	}
	
	@Override
    @Transactional
	public IdCard insert(IdCard persisted) {
		return repository.save(persisted);
	}
	
    @Transactional
	@Modifying(clearAutomatically = true)
	public IdCard update(IdCard persisted) {
		return repository.save(persisted);
	}

    @Transactional
	public void delete(Integer id) {
		// TODO Auto-generated method stub
//		Long delId = Long.parseLong(id.toString());
		repository.delete(id);
	}
	
//    @Transactional
//	@Modifying
//	public void delete(IdCard persisted) {
//		// TODO Auto-generated method stub
//		repository.delete(persisted);
//	}
}
