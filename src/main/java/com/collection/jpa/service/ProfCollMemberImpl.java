package com.collection.jpa.service;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.collection.jpa.dao.ProfCollectionMemberRepository;
import com.collection.jpa.entity.ProfCollMember;


@Repository
@Transactional(readOnly = true)
public class ProfCollMemberImpl implements ProfCollMemberService {
	
	@PersistenceContext
	private EntityManager entityManager;
	
	@Autowired
	private ProfCollectionMemberRepository repository;
	
	@Override
	public List<ProfCollMember> findAll() {
		return repository.findAll();
	}
	
	@Override
    @Transactional
	public ProfCollMember insert(ProfCollMember persisted) {
		return repository.save(persisted);
	}
	
    @Transactional
	@Modifying(clearAutomatically = true)
	public ProfCollMember update(ProfCollMember persisted) {
		return repository.save(persisted);
	}

    @Transactional
	public void delete(Integer id) {
		// TODO Auto-generated method stub
//		Long delId = Long.parseLong(id.toString());
		repository.delete(id);
	}
	
//    @Transactional
//	@Modifying
//	public void delete(ProfCollMember persisted) {
//		// TODO Auto-generated method stub
//		repository.delete(persisted);
//	}
}
