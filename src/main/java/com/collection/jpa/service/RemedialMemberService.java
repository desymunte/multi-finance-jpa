package com.collection.jpa.service;

import org.springframework.data.domain.Pageable;

import com.collection.jpa.dto.RemedialMemberDTO;
import com.collection.jpa.dto.MemberCriteria;
import com.collection.jpa.entity.RemedialMember;
import com.collection.jpa.util.Page;
import com.collection.jpa.util.ServiceResult;


public interface RemedialMemberService {

	Page<RemedialMemberDTO> page(MemberCriteria criteria, Pageable pageable);

	ServiceResult insert(RemedialMember persisted);
	
	//MemberDTO member(StaffCriteria criteria);
	
}
