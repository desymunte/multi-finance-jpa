package com.collection.jpa.service;

import java.util.List;

import javax.persistence.TypedQuery;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.collection.jpa.CustomUserDetails;
import com.collection.jpa.dao.DeskcallMemberRepository;
import com.collection.jpa.dao.DeskcallTeamRepository;
import com.collection.jpa.dto.DeskCallTeamMemberDTO;
import com.collection.jpa.entity.DeskcallMember;
import com.collection.jpa.entity.DeskcallTeam;
import com.collection.jpa.dto.MemberCriteria;
import com.collection.jpa.util.Page;
import com.collection.jpa.util.ServiceResult;


@Repository
@Transactional(readOnly = true)
public class DeskcallMemberImpl implements DeskcallMemberService {
	
	@Autowired
	private DeskcallMemberRepository repository;
	
	@Autowired
	private DeskcallTeamRepository teamrepository;
	
	@Autowired
	private QueryImpl queryService;
	

    @PreAuthorize("hasRole('ROLE_SUPERADMIN') or hasRole('ROLE_DESKCALL_HEAD') or hasRole('ROLE_DESKCALL_SPV')")
	public Page<DeskCallTeamMemberDTO> page(MemberCriteria criteria, Pageable pageable) {
        CustomUserDetails user = (CustomUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        if (criteria == null) {
        	return new Page<DeskCallTeamMemberDTO>();
        }
        criteria.setOfficeId(user.getOfficeId());

        TypedQuery<DeskCallTeamMemberDTO> query = queryService.deskcallMember(criteria);
		return (Page<DeskCallTeamMemberDTO>) new Page<DeskCallTeamMemberDTO>(query, pageable);
	}

	@Transactional
    @PreAuthorize("hasRole('ROLE_DESKCALL_SPV') or hasRole('ROLE_DESKCALL_HEAD')")
	public ServiceResult insert(DeskcallMember persisted) {
        CustomUserDetails user = (CustomUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

    	if (persisted == null ||
    		persisted.getId() != null ||
    		persisted.getDeskcallTeam().getId() == null ||
    		persisted.getEmployee().getId() == null) {
    		return new ServiceResult(ServiceResult.MessageType.ERROR, "Invalid Deskcall Member data", null);
    	}
    	
    	DeskcallTeam team = teamrepository.findOne(persisted.getDeskcallTeam().getId());
    	if (team == null ||
    		team.getSupervisor().getId() != user.getEmplId()) {
    		return new ServiceResult(ServiceResult.MessageType.ERROR, "Not authorized user", null);
    	}

    	MemberCriteria criteria = new MemberCriteria(); 
    	criteria.setEmplId(persisted.getEmployee().getId());
		TypedQuery<DeskCallTeamMemberDTO> query = queryService.deskcallMember(criteria);
		List<DeskCallTeamMemberDTO> results = query.getResultList();
		boolean exist = !results.isEmpty();
		if (exist) {
			return new ServiceResult(ServiceResult.MessageType.ERROR, "Staff already assigned as a Deskcall Member", null);
		}
		
		DeskcallMember member = new DeskcallMember();
		member.setDeskcallTeam(team);
		member.setEmployee(persisted.getEmployee());
		member.setDailyLoad(persisted.getDailyLoad());
		member.setMonthlyLoad(persisted.getMonthlyLoad());

		try {
			repository.save(member);
		} catch (Exception excp) {
    		return new ServiceResult(ServiceResult.MessageType.ERROR, "Fail to add new Deskcall Member", null);
		}

		return new ServiceResult(ServiceResult.MessageType.SUCCESS, "New Deskcall Member successfully added", member.getId());
	}
	
	@Transactional
	@PreAuthorize("hasRole('ROLE_DESKCALL_HEAD') or hasRole('ROLE_DESKCALL_SPV')")
	public ServiceResult delete(DeskcallMember persisted) {
		repository.delete(persisted);
		return new ServiceResult(ServiceResult.MessageType.SUCCESS, "Member Deskcall has been deleted", persisted.getId());
	}
	
}
