package com.collection.jpa.service;

import java.util.List;

import org.springframework.data.domain.Pageable;

import com.collection.jpa.dto.LkrTeamPageDTO;
import com.collection.jpa.dto.SpecialDelegationDTO;
import com.collection.jpa.dto.LkrCriteria;
import com.collection.jpa.dto.LkrHeaderDTO;
import com.collection.jpa.dto.LkrPageDTO;
import com.collection.jpa.dto.LkrPrintDTO;
import com.collection.jpa.dto.LkrPrintPageDTO;
import com.collection.jpa.dto.LkrCyclePageDTO;
import com.collection.jpa.dto.PrintCriteria;
import com.collection.jpa.dto.ReclusteringDTO;
import com.collection.jpa.dto.StaffCriteria;
import com.collection.jpa.entity.LKRTransaction;
import com.collection.jpa.entity.PrintActivity;
import com.collection.jpa.util.Page;
import com.collection.jpa.util.ServiceResult;


public interface LKRTransactionService {

	Page<LkrCyclePageDTO> pageManualAssignment(Pageable pageable);

	Page<LkrTeamPageDTO> pageClustering(Pageable pageable);

	ServiceResult assign(LKRTransaction persisted);
	
	List<LkrPrintDTO> listPrint(StaffCriteria criteria);

	LkrHeaderDTO headerPrint(StaffCriteria criteria);
	
	ServiceResult setPrinted(LkrHeaderDTO header, List<LkrPrintDTO> listData);

	Page<PrintActivity> pageReprint(StaffCriteria criteria, Pageable pageable);
	
	Page<LkrPrintPageDTO> pageReprintDetail(PrintCriteria criteria, Pageable pageable);
	
	List<LkrPrintDTO> listReprint(PrintCriteria criteria);
	
	LkrPageDTO contract(LkrCriteria criteria);
	
	ServiceResult entry(LKRTransaction persisted);

	Page<LkrPrintPageDTO> pageReclustering(StaffCriteria criteria, Pageable pageable);
	
	ServiceResult recluster(ReclusteringDTO data);
	
	Page<LkrPageDTO> pageSpecialDelegation(Pageable pageable);

	ServiceResult delegate(SpecialDelegationDTO data);
}
