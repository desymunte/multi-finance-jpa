package com.collection.jpa.service;

import java.util.Iterator;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.collection.jpa.CustomUserDetails;
import com.collection.jpa.dao.CycleRepository;
import com.collection.jpa.dao.DeskcallTeamRepository;
import com.collection.jpa.dao.EmployeeRepository;
import com.collection.jpa.dao.OfficeRepository;
import com.collection.jpa.dto.TeamDTO;
import com.collection.jpa.entity.Cycle;
import com.collection.jpa.entity.DeskcallMember;
import com.collection.jpa.entity.DeskcallTeam;
import com.collection.jpa.entity.Employee;
import com.collection.jpa.dto.ChangeSpvParamDTO;
import com.collection.jpa.dto.MemberCriteria;
import com.collection.jpa.dto.DeskCallTeamMemberDTO;
import com.collection.jpa.dto.TeamCriteria;
import com.collection.jpa.util.InList;
import com.collection.jpa.util.Page;
import com.collection.jpa.util.ServiceResult;


@Repository
@Transactional(readOnly = true)
public class DeskcallTeamImpl implements DeskcallTeamService {
	
	@PersistenceContext
	private EntityManager entityManager;
	
	@Autowired
	private DeskcallTeamRepository repository; 
	
	@Autowired
	private EmployeeRepository emplrepository; 
	
	@Autowired
	private OfficeRepository offrepository; 
	
	@Autowired
	private CycleRepository cyclerepository; 
	
	@Autowired
	private QueryImpl queryService;

	public DeskcallTeam findOne(Integer id) {
		return repository.findOne(id);
	}
	
	public InList<TeamDTO> asSupervisor() {
        CustomUserDetails user = (CustomUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        TeamCriteria newcriteria = new TeamCriteria();
        newcriteria.setSpvId(user.getEmplId());

    	TypedQuery<TeamDTO> query = queryService.collectionTeam(newcriteria);
		List<TeamDTO> list = query.getResultList();
		boolean exist = !list.isEmpty();

		return new InList<TeamDTO>(list, exist);
	}

	private InList<TeamDTO> asSupervisor(Employee criteria) {
        TeamCriteria newcriteria = new TeamCriteria();
    	newcriteria.setSpvId(criteria.getId());

    	TypedQuery<TeamDTO> query = queryService.deskcallTeam(newcriteria);
		List<TeamDTO> list = query.getResultList();
		boolean exist = !list.isEmpty();

		return new InList<TeamDTO>(list, exist);
	}
	
	private InList<DeskCallTeamMemberDTO> asMember(Employee criteria) {
        MemberCriteria newcriteria = new MemberCriteria();
        newcriteria.setEmplId(criteria.getId());

    	TypedQuery<DeskCallTeamMemberDTO> query = queryService.deskcallMember(newcriteria);
		List<DeskCallTeamMemberDTO> list = query.getResultList();
		boolean exist = !list.isEmpty();

		return new InList<DeskCallTeamMemberDTO>(list, exist);
	}

    @PreAuthorize("hasRole('ROLE_SUPERADMIN') or hasRole('ROLE_DESKCALL_HEAD') or hasRole('ROLE_DESKCALL_SPV')")
	public Page<TeamDTO> page(TeamCriteria criteria, Pageable pageable) {
        CustomUserDetails user = (CustomUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

    	criteria.setOfficeId(user.getOfficeId());
    	if (user.getJobCode() == "DESKSPV") {
    		criteria.setSpvId(user.getEmplId());
    	}

    	TypedQuery<TeamDTO> query = queryService.deskcallTeam(criteria);
		return (Page<TeamDTO>) new Page<TeamDTO>(query, pageable);
	}
	
    @PreAuthorize("hasRole('ROLE_SUPERADMIN') or hasRole('ROLE_DESKCALL_HEAD') or hasRole('ROLE_DESKCALL_SPV')")
	public TeamDTO content(TeamCriteria criteria) {
        CustomUserDetails user = (CustomUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

		TeamDTO result = new TeamDTO();
    	if (criteria == null || criteria.getId() == null) {
    		return result;
    	}
    	criteria.setOfficeId(user.getOfficeId());

    	if (user.getJobCode() == "DESKSPV") {
    		criteria.setSpvId(user.getEmplId());
    	}

    	TypedQuery<TeamDTO> query = queryService.deskcallTeam(criteria);

		try {
			result = query.getSingleResult();
		} catch (Exception excp) {
		}
		return result;
	}
		
    @Transactional
	@PreAuthorize("hasRole('ROLE_DESKCALL_HEAD')")
    public ServiceResult changeSpv(ChangeSpvParamDTO data) {
        CustomUserDetails user = (CustomUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        if (data == null || data.getTeamId() == null || data.getSpvId() == null) {
    		return new ServiceResult(ServiceResult.MessageType.ERROR, "Data for Supervisor changing not complete", null);
        }
        TeamCriteria newcriteria = new TeamCriteria();
    	newcriteria.setOfficeId(user.getOfficeId());
       	newcriteria.setId(data.getTeamId());
    	
       	DeskcallTeam team = repository.findOne(data.getTeamId());
    	if (team == null) {
    		return new ServiceResult(ServiceResult.MessageType.ERROR, "Deskcall Team not found", null);
    	}
    	
    	TypedQuery<TeamDTO> query = queryService.deskcallTeam(newcriteria);
    	if (query == null) {
			return new ServiceResult(ServiceResult.MessageType.ERROR, "Not authorized user", null);
    	}

    	Employee empl =  emplrepository.findOne(data.getSpvId());
    	if (empl == null) {
    		return new ServiceResult(ServiceResult.MessageType.ERROR, "Employee not found", null);
    	}
    	
		InList<TeamDTO> spv = this.asSupervisor(empl);
		if (spv.exists()) {
    		return new ServiceResult(ServiceResult.MessageType.ERROR, "Supervisor already assigned", spv);
		}
    	
		team.setSupervisor(empl);
		try {
			repository.save(team);
		} catch (Exception excp) {
    		return new ServiceResult(ServiceResult.MessageType.ERROR, "Failed to change supervisor", null);
		}
		return new ServiceResult(ServiceResult.MessageType.SUCCESS, "Supervisor changes successfully", team.getId());
    }

	@Transactional
	@PreAuthorize("hasRole('ROLE_DESKCALL_HEAD')")
    public ServiceResult setup(DeskcallTeam persisted) {
        CustomUserDetails user = (CustomUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        if (!(user.getJobCode().equals("DESKHD"))) {
			return new ServiceResult(ServiceResult.MessageType.ERROR, "Not authorized user", null);
        }
        
        if (persisted == null || 
        	persisted.getName() == null || 
        	persisted.getCycle() == null || persisted.getCycle().getId() == null || 
        	persisted.getSupervisor() == null | persisted.getSupervisor().getId() == null) {
    		return new ServiceResult(ServiceResult.MessageType.ERROR, "Deskcall Team data not completely supplied", null);
        }

        persisted.setId(null);
        persisted.setOffice(offrepository.findOne(user.getOfficeId()));
        
		Cycle cycle = cyclerepository.findOne(persisted.getCycle().getId());
		if (cycle == null) {
			return new ServiceResult(ServiceResult.MessageType.ERROR, "Cycle is not recognized", persisted.getCycle().getId());
		}
        if (!cycle.getCode().equals("DSC")) {
			return new ServiceResult(ServiceResult.MessageType.ERROR, "Not a proper cycle type for Deskcall Team", cycle.getCode());
		}

        Employee supervisor = emplrepository.findOne(persisted.getSupervisor().getId());
		if (supervisor == null) {
			return new ServiceResult(ServiceResult.MessageType.ERROR, "Supervisor is not recognized", persisted.getSupervisor().getId());
		}
		if (!supervisor.getJobTitle().getCode().equals("DESKSPV")) {
			return new ServiceResult(ServiceResult.MessageType.ERROR, "Assigned Staff as Supervisor is not valid, the job title is not a Deskcall Supervisor", supervisor.getJobTitle().getCode());
		}
		if (supervisor.getOffice().getId() != user.getOfficeId()) {
			return new ServiceResult(ServiceResult.MessageType.ERROR, "Supervisor's office is not valid", supervisor.getOffice().getId());
		}
		
		InList<TeamDTO> spv = this.asSupervisor(persisted.getSupervisor());
		if (spv.exists()) {
    		return new ServiceResult(ServiceResult.MessageType.ERROR, "Supervisor already assigned", null);
		}
    	
		DeskcallTeam team = new DeskcallTeam();
    	team.setCycle(persisted.getCycle());
    	team.setName(persisted.getName()); 
    	team.setOffice(persisted.getOffice());
    	team.setSupervisor(persisted.getSupervisor());

    	for (Iterator<DeskcallMember> itr = persisted.getDeskcallMembers().iterator(); itr.hasNext();) { 

    		DeskcallMember persistedmember = itr.next();

            if (persistedmember.getEmployee() == null || persistedmember.getEmployee().getId() == null) {
        		return new ServiceResult(ServiceResult.MessageType.ERROR, "Deskcall Member data not supplied", null);
            }

            Employee empl = emplrepository.findOne(persistedmember.getEmployee().getId());
    		if (empl == null) {
    			return new ServiceResult(ServiceResult.MessageType.ERROR, "The employee is not recognized", persistedmember.getEmployee().getId());
    		}
    		if (!empl.getJobTitle().getCode().equals("DESK")) {
    			return new ServiceResult(ServiceResult.MessageType.ERROR, "Assigned employee is not valid, the job title is not a Collector", empl.getJobTitle().getCode());
    		}
    		if (empl.getOffice().getId() != user.getOfficeId()) {
    			return new ServiceResult(ServiceResult.MessageType.ERROR, "Employee's office is not valid", empl.getOffice().getId());
    		}

    		InList<DeskCallTeamMemberDTO> memberdto = this.asMember(empl);
    		if (memberdto.exists()) {
        		return new ServiceResult(ServiceResult.MessageType.ERROR, "The employee already assigned as a Deskcall", null);
    		}
        	
    		DeskcallMember member = new DeskcallMember();
    		if (persistedmember.getDailyLoad() == null) {
    			persistedmember.setDailyLoad(0);
    		}
    		if (persistedmember.getMonthlyLoad() == null) {
    			persistedmember.setMonthlyLoad(0);
    		}
    		member.setEmployee(empl);
    		member.setDailyLoad(persistedmember.getDailyLoad());
    		member.setMonthlyLoad(persistedmember.getMonthlyLoad());
    		team.addDeskcallMember(member);
    	}

    	try {
    		repository.save(team);
    		
		} catch (Exception excp) {
			return new ServiceResult(ServiceResult.MessageType.ERROR, "Deskcall Team setup failed", team.getId());
		}
    	
		return new ServiceResult(ServiceResult.MessageType.SUCCESS, "Deskcall Team successfully set up", team.getId());
	}

}
