package com.collection.jpa.service;

import org.springframework.data.domain.Pageable;

import com.collection.jpa.dto.CollectionMemberDTO;
import com.collection.jpa.dto.MemberCriteria;
import com.collection.jpa.entity.CollectionMember;
import com.collection.jpa.util.Page;
import com.collection.jpa.util.ServiceResult;


public interface CollectionMemberService {

	Page<CollectionMemberDTO> page(MemberCriteria criteria, Pageable pageable);

	ServiceResult insert(CollectionMember persisted);
	
	//MemberDTO member(StaffCriteria criteria);
	
}
