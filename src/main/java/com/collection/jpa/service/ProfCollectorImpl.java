package com.collection.jpa.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.collection.jpa.CustomUserDetails;
import com.collection.jpa.dao.GeneralParameterRepository;
import com.collection.jpa.dao.OfficeRepository;
import com.collection.jpa.dao.ProfCollectionMOURepository;
import com.collection.jpa.dao.ProfCollectionRepository;
import com.collection.jpa.dao.StatusRepository;
import com.collection.jpa.dto.ProfCollMemberDTO;
import com.collection.jpa.dto.ProfCollMouPrintDTO;
import com.collection.jpa.dto.ProfCollectorCriteria;
import com.collection.jpa.dto.ProfCollectorDTO;
import com.collection.jpa.entity.GeneralParameter;
import com.collection.jpa.entity.Office;
import com.collection.jpa.entity.ProfCollector;
import com.collection.jpa.entity.ProfCollMOUHistory;
import com.collection.jpa.entity.ProfCollMember;
import com.collection.jpa.entity.Status;
import com.collection.jpa.util.FPrint;
import com.collection.jpa.util.Page;
import com.collection.jpa.util.ServiceResult;


@Repository
@Transactional(readOnly = true)
public class ProfCollectorImpl implements ProfCollectorService {
	
	@PersistenceContext
	private EntityManager entityManager;
	
	@Autowired
	private ProfCollectionRepository repository;
	
	@Autowired
	private ProfCollectionMOURepository mourepository;
	
	@Autowired
	private OfficeRepository offrepository;
	
	@Autowired
	private GeneralParameterRepository paramrepository;
	
	@Autowired
	private StatusRepository statusrepository;
	
	@Autowired
	private QueryImpl queryService;

	@Autowired
	private FPrint fprint;

    @PreAuthorize("hasRole('ROLE_REMEDIAL_HEAD')")
	public Page<ProfCollectorDTO> page(ProfCollectorCriteria criteria, Pageable pageable) {
		if (criteria.getOfficeId() == null) {
			return new Page<ProfCollectorDTO>();
	    }
		TypedQuery<ProfCollectorDTO> query = queryService.profCollector(criteria);
		return (Page<ProfCollectorDTO>) new Page<ProfCollectorDTO>(query, pageable);
	}

	@Transactional
	@Modifying(clearAutomatically = true)
    @PreAuthorize("hasRole('ROLE_REMEDIAL_HEAD')")
    public ServiceResult setup(ProfCollector persisted) {
        CustomUserDetails user = (CustomUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

		if (persisted.getOffice() == null || persisted.getOffice().getId() == null) {
			return new ServiceResult(ServiceResult.MessageType.ERROR, "Office is required", null);
		}
        Office office = offrepository.findOne(persisted.getOffice().getId());
		if (office == null) {
			return new ServiceResult(ServiceResult.MessageType.ERROR, "Office is required", null);
		}
		if (office.getId() != user.getOfficeId()) {
			return new ServiceResult(ServiceResult.MessageType.ERROR, "Not authorized user", null);
		}

		ProfCollector profcoll = new ProfCollector(); 
		profcoll.setOffice(office);
		profcoll.setName(persisted.getName()); 
		profcoll.setCode(persisted.getCode());
		profcoll.setIdNo(persisted.getIdNo());
		profcoll.setAddress(persisted.getAddress());
		profcoll.setPhone(persisted.getPhone());
		profcoll.setEmail(persisted.getEmail());
		profcoll.setType(persisted.getType());

        String printNo = fprint.getNextPrintNo("MOU", String.format("%-5s", profcoll.getCode()));
		profcoll.setMouNo(printNo);
		Date today = new Date();
		profcoll.setMouDate(today);
		Date start = persisted.getMouStartDate();
		Date end = persisted.getMouEndDate();
		profcoll.setMouStartDate(start);
		profcoll.setMouEndDate(end);
		
		if (end.compareTo(today) < 0) {
			profcoll.setMouStatus(statusrepository.findOne(2));
		}
		else if (start.compareTo(today) < 0) {
			profcoll.setMouStatus(statusrepository.findOne(1));
		}
		else {
			profcoll.setMouStatus(statusrepository.findOne(0));
		}
		profcoll.setPerformance(0.0);

		if (persisted.getType() == 1) { // Corporate
	    	for(Iterator<ProfCollMember> itr = persisted.getProfcollMembers().iterator(); itr.hasNext();) { 
	    		ProfCollMember each = itr.next(); 
	    		ProfCollMember staff = new ProfCollMember();
	    		staff.setName(each.getName());
	    		staff.setIdCardNo(each.getIdCardNo());
	    		staff.setAddress(each.getAddress());
	    		staff.setPhone(each.getPhone());
	    		staff.setEmail(each.getEmail());
	    		
	    		profcoll.addProfCollMember(staff);
	    	}
    	}
    	else { // persisted.getType() == 0 Individual
    		ProfCollMember staff = new ProfCollMember();
    		staff.setName(persisted.getName());
    		staff.setIdCardNo(persisted.getIdNo());
    		staff.setAddress(persisted.getAddress());
    		staff.setPhone(persisted.getPhone());
    		staff.setEmail(persisted.getEmail());
    		
    		profcoll.addProfCollMember(staff);
    	}

		repository.save(profcoll);
		return new ServiceResult(ServiceResult.MessageType.SUCCESS, "Professional Collector has been setup", profcoll.getId());
	}

	public List<ProfCollMemberDTO> listActiveProfColl(ProfCollectorCriteria criteria) {
        CustomUserDetails user = (CustomUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        final List<Predicate> predicates = new ArrayList<Predicate>();
		CriteriaBuilder cBuilder = entityManager.getCriteriaBuilder();
		CriteriaQuery<ProfCollMemberDTO> cQuery = cBuilder.createQuery(ProfCollMemberDTO.class);
		Root<ProfCollector> root = cQuery.from(ProfCollector.class);
		Join<ProfCollector, Office> offi = root.join("office", JoinType.LEFT); //left outer join
		Join<ProfCollector, ProfCollMember> staf = root.join("profcollMembers", JoinType.LEFT); //left outer join
		Join<ProfCollector, Status> stat = root.join("mouStatus", JoinType.LEFT); //left outer join

		if (criteria.getOfficeId() == null || criteria.getOfficeId() != user.getOfficeId()) {
			return null;
		}
        predicates.add(cBuilder.equal(offi.get("id"), criteria.getOfficeId()));
        predicates.add(cBuilder.equal(stat.get("id"), 1));

        cQuery.select(cBuilder.construct(ProfCollMemberDTO.class, 
        		root.get("id"),
				root.get("type"),
        		root.get("name"), 
        		staf.get("id"), 
        		staf.get("name")));

        cQuery.where(cBuilder.and(predicates.toArray(new Predicate[predicates.size()])));

		TypedQuery<ProfCollMemberDTO> query = entityManager.createQuery(cQuery);
		List<ProfCollMemberDTO> data = query.getResultList();

		return data;
	}

	@Override
    @Transactional
    @PreAuthorize("hasRole('ROLE_REMEDIAL_HEAD')")
	public ServiceResult update(ProfCollector persisted) {
		ProfCollector profcoll = repository.findOne(persisted.getId());
    	if (profcoll == null || profcoll.getId() == null) {
			return new ServiceResult(ServiceResult.MessageType.ERROR, "Failed to create MOU, Professional Collector not found", null);
		}
		
    	if (profcoll.getMouNo() != null) {
    		if (profcoll.getMouStatus().getId() != 2) {
    			return new ServiceResult(ServiceResult.MessageType.ERROR, "Failed to create MOU, last MOU exists", null);
    		}

    		GeneralParameter params = paramrepository.findOne(1);
    		if (profcoll.getPerformance() < params.getMou_min_performance()) {
    			return new ServiceResult(ServiceResult.MessageType.ERROR, "Failed to create MOU, last performance is below limit", null);
    		}
    	}

    	ProfCollMOUHistory mouHist = new ProfCollMOUHistory();
    	mouHist.setMouNo(profcoll.getMouNo());
    	mouHist.setMouDate(profcoll.getMouDate());
    	mouHist.setStartDate(profcoll.getMouStartDate());
    	mouHist.setEndDate(profcoll.getMouEndDate());
    	mouHist.setLastStatus(profcoll.getMouStatus().getId());
    	mouHist.setPerformance(profcoll.getPerformance());
    	mouHist.setProfCollection(profcoll);
    	mourepository.save(mouHist);
    	
        String printNo = fprint.getNextPrintNo("MOU", String.format("%-5s", profcoll.getName()));
		profcoll.setMouNo(printNo);
		Date today = new Date();
		profcoll.setMouDate(today);
		Date start = persisted.getMouStartDate();
		Date end = persisted.getMouEndDate();
		profcoll.setMouStartDate(start);
		profcoll.setMouEndDate(end);
		
		if (end.compareTo(today) < 0) {
			profcoll.setMouStatus(statusrepository.findOne(2));
		}
		else if (start.compareTo(today) < 0) {
			profcoll.setMouStatus(statusrepository.findOne(1));
		}
		else {
			profcoll.setMouStatus(statusrepository.findOne(0));
		}
		profcoll.setPerformance(0.0);

		repository.save(profcoll);
		return new ServiceResult(ServiceResult.MessageType.SUCCESS, "MOU created successfully", profcoll.getId());
	}
	
    @PreAuthorize("hasRole('ROLE_REMEDIAL_HEAD')")
	public ProfCollMouPrintDTO print(ProfCollector criteria) {
		if (criteria.getId() == null) {
			return null;
        }
		final List<Predicate> predicates = new ArrayList<Predicate>();

		CriteriaBuilder cBuilder = entityManager.getCriteriaBuilder();
		CriteriaQuery<ProfCollMouPrintDTO> cQuery = cBuilder.createQuery(ProfCollMouPrintDTO.class);
		Root<ProfCollector> root = cQuery.from(ProfCollector.class);
		Join<ProfCollector, Office> o = root.join("office", JoinType.LEFT); //left outer join

		if (criteria.getId() == null) {
			return null;
        }
		
		predicates.add(cBuilder.equal(root.get("id"), criteria.getId()));
		cQuery.select(cBuilder.construct(ProfCollMouPrintDTO.class, 
        		root.get("id"),
        		root.get("mouNo"),
        		root.get("mouDate"),
        		root.get("mouStartDate"),
        		root.get("mouEndDate"),
        		root.get("name"),
        		root.get("code"),
        		root.get("idNo"),
        		root.get("address"),
        		root.get("phone"),
        		root.get("email"),
        		o.get("name"),
        		o.get("code"),
        		o.get("address"),
        		o.get("skpcFee")
				));
		cQuery.where(cBuilder.and(predicates.toArray(new Predicate[predicates.size()])));
		
		TypedQuery<ProfCollMouPrintDTO> query = entityManager.createQuery(cQuery);
		
		ProfCollMouPrintDTO print = query.getSingleResult();
		
		return print;
	}	

}
