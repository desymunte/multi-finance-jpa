package com.collection.jpa.service;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Subquery;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.collection.jpa.entity.BucketDetail;
import com.collection.jpa.entity.LKPTransaction;


@Repository
@Transactional(readOnly = true)
public class BucketDetailImpl implements BucketDetailService {
	
	@PersistenceContext
	private EntityManager entityManager;
	
/*	@Autowired
	public List<BucketDetail> listPrinting(List<LKPTransaction> list) {
		final List<Predicate> predicates = new ArrayList<Predicate>();

//		CriteriaBuilder cBuilder = entityManager.getCriteriaBuilder();
//		CriteriaQuery<BucketDetail> cQuery = cBuilder.createQuery(BucketDetail.class);
//		Root<BucketDetail> root = cQuery.from(BucketDetail.class);
//
//        predicates.add(cBuilder.exists(subquery).isMember(root.get("bucket").get("contractNo"), list));
//		cQuery.select(root).where(cBuilder.and(predicates.toArray(new Predicate[predicates.size()])));
//
//		TypedQuery<BucketDetail> query = entityManager.createQuery(cQuery);
		
		return entityManager.createQuery("SELECT bd FROM bucketdetail bd ").getResultList();
	}
*/
}
