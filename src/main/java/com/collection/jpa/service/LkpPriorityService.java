package com.collection.jpa.service;

import java.util.List;

import com.collection.jpa.entity.LKPPriority;


public interface LkpPriorityService {

	List<LKPPriority> findAll();

	LKPPriority insert(LKPPriority persisted);
	
//	LkpPriority save(LkpPriority persisted);

}
