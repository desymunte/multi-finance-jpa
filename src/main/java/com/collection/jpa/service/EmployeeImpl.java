package com.collection.jpa.service;

import java.util.List;

import javax.persistence.TypedQuery;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.collection.jpa.CustomUserDetails;
import com.collection.jpa.dao.EmployeeRepository;
import com.collection.jpa.dto.EmployeeCriteria;
import com.collection.jpa.dto.EmployeeDTO;
import com.collection.jpa.dto.OfficeCriteria;
import com.collection.jpa.entity.Employee;
import com.collection.jpa.util.Page;


@Repository
@Transactional(readOnly = true)
public class EmployeeImpl implements EmployeeService {
	
	@Autowired
	private EmployeeRepository repository; 
	
	@Autowired
	private QueryImpl queryService;


	public Employee findOne(Integer id) {
		return repository.findOne(id);
	}
	
	public List<EmployeeDTO> listCollectionSpv() {
        CustomUserDetails user = (CustomUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        OfficeCriteria criteria = new OfficeCriteria();
        criteria.setId(user.getOfficeId());
		return queryService.collectionSpvAvailable(criteria).getResultList();
	}

	public List<EmployeeDTO> listRemedialSpv() {
        CustomUserDetails user = (CustomUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        OfficeCriteria criteria = new OfficeCriteria();
        criteria.setId(user.getOfficeId());
		return queryService.remedialSpvAvailable(criteria).getResultList();
	}

	public List<EmployeeDTO> listDeskcallSpv() {
        CustomUserDetails user = (CustomUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        OfficeCriteria criteria = new OfficeCriteria();
        criteria.setId(user.getOfficeId());
		return queryService.deskcallSpvAvailable(criteria).getResultList();
	}
/*
	public List<EmployeeDTO> listCollectionSpv() {
        CustomUserDetails user = (CustomUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        EmployeeCriteria criteria = new EmployeeCriteria();
        criteria.setOfficeId(user.getOfficeId());
        criteria.setJobTitle("COLLSPV");
        criteria.setJobLevel(1);
		return queryService.employee(criteria).getResultList();
	}

	public List<EmployeeDTO> listRemedialSpv() {
        CustomUserDetails user = (CustomUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        EmployeeCriteria criteria = new EmployeeCriteria();
        criteria.setOfficeId(user.getOfficeId());
        criteria.setJobTitle("REMSPV");
        criteria.setJobLevel(1);
		return queryService.employee(criteria).getResultList();
	}

	public List<EmployeeDTO> listDeskcallSpv() {
        CustomUserDetails user = (CustomUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        EmployeeCriteria criteria = new EmployeeCriteria();
        criteria.setOfficeId(user.getOfficeId());
        criteria.setJobTitle("DSKSPV");
        criteria.setJobLevel(1);
		return queryService.employee(criteria).getResultList();
	}

//	public List<Employee> listCollectionStaff() {
//        CustomUserDetails user = (CustomUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
//
//        EmployeeCriteria criteria = new EmployeeCriteria();
//        criteria.setOfficeId(user.getOfficeId());
//		criteria.setJobTitle("COLL");
//        criteria.setJobLevel(0);
//		return queryService.employee(criteria).getResultList();
//	}
*/

	public List<EmployeeDTO> listCollectionStaff() {
        CustomUserDetails user = (CustomUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        OfficeCriteria criteria = new OfficeCriteria();
        criteria.setId(user.getOfficeId());
		return queryService.collectionStaffAvailable(criteria).getResultList();
	}

	public List<EmployeeDTO> listRemedialStaff() {
        CustomUserDetails user = (CustomUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        OfficeCriteria criteria = new OfficeCriteria();
        criteria.setId(user.getOfficeId());
		return queryService.remedialStaffAvailable(criteria).getResultList();
	}

	public List<EmployeeDTO> listDeskcallStaff() {
        CustomUserDetails user = (CustomUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        OfficeCriteria criteria = new OfficeCriteria();
        criteria.setId(user.getOfficeId());
		return queryService.deskcallStaffAvailable(criteria).getResultList();
	}

	public List<Employee> list() {
        CustomUserDetails user = (CustomUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        EmployeeCriteria criteria = new EmployeeCriteria();
        criteria.setOfficeId(user.getOfficeId());
		return queryService.employee(criteria).getResultList();
	}

	public Page<Employee> page(EmployeeCriteria criteria, Pageable pageable) {
        CustomUserDetails user = (CustomUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        if (criteria == null) {
        	return new Page<Employee>();
        }
        criteria.setOfficeId(user.getOfficeId());
		TypedQuery<Employee> query = queryService.employee(criteria);
		return (Page<Employee>) new Page<Employee>(query, pageable);
	}
	
}
