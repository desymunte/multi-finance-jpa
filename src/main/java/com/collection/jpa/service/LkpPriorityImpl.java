package com.collection.jpa.service;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.collection.jpa.dao.LKPPriorityRepository;
import com.collection.jpa.entity.LKPPriority;


@Repository
@Transactional(readOnly = true)
public class LkpPriorityImpl implements LkpPriorityService {
	
	@PersistenceContext
	private EntityManager entityManager;
	
	@Autowired
	private LKPPriorityRepository repository;
	
	@Override
	public List<LKPPriority> findAll() {
		return repository.findAll();
	}
	
	@Override
    @Transactional
	public LKPPriority insert(LKPPriority persisted) {
		return repository.save(persisted);
	}
	
    @Transactional
	@Modifying(clearAutomatically = true)
	public LKPPriority update(LKPPriority persisted) {
		return repository.save(persisted);
	}

    @Transactional
	public void delete(Integer id) {
		// TODO Auto-generated method stub
//		Long delId = Long.parseLong(id.toString());
		repository.delete(id);
	}
	
//    @Transactional
//	@Modifying
//	public void delete(LkpPriority persisted) {
//		// TODO Auto-generated method stub
//		repository.delete(persisted);
//	}
}
