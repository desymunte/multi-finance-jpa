package com.collection.jpa.service;

import java.sql.Date;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.StoredProcedureQuery;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.CriteriaUpdate;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.collection.jpa.dto.CollectionMemberDTO;
import com.collection.jpa.dto.OfficeCriteria;
import com.collection.jpa.dto.OfficeCycleCriteria;
import com.collection.jpa.dto.PrintCriteria;
import com.collection.jpa.dto.ProfCollectorCriteria;
import com.collection.jpa.dto.ProfCollectorDTO;
import com.collection.jpa.dto.RemedialMemberDTO;
import com.collection.jpa.dto.SkpcCriteria;
import com.collection.jpa.dto.SkpcPageDTO;
import com.collection.jpa.CustomUserDetails;
import com.collection.jpa.dto.AreaCriteria;
import com.collection.jpa.dto.BucketDetailDTO;
import com.collection.jpa.dto.TeamCriteria;
import com.collection.jpa.dto.TeamDTO;
import com.collection.jpa.dto.ZipDTO;
import com.collection.jpa.dto.CycleCriteria;
import com.collection.jpa.dto.CycleDTO;
import com.collection.jpa.dto.DeskCallTeamMemberDTO;
import com.collection.jpa.dto.LkpTeamPageDTO;
import com.collection.jpa.dto.EmployeeCriteria;
import com.collection.jpa.dto.EmployeeDTO;
import com.collection.jpa.dto.LkdCriteria;
import com.collection.jpa.dto.LkdDataDTO;
import com.collection.jpa.dto.LkpCriteria;
import com.collection.jpa.dto.LkpDataDTO;
import com.collection.jpa.dto.LkpPageDTO;
import com.collection.jpa.dto.LkpPrintPageDTO;
import com.collection.jpa.dto.LkpCyclePageDTO;
import com.collection.jpa.dto.LkrTeamPageDTO;
import com.collection.jpa.dto.LkrCriteria;
import com.collection.jpa.dto.LkrDataDTO;
import com.collection.jpa.dto.LkrPageDTO;
import com.collection.jpa.dto.LkrPrintPageDTO;
import com.collection.jpa.dto.LkrCyclePageDTO;
import com.collection.jpa.dto.MemberCriteria;
import com.collection.jpa.entity.Bucket;
import com.collection.jpa.entity.BucketDetail;
import com.collection.jpa.entity.CollectionDormant;
import com.collection.jpa.entity.CollectionMember;
import com.collection.jpa.entity.CollectionTeam;
import com.collection.jpa.entity.CollectionWorkingArea;
import com.collection.jpa.entity.Cycle;
import com.collection.jpa.entity.CycleType;
import com.collection.jpa.entity.DeskcallMember;
import com.collection.jpa.entity.DeskcallTeam;
import com.collection.jpa.entity.Employee;
import com.collection.jpa.entity.JobTitle;
import com.collection.jpa.entity.LKDTransaction;
import com.collection.jpa.entity.LKPTransaction;
import com.collection.jpa.entity.LKRTransaction;
import com.collection.jpa.entity.Office;
import com.collection.jpa.entity.PrintActivity;
import com.collection.jpa.entity.ProfCollector;
import com.collection.jpa.entity.RemedialMember;
import com.collection.jpa.entity.ProfCollMember;
import com.collection.jpa.entity.RemedialTeam;
import com.collection.jpa.entity.RemedialWorkingArea;
import com.collection.jpa.entity.SKPCTransaction;
import com.collection.jpa.entity.SKPCTransactionDetail;
import com.collection.jpa.entity.Status;
import com.collection.jpa.entity.Unit;
import com.collection.jpa.entity.Zipcode;


@Repository
@Transactional(readOnly = true)
public class QueryImpl implements QueryService {
	
	@PersistenceContext
	private EntityManager entityManager;
	

	/* CYCLE */

	public TypedQuery<CycleDTO> cycle(CycleCriteria criteria) {
		final List<Predicate> predicates = new ArrayList<Predicate>();

		CriteriaBuilder cBuilder = entityManager.getCriteriaBuilder();
		CriteriaQuery<CycleDTO> cQuery = cBuilder.createQuery(CycleDTO.class);
		Root<Cycle> root = cQuery.from(Cycle.class);
		Join<Cycle, CycleType> dscr = root.join("type", JoinType.LEFT); //left outer join

		if (criteria == null) {
			return null;
		}
        if (criteria.getId() != null) {
            predicates.add(cBuilder.equal(root.get("id"), criteria.getId()));
        }
        if (criteria.getCode() != null) {
            predicates.add(cBuilder.equal(root.get("code"), criteria.getCode()));
        }
        if (criteria.getOverdue() != null) {
            predicates.add(cBuilder.ge(root.get("overdueStart"), criteria.getOverdue()));
            predicates.add(cBuilder.le(root.get("overdueEnd"), criteria.getOverdue()));
        }
        if (criteria.getType() != null) {
            predicates.add(cBuilder.equal(dscr.get("type"), criteria.getType()));
        }

        cQuery.select(cBuilder.construct(CycleDTO.class,
        		root.get("id"),
        		root.get("code"),
        		root.get("name"),
        		root.get("description"),
        		root.get("overdueStart"),
        		root.get("overdueEnd"),
        		dscr.get("type"),
        		dscr.get("description"))); 
        
        cQuery.where(cBuilder.and(predicates.toArray(new Predicate[predicates.size()])))
		      .orderBy(cBuilder.asc(root.get("overdueStart")));

		TypedQuery<CycleDTO> query = entityManager.createQuery(cQuery);
		
		return query;
	}

	public Integer numCollectionTeam(Integer cycleId) {
		return (Integer) entityManager
				.createNativeQuery("select fnGet_NumCollectionTeam(:cycle)")
				.setParameter("cycle", cycleId)
				.getSingleResult();
	}

	public Integer numRemedialTeam(Integer cycleId) {
		return (Integer) entityManager
				.createNativeQuery("select fnGet_NumRemedialTeam(:cycle)")
				.setParameter("cycle", cycleId)
				.getSingleResult();
	}

	public Integer numDeskcallTeam(Integer cycleId) {
		return (Integer) entityManager
				.createNativeQuery("select fnGet_NumDeskcallTeam(:cycle)")
				.setParameter("cycle", cycleId)
				.getSingleResult();
	}


	/* ZIPCODE */
	
	public TypedQuery<ZipDTO> zipcodeAvailable(OfficeCycleCriteria criteria, List<Integer> theList) {
		final List<Predicate> predicates = new ArrayList<Predicate>();

		CriteriaBuilder cBuilder = entityManager.getCriteriaBuilder();
		CriteriaQuery<ZipDTO> cQuery = cBuilder.createQuery(ZipDTO.class);
		Root<Zipcode> root = cQuery.from(Zipcode.class);
    	Join<Zipcode, Office> office = root.join("office", JoinType.LEFT);

        if (criteria == null || criteria.getOfficeId() == null || criteria.getCycleId() == null) {
        	return null;
        }
        predicates.add(cBuilder.equal(office.get("id"), criteria.getOfficeId()));
        if (theList.size() > 0) {
        	Expression<Integer> parentExpression = root.get("id");
        	predicates.add(parentExpression.in(theList).not());
        }

        cQuery.select(cBuilder.construct(ZipDTO.class,
        		root.get("id"),
        		root.get("zipcode"),
        		root.get("subzipcode"),
        		root.get("name"))); 
        
		//cQuery.orderBy(cBuilder.asc(root.get("zipcode")), cBuilder.asc(root.get("subzipcode")));
		cQuery.orderBy(cBuilder.asc(root.get("id")));
        cQuery.where(cBuilder.and(predicates.toArray(new Predicate[predicates.size()])));

		TypedQuery<ZipDTO> query = entityManager.createQuery(cQuery);
		
		return query;
	}

	public TypedQuery<ZipDTO> zipcodeCollectionMember(MemberCriteria criteria) {
		final List<Predicate> predicates = new ArrayList<Predicate>();

		CriteriaBuilder cBuilder = entityManager.getCriteriaBuilder();
		CriteriaQuery<ZipDTO> cQuery = cBuilder.createQuery(ZipDTO.class);
		Root<Zipcode> root = cQuery.from(Zipcode.class);
		Join<Zipcode, Office> o = root.join("office", JoinType.LEFT);
		Join<Zipcode, CollectionWorkingArea> area = root.join("collectionWorkingAreas", JoinType.LEFT); //left outer join
		Join<CollectionWorkingArea, CollectionMember> member = area.join("collectionMember", JoinType.LEFT); //left outer join
		//Join<CollectionMember, CollectionTeam> team = member.join("collectionTeam", JoinType.LEFT); //left outer join

        if (criteria == null || criteria.getOfficeId() == null || criteria.getMemberId() == null) {
        	return null;
        }
        predicates.add(cBuilder.equal(o.get("id"), criteria.getOfficeId()));
        predicates.add(cBuilder.equal(member.get("id"), criteria.getMemberId()));

        cQuery.select(cBuilder.construct(ZipDTO.class,
        		root.get("id"),
        		root.get("zipcode"),
        		root.get("subzipcode"),
        		root.get("name"))); 
        
		cQuery.orderBy(cBuilder.asc(root.get("zipcode")), cBuilder.asc(root.get("subzipcode")));
        cQuery.where(cBuilder.and(predicates.toArray(new Predicate[predicates.size()])));

		TypedQuery<ZipDTO> query = entityManager.createQuery(cQuery);
		
		return query;
	}

	public TypedQuery<ZipDTO> zipcodeRemedialMember(MemberCriteria criteria) {
		final List<Predicate> predicates = new ArrayList<Predicate>();

		CriteriaBuilder cBuilder = entityManager.getCriteriaBuilder();
		CriteriaQuery<ZipDTO> cQuery = cBuilder.createQuery(ZipDTO.class);
		Root<Zipcode> root = cQuery.from(Zipcode.class);
		Join<Zipcode, Office> o = root.join("office", JoinType.LEFT);
		Join<Zipcode, RemedialWorkingArea> area = root.join("remedialWorkingAreas", JoinType.LEFT); //left outer join
		Join<RemedialWorkingArea, RemedialMember> member = area.join("remedialMember", JoinType.LEFT); //left outer join
		//Join<RemedialMember, RemedialTeam> team = member.join("remedialTeam", JoinType.LEFT); //left outer join

        if (criteria == null || criteria.getOfficeId() == null || criteria.getMemberId() == null) {
        	return null;
        }
        predicates.add(cBuilder.equal(o.get("id"), criteria.getOfficeId()));
        predicates.add(cBuilder.equal(member.get("id"), criteria.getMemberId()));

        cQuery.select(cBuilder.construct(ZipDTO.class,
        		root.get("id"),
        		root.get("zipcode"),
        		root.get("subzipcode"),
        		root.get("name"))); 
        
		cQuery.orderBy(cBuilder.asc(root.get("zipcode")), cBuilder.asc(root.get("subzipcode")));
        cQuery.where(cBuilder.and(predicates.toArray(new Predicate[predicates.size()])));

		TypedQuery<ZipDTO> query = entityManager.createQuery(cQuery);
		
		return query;
	}


	/* EMPLOYEE */
	
	public TypedQuery<Employee> employee(EmployeeCriteria criteria) {
		final List<Predicate> predicates = new ArrayList<Predicate>();

		CriteriaBuilder cBuilder = entityManager.getCriteriaBuilder();
		//CriteriaQuery<EmployeeDTO> cQuery = cBuilder.createQuery(EmployeeDTO.class);
		CriteriaQuery<Employee> cQuery = cBuilder.createQuery(Employee.class);
		Root<Employee> root = cQuery.from(Employee.class);
		Join<Employee, Office> o = root.join("office", JoinType.LEFT); //left outer join
		Join<Employee, JobTitle> jt = root.join("jobTitle", JoinType.LEFT); //left outer join

		if (criteria == null) {
			return null;
		}
		if(criteria.getOfficeId() != null) {
            predicates.add(cBuilder.equal(o.get("id"), criteria.getOfficeId()));
        }
		if(criteria.getJobTitle() != null) {
            predicates.add(cBuilder.equal(jt.get("code"), criteria.getJobTitle()));
        }
		if(criteria.getJobLevel() != null) {
            predicates.add(cBuilder.equal(jt.get("level"), criteria.getJobLevel()));
        }
        if(criteria.getRegno() != null) {
            predicates.add(cBuilder.equal(root.get("regNo"), criteria.getRegno()));
        }
        if(criteria.getName() != null) {
            predicates.add(cBuilder.like(cBuilder.lower(root.get("name")), "%"+criteria.getName().toLowerCase()+"%"));
        }

//        cQuery.select(cBuilder.construct(EmployeeDTO.class,
//        		root.get("id"),
//        		root.get("regNo"),
//        		root.get("name"),
//        		o.get("id"),
//        		jt.get("code"),
//        		jt.get("name"),
//        		jt.get("level"))); 
        
        cQuery.select(root); 
        cQuery.orderBy(cBuilder.asc(root.get("name")));
        cQuery.where(cBuilder.and(predicates.toArray(new Predicate[predicates.size()])));

		TypedQuery<Employee> query = entityManager.createQuery(cQuery);
		
		return query;
	}
	

	/* COLLECTION */
	
	public TypedQuery<TeamDTO> collectionTeam(TeamCriteria criteria) {
		final List<Predicate> predicates = new ArrayList<Predicate>();

		CriteriaBuilder cBuilder = entityManager.getCriteriaBuilder();
		CriteriaQuery<TeamDTO> cQuery = cBuilder.createQuery(TeamDTO.class);
		Root<CollectionTeam> root = cQuery.from(CollectionTeam.class);
		Join<CollectionTeam, Office> o = root.join("office", JoinType.LEFT); //left outer join
		Join<CollectionTeam, Cycle> c = root.join("cycle", JoinType.LEFT);
		Join<CollectionTeam, Employee> s = root.join("supervisor", JoinType.LEFT);

		if (criteria == null) {
			return null;
		}
		if (criteria.getId() != null) {
            predicates.add(cBuilder.equal(root.get("id"), criteria.getId()));
        }
        if (criteria.getName() != null && !criteria.getName().isEmpty()) {
            predicates.add(cBuilder.like(cBuilder.lower(root.get("name")), "%"+criteria.getName().toLowerCase()+"%"));
        }
		if (criteria.getOfficeId() != null) {
            predicates.add(cBuilder.equal(o.get("id"), criteria.getOfficeId()));
        }
        if (criteria.getSpvId() != null) {
            predicates.add(cBuilder.equal(s.get("id"), criteria.getSpvId()));
        }
        if (criteria.getCycleId() != null) {
            predicates.add(cBuilder.equal(c.get("id"), criteria.getCycleId()));
        }
        
        cQuery.select(cBuilder.construct(TeamDTO.class, 
        		root.get("id"),
        		root.get("name"), 
				o.get("id"),
				o.get("code"),
				o.get("name"), 
				s.get("id"),
				s.get("name"), 
				s.get("regNo"), 
				c.get("id"),
				c.get("name"),
				c.get("code"),
				c.get("type").get("type")));
		cQuery.where(cBuilder.and(predicates.toArray(new Predicate[predicates.size()])));

		TypedQuery<TeamDTO> query = entityManager.createQuery(cQuery);
		
		return query;
	}
	
	public TypedQuery<CollectionMemberDTO> collectionMember(MemberCriteria criteria) {
		final List<Predicate> predicates = new ArrayList<Predicate>();

		CriteriaBuilder cBuilder = entityManager.getCriteriaBuilder();
		CriteriaQuery<CollectionMemberDTO> cQuery = cBuilder.createQuery(CollectionMemberDTO.class);
		Root<CollectionMember> root = cQuery.from(CollectionMember.class);
		Join<CollectionMember, Employee> e = root.join("employee", JoinType.LEFT); //left outer join
		Join<CollectionMember, CollectionTeam> t = root.join("collectionTeam", JoinType.LEFT);

		if (criteria == null) {
			return null;
		}
        if (criteria.getEmplId() != null) {
            predicates.add(cBuilder.equal(e.get("id"), criteria.getEmplId()));
        }
        if (criteria.getEmplName() != null && !criteria.getEmplName().isEmpty()) {
            predicates.add(cBuilder.like(cBuilder.lower(e.get("name")), "%"+criteria.getEmplName().toLowerCase()+"%"));
        }
        if (criteria.getTeamId() != null) {
            predicates.add(cBuilder.equal(t.get("id"), criteria.getTeamId()));
        }
		if (criteria.getTeamIdList() != null) {
			Expression<Integer> teamIdExpr = t.get("id");
			Predicate team = teamIdExpr.in(criteria.getTeamIdList());
    		predicates.add(team);
		}
        if (criteria.getSupervisorId() != null) {
            predicates.add(cBuilder.equal(t.get("supervisor").get("id"), criteria.getSupervisorId()));
        }
        if (criteria.getMonthlyLoad() != null) {
            predicates.add(cBuilder.equal(root.get("monthlyLoad"), criteria.getMonthlyLoad()));
        }
        if (criteria.getDailyLoad() != null) {
            predicates.add(cBuilder.equal(root.get("dailyLoad"), criteria.getDailyLoad()));
        }
		if (criteria.getOfficeId() != null) {
            predicates.add(cBuilder.equal(t.get("office").get("id"), criteria.getOfficeId()));
        }
		if (criteria.getCycleId() != null) {
            predicates.add(cBuilder.equal(t.get("cycle").get("id"), criteria.getCycleId()));
        }
		
		cQuery.select(cBuilder.construct(CollectionMemberDTO.class, 
				root,
				e.get("id"),
				e.get("name"),
				e.get("regNo"), 
				t.get("id"),
				t.get("name"),
				t.get("supervisor").get("id")));
		
        cQuery.orderBy(cBuilder.asc(t.get("name")), cBuilder.asc(e.get("name")));
		cQuery.where(cBuilder.and(predicates.toArray(new Predicate[predicates.size()])));

		TypedQuery<CollectionMemberDTO> query = entityManager.createQuery(cQuery);
		
		return query;
	}

	public TypedQuery<ZipDTO> collectionArea(AreaCriteria criteria) {
		final List<Predicate> predicates = new ArrayList<Predicate>();

		CriteriaBuilder cBuilder = entityManager.getCriteriaBuilder();
		CriteriaQuery<ZipDTO> cQuery = cBuilder.createQuery(ZipDTO.class);
		Root<CollectionWorkingArea> root = cQuery.from(CollectionWorkingArea.class);
		Join<CollectionWorkingArea, Zipcode> zip = root.join("zipcode", JoinType.LEFT); //left outer join
		Join<CollectionWorkingArea, CollectionMember> m = root.join("collectionMember", JoinType.LEFT);
		Join<CollectionMember, CollectionTeam> t = m.join("collectionTeam", JoinType.LEFT);

		if (criteria == null) {
			return null;
		}
		if (criteria.getZipId() != null) {
            predicates.add(cBuilder.equal(zip.get("id"), criteria.getZipId()));
        }
        if (criteria.getZipcode() != null && !criteria.getZipcode().isEmpty()) {
            predicates.add(cBuilder.like(zip.get("zipcode"), criteria.getZipcode()));
        }
        if (criteria.getSubzipcode() != null && !criteria.getSubzipcode().isEmpty()) {
            predicates.add(cBuilder.like(zip.get("subzipcode"), criteria.getSubzipcode()));
        }
        if (criteria.getOfficeId() != null) {
            predicates.add(cBuilder.equal(t.get("office").get("id"), criteria.getOfficeId()));
        }
        if (criteria.getCycleId() != null) {
            predicates.add(cBuilder.equal(t.get("cycle").get("id"), criteria.getCycleId()));
        }
		
		cQuery.select(cBuilder.construct(ZipDTO.class, 
				zip.get("id"), 
				zip.get("zipcode"), 
				zip.get("subzipcode"), 
				zip.get("name")));
		cQuery.where(cBuilder.and(predicates.toArray(new Predicate[predicates.size()])));

		TypedQuery<ZipDTO> query = entityManager.createQuery(cQuery);
		
		return query;
	}

	public TypedQuery<EmployeeDTO> collectionSpvAvailable(OfficeCriteria criteria) {
		final List<Predicate> predicates = new ArrayList<Predicate>();

		CriteriaBuilder cBuilder = entityManager.getCriteriaBuilder();
		CriteriaQuery<EmployeeDTO> cQuery = cBuilder.createQuery(EmployeeDTO.class);
		Root<Employee> root = cQuery.from(Employee.class);
		Join<Employee, Office> o = root.join("office", JoinType.LEFT); //left outer join
		Join<Employee, JobTitle> jt = root.join("jobTitle", JoinType.LEFT); //left outer join
		Join<Employee, CollectionTeam> team = root.join("collectionSupervisor", JoinType.LEFT); //left outer join

        if (criteria == null || criteria.getId() == null) {
        	return null;
        }
		predicates.add(cBuilder.equal(o.get("id"), criteria.getId()));
		predicates.add(cBuilder.like(jt.get("code"), "COLLSPV"));
        predicates.add(cBuilder.isNull(team));

        cQuery.select(cBuilder.construct(EmployeeDTO.class,
        		root.get("id"),
        		root.get("regNo"),
        		root.get("name"),
        		o.get("id"),
        		jt.get("code"),
        		jt.get("name"),
        		jt.get("level"))); 
        
        cQuery.orderBy(cBuilder.asc(root.get("name")));
        cQuery.where(cBuilder.and(predicates.toArray(new Predicate[predicates.size()])));

		TypedQuery<EmployeeDTO> query = entityManager.createQuery(cQuery);
		
		return query;
	}
	
	public TypedQuery<EmployeeDTO> collectionStaffAvailable(OfficeCriteria criteria) {
		final List<Predicate> predicates = new ArrayList<Predicate>();

		CriteriaBuilder cBuilder = entityManager.getCriteriaBuilder();
		CriteriaQuery<EmployeeDTO> cQuery = cBuilder.createQuery(EmployeeDTO.class);
		Root<Employee> root = cQuery.from(Employee.class);
		Join<Employee, Office> o = root.join("office", JoinType.LEFT); //left outer join
		Join<Employee, JobTitle> jt = root.join("jobTitle", JoinType.LEFT); //left outer join
		Join<Employee, CollectionMember> member = root.join("collectionMembers", JoinType.LEFT); //left outer join
		//Join<Employee, CollectionDormant> dormant = root.join("collectionDormant", JoinType.LEFT); //left outer join

        if (criteria == null || criteria.getId() == null) {
        	return null;
        }
		predicates.add(cBuilder.equal(o.get("id"), criteria.getId()));
		predicates.add(cBuilder.like(jt.get("code"), "COLL"));
        predicates.add(cBuilder.isNull(member));

        cQuery.select(cBuilder.construct(EmployeeDTO.class,
        		root.get("id"),
        		root.get("regNo"),
        		root.get("name"),
        		o.get("id"),
        		jt.get("code"),
        		jt.get("name"),
        		jt.get("level"))); 
        
        cQuery.orderBy(cBuilder.asc(root.get("name")));
        cQuery.where(cBuilder.and(predicates.toArray(new Predicate[predicates.size()])));

		TypedQuery<EmployeeDTO> query = entityManager.createQuery(cQuery);
		
		return query;
	}
	
    
	/* REMEDIAL */
	
	public TypedQuery<TeamDTO> remedialTeam(TeamCriteria criteria) {
		final List<Predicate> predicates = new ArrayList<Predicate>();

		CriteriaBuilder cBuilder = entityManager.getCriteriaBuilder();
		CriteriaQuery<TeamDTO> cQuery = cBuilder.createQuery(TeamDTO.class);
		Root<RemedialTeam> root = cQuery.from(RemedialTeam.class);
		Join<RemedialTeam, Office> o = root.join("office", JoinType.LEFT); //left outer join
		Join<RemedialTeam, Cycle> c = root.join("cycle", JoinType.LEFT);
		Join<RemedialTeam, Employee> s = root.join("supervisor", JoinType.LEFT);

		if (criteria == null) {
			return null;
		}
		if (criteria.getId() != null) {
            predicates.add(cBuilder.equal(root.get("id"), criteria.getId()));
        }
        if (criteria.getName() != null && !criteria.getName().isEmpty()) {
            predicates.add(cBuilder.like(cBuilder.lower(root.get("name")), "%"+criteria.getName().toLowerCase()+"%"));
        }
		if (criteria.getOfficeId() != null) {
            predicates.add(cBuilder.equal(o.get("id"), criteria.getOfficeId()));
        }
        if (criteria.getSpvId() != null) {
            predicates.add(cBuilder.equal(s.get("id"), criteria.getSpvId()));
        }
        if (criteria.getCycleId() != null) {
            predicates.add(cBuilder.equal(c.get("id"), criteria.getCycleId()));
        }
        
        cQuery.select(cBuilder.construct(TeamDTO.class, 
        		root.get("id"),
        		root.get("name"), 
				o.get("id"),
				o.get("code"),
				o.get("name"), 
				s.get("id"),
				s.get("name"), 
				s.get("regNo"), 
				c.get("id"),
				c.get("name"),
				c.get("code"),
				c.get("type").get("type")));
		cQuery.where(cBuilder.and(predicates.toArray(new Predicate[predicates.size()])));

		TypedQuery<TeamDTO> query = entityManager.createQuery(cQuery);
		
		return query;
	}
	
	public TypedQuery<RemedialMemberDTO> remedialMember(MemberCriteria criteria) {
		final List<Predicate> predicates = new ArrayList<Predicate>();

		CriteriaBuilder cBuilder = entityManager.getCriteriaBuilder();
		CriteriaQuery<RemedialMemberDTO> cQuery = cBuilder.createQuery(RemedialMemberDTO.class);
		Root<RemedialMember> root = cQuery.from(RemedialMember.class);
		Join<RemedialMember, Employee> e = root.join("employee", JoinType.LEFT); //left outer join
		Join<RemedialMember, RemedialTeam> t = root.join("remedialTeam", JoinType.LEFT);

		if (criteria == null) {
			return null;
		}
        if (criteria.getEmplId() != null) {
            predicates.add(cBuilder.equal(e.get("id"), criteria.getEmplId()));
        }
        if (criteria.getEmplName() != null && !criteria.getEmplName().isEmpty()) {
            predicates.add(cBuilder.like(cBuilder.lower(e.get("name")), "%"+criteria.getEmplName().toLowerCase()+"%"));
        }
        if (criteria.getTeamId() != null) {
            predicates.add(cBuilder.equal(t.get("id"), criteria.getTeamId()));
        }
		if (criteria.getTeamIdList() != null) {
			Expression<Integer> teamIdExpr = t.get("id");
			Predicate team = teamIdExpr.in(criteria.getTeamIdList());
    		predicates.add(team);
		}
        if (criteria.getSupervisorId() != null) {
            predicates.add(cBuilder.equal(t.get("supervisor").get("id"), criteria.getSupervisorId()));
        }
        if (criteria.getMonthlyLoad() != null) {
            predicates.add(cBuilder.equal(root.get("monthlyLoad"), criteria.getMonthlyLoad()));
        }
        if (criteria.getDailyLoad() != null) {
            predicates.add(cBuilder.equal(root.get("dailyLoad"), criteria.getDailyLoad()));
        }
		if (criteria.getOfficeId() != null) {
            predicates.add(cBuilder.equal(t.get("office").get("id"), criteria.getOfficeId()));
        }
		if (criteria.getCycleId() != null) {
            predicates.add(cBuilder.equal(t.get("cycle").get("id"), criteria.getCycleId()));
        }
		
		cQuery.select(cBuilder.construct(RemedialMemberDTO.class, 
				root,
				e.get("id"),
				e.get("name"),
				e.get("regNo"), 
				t.get("id"),
				t.get("name"),
				t.get("supervisor").get("id")));
		cQuery.orderBy(cBuilder.asc(t.get("name")), cBuilder.asc(e.get("name")));
		cQuery.where(cBuilder.and(predicates.toArray(new Predicate[predicates.size()])));

		TypedQuery<RemedialMemberDTO> query = entityManager.createQuery(cQuery);
		
		return query;
	}

	public TypedQuery<ZipDTO> remedialArea(AreaCriteria criteria) {
		final List<Predicate> predicates = new ArrayList<Predicate>();

		CriteriaBuilder cBuilder = entityManager.getCriteriaBuilder();
		CriteriaQuery<ZipDTO> cQuery = cBuilder.createQuery(ZipDTO.class);
		Root<RemedialWorkingArea> root = cQuery.from(RemedialWorkingArea.class);
		Join<RemedialWorkingArea, Zipcode> zip = root.join("zipcode", JoinType.LEFT); //left outer join
		Join<RemedialWorkingArea, RemedialMember> m = root.join("remedialMember", JoinType.LEFT);
		Join<RemedialMember, RemedialTeam> t = m.join("remedialTeam", JoinType.LEFT);

		if (criteria == null) {
			return null;
		}
		if (criteria.getZipId() != null) {
            predicates.add(cBuilder.equal(zip.get("id"), criteria.getZipId()));
        }
        if (criteria.getZipcode() != null && !criteria.getZipcode().isEmpty()) {
            predicates.add(cBuilder.like(zip.get("zipcode"), criteria.getZipcode()));
        }
        if (criteria.getSubzipcode() != null && !criteria.getSubzipcode().isEmpty()) {
            predicates.add(cBuilder.like(zip.get("subzipcode"), criteria.getSubzipcode()));
        }
        if (criteria.getOfficeId() != null) {
            predicates.add(cBuilder.equal(t.get("office").get("id"), criteria.getOfficeId()));
        }
        if (criteria.getCycleId() != null) {
            predicates.add(cBuilder.equal(t.get("cycle").get("id"), criteria.getCycleId()));
        }
		
		cQuery.select(cBuilder.construct(ZipDTO.class, 
				zip.get("id"), 
				zip.get("zipcode"), 
				zip.get("subzipcode"), 
				zip.get("name")));
		cQuery.where(cBuilder.and(predicates.toArray(new Predicate[predicates.size()])));

		TypedQuery<ZipDTO> query = entityManager.createQuery(cQuery);
		
		return query;
	}

	public TypedQuery<EmployeeDTO> remedialSpvAvailable(OfficeCriteria criteria) {
		final List<Predicate> predicates = new ArrayList<Predicate>();

		CriteriaBuilder cBuilder = entityManager.getCriteriaBuilder();
		CriteriaQuery<EmployeeDTO> cQuery = cBuilder.createQuery(EmployeeDTO.class);
		Root<Employee> root = cQuery.from(Employee.class);
		Join<Employee, Office> o = root.join("office", JoinType.LEFT); //left outer join
		Join<Employee, JobTitle> jt = root.join("jobTitle", JoinType.LEFT); //left outer join
		Join<Employee, RemedialTeam> team = root.join("remedialSupervisor", JoinType.LEFT); //left outer join

        if (criteria == null || criteria.getId() == null) {
        	return null;
        }
		predicates.add(cBuilder.equal(o.get("id"), criteria.getId()));
		predicates.add(cBuilder.like(jt.get("code"), "REMSPV"));
        predicates.add(cBuilder.isNull(team));

        cQuery.select(cBuilder.construct(EmployeeDTO.class,
        		root.get("id"),
        		root.get("regNo"),
        		root.get("name"),
        		o.get("id"),
        		jt.get("code"),
        		jt.get("name"),
        		jt.get("level"))); 
        
        cQuery.orderBy(cBuilder.asc(root.get("name")));
        cQuery.where(cBuilder.and(predicates.toArray(new Predicate[predicates.size()])));

		TypedQuery<EmployeeDTO> query = entityManager.createQuery(cQuery);
		
		return query;
	}
	
	public TypedQuery<EmployeeDTO> remedialStaffAvailable(OfficeCriteria criteria) {
		final List<Predicate> predicates = new ArrayList<Predicate>();

		CriteriaBuilder cBuilder = entityManager.getCriteriaBuilder();
		CriteriaQuery<EmployeeDTO> cQuery = cBuilder.createQuery(EmployeeDTO.class);
		Root<Employee> root = cQuery.from(Employee.class);
		Join<Employee, Office> o = root.join("office", JoinType.LEFT); //left outer join
		Join<Employee, JobTitle> jt = root.join("jobTitle", JoinType.LEFT); //left outer join
		Join<Employee, RemedialMember> member = root.join("remedialMembers", JoinType.LEFT); //left outer join

        if (criteria == null || criteria.getId() == null) {
        	return null;
        }
		predicates.add(cBuilder.equal(o.get("id"), criteria.getId()));
		predicates.add(cBuilder.like(jt.get("code"), "REM"));
        predicates.add(cBuilder.isNull(member));

        cQuery.select(cBuilder.construct(EmployeeDTO.class,
        		root.get("id"),
        		root.get("regNo"),
        		root.get("name"),
        		o.get("id"),
        		jt.get("code"),
        		jt.get("name"),
        		jt.get("level"))); 
        
        cQuery.orderBy(cBuilder.asc(root.get("name")));
        cQuery.where(cBuilder.and(predicates.toArray(new Predicate[predicates.size()])));

		TypedQuery<EmployeeDTO> query = entityManager.createQuery(cQuery);
		
		return query;
	}
	

	/* DESK CALL */
	
	public TypedQuery<TeamDTO> deskcallTeam(TeamCriteria criteria) {
		final List<Predicate> predicates = new ArrayList<Predicate>();

		CriteriaBuilder cBuilder = entityManager.getCriteriaBuilder();
		CriteriaQuery<TeamDTO> cQuery = cBuilder.createQuery(TeamDTO.class);
		Root<DeskcallTeam> root = cQuery.from(DeskcallTeam.class);
		Join<DeskcallTeam, Office> o = root.join("office", JoinType.LEFT); //left outer join
		Join<DeskcallTeam, Cycle> c = root.join("cycle", JoinType.LEFT);
		Join<DeskcallTeam, Employee> s = root.join("supervisor", JoinType.LEFT);

		if (criteria == null) {
			return null;
		}
		if (criteria.getId() != null) {
            predicates.add(cBuilder.equal(root.get("id"), criteria.getId()));
        }
        if (criteria.getName() != null && !criteria.getName().isEmpty()) {
            predicates.add(cBuilder.like(cBuilder.lower(root.get("name")), "%"+criteria.getName().toLowerCase()+"%"));
        }
		if (criteria.getOfficeId() != null) {
            predicates.add(cBuilder.equal(o.get("id"), criteria.getOfficeId()));
        }
        if (criteria.getSpvId() != null) {
            predicates.add(cBuilder.equal(s.get("id"), criteria.getSpvId()));
        }
        if (criteria.getCycleId() != null) {
            predicates.add(cBuilder.equal(c.get("id"), criteria.getCycleId()));
        }
        
        cQuery.select(cBuilder.construct(TeamDTO.class, 
        		root.get("id"),
        		root.get("name"), 
				o.get("id"),
				o.get("code"),
				o.get("name"), 
				s.get("id"),
				s.get("name"), 
				s.get("regNo"), 
				c.get("id"),
				c.get("name"),
				c.get("code"),
				c.get("type").get("type")));
		cQuery.where(cBuilder.and(predicates.toArray(new Predicate[predicates.size()])));

		TypedQuery<TeamDTO> query = entityManager.createQuery(cQuery);
		
		return query;
	}
	
	public TypedQuery<DeskCallTeamMemberDTO> deskcallMember(MemberCriteria criteria) {
		final List<Predicate> predicates = new ArrayList<Predicate>();

		CriteriaBuilder cBuilder = entityManager.getCriteriaBuilder();
		CriteriaQuery<DeskCallTeamMemberDTO> cQuery = cBuilder.createQuery(DeskCallTeamMemberDTO.class);
		Root<DeskcallMember> root = cQuery.from(DeskcallMember.class);
		Join<DeskcallMember, Employee> e = root.join("employee", JoinType.LEFT); //left outer join
		Join<DeskcallMember, DeskcallTeam> t = root.join("deskcallTeam", JoinType.LEFT);

		if (criteria == null) {
			return null;
		}
        if (criteria.getEmplId() != null) {
            predicates.add(cBuilder.equal(e.get("id"), criteria.getEmplId()));
        }
        if (criteria.getEmplName() != null && !criteria.getEmplName().isEmpty()) {
            predicates.add(cBuilder.like(cBuilder.lower(e.get("name")), "%"+criteria.getEmplName().toLowerCase()+"%"));
        }
        if (criteria.getTeamId() != null) {
            predicates.add(cBuilder.equal(t.get("id"), criteria.getTeamId()));
		}
		if (criteria.getTeamIdList() != null) {
			Expression<Integer> teamIdExpr = t.get("id");
			Predicate team = teamIdExpr.in(criteria.getTeamIdList());
    		predicates.add(team);
		}
        if (criteria.getSupervisorId() != null) {
            predicates.add(cBuilder.equal(t.get("supervisor").get("id"), criteria.getSupervisorId()));
        }
        if (criteria.getMonthlyLoad() != null) {
            predicates.add(cBuilder.equal(root.get("monthlyLoad"), criteria.getMonthlyLoad()));
        }
        if (criteria.getDailyLoad() != null) {
            predicates.add(cBuilder.equal(root.get("dailyLoad"), criteria.getDailyLoad()));
        }
		if (criteria.getOfficeId() != null) {
            predicates.add(cBuilder.equal(t.get("office").get("id"), criteria.getOfficeId()));
        }
		
		// cQuery.select(cBuilder.construct(DeskCallTeamMemberDTO.class, 
		// 		root.get("id"), 
		// 		e.get("id"),
		// 		e.get("name"),
		// 		e.get("regNo"), 
		// 		t.get("id"),
		// 		t.get("name"),
		// 		t.get("supervisor").get("id"),
		// 		root.get("monthlyLoad"),
		// 		root.get("dailyLoad")));
		// cQuery.where(cBuilder.and(predicates.toArray(new Predicate[predicates.size()])));

		cQuery.select(cBuilder.construct(DeskCallTeamMemberDTO.class, 
				root,
				e.get("id"),
				e.get("name"),
				e.get("regNo"), 
				t.get("id"),
				t.get("name"),
				t.get("supervisor").get("id")));
		
        cQuery.orderBy(cBuilder.asc(t.get("name")), cBuilder.asc(e.get("name")));
		cQuery.where(cBuilder.and(predicates.toArray(new Predicate[predicates.size()])));

		TypedQuery<DeskCallTeamMemberDTO> query = entityManager.createQuery(cQuery);
		
		return query;
	}

	public TypedQuery<EmployeeDTO> deskcallSpvAvailable(OfficeCriteria criteria) {
		final List<Predicate> predicates = new ArrayList<Predicate>();

		CriteriaBuilder cBuilder = entityManager.getCriteriaBuilder();
		CriteriaQuery<EmployeeDTO> cQuery = cBuilder.createQuery(EmployeeDTO.class);
		Root<Employee> root = cQuery.from(Employee.class);
		Join<Employee, Office> o = root.join("office", JoinType.LEFT); //left outer join
		Join<Employee, JobTitle> jt = root.join("jobTitle", JoinType.LEFT); //left outer join
		Join<Employee, DeskcallTeam> team = root.join("deskcallSupervisor", JoinType.LEFT); //left outer join

        if (criteria == null || criteria.getId() == null) {
        	return null;
        }
		predicates.add(cBuilder.equal(o.get("id"), criteria.getId()));
		predicates.add(cBuilder.like(jt.get("code"), "DESKSPV"));
        predicates.add(cBuilder.isNull(team));

        cQuery.select(cBuilder.construct(EmployeeDTO.class,
        		root.get("id"),
        		root.get("regNo"),
        		root.get("name"),
        		o.get("id"),
        		jt.get("code"),
        		jt.get("name"),
        		jt.get("level"))); 
        
        cQuery.orderBy(cBuilder.asc(root.get("name")));
        cQuery.where(cBuilder.and(predicates.toArray(new Predicate[predicates.size()])));

		TypedQuery<EmployeeDTO> query = entityManager.createQuery(cQuery);
		
		return query;
	}
	
	public TypedQuery<EmployeeDTO> deskcallStaffAvailable(OfficeCriteria criteria) {
		final List<Predicate> predicates = new ArrayList<Predicate>();

		CriteriaBuilder cBuilder = entityManager.getCriteriaBuilder();
		CriteriaQuery<EmployeeDTO> cQuery = cBuilder.createQuery(EmployeeDTO.class);
		Root<Employee> root = cQuery.from(Employee.class);
		Join<Employee, Office> o = root.join("office", JoinType.LEFT); //left outer join
		Join<Employee, JobTitle> jt = root.join("jobTitle", JoinType.LEFT); //left outer join
		Join<Employee, DeskcallMember> member = root.join("deskcallMembers", JoinType.LEFT); //left outer join

        if (criteria == null || criteria.getId() == null) {
        	return null;
        }
		predicates.add(cBuilder.equal(o.get("id"), criteria.getId()));
		predicates.add(cBuilder.like(jt.get("code"), "DESK"));
        predicates.add(cBuilder.isNull(member));

        cQuery.select(cBuilder.construct(EmployeeDTO.class,
        		root.get("id"),
        		root.get("regNo"),
        		root.get("name"),
        		o.get("id"),
        		jt.get("code"),
        		jt.get("name"),
        		jt.get("level"))); 
        
        cQuery.orderBy(cBuilder.asc(root.get("name")));
        cQuery.where(cBuilder.and(predicates.toArray(new Predicate[predicates.size()])));

		TypedQuery<EmployeeDTO> query = entityManager.createQuery(cQuery);
		
		return query;
	}
	
    
	/* PROFESSIONAL COLLECTOR */
	
    public TypedQuery<ProfCollectorDTO> profCollector(ProfCollectorCriteria criteria) {
        CustomUserDetails user = (CustomUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        final List<Predicate> predicates = new ArrayList<Predicate>();
		CriteriaBuilder cBuilder = entityManager.getCriteriaBuilder();
		CriteriaQuery<ProfCollectorDTO> cQuery = cBuilder.createQuery(ProfCollectorDTO.class);
		Root<ProfCollector> root = cQuery.from(ProfCollector.class);
		Join<ProfCollector, Office> offi = root.join("office", JoinType.LEFT); //left outer join
		Join<ProfCollector, ProfCollMember> staf = root.join("profcollMembers", JoinType.LEFT); //left outer join
		Join<ProfCollector, Status> stat = root.join("mouStatus", JoinType.LEFT); //left outer join

        predicates.add(cBuilder.equal(offi.get("id"), user.getOfficeId()));

        if (criteria.getOfficeId() != null) {
            predicates.add(cBuilder.equal(offi.get("id"), criteria.getOfficeId()));
		}

		if (criteria.getType() != null) {
            predicates.add(cBuilder.equal(root.get("type"), criteria.getType()));
        }

        if (criteria.getName() != null && !criteria.getName().isEmpty()) {
        	Predicate name = cBuilder.like(cBuilder.lower(root.get("name")), "%"+criteria.getName().toLowerCase()+"%");
        	name = cBuilder.or(name, cBuilder.like(cBuilder.lower(staf.get("name")), "%"+criteria.getName().toLowerCase()+"%"));
        	predicates.add(name);
        }

        if (criteria.getIdNo() != null && !criteria.getIdNo().isEmpty()) {
        	Predicate pcid = cBuilder.equal(cBuilder.lower(root.get("idNo")), criteria.getIdNo().toLowerCase());
        	pcid = cBuilder.or(pcid, cBuilder.equal(cBuilder.lower(staf.get("idCardNo")), criteria.getIdNo().toLowerCase()));
        	predicates.add(pcid);
        }

        cQuery.select(cBuilder.construct(ProfCollectorDTO.class, 
        		root.get("id"),
				offi.get("id"),
				offi.get("code"),
				offi.get("name"),
				offi.get("skpcFee"),
				root.get("type"),
        		root.get("name"), 
        		root.get("idNo"), 
				root.get("address"), 
				root.get("phone"), 
				root.get("email"),
        		staf.get("id"), 
        		staf.get("name"), 
        		staf.get("idCardNo"),
        		staf.get("address"), 
        		staf.get("phone"), 
        		staf.get("email"),
        		root.get("mouNo"),
        		root.get("mouDate"),
        		root.get("mouStartDate"),
        		root.get("mouEndDate"),
        		stat.get("id"),
        		stat.get("description"),
        		root.get("performance"))); 

        cQuery.where(cBuilder.and(predicates.toArray(new Predicate[predicates.size()])));

		TypedQuery<ProfCollectorDTO> query = entityManager.createQuery(cQuery);
		
		return query;
	}
	

	/* BUCKET DETAIL */
    
    public TypedQuery<BucketDetailDTO> bucketDetail(String contractNo, Integer from, Integer to) {
    	final List<Predicate> predicates = new ArrayList<Predicate>();

		CriteriaBuilder cBuilder = entityManager.getCriteriaBuilder();
		CriteriaQuery<BucketDetailDTO> cQuery = cBuilder.createQuery(BucketDetailDTO.class);
		Root<Bucket> root = cQuery.from(Bucket.class);
		Join<Bucket, BucketDetail> d = root.join("details", JoinType.LEFT); //left outer join

		if (contractNo == null) {
			return null;
        }
		
		predicates.add(cBuilder.equal(root.get("contractNo"), contractNo));
		predicates.add(cBuilder.between(d.get("installmentNo"), from, to));
		cQuery.select(cBuilder.construct(BucketDetailDTO.class, 
        		d.get("id"),
        		d.get("installmentNo"),
        		d.get("principalAmnt"),
        		d.get("interestAmnt"),
        		d.get("dueDate"),
        		d.get("overdue"),
        		d.get("principalAmntPaid"),
        		d.get("interestAmntPaid"),
        		d.get("paymentDate"),
        		d.get("penaltyAmnt"),
        		d.get("penaltyAmntPaid"),
        		d.get("penaltyPaymentDate"),
        		d.get("collectionFee"),
        		d.get("collectionFeePaid"),
        		d.get("collectionFeePaymentDate")
				));
		cQuery.orderBy(cBuilder.asc(d.get("installmentNo")));
		cQuery.where(cBuilder.and(predicates.toArray(new Predicate[predicates.size()])));
		
		TypedQuery<BucketDetailDTO> query = entityManager.createQuery(cQuery);
		
		return query;
	}

    
    /* LKP */
	
    public TypedQuery<LkpCyclePageDTO> lkpManualAssignment(LkpCriteria criteria) {
        final List<Predicate> predicates = new ArrayList<Predicate>();
        
		CriteriaBuilder cBuilder = entityManager.getCriteriaBuilder();
		CriteriaQuery<LkpCyclePageDTO> cQuery = cBuilder.createQuery(LkpCyclePageDTO.class);
		Root<LKPTransaction> lkp = cQuery.from(LKPTransaction.class);
		Join<LKPTransaction, Bucket> bucket = lkp.join("bucket", JoinType.LEFT); //left outer join
		Join<Bucket, Cycle> cycle = bucket.join("cycle", JoinType.LEFT); //left outer join

		if (criteria == null) {
			return null;
		}
		if (criteria.getOfficeCode() != null) {
			predicates.add(cBuilder.equal(bucket.get("officeCode"), criteria.getOfficeCode()));
		}
		if (criteria.getCycleIdList() != null) {
			Expression<Integer> cycleIdExpr = cycle.get("id");
			Predicate cyclePred = cycleIdExpr.in(criteria.getCycleIdList());
    		predicates.add(cyclePred);
		}
		if (criteria.getContractNo() != null) {
			if (criteria.getContractNo().contains("%")) {
				predicates.add(cBuilder.like(bucket.get("contractNo"), criteria.getContractNo()));
			} else {
				predicates.add(cBuilder.equal(bucket.get("contractNo"), criteria.getContractNo()));
			}
        }
		predicates.add(cBuilder.isNull(lkp.get("assignedTo")));
		predicates.add(cBuilder.isNull(lkp.get("team")));
		predicates.add(cBuilder.isNull(lkp.get("printActivity")));
		predicates.add(cBuilder.isNull(lkp.get("lkpEntryStatus")));

		cQuery.select(cBuilder.construct(LkpCyclePageDTO.class, 
				lkp.get("id"),
        		bucket,
        		cycle
				));
        cQuery.orderBy(cBuilder.asc(cycle.get("id")), cBuilder.asc(bucket.get("contractNo")));
		cQuery.where(cBuilder.and(predicates.toArray(new Predicate[predicates.size()])));
		
		TypedQuery<LkpCyclePageDTO> query = entityManager.createQuery(cQuery);
		
		return query;
	}
    
    public TypedQuery<LkpTeamPageDTO> lkpClustering(LkpCriteria criteria) {
        final List<Predicate> predicates = new ArrayList<Predicate>();

        CriteriaBuilder cBuilder = entityManager.getCriteriaBuilder();
		CriteriaQuery<LkpTeamPageDTO> cQuery = cBuilder.createQuery(LkpTeamPageDTO.class);
		Root<LKPTransaction> lkp = cQuery.from(LKPTransaction.class);
		Join<LKPTransaction, Bucket> bucket = lkp.join("bucket", JoinType.LEFT); //left outer join
		Join<LKPTransaction, CollectionTeam> team = lkp.join("team", JoinType.LEFT); //left outer join

		if (criteria == null) {
			return null;
		}
		if (criteria.getOfficeCode() != null) {
			predicates.add(cBuilder.equal(bucket.get("officeCode"), criteria.getOfficeCode()));
		}
		if (criteria.getSupervisorId() != null) {
			predicates.add(cBuilder.equal(lkp.get("supervisor").get("id"), criteria.getSupervisorId()));
		}
		if (criteria.getTeamId() != null) {
			predicates.add(cBuilder.equal(team.get("id"), criteria.getTeamId()));
		}
		if (criteria.getTeamIdList() != null) {
			Expression<Integer> teamIdExpr = team.get("id");
			Predicate teamPred = teamIdExpr.in(criteria.getTeamIdList());
    		predicates.add(teamPred);
		}
		if (criteria.getContractNo() != null) {
			if (criteria.getContractNo().contains("%")) {
				predicates.add(cBuilder.like(bucket.get("contractNo"), criteria.getContractNo()));
			} else {
				predicates.add(cBuilder.equal(bucket.get("contractNo"), criteria.getContractNo()));
			}
        }
		predicates.add(cBuilder.isNull(lkp.get("assignedTo")));
    	predicates.add(cBuilder.isNotNull(lkp.get("team")));
		predicates.add(cBuilder.isNull(lkp.get("printActivity")));
		predicates.add(cBuilder.isNull(lkp.get("lkpEntryStatus")));

		cQuery.select(cBuilder.construct(LkpTeamPageDTO.class, 
				lkp.get("id"),
				bucket,
				team.get("id"),
				team.get("name")
				));
        cQuery.orderBy(cBuilder.asc(team.get("id")), cBuilder.asc(bucket.get("contractNo")));
		cQuery.where(cBuilder.and(predicates.toArray(new Predicate[predicates.size()])));
		
		TypedQuery<LkpTeamPageDTO> query = entityManager.createQuery(cQuery);
		
		return query;
	}
    
    public TypedQuery<LkpDataDTO> lkpPrint(LkpCriteria criteria) {
        final List<Predicate> predicates = new ArrayList<Predicate>();

        CriteriaBuilder cBuilder = entityManager.getCriteriaBuilder();
		CriteriaQuery<LkpDataDTO> cQuery = cBuilder.createQuery(LkpDataDTO.class);
		Root<LKPTransaction> lkp = cQuery.from(LKPTransaction.class);
		Join<LKPTransaction, Bucket> bucket = lkp.join("bucket", JoinType.LEFT); //left outer join
		Join<Bucket, Unit> unit = bucket.join("units", JoinType.LEFT); //left outer join
		Join<LKPTransaction, PrintActivity> print = lkp.join("printActivity", JoinType.LEFT); //left outer join

		if (criteria == null) {
			return null;
		}
		if (criteria.getOfficeCode() != null) {
			predicates.add(cBuilder.equal(bucket.get("officeCode"), criteria.getOfficeCode()));
		}
		if (criteria.getSupervisorId() != null) {
			predicates.add(cBuilder.equal(lkp.get("supervisor").get("id"), criteria.getSupervisorId()));
		}
		if (criteria.getAssignedTo() != null) {
			predicates.add(cBuilder.equal(lkp.get("assignedTo").get("id"), criteria.getAssignedTo()));
		}
		if (criteria.getPrintId() != null) {
        	if (criteria.getPrintId() > 0) {
        		predicates.add(cBuilder.equal(print.get("id"), criteria.getPrintId()));
			} else if (criteria.getPrintId() == 0) {
				predicates.add(cBuilder.isNull(lkp.get("printActivity")));
			} else {
				predicates.add(cBuilder.isNotNull(lkp.get("printActivity")));
			}
		}
		if (criteria.getPrintNo() != null) {
			predicates.add(cBuilder.equal(print.get("printNo"), criteria.getPrintNo()));
		}
		if (criteria.getContractNo() != null) {
			if (criteria.getContractNo().contains("%")) {
				predicates.add(cBuilder.like(bucket.get("contractNo"), criteria.getContractNo()));
			} else {
				predicates.add(cBuilder.equal(bucket.get("contractNo"), criteria.getContractNo()));
			}
        }
		predicates.add(cBuilder.isNotNull(lkp.get("assignedTo")));
    	predicates.add(cBuilder.isNotNull(lkp.get("team")));
		predicates.add(cBuilder.isNull(lkp.get("lkpEntryStatus")));

		cQuery.select(cBuilder.construct(LkpDataDTO.class, 
        		lkp.get("id"),
        		lkp.get("team").get("id"),
        		lkp.get("supervisor").get("id"),
        		lkp.get("assignedTo").get("id"),
        		bucket,
        		unit,
        		print.get("printNo"),
        		print.get("printDate")
				));
        cQuery.orderBy(cBuilder.asc(bucket.get("contractNo")));
		cQuery.where(cBuilder.and(predicates.toArray(new Predicate[predicates.size()])));
		
		TypedQuery<LkpDataDTO> query = entityManager.createQuery(cQuery);
		
		return query;
	}
    
    public TypedQuery<LkpPageDTO> lkpPage(LkpCriteria criteria) {
        final List<Predicate> predicates = new ArrayList<Predicate>();

        CriteriaBuilder cBuilder = entityManager.getCriteriaBuilder();
		CriteriaQuery<LkpPageDTO> cQuery = cBuilder.createQuery(LkpPageDTO.class);
		Root<LKPTransaction> lkp = cQuery.from(LKPTransaction.class);
		Join<LKPTransaction, Bucket> bucket = lkp.join("bucket", JoinType.LEFT); //left outer join
		Join<LKPTransaction, CollectionTeam> team = lkp.join("team", JoinType.LEFT); //left outer join
		Join<LKPTransaction, Employee> empl = lkp.join("assignedTo", JoinType.LEFT); //left outer join

		if (criteria == null) {
			return null;
		}
		if (criteria.getOfficeCode() != null) {
			predicates.add(cBuilder.equal(bucket.get("officeCode"), criteria.getOfficeCode()));
		}
		if (criteria.getId() != null) {
			predicates.add(cBuilder.equal(lkp.get("id"), criteria.getId()));
		}
		if (criteria.getSupervisorId() != null) {
			predicates.add(cBuilder.equal(lkp.get("supervisor").get("id"), criteria.getSupervisorId()));
		}
		if (criteria.getAssignedTo() != null) {
        	if (criteria.getAssignedTo() > 0) {
    			predicates.add(cBuilder.equal(empl.get("id"), criteria.getAssignedTo()));
			} else if (criteria.getAssignedTo() == 0) {
				predicates.add(cBuilder.isNull(lkp.get("assignedTo")));
			} else {
				predicates.add(cBuilder.isNotNull(lkp.get("assignedTo")));
			}
		}
		if (criteria.getTeamId() != null) {
			predicates.add(cBuilder.equal(team.get("id"), criteria.getTeamId()));
		}
		if (criteria.getTeamIdList() != null) {
			Expression<Integer> teamIdExpr = team.get("id");
			Predicate teamPred = teamIdExpr.in(criteria.getTeamIdList());
    		predicates.add(teamPred);
		}
		if (criteria.getPrintId() != null) {
        	if (criteria.getPrintId() == 0) {
				predicates.add(cBuilder.isNull(lkp.get("printActivity")));
			} else {
				predicates.add(cBuilder.isNotNull(lkp.get("printActivity")));
			}
		}
		if (criteria.getContractNo() != null) {
			if (criteria.getContractNo().contains("%")) {
				predicates.add(cBuilder.like(bucket.get("contractNo"), criteria.getContractNo()));
			} else {
				predicates.add(cBuilder.equal(bucket.get("contractNo"), criteria.getContractNo()));
			}
        }

		if (criteria.getCodeEntryStatus() != null) {
			if (criteria.getCodeEntryStatus().equals("null")) {
				predicates.add(cBuilder.isNull(lkp.get("lkpEntryStatus")));
			} else if (criteria.getCodeEntryStatus().equals("notnull")) {
				predicates.add(cBuilder.isNotNull(lkp.get("lkpEntryStatus")));
			} else {
				predicates.add(cBuilder.equal(lkp.get("lkpEntryStatus").get("code"), criteria.getCodeEntryStatus()));
			}
		}
		if (criteria.isReclustering()) {
			Predicate entrystatusPred = cBuilder.notLike(lkp.get("lkpEntryStatus").get("code"), "NEW");
			entrystatusPred = cBuilder.or(entrystatusPred, cBuilder.isNull(lkp.get("lkpEntryStatus")));
			predicates.add(entrystatusPred);
		}
		//predicates.add(cBuilder.isNull(lkp.get("lkpEntryStatus")));

		cQuery.select(cBuilder.construct(LkpPageDTO.class, 
				lkp.get("id"),
				bucket,
				team.get("id"),
				team.get("name"),
				empl.get("id"),
				empl.get("name"),
				empl.get("regNo")
				));
        cQuery.orderBy(cBuilder.asc(bucket.get("contractNo")));
		cQuery.where(cBuilder.and(predicates.toArray(new Predicate[predicates.size()])));
		
		TypedQuery<LkpPageDTO> query = entityManager.createQuery(cQuery);
		
		return query;
	}
    
    public TypedQuery<PrintActivity> printActivity(PrintCriteria criteria) {
    	Date today = java.sql.Date.valueOf(LocalDate.now());
    	
    	final List<Predicate> predicates = new ArrayList<Predicate>();
    	
		CriteriaBuilder cBuilder = entityManager.getCriteriaBuilder();
		CriteriaQuery<PrintActivity> cQuery = cBuilder.createQuery(PrintActivity.class);
		Root<PrintActivity> root = cQuery.from(PrintActivity.class);

		if (criteria == null) {
			return null;
        }
		if (criteria.getStaffId() != null) {
			predicates.add(cBuilder.equal(root.get("employeeId"), criteria.getStaffId()));
		}
		if (criteria.getPrintId() != null) {
			predicates.add(cBuilder.equal(root.get("id"), criteria.getPrintId()));
        }
		if (criteria.getPrintNo() != null) {
			predicates.add(cBuilder.equal(root.get("printNo"), criteria.getPrintNo()));
        }
		if (criteria.getPrintType() != null) {
			predicates.add(cBuilder.equal(root.get("type"), criteria.getPrintType()));
        }
		predicates.add(cBuilder.greaterThanOrEqualTo(root.<Date>get("printDate"), today));
		cQuery.select(root).where(cBuilder.and(predicates.toArray(new Predicate[predicates.size()])));
		
		TypedQuery<PrintActivity> query = entityManager.createQuery(cQuery);
		
		return query;
	}

    public TypedQuery<LkpPrintPageDTO> lkpPrintPage(LkpCriteria criteria) {
        final List<Predicate> predicates = new ArrayList<Predicate>();

        CriteriaBuilder cBuilder = entityManager.getCriteriaBuilder();
		CriteriaQuery<LkpPrintPageDTO> cQuery = cBuilder.createQuery(LkpPrintPageDTO.class);
		Root<LKPTransaction> lkp = cQuery.from(LKPTransaction.class);
		Join<LKPTransaction, Bucket> bucket = lkp.join("bucket", JoinType.LEFT); //left outer join
		Join<LKPTransaction, CollectionTeam> team = lkp.join("team", JoinType.LEFT); //left outer join
		Join<LKPTransaction, Employee> empl = lkp.join("assignedTo", JoinType.LEFT); //left outer join
		Join<LKPTransaction, PrintActivity> print = lkp.join("printActivity", JoinType.LEFT); //left outer join

		if (criteria == null) {
			return null;
		}
		if (criteria.getOfficeCode() != null) {
			predicates.add(cBuilder.equal(bucket.get("officeCode"), criteria.getOfficeCode()));
		}
		if (criteria.getId() != null) {
			predicates.add(cBuilder.equal(lkp.get("id"), criteria.getId()));
		}
		if (criteria.getSupervisorId() != null) {
			predicates.add(cBuilder.equal(lkp.get("supervisor").get("id"), criteria.getSupervisorId()));
		}
		if (criteria.getAssignedTo() != null) {
        	if (criteria.getAssignedTo() > 0) {
    			predicates.add(cBuilder.equal(empl.get("id"), criteria.getAssignedTo()));
			} else if (criteria.getAssignedTo() == 0) {
				predicates.add(cBuilder.isNull(lkp.get("assignedTo")));
			} else {
				predicates.add(cBuilder.isNotNull(lkp.get("assignedTo")));
			}
		}
		if (criteria.getTeamId() != null) {
			predicates.add(cBuilder.equal(team.get("id"), criteria.getTeamId()));
		}
		if (criteria.getTeamIdList() != null) {
			Expression<Integer> teamIdExpr = team.get("id");
			Predicate teamPred = teamIdExpr.in(criteria.getTeamIdList());
    		predicates.add(teamPred);
		}
		if (criteria.getPrintId() != null) {
        	if (criteria.getPrintId() > 0) {
        		predicates.add(cBuilder.equal(print.get("id"), criteria.getPrintId()));
			} else if (criteria.getPrintId() == 0) {
				predicates.add(cBuilder.isNull(lkp.get("printActivity")));
			} else {
				predicates.add(cBuilder.isNotNull(lkp.get("printActivity")));
			}
		}
		if (criteria.getPrintNo() != null) {
			predicates.add(cBuilder.equal(print.get("printNo"), criteria.getPrintNo()));
		}
		if (criteria.getContractNo() != null) {
			if (criteria.getContractNo().contains("%")) {
				predicates.add(cBuilder.like(bucket.get("contractNo"), criteria.getContractNo()));
			} else {
				predicates.add(cBuilder.equal(bucket.get("contractNo"), criteria.getContractNo()));
			}
        }
		if (criteria.isReclustering()) {
			Predicate entrystatus = cBuilder.notLike(lkp.get("lkpEntryStatus").get("code"), "NEW");
			entrystatus = cBuilder.or(entrystatus, cBuilder.isNull(lkp.get("lkpEntryStatus")));
			predicates.add(entrystatus);
		} else {
			if (criteria.getCodeEntryStatus() != null) {
				predicates.add(cBuilder.equal(lkp.get("lkpEntryStatus").get("code"), criteria.getCodeEntryStatus()));
			} else {
				predicates.add(cBuilder.isNull(lkp.get("lkpEntryStatus")));
			}
		}

		cQuery.select(cBuilder.construct(LkpPrintPageDTO.class, 
				lkp.get("id"),
				bucket,
				team.get("id"),
				team.get("name"),
				empl.get("id"),
				empl.get("name"),
				empl.get("regNo"),
				print.get("id"),
				print.get("printNo"),
				print.get("printDate")
				));
        cQuery.orderBy(cBuilder.asc(bucket.get("contractNo")));
		cQuery.where(cBuilder.and(predicates.toArray(new Predicate[predicates.size()])));
		
		TypedQuery<LkpPrintPageDTO> query = entityManager.createQuery(cQuery);
		
		return query;
	}
    
    public void lkpPrinted(List<Integer> theList, Integer printId) {
    	final List<Predicate> predicates = new ArrayList<Predicate>();

    	CriteriaBuilder cBuilder = entityManager.getCriteriaBuilder();
    	CriteriaUpdate<LKPTransaction> update = cBuilder.createCriteriaUpdate(LKPTransaction.class);
    	Root<LKPTransaction> root = update.from(LKPTransaction.class);
    	Expression<Integer> parentExpression = root.get("id");
    	predicates.add(parentExpression.in(theList));
    	update.set("printActivity", printId);
    	update.where(cBuilder.and(predicates.toArray(new Predicate[predicates.size()])));
    	Query query = entityManager.createQuery(update);
    	int rowCount = query.executeUpdate();
    }

    public boolean hasMaxLoadReached(Integer assignedTo) { 
    	return (boolean) entityManager.createNativeQuery("select fnHas_MaxLoadReached(:empl)")
    			.setParameter("empl", assignedTo)
    			.getSingleResult();
    }
			
    public void lkpDelete(String contractNo) { 
		StoredProcedureQuery query = entityManager
			    .createStoredProcedureQuery("public.spLKP_Transaction_Delete")
			    .registerStoredProcedureParameter("contractNo", 
			        String.class, ParameterMode.IN)
			    .registerStoredProcedureParameter("retVal", 
	    		        Integer.class, ParameterMode.OUT)
			    .registerStoredProcedureParameter("retStr", 
	    		        String.class, ParameterMode.OUT)
			    .setParameter("contractNo", contractNo);
			 
		query.execute();
    }

	
        /* LKR */
	
		public TypedQuery<LkrCyclePageDTO> lkrRemedialAssignment(LkrCriteria criteria) {
			final List<Predicate> predicates = new ArrayList<Predicate>();
			
			CriteriaBuilder cBuilder = entityManager.getCriteriaBuilder();
			CriteriaQuery<LkrCyclePageDTO> cQuery = cBuilder.createQuery(LkrCyclePageDTO.class);
			Root<LKRTransaction> lkr = cQuery.from(LKRTransaction.class);
			Join<LKRTransaction, Bucket> bucket = lkr.join("bucket", JoinType.LEFT); //left outer join
			Join<Bucket, Cycle> cycle = bucket.join("cycle", JoinType.LEFT); //left outer join
	
			if (criteria == null) {
				return null;
			}
			if (criteria.getOfficeCode() != null) {
				predicates.add(cBuilder.equal(bucket.get("officeCode"), criteria.getOfficeCode()));
			}
			if (criteria.getContractNo() != null) {
				if (criteria.getContractNo().contains("%")) {
					predicates.add(cBuilder.like(bucket.get("contractNo"), criteria.getContractNo()));
				} else {
					predicates.add(cBuilder.equal(bucket.get("contractNo"), criteria.getContractNo()));
				}
			}
			predicates.add(cBuilder.isNull(lkr.get("assignedTo")));
			predicates.add(cBuilder.isNull(lkr.get("printActivity")));
			predicates.add(cBuilder.isNull(lkr.get("lkrEntryStatus")));
	
			cQuery.select(cBuilder.construct(LkrCyclePageDTO.class, 
					lkr.get("id"),
					bucket,
					cycle
					));
			cQuery.orderBy(cBuilder.asc(cycle.get("id")), cBuilder.asc(bucket.get("contractNo")));
			cQuery.where(cBuilder.and(predicates.toArray(new Predicate[predicates.size()])));
			
			TypedQuery<LkrCyclePageDTO> query = entityManager.createQuery(cQuery);
			
			return query;
		}
	
		public TypedQuery<LkrTeamPageDTO> lkrClustering(LkrCriteria criteria) {
			final List<Predicate> predicates = new ArrayList<Predicate>();
	
			CriteriaBuilder cBuilder = entityManager.getCriteriaBuilder();
			CriteriaQuery<LkrTeamPageDTO> cQuery = cBuilder.createQuery(LkrTeamPageDTO.class);
			Root<LKRTransaction> lkr = cQuery.from(LKRTransaction.class);
			Join<LKRTransaction, Bucket> bucket = lkr.join("bucket", JoinType.LEFT); //left outer join
			Join<LKRTransaction, RemedialTeam> team = lkr.join("team", JoinType.LEFT); //left outer join
	
			if (criteria == null) {
				return null;
			}
			if (criteria.getOfficeCode() != null) {
				predicates.add(cBuilder.equal(bucket.get("officeCode"), criteria.getOfficeCode()));
			}
			if (criteria.getSupervisorId() != null) {
				predicates.add(cBuilder.equal(lkr.get("supervisor").get("id"), criteria.getSupervisorId()));
			}
			if (criteria.getTeamId() != null) {
				predicates.add(cBuilder.equal(team.get("id"), criteria.getTeamId()));
			}
			if (criteria.getTeamIdList() != null) {
				Expression<Integer> teamIdExpr = team.get("id");
				Predicate teamPred = teamIdExpr.in(criteria.getTeamIdList());
				predicates.add(teamPred);
			}
			if (criteria.getContractNo() != null) {
				if (criteria.getContractNo().contains("%")) {
					predicates.add(cBuilder.like(bucket.get("contractNo"), criteria.getContractNo()));
				} else {
					predicates.add(cBuilder.equal(bucket.get("contractNo"), criteria.getContractNo()));
				}
			}
			predicates.add(cBuilder.isNull(lkr.get("assignedTo")));
			predicates.add(cBuilder.isNotNull(lkr.get("team")));
			predicates.add(cBuilder.isNull(lkr.get("printActivity")));
			predicates.add(cBuilder.isNull(lkr.get("lkrEntryStatus")));
	
			cQuery.select(cBuilder.construct(LkrTeamPageDTO.class, 
					lkr.get("id"),
					bucket,
					team.get("id"),
					team.get("name")
					));
			cQuery.orderBy(cBuilder.asc(team.get("id")), cBuilder.asc(bucket.get("contractNo")));
			cQuery.where(cBuilder.and(predicates.toArray(new Predicate[predicates.size()])));
			
			TypedQuery<LkrTeamPageDTO> query = entityManager.createQuery(cQuery);
			
			return query;
		}
	
		public TypedQuery<LkrDataDTO> lkrPrint(LkrCriteria criteria) {
			final List<Predicate> predicates = new ArrayList<Predicate>();
	
			CriteriaBuilder cBuilder = entityManager.getCriteriaBuilder();
			CriteriaQuery<LkrDataDTO> cQuery = cBuilder.createQuery(LkrDataDTO.class);
			Root<LKRTransaction> lkr = cQuery.from(LKRTransaction.class);
			Join<LKRTransaction, Bucket> bucket = lkr.join("bucket", JoinType.LEFT); //left outer join
			Join<Bucket, Unit> unit = bucket.join("units", JoinType.LEFT); //left outer join
			Join<LKRTransaction, PrintActivity> print = lkr.join("printActivity", JoinType.LEFT); //left outer join
	
			if (criteria == null) {
				return null;
			}
			if (criteria.getOfficeCode() != null) {
				predicates.add(cBuilder.equal(bucket.get("officeCode"), criteria.getOfficeCode()));
			}
			if (criteria.getSupervisorId() != null) {
				predicates.add(cBuilder.equal(lkr.get("supervisor").get("id"), criteria.getSupervisorId()));
			}
			if (criteria.getAssignedTo() != null) {
				predicates.add(cBuilder.equal(lkr.get("assignedTo").get("id"), criteria.getAssignedTo()));
			}
			if (criteria.getPrintId() != null) {
				if (criteria.getPrintId() > 0) {
					predicates.add(cBuilder.equal(print.get("id"), criteria.getPrintId()));
				} else if (criteria.getPrintId() == 0) {
					predicates.add(cBuilder.isNull(lkr.get("printActivity")));
				} else {
					predicates.add(cBuilder.isNotNull(lkr.get("printActivity")));
				}
			}
			if (criteria.getPrintNo() != null) {
				predicates.add(cBuilder.equal(print.get("printNo"), criteria.getPrintNo()));
			}
			if (criteria.getContractNo() != null) {
				if (criteria.getContractNo().contains("%")) {
					predicates.add(cBuilder.like(bucket.get("contractNo"), criteria.getContractNo()));
				} else {
					predicates.add(cBuilder.equal(bucket.get("contractNo"), criteria.getContractNo()));
				}
			}
			predicates.add(cBuilder.isNotNull(lkr.get("assignedTo")));
			predicates.add(cBuilder.isNotNull(lkr.get("team")));
			predicates.add(cBuilder.isNull(lkr.get("lkrEntryStatus")));
	
			cQuery.select(cBuilder.construct(LkrDataDTO.class, 
					lkr.get("id"),
					lkr.get("team").get("id"),
					lkr.get("supervisor").get("id"),
					lkr.get("assignedTo").get("id"),
					bucket,
					unit,
					print.get("printNo"),
					print.get("printDate")
					));
			cQuery.orderBy(cBuilder.asc(bucket.get("contractNo")));
			cQuery.where(cBuilder.and(predicates.toArray(new Predicate[predicates.size()])));
			
			TypedQuery<LkrDataDTO> query = entityManager.createQuery(cQuery);
			
			return query;
		}
	
		public TypedQuery<LkrPageDTO> lkrPage(LkrCriteria criteria) {
			final List<Predicate> predicates = new ArrayList<Predicate>();
	
			CriteriaBuilder cBuilder = entityManager.getCriteriaBuilder();
			CriteriaQuery<LkrPageDTO> cQuery = cBuilder.createQuery(LkrPageDTO.class);
			Root<LKRTransaction> lkr = cQuery.from(LKRTransaction.class);
			Join<LKRTransaction, Bucket> bucket = lkr.join("bucket", JoinType.LEFT); //left outer join
			Join<LKRTransaction, RemedialTeam> team = lkr.join("team", JoinType.LEFT); //left outer join
			Join<LKRTransaction, Employee> empl = lkr.join("assignedTo", JoinType.LEFT); //left outer join
	
			if (criteria == null) {
				return null;
			}
			if (criteria.getOfficeCode() != null) {
				predicates.add(cBuilder.equal(bucket.get("officeCode"), criteria.getOfficeCode()));
			}
			if (criteria.getId() != null) {
				predicates.add(cBuilder.equal(lkr.get("id"), criteria.getId()));
			}
			if (criteria.getSupervisorId() != null) {
				predicates.add(cBuilder.equal(lkr.get("supervisor").get("id"), criteria.getSupervisorId()));
			}
			if (criteria.getAssignedTo() != null) {
				if (criteria.getAssignedTo() > 0) {
					predicates.add(cBuilder.equal(empl.get("id"), criteria.getAssignedTo()));
				} else if (criteria.getAssignedTo() == 0) {
					predicates.add(cBuilder.isNull(lkr.get("assignedTo")));
				} else {
					predicates.add(cBuilder.isNotNull(lkr.get("assignedTo")));
				}
			}
			if (criteria.getTeamId() != null) {
				predicates.add(cBuilder.equal(team.get("id"), criteria.getTeamId()));
			}
			if (criteria.getTeamIdList() != null) {
				Expression<Integer> teamIdExpr = team.get("id");
				Predicate teamPred = teamIdExpr.in(criteria.getTeamIdList());
				predicates.add(teamPred);
			}
			if (criteria.getPrintId() != null) {
				if (criteria.getPrintId() == 0) {
					predicates.add(cBuilder.isNull(lkr.get("printActivity")));
				} else {
					predicates.add(cBuilder.isNotNull(lkr.get("printActivity")));
				}
			}
			if (criteria.getContractNo() != null) {
				if (criteria.getContractNo().contains("%")) {
					predicates.add(cBuilder.like(bucket.get("contractNo"), criteria.getContractNo()));
				} else {
					predicates.add(cBuilder.equal(bucket.get("contractNo"), criteria.getContractNo()));
				}
			}
	
			if (criteria.getCodeEntryStatus() != null) {
				if (criteria.getCodeEntryStatus().equals("null")) {
					predicates.add(cBuilder.isNull(lkr.get("lkrEntryStatus")));
				} else if (criteria.getCodeEntryStatus().equals("notnull")) {
					predicates.add(cBuilder.isNotNull(lkr.get("lkrEntryStatus")));
				} else {
					predicates.add(cBuilder.equal(lkr.get("lkrEntryStatus").get("code"), criteria.getCodeEntryStatus()));
				}
			}
			if (criteria.isReclustering()) {
				Predicate entrystatusPred = cBuilder.notLike(lkr.get("lkrEntryStatus").get("code"), "NEW");
				entrystatusPred = cBuilder.or(entrystatusPred, cBuilder.isNull(lkr.get("lkrEntryStatus")));
				predicates.add(entrystatusPred);
			}
			//predicates.add(cBuilder.isNull(lkr.get("lkrEntryStatus")));
	
			cQuery.select(cBuilder.construct(LkrPageDTO.class, 
					lkr.get("id"),
					bucket,
					team.get("id"),
					team.get("name"),
					empl.get("id"),
					empl.get("name"),
					empl.get("regNo")
					));
			cQuery.orderBy(cBuilder.asc(bucket.get("contractNo")));
			cQuery.where(cBuilder.and(predicates.toArray(new Predicate[predicates.size()])));
			
			TypedQuery<LkrPageDTO> query = entityManager.createQuery(cQuery);
			
			return query;
		}
	
		public TypedQuery<PrintActivity> printActivity1(PrintCriteria criteria) {
			Date today = java.sql.Date.valueOf(LocalDate.now());
			
			final List<Predicate> predicates = new ArrayList<Predicate>();
			
			CriteriaBuilder cBuilder = entityManager.getCriteriaBuilder();
			CriteriaQuery<PrintActivity> cQuery = cBuilder.createQuery(PrintActivity.class);
			Root<PrintActivity> root = cQuery.from(PrintActivity.class);
	
			if (criteria == null) {
				return null;
			}
			if (criteria.getStaffId() != null) {
				predicates.add(cBuilder.equal(root.get("employeeId"), criteria.getStaffId()));
			}
			if (criteria.getPrintId() != null) {
				predicates.add(cBuilder.equal(root.get("id"), criteria.getPrintId()));
			}
			if (criteria.getPrintNo() != null) {
				predicates.add(cBuilder.equal(root.get("printNo"), criteria.getPrintNo()));
			}
			if (criteria.getPrintType() != null) {
				predicates.add(cBuilder.equal(root.get("type"), criteria.getPrintType()));
			}
			predicates.add(cBuilder.greaterThanOrEqualTo(root.<Date>get("printDate"), today));
			cQuery.select(root).where(cBuilder.and(predicates.toArray(new Predicate[predicates.size()])));
			
			TypedQuery<PrintActivity> query = entityManager.createQuery(cQuery);
			
			return query;
		}
	
		public TypedQuery<LkrPrintPageDTO> lkrPrintPage(LkrCriteria criteria) {
			final List<Predicate> predicates = new ArrayList<Predicate>();
	
			CriteriaBuilder cBuilder = entityManager.getCriteriaBuilder();
			CriteriaQuery<LkrPrintPageDTO> cQuery = cBuilder.createQuery(LkrPrintPageDTO.class);
			Root<LKRTransaction> lkr = cQuery.from(LKRTransaction.class);
			Join<LKRTransaction, Bucket> bucket = lkr.join("bucket", JoinType.LEFT); //left outer join
			Join<LKRTransaction, RemedialTeam> team = lkr.join("team", JoinType.LEFT); //left outer join
			Join<LKRTransaction, Employee> empl = lkr.join("assignedTo", JoinType.LEFT); //left outer join
			Join<LKRTransaction, PrintActivity> print = lkr.join("printActivity", JoinType.LEFT); //left outer join
	
			if (criteria == null) {
				return null;
			}
			if (criteria.getOfficeCode() != null) {
				predicates.add(cBuilder.equal(bucket.get("officeCode"), criteria.getOfficeCode()));
			}
			if (criteria.getId() != null) {
				predicates.add(cBuilder.equal(lkr.get("id"), criteria.getId()));
			}
			if (criteria.getSupervisorId() != null) {
				predicates.add(cBuilder.equal(lkr.get("supervisor").get("id"), criteria.getSupervisorId()));
			}
			if (criteria.getAssignedTo() != null) {
				if (criteria.getAssignedTo() > 0) {
					predicates.add(cBuilder.equal(empl.get("id"), criteria.getAssignedTo()));
				} else if (criteria.getAssignedTo() == 0) {
					predicates.add(cBuilder.isNull(lkr.get("assignedTo")));
				} else {
					predicates.add(cBuilder.isNotNull(lkr.get("assignedTo")));
				}
			}
			if (criteria.getTeamId() != null) {
				predicates.add(cBuilder.equal(team.get("id"), criteria.getTeamId()));
			}
			if (criteria.getTeamIdList() != null) {
				Expression<Integer> teamIdExpr = team.get("id");
				Predicate teamPred = teamIdExpr.in(criteria.getTeamIdList());
				predicates.add(teamPred);
			}
			if (criteria.getPrintId() != null) {
				if (criteria.getPrintId() > 0) {
					predicates.add(cBuilder.equal(print.get("id"), criteria.getPrintId()));
				} else if (criteria.getPrintId() == 0) {
					predicates.add(cBuilder.isNull(lkr.get("printActivity")));
				} else {
					predicates.add(cBuilder.isNotNull(lkr.get("printActivity")));
				}
			}
			if (criteria.getPrintNo() != null) {
				predicates.add(cBuilder.equal(print.get("printNo"), criteria.getPrintNo()));
			}
			if (criteria.getContractNo() != null) {
				if (criteria.getContractNo().contains("%")) {
					predicates.add(cBuilder.like(bucket.get("contractNo"), criteria.getContractNo()));
				} else {
					predicates.add(cBuilder.equal(bucket.get("contractNo"), criteria.getContractNo()));
				}
			}
			if (criteria.isReclustering()) {
				Predicate entrystatus = cBuilder.notLike(lkr.get("lkrEntryStatus").get("code"), "NEW");
				entrystatus = cBuilder.or(entrystatus, cBuilder.isNull(lkr.get("lkrEntryStatus")));
				predicates.add(entrystatus);
			} else {
				if (criteria.getCodeEntryStatus() != null) {
					predicates.add(cBuilder.equal(lkr.get("lkrEntryStatus").get("code"), criteria.getCodeEntryStatus()));
				} else {
					predicates.add(cBuilder.isNull(lkr.get("lkrEntryStatus")));
				}
			}
	
			cQuery.select(cBuilder.construct(LkrPrintPageDTO.class, 
					lkr.get("id"),
					bucket,
					team.get("id"),
					team.get("name"),
					empl.get("id"),
					empl.get("name"),
					empl.get("regNo"),
					print.get("id"),
					print.get("printNo"),
					print.get("printDate")
					));
			cQuery.orderBy(cBuilder.asc(bucket.get("contractNo")));
			cQuery.where(cBuilder.and(predicates.toArray(new Predicate[predicates.size()])));
			
			TypedQuery<LkrPrintPageDTO> query = entityManager.createQuery(cQuery);
			
			return query;
		}
		
		public void lkrPrinted(List<Integer> theList, Integer printId) {
			final List<Predicate> predicates = new ArrayList<Predicate>();
	
			CriteriaBuilder cBuilder = entityManager.getCriteriaBuilder();
			CriteriaUpdate<LKRTransaction> update = cBuilder.createCriteriaUpdate(LKRTransaction.class);
			Root<LKRTransaction> root = update.from(LKRTransaction.class);
			Expression<Integer> parentExpression = root.get("id");
			predicates.add(parentExpression.in(theList));
			update.set("printActivity", printId);
			update.where(cBuilder.and(predicates.toArray(new Predicate[predicates.size()])));
			Query query = entityManager.createQuery(update);
			int rowCount = query.executeUpdate();
		}
	
		public boolean hasMaxLoadReached1(Integer assignedTo) { 
			return (boolean) entityManager.createNativeQuery("select fnHas_MaxLoadReached(:empl)")
					.setParameter("empl", assignedTo)
					.getSingleResult();
		}
				
		public void lkrDelete(String contractNo) { 
			StoredProcedureQuery query = entityManager
					.createStoredProcedureQuery("public.spLKP_Transaction_Delete")
					.registerStoredProcedureParameter("contractNo", 
						String.class, ParameterMode.IN)
					.registerStoredProcedureParameter("retVal", 
							Integer.class, ParameterMode.OUT)
					.registerStoredProcedureParameter("retStr", 
							String.class, ParameterMode.OUT)
					.setParameter("contractNo", contractNo);
				 
			query.execute();
		}
    
    /* LKD */
	
    public TypedQuery<LkdDataDTO> lkd(LkdCriteria criteria) {
        CustomUserDetails user = (CustomUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        final List<Predicate> predicates = new ArrayList<Predicate>();
		CriteriaBuilder cBuilder = entityManager.getCriteriaBuilder();
		CriteriaQuery<LkdDataDTO> cQuery = cBuilder.createQuery(LkdDataDTO.class);
		Root<LKDTransaction> root = cQuery.from(LKDTransaction.class);
		Join<LKDTransaction, Bucket> b = root.join("bucket", JoinType.LEFT); //left outer join

		if (criteria == null) {
			return null;
		}
		predicates.add(cBuilder.equal(b.get("officeCode"), user.getOfficeCode()));
		if (criteria.getId() != null) {
			predicates.add(cBuilder.equal(root.get("id"), criteria.getId()));
		}
		if (criteria.getTeamId() != null) {
			predicates.add(cBuilder.equal(root.get("teamId"), criteria.getTeamId()));
		}
		if (criteria.getSupervisorId() != null) {
			predicates.add(cBuilder.equal(root.get("supervisorId"), criteria.getSupervisorId()));
		}
		if (criteria.getContractNo() != null) {
			predicates.add(cBuilder.equal(cBuilder.lower(b.get("contractNo")), criteria.getContractNo().toLowerCase()));
        }
		if (criteria.getPrintId() == null && criteria.getPrintNo() == null) {
			predicates.add(cBuilder.isNull(root.get("printActivity")));
        }
		if (criteria.getPrintId() != null) {
			predicates.add(cBuilder.equal(root.get("printActivity").get("id"), criteria.getPrintId()));
		}
		if (criteria.getPrintNo() != null) {
			predicates.add(cBuilder.equal(root.get("printActivity").get("printNo"), criteria.getPrintNo()));
		}
		if (criteria.getCodeEntryStatus() != null) {
			predicates.add(cBuilder.equal(root.get("lkdEntryStatus").get("code"), criteria.getCodeEntryStatus()));
		}

		cQuery.select(cBuilder.construct(LkdDataDTO.class, 
        		root.get("id"),
        		root.get("teamId"),
        		root.get("supervisorId"),
        		b.get("contractNo"),
        		b.get("custName"),
        		b.get("custBillingAddress"),
        		b.get("phone1"),
        		b.get("phone2"),
        		b.get("leastInstallmentNo"),
        		b.get("lastInstallmentNo"),
        		b.get("topInstallment"),
        		b.get("leastDueDate"),
        		b.get("lastDueDate"),
        		b.get("overdue"),
        		b.get("outstandingPrincipalAmnt"),
        		b.get("outstandingInterestAmnt"),
        		b.get("monthlyInstallment"),
        		b.get("businessUnit")));
		cQuery.where(cBuilder.and(predicates.toArray(new Predicate[predicates.size()])));
		
		TypedQuery<LkdDataDTO> query = entityManager.createQuery(cQuery);
		
		return query;
	}

    public void lkdPrinted(List<Integer> theList, Integer printId) {
    	final List<Predicate> predicates = new ArrayList<Predicate>();

    	CriteriaBuilder cBuilder = entityManager.getCriteriaBuilder();
    	CriteriaUpdate<LKDTransaction> update = cBuilder.createCriteriaUpdate(LKDTransaction.class);
    	Root<LKDTransaction> root = update.from(LKDTransaction.class);
    	Expression<Integer> parentExpression = root.get("id");
    	predicates.add(parentExpression.in(theList));
    	update.set("printActivity", printId);
    	update.where(cBuilder.and(predicates.toArray(new Predicate[predicates.size()])));
    	Query query = entityManager.createQuery(update);
    	int rowCount = query.executeUpdate();
    }


    /* SKPC */

    public TypedQuery<SkpcPageDTO> skpc(SkpcCriteria criteria) {
    	final List<Predicate> predicates = new ArrayList<Predicate>();

    	CriteriaBuilder cBuilder = entityManager.getCriteriaBuilder();
		CriteriaQuery<SkpcPageDTO> cQuery = cBuilder.createQuery(SkpcPageDTO.class);
		Root<SKPCTransaction> skpc = cQuery.from(SKPCTransaction.class);
		Join<SKPCTransaction, Bucket> bucket = skpc.join("bucket", JoinType.LEFT); //left outer join
		Join<Bucket, Unit> unit = bucket.join("units", JoinType.LEFT); //left outer join
		Join<SKPCTransaction, Status> status = skpc.join("status", JoinType.LEFT); //left outer join
		Join<SKPCTransaction, SKPCTransactionDetail> detail = skpc.join("skpcTransDetails", JoinType.LEFT); //left outer join
		Join<SKPCTransaction, PrintActivity> print = skpc.join("printActivity", JoinType.LEFT); //left outer join

    	Predicate seqPred = cBuilder.isNull(skpc.get("sequenceNo"));
    	seqPred = cBuilder.or(seqPred, cBuilder.equal(skpc.get("sequenceNo"), detail.get("sequenceNo")));
    	predicates.add(seqPred);

		if (criteria.getOfficeCode() != null) {
			predicates.add(cBuilder.equal(bucket.get("officeCode"), criteria.getOfficeCode()));
		}

		if (criteria.getId() != null) {
	        predicates.add(cBuilder.equal(skpc.get("id"), criteria.getId()));
        }

		Integer type = (criteria.getType() == null? 2 : criteria.getType());

        if (type == 0) { // NEW
        	predicates.add(cBuilder.isNull(skpc.get("assignedTo")));
        }
        else if (type == 1) { // EXTEND
    		if (criteria.getStaffId() != null) {
    			predicates.add(cBuilder.equal(skpc.get("assignedTo"), criteria.getStaffId()));
    		}
        }
        else { // BOTH
            Predicate pred = cBuilder.isNull(skpc.get("assignedTo"));
    		if (criteria.getStaffId() != null) {
    			pred = cBuilder.or(pred, cBuilder.equal(skpc.get("assignedTo"), criteria.getStaffId()));
            	predicates.add(pred);
    		}
        }
        
        if (criteria.getPrintNo() != null) {
	        predicates.add(cBuilder.equal(print.get("printNo"), criteria.getPrintNo()));
        }
        
        cQuery.select(cBuilder.construct(SkpcPageDTO.class, 
        		skpc.get("id"),
        		skpc.get("assignedTo"),
        		bucket.get("contractNo"),
        		bucket.get("custName"),
        		bucket.get("custBillingAddress"),
        		bucket.get("phone1"),
        		bucket.get("phone2"),
        		bucket.get("topInstallment"),
        		bucket.get("outstandingPrincipalAmnt"),
        		bucket.get("outstandingInterestAmnt"),
        		bucket.get("monthlyInstallment"),
        		bucket.get("leastDueDate"),
        		bucket.get("leastInstallmentNo"),
        		bucket.get("lastDueDate"),
        		bucket.get("lastInstallmentNo"),
        		bucket.get("overdue"),
        		bucket.get("businessUnit"),
        		unit.get("brand"),
        		unit.get("model"),
        		unit.get("color"),
        		unit.get("frameNo"),
        		unit.get("engineNo"),
        		unit.get("policeNo"),
        		unit.get("stnk"),
        	    skpc.get("result"),
        	    status.get("id"),
        	    status.get("description"),
        	    skpc.get("mouNo"),
        	    detail.get("sequenceNo"),
        	    print.get("printDate"),
        	    detail.get("startDate"),
        	    detail.get("endDate"),
        	    print.get("id"),
        	    print.get("printNo")
				));

        cQuery.where(cBuilder.and(predicates.toArray(new Predicate[predicates.size()])));

		TypedQuery<SkpcPageDTO> query = entityManager.createQuery(cQuery);
		
		return query;
    }

    public boolean hasPCMaxLoadReached(Integer profcollId) { 
    	return (boolean) entityManager.createNativeQuery("select fnHas_PCMaxLoadReached(:pc)")
    			.setParameter("pc", profcollId)
    			.getSingleResult();
    }
			

    /* CHANGE SUPERVISOR */
	
}
