package com.collection.jpa.service;

import org.springframework.data.domain.Pageable;

import com.collection.jpa.dto.ContractCriteria;
import com.collection.jpa.entity.LKPHistory;
import com.collection.jpa.util.Page;


public interface LKPHistoryService {

	LKPHistory getOne(Integer id);
	
	Page<LKPHistory> pageByContract(ContractCriteria criteria, Pageable pageable);

}
