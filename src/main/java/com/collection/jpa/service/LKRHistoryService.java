package com.collection.jpa.service;

import org.springframework.data.domain.Pageable;

import com.collection.jpa.dto.ContractCriteria;
import com.collection.jpa.entity.LKRHistory;
import com.collection.jpa.util.Page;


public interface LKRHistoryService {

	LKRHistory getOne(Integer id);
	
	Page<LKRHistory> pageByContract(ContractCriteria criteria, Pageable pageable);

}
