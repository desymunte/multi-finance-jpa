package com.collection.jpa.service;

import java.util.List;

import com.collection.jpa.entity.CycleType;


public interface CycleTypeService {

	List<CycleType> listCycleType();

}
