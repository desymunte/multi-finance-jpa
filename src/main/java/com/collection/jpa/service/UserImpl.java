package com.collection.jpa.service;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.collection.jpa.dao.UserRepository;
import com.collection.jpa.dto.UserPageDTO;
import com.collection.jpa.entity.Employee;
import com.collection.jpa.entity.User;
import com.collection.jpa.util.Page;
import com.collection.jpa.util.ServiceResult;


@Repository
@Transactional(readOnly = true)
public class UserImpl implements UserService {
	
	@PersistenceContext
	private EntityManager entityManager;
	
	@Autowired
	private UserRepository repository;

//	@Override
//	public UserDetails findByUsername(String username) {
//		return (UserDetails) repository.findByUsername(username);
//	}

//	@Override
//	public org.springframework.security.core.userdetails.User findByUsername(String username) {
//		return repository.findByUsername(username);
//	}
	
	@PreAuthorize("hasRole('SUPERADMIN')")
	public Page<UserPageDTO> page(Pageable pageable) {
		final List<Predicate> predicates = new ArrayList<Predicate>();

		CriteriaBuilder cBuilder = entityManager.getCriteriaBuilder();
		CriteriaQuery<UserPageDTO> cQuery = cBuilder.createQuery(UserPageDTO.class);
		Root<User> root = cQuery.from(User.class);
//		Join<User, Role> role = root.join("roles", JoinType.LEFT); //left outer join
//		role.alias("ra");
		Join<User, Employee> e = root.join("employee", JoinType.LEFT); //left outer join
		
//		query.multiselect(
//		        cb.count(dogsJoin.get("id")).alias("numDogs"),
//		        person.alias("person")
//		).groupBy(person);
		
//		query.select(
//			    cb.construct(CountableEntryDto.class,
//			                 person,
//			                 cb.count(dogsJoin.get("id")))
//			).groupBy(person);

        cQuery.select(cBuilder.construct(UserPageDTO.class, 
        		root,
        		e.get("id"),
        		e.get("regNo"),
        		e.get("name")
				));

        cQuery.where(cBuilder.and(predicates.toArray(new Predicate[predicates.size()])));
        
//        cQuery.groupBy(root.get("id"), e.get("id"), role);

		TypedQuery<UserPageDTO> query = entityManager.createQuery(cQuery);
		
		return (Page<UserPageDTO>) new Page<UserPageDTO>(query, pageable);
	}

	@Transactional
	public ServiceResult create(User persisted) {
		User user = new User();
		user.setEmployee(persisted.getEmployee());
		user.setUsername(persisted.getUsername());
		user.setPassword(persisted.getPassword());
		user.setRoles(persisted.getRoles());
		System.out.println(user);
		repository.save(user);
		return new ServiceResult(ServiceResult.MessageType.SUCCESS, "User has been created", user.getId());
	}
	
	@Transactional
	public ServiceResult update(User persisted) {
		User user = repository.findOne(persisted.getId());
		user.setEmployee(persisted.getEmployee());
		user.setUsername(persisted.getUsername());
		user.setPassword(persisted.getPassword());
		user.setRoles(persisted.getRoles());
		System.out.println(user);
		repository.save(user);
		return new ServiceResult(ServiceResult.MessageType.SUCCESS, "User has been updated", user.getId());
	}
	
	@Transactional
	public ServiceResult delete(User persisted) {
		repository.delete(persisted);
		return new ServiceResult(ServiceResult.MessageType.SUCCESS, "User has been deleted", persisted.getId());
	}

}
