package com.collection.jpa.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.TypedQuery;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.collection.jpa.CustomUserDetails;
import com.collection.jpa.dao.LKDEntryStatusRepository;
import com.collection.jpa.dao.LKDTransactionRepository;
import com.collection.jpa.dao.PrintActivityRepository;
import com.collection.jpa.dto.BucketDetailDTO;
import com.collection.jpa.dto.LkdCriteria;
import com.collection.jpa.dto.LkdDataDTO;
import com.collection.jpa.dto.LkdPrintDTO;
import com.collection.jpa.dto.TeamCriteria;
import com.collection.jpa.dto.TeamDTO;
import com.collection.jpa.dto.LkdHeaderDTO;
import com.collection.jpa.entity.LKDEntryStatus;
import com.collection.jpa.entity.LKDTransaction;
import com.collection.jpa.entity.PrintActivity;
import com.collection.jpa.util.FPrint;
import com.collection.jpa.util.ServiceResult;


@Repository
@Transactional(readOnly = true)
public class LKDTransactionImpl implements LKDTransactionService {
	
	Date currentDate = new Date();

	@Autowired
	private LKDTransactionRepository repository;
	
	@Autowired
	private LKDEntryStatusRepository statusrepository;
	
	@Autowired
	private PrintActivityRepository printrepository;

	@Autowired
	private QueryImpl queryService;
	
	@Autowired
	private FPrint fprint;


    private List<LkdPrintDTO> getPrintList(List<LkdDataDTO> printList) {
		List<LkdPrintDTO> lkdList = new ArrayList<LkdPrintDTO>();

		for (LkdDataDTO m: printList) {
			LkdPrintDTO lkd = new LkdPrintDTO();
			lkd.setId(m.getId());
			lkd.setTeamId(m.getTeamId());
			lkd.setSupervisorId(m.getSupervisorId());
			lkd.setContractNo(m.getContractNo());
			lkd.setCustName(m.getCustName());
			lkd.setCustBillingAddress(m.getCustBillingAddress());
			lkd.setPhone1(m.getPhone1());
			lkd.setPhone2(m.getPhone2());
			lkd.setLeastInstallmentNo(m.getLeastInstallmentNo());
			lkd.setLastInstallmentNo(m.getLastInstallmentNo());
			lkd.setTopInstallment(m.getTopInstallment());
			lkd.setLeastDueDate(m.getLeastDueDate());
			lkd.setLastDueDate(m.getLastDueDate());
			lkd.setOverdue(m.getOverdue());
			lkd.setOutstandingPrincipalAmnt(m.getOutstandingPrincipalAmnt());
			lkd.setOutstandingInterestAmnt(m.getOutstandingInterestAmnt());
			lkd.setMonthlyInstallment(m.getMonthlyInstallment());
			lkd.setBusinessUnit(m.getBusinessUnit());
			List<BucketDetailDTO> theDetails = queryService.bucketDetail(m.getContractNo(), m.getLeastInstallmentNo(), m.getLastInstallmentNo()).getResultList();
			lkd.setDetails(theDetails);

			lkdList.add(lkd);
		}

		return lkdList;
	}

    @PreAuthorize("hasRole('ROLE_DESKCALL_SPV')")
	public List<LkdPrintDTO> listPrint() {
        CustomUserDetails user = (CustomUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        if (!user.getJobCode().equals("DESKSPV")) {
        	return null;
    	}

        TeamCriteria criteria = new TeamCriteria();
        criteria.setSpvId(user.getEmplId());
        TeamDTO team = new TeamDTO();
        TypedQuery<TeamDTO> teamQuery = queryService.deskcallTeam(criteria);
		try {
			team = teamQuery.getSingleResult();
		} catch (Exception excp) {
    		return null;
		}

		if (team.getId() == null) {
    		return null;
    	}

    	LkdCriteria lkdcriteria = new LkdCriteria();
    	lkdcriteria.setTeamId(team.getId());

        TypedQuery<LkdDataDTO> query = queryService.lkd(lkdcriteria);
		if (query == null) {
			return null;
        }

        List<LkdDataDTO> printList = query.getResultList();
		return getPrintList(printList);
    }

    @PreAuthorize("hasRole('ROLE_DESKCALL_SPV')")
	public LkdHeaderDTO headerPrint() {
        CustomUserDetails user = (CustomUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        if (!user.getJobCode().equals("DESKSPV")) {
        	return null;
    	}

        TeamCriteria criteria = new TeamCriteria();
        criteria.setSpvId(user.getEmplId());
        TeamDTO team = new TeamDTO();
        TypedQuery<TeamDTO> teamQuery = queryService.deskcallTeam(criteria);
		try {
			team = teamQuery.getSingleResult();
		} catch (Exception excp) {
    		return null;
		}

		if (team.getId() == null) {
    		return null;
    	}

        String printNo = fprint.getNextPrintNo("LKD", String.format("%05d", team.getId()));

        LkdHeaderDTO header = new LkdHeaderDTO();
		header.setPrintNo(printNo);
		header.setPrintDate(currentDate);
		header.setTeamId(team.getId());
		header.setTeamName(team.getName());

		return header;
    }

	@Transactional
	@Modifying(clearAutomatically = true)
    @PreAuthorize("hasRole('ROLE_DESKCALL_SPV')")
	public ServiceResult setPrinted(LkdHeaderDTO header, List<LkdPrintDTO> listPersisted) {
        if (listPersisted.size() == 0) {
        	return new ServiceResult(ServiceResult.MessageType.INFO, "No LKD to print", null);
        }

		PrintActivity print = new PrintActivity();
		print.setTeamId(header.getTeamId());
        print.setPrintDate(header.getPrintDate());
        print.setPrintNo(header.getPrintNo());
        print.setType("D");

        try {
            PrintActivity result = printrepository.save(print);
            Integer printId = result.getId();
            
            List<Integer> theList = new ArrayList<Integer>();
            for (LkdPrintDTO m: listPersisted) {
                theList.add(m.getId());
            }
            
            queryService.lkdPrinted(theList, printId);
            
		} catch (Exception excp) {
	        return new ServiceResult(ServiceResult.MessageType.ERROR, "Failed to update print activity", null);
		}

        return new ServiceResult(ServiceResult.MessageType.SUCCESS, "Print activity successfully updated", null);
	}
	
	@PreAuthorize("hasRole('ROLE_DESKCALL_ADMIN')")
	public LkdDataDTO contract(LkdCriteria criteria) {
		LkdDataDTO result = new LkdDataDTO();
		if (criteria == null || criteria.getContractNo() == null) {
			return result;
        }
		criteria.setId(null);
		criteria.setTeamId(null);
		criteria.setSupervisorId(null);
		criteria.setPrintId(null);
		criteria.setPrintNo(null);
		criteria.setCodeEntryStatus(null);
		TypedQuery<LkdDataDTO> query = queryService.lkd(criteria);

		try {
			result = query.getSingleResult();
		} catch (Exception excp) {
		}
		
		return result;
	}

    @Transactional
	@Modifying(clearAutomatically = true)
    @PreAuthorize("hasRole('ROLE_DESKCALL_ADMIN')")
	public ServiceResult entry(LKDTransaction persisted) {
    	if (persisted == null || persisted.getId() == null) {
    		return new ServiceResult(ServiceResult.MessageType.ERROR, "LKD not found", null);
    	}

    	LKDTransaction old = repository.findOne(persisted.getId());
    	if (old == null) {
    		return new ServiceResult(ServiceResult.MessageType.ERROR, "LKD not found", null);
    	}

    	if (old.getPrintActivity() == null) {
    		return new ServiceResult(ServiceResult.MessageType.ERROR, "LKD has not been printed yet", old.getId());
    	}

    	if (old.getLkdEntryStatus() != null) {
    		return new ServiceResult(ServiceResult.MessageType.ERROR, "LKD already done", old.getId());
    	}
    	
    	old.setLkdEntryStatus(persisted.getLkdEntryStatus());

    	try {
    		repository.save(old);
		} catch (Exception excp) {
    		return new ServiceResult(ServiceResult.MessageType.ERROR, "LKD result entry failed", null);
		}
    	return new ServiceResult(ServiceResult.MessageType.SUCCESS, "LKD result entry succeed", old.getId());
	}

	public List<LKDEntryStatus> listStatus() {
		return statusrepository.findAll();
	}

}
