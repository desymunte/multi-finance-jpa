package com.collection.jpa.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.collection.jpa.dao.LKPEntryStatusRepository;
import com.collection.jpa.entity.LKPEntryStatus;


@Repository
@Transactional(readOnly = true)
public class LKPEntryStatusImpl implements LKPEntryStatusService {
	
	@Autowired
	private LKPEntryStatusRepository repository;
	
	
	public List<LKPEntryStatus> listStatus() {
		return repository.findAll();
	}

}
