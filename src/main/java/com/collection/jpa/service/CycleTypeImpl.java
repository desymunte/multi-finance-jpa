package com.collection.jpa.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.collection.jpa.dao.CycleTypeRepository;
import com.collection.jpa.entity.CycleType;


@Repository
@Transactional(readOnly = true)
public class CycleTypeImpl implements CycleTypeService {
	
	@Autowired
	private CycleTypeRepository repository;
	
	
	public List<CycleType> listCycleType() {
		return repository.findAll();
	}

}
