package com.collection.jpa.service;

import java.util.List;

import com.collection.jpa.dto.CollectionWorkingAreaCriteria;
import com.collection.jpa.entity.CollectionWorkingArea;
import com.collection.jpa.util.ServiceResult;


public interface CollectionWorkingAreaService {

	List<CollectionWorkingArea> listOccupied(CollectionWorkingAreaCriteria criteria);
	
	//List<Zipcode> listAvailable(ZipcodeAvailableCriteria criteria);
	
	ServiceResult insert(CollectionWorkingArea persisted);
	
	ServiceResult delete(Integer id);

}
