package com.collection.jpa.service;

import java.util.List;

import com.collection.jpa.entity.Role;

public interface RoleService {
	
	List<Role> listRole();

}
