package com.collection.jpa.service;

import java.util.List;

import com.collection.jpa.entity.ProfCollMember;


public interface ProfCollMemberService {

	List<ProfCollMember> findAll();

	ProfCollMember insert(ProfCollMember persisted);
	
//	ProfCollMember save(ProfCollMember persisted);

}
