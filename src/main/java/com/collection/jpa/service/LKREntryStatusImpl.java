package com.collection.jpa.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.collection.jpa.dao.LKREntryStatusRepository;
import com.collection.jpa.entity.LKREntryStatus;


@Repository
@Transactional(readOnly = true)
public class LKREntryStatusImpl implements LKREntryStatusService {
	
	@Autowired
	private LKREntryStatusRepository repository;
	
	
	public List<LKREntryStatus> listStatus() {
		return repository.findAll();
	}

}
