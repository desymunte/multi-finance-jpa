package com.collection.jpa.service;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.collection.jpa.dao.RemedialWorkingAreaRepository;
import com.collection.jpa.entity.RemedialWorkingArea;


@Repository
@Transactional(readOnly = true)
public class RemedialWorkingAreaImpl implements RemedialWorkingAreaService {
	
	@PersistenceContext
	private EntityManager entityManager;
	
	@Autowired
	private RemedialWorkingAreaRepository repository;
	
	@Override
	public List<RemedialWorkingArea> findAll() {
		return repository.findAll();
	}
	
	@Override
    @Transactional
	public RemedialWorkingArea insert(RemedialWorkingArea persisted) {
		return repository.save(persisted);
	}
	
    @Transactional
	@Modifying(clearAutomatically = true)
	public RemedialWorkingArea update(RemedialWorkingArea persisted) {
		return repository.save(persisted);
	}

    @Transactional
	public void delete(Integer id) {
		// TODO Auto-generated method stub
//		Long delId = Long.parseLong(id.toString());
		repository.delete(id);
	}
    
    @Transactional
	public void deleteList(Integer empId) {
		repository.deleteList(empId);
	}
	
//    @Transactional
//	@Modifying
//	public void delete(RemedialWorkingArea persisted) {
//		// TODO Auto-generated method stub
//		repository.delete(persisted);
//	}
}
