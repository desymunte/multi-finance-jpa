package com.collection.jpa.service;

import java.util.Iterator;
import java.util.List;

import javax.persistence.TypedQuery;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.collection.jpa.CustomUserDetails;
import com.collection.jpa.dao.CollectionTeamRepository;
import com.collection.jpa.dao.CycleRepository;
import com.collection.jpa.dao.EmployeeRepository;
import com.collection.jpa.dao.OfficeRepository;
import com.collection.jpa.dao.ZipcodeRepository;
import com.collection.jpa.dto.AreaCriteria;
import com.collection.jpa.dto.ChangeSpvParamDTO;
import com.collection.jpa.dto.MemberCriteria;
import com.collection.jpa.dto.CollectionMemberDTO;
import com.collection.jpa.dto.TeamCriteria;
import com.collection.jpa.dto.TeamDTO;
import com.collection.jpa.dto.ZipDTO;
import com.collection.jpa.entity.CollectionMember;
import com.collection.jpa.entity.CollectionTeam;
import com.collection.jpa.entity.CollectionWorkingArea;
import com.collection.jpa.entity.Cycle;
import com.collection.jpa.entity.Employee;
import com.collection.jpa.entity.Zipcode;
import com.collection.jpa.util.InList;
import com.collection.jpa.util.Page;
import com.collection.jpa.util.ServiceResult;


@Repository
@Transactional(readOnly = true)
public class CollectionTeamImpl implements CollectionTeamService {
	
	@Autowired
	private CollectionTeamRepository repository; 
	
	@Autowired
	private EmployeeRepository emplrepository; 
	
	@Autowired
	private OfficeRepository offrepository; 
	
	@Autowired
	private CycleRepository cyclerepository; 
	
	@Autowired
	private ZipcodeRepository ziprepository; 
	
	@Autowired
	private QueryImpl queryService;


	public CollectionTeam findOne(Integer id) {
		return repository.findOne(id);
	}
	
	public InList<TeamDTO> asSupervisor() {
        CustomUserDetails user = (CustomUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        TeamCriteria newcriteria = new TeamCriteria();
        newcriteria.setSpvId(user.getEmplId());

    	TypedQuery<TeamDTO> query = queryService.collectionTeam(newcriteria);
		List<TeamDTO> list = query.getResultList();
		boolean exist = !list.isEmpty();

		return new InList<TeamDTO>(list, exist);
	}
	
	private InList<TeamDTO> asSupervisor(Employee criteria) {
        TeamCriteria newcriteria = new TeamCriteria();
        newcriteria.setSpvId(criteria.getId());

    	TypedQuery<TeamDTO> query = queryService.collectionTeam(newcriteria);
		List<TeamDTO> list = query.getResultList();
		boolean exist = !list.isEmpty();

		return new InList<TeamDTO>(list, exist);
	}
	
	private InList<CollectionMemberDTO> asMember(Employee criteria) {
        MemberCriteria newcriteria = new MemberCriteria();
        newcriteria.setEmplId(criteria.getId());

    	TypedQuery<CollectionMemberDTO> query = queryService.collectionMember(newcriteria);
		List<CollectionMemberDTO> list = query.getResultList();
		boolean exist = !list.isEmpty();

		return new InList<CollectionMemberDTO>(list, exist);
	}

	private InList<ZipDTO> asOccupiedArea (AreaCriteria criteria) {
    	TypedQuery<ZipDTO> query = queryService.collectionArea(criteria);
		List<ZipDTO> list = query.getResultList();
		boolean exist = !list.isEmpty();

		return new InList<ZipDTO>(list, exist);
	}

    @PreAuthorize("hasRole('ROLE_SUPERADMIN') or hasRole('ROLE_COLLECTION_HEAD') or hasRole('ROLE_COLLECTION_SPV')")
	public List<TeamDTO> list() {
        CustomUserDetails user = (CustomUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        
        TeamCriteria criteria = new TeamCriteria();  
    	criteria.setOfficeId(user.getOfficeId());

    	TypedQuery<TeamDTO> query = queryService.collectionTeam(criteria);
		return query.getResultList();
	}
	
	@PreAuthorize("hasRole('ROLE_SUPERADMIN') or hasRole('ROLE_COLLECTION_HEAD') or hasRole('ROLE_COLLECTION_SPV')")
	public Page<TeamDTO> page(TeamCriteria criteria, Pageable pageable) {
        CustomUserDetails user = (CustomUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

    	criteria.setOfficeId(user.getOfficeId());
    	if (user.getJobCode() == "COLLSPV") {
    		criteria.setSpvId(user.getEmplId());
    	}

    	TypedQuery<TeamDTO> query = queryService.collectionTeam(criteria);
		return (Page<TeamDTO>) new Page<TeamDTO>(query, pageable);
	}
	
    @PreAuthorize("hasRole('ROLE_SUPERADMIN') or hasRole('ROLE_COLLECTION_HEAD') or hasRole('ROLE_COLLECTION_SPV')")
	public TeamDTO content(TeamCriteria criteria) {
        CustomUserDetails user = (CustomUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

		TeamDTO result = new TeamDTO();
    	if (criteria == null || criteria.getId() == null) {
    		return result;
    	}
    	criteria.setOfficeId(user.getOfficeId());

    	if (user.getJobCode() == "COLLSPV") {
    		criteria.setSpvId(user.getEmplId());
    	}

    	TypedQuery<TeamDTO> query = queryService.collectionTeam(criteria);

		try {
			result = query.getSingleResult();
		} catch (Exception excp) {
		}
		return result;
	}
	
    @Transactional
	@PreAuthorize("hasRole('ROLE_COLLECTION_HEAD')")
    public ServiceResult changeSpv(ChangeSpvParamDTO data) {
        CustomUserDetails user = (CustomUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        if (!(user.getJobCode().equals("COLLHD"))) {
			return new ServiceResult(ServiceResult.MessageType.ERROR, "Not authorized user", null);
        }
        
        if (data == null || data.getTeamId() == null || data.getSpvId() == null) {
    		return new ServiceResult(ServiceResult.MessageType.ERROR, "Data for Supervisor changing not complete", null);
        }
        TeamCriteria newcriteria = new TeamCriteria();
    	newcriteria.setOfficeId(user.getOfficeId());
       	newcriteria.setId(data.getTeamId());
    	
    	CollectionTeam team = repository.findOne(data.getTeamId());
    	if (team == null) {
    		return new ServiceResult(ServiceResult.MessageType.ERROR, "Collection Team not found", null);
    	}
    	
    	TypedQuery<TeamDTO> query = queryService.collectionTeam(newcriteria);
    	if (query == null) {
			return new ServiceResult(ServiceResult.MessageType.ERROR, "Not authorized user", null);
    	}

    	Employee empl =  emplrepository.findOne(data.getSpvId());
    	if (empl == null) {
    		return new ServiceResult(ServiceResult.MessageType.ERROR, "Employee not found", null);
    	}
    	
		InList<TeamDTO> spv = this.asSupervisor(empl);
		if (spv.exists()) {
    		return new ServiceResult(ServiceResult.MessageType.ERROR, "Supervisor already assigned", spv);
		}
    	
		team.setSupervisor(empl);
		try {
			repository.save(team);
		} catch (Exception excp) {
    		return new ServiceResult(ServiceResult.MessageType.ERROR, "Failed to change supervisor", null);
		}
		return new ServiceResult(ServiceResult.MessageType.SUCCESS, "Supervisor changes successfully", team.getId());
    }

	@Transactional
    @PreAuthorize("hasRole('ROLE_COLLECTION_HEAD')")
    public ServiceResult setup(CollectionTeam persisted) {
        CustomUserDetails user = (CustomUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        if (!(user.getJobCode().equals("COLLHD"))) {
			return new ServiceResult(ServiceResult.MessageType.ERROR, "Not authorized user", null);
        }
        
        if (persisted == null || 
        	persisted.getName() == null || 
        	persisted.getCycle() == null || persisted.getCycle().getId() == null || 
        	persisted.getSupervisor() == null | persisted.getSupervisor().getId() == null) {
    		return new ServiceResult(ServiceResult.MessageType.ERROR, "Collection Team data not completely supplied", null);
        }

        persisted.setId(null);
        persisted.setOffice(offrepository.findOne(user.getOfficeId()));
        
		Cycle cycle = cyclerepository.findOne(persisted.getCycle().getId());
		if (cycle == null) {
			return new ServiceResult(ServiceResult.MessageType.ERROR, "Cycle is not recognized", persisted.getCycle().getId());
		}
        if (!cycle.getType().getType().equals("C")) {
			return new ServiceResult(ServiceResult.MessageType.ERROR, "Not a proper cycle type for Collection Team", cycle.getType().getType());
		}

        Employee supervisor = emplrepository.findOne(persisted.getSupervisor().getId());
		if (supervisor == null) {
			return new ServiceResult(ServiceResult.MessageType.ERROR, "Supervisor is not recognized", persisted.getSupervisor().getId());
		}
		if (!supervisor.getJobTitle().getCode().equals("COLLSPV")) {
			return new ServiceResult(ServiceResult.MessageType.ERROR, "Assigned Staff as Supervisor is not valid, the job title is not a Collection Supervisor", supervisor.getJobTitle().getCode());
		}
		if (supervisor.getOffice().getId() != user.getOfficeId()) {
			return new ServiceResult(ServiceResult.MessageType.ERROR, "Supervisor's office is not valid", supervisor.getOffice().getId());
		}
		
		InList<TeamDTO> spv = this.asSupervisor(persisted.getSupervisor());
		if (spv.exists()) {
    		return new ServiceResult(ServiceResult.MessageType.ERROR, "Supervisor already assigned", null);
		}
    	
    	CollectionTeam team = new CollectionTeam();
    	team.setCycle(persisted.getCycle());
    	team.setName(persisted.getName()); 
    	team.setOffice(persisted.getOffice());
    	team.setSupervisor(persisted.getSupervisor());
    	
    	for (Iterator<CollectionMember> itr = persisted.getCollectionMembers().iterator(); itr.hasNext();) { 

    		CollectionMember persistedmember = itr.next();

            if (persistedmember.getEmployee() == null || persistedmember.getEmployee().getId() == null) {
        		return new ServiceResult(ServiceResult.MessageType.ERROR, "Collection Member data not supplied", null);
            }

            Employee empl = emplrepository.findOne(persistedmember.getEmployee().getId());
    		if (empl == null) {
    			return new ServiceResult(ServiceResult.MessageType.ERROR, "The employee is not recognized", persistedmember.getEmployee().getId());
    		}
    		if (!empl.getJobTitle().getCode().equals("COLL")) {
    			return new ServiceResult(ServiceResult.MessageType.ERROR, "Assigned employee is not valid, the job title is not a Collector", empl.getJobTitle().getCode());
    		}
    		if (empl.getOffice().getId() != user.getOfficeId()) {
    			return new ServiceResult(ServiceResult.MessageType.ERROR, "Employee's office is not valid", empl.getOffice().getId());
    		}

    		InList<CollectionMemberDTO> memberdto = this.asMember(empl);
    		if (memberdto.exists()) {
        		return new ServiceResult(ServiceResult.MessageType.ERROR, "The employee already assigned as a Collector", null);
    		}
        	
    		CollectionMember member = new CollectionMember();
    		if (persistedmember.getDailyLoad() == null) {
    			persistedmember.setDailyLoad(0);
    		}
    		if (persistedmember.getMonthlyLoad() == null) {
    			persistedmember.setMonthlyLoad(0);
    		}
    		member.setEmployee(empl);
    		member.setDailyLoad(persistedmember.getDailyLoad());
    		member.setMonthlyLoad(persistedmember.getMonthlyLoad());
    		
    		for (Iterator<CollectionWorkingArea> itr2 = persistedmember.getCollectionWorkingAreas().iterator(); itr2.hasNext();) { 
    			CollectionWorkingArea area = itr2.next();

                if (area.getZipcode() == null || area.getZipcode().getId() == null) {
            		return new ServiceResult(ServiceResult.MessageType.ERROR, "Collection Working Area data not supplied", null);
                }
                Zipcode zip = ziprepository.findOne(area.getZipcode().getId());
        		if (zip == null) {
        			return new ServiceResult(ServiceResult.MessageType.ERROR, "Zipcode is not recognized", area.getZipcode().getId());
        		}
        		if (zip.getOffice().getId() != user.getOfficeId()) {
        			return new ServiceResult(ServiceResult.MessageType.ERROR, "Area / zipcode's office is not valid", zip.getOffice().getId());
        		}

        		AreaCriteria areacriteria = new AreaCriteria();
        		areacriteria.setZipId(zip.getId());
        		areacriteria.setOfficeId(user.getOfficeId());
        		areacriteria.setCycleId(persisted.getCycle().getId());

        		InList<ZipDTO> zipdto = this.asOccupiedArea(areacriteria);
        		if (zipdto.exists()) {
            		return new ServiceResult(ServiceResult.MessageType.ERROR, "The area / zipcode already occupied", null);
        		}

        		member.addCollectionWorkingArea(area);
        	}    		
    		team.addCollectionMember(member);
    	}
    	
    	try {
    		repository.save(team);
		} catch (Exception excp) {
			return new ServiceResult(ServiceResult.MessageType.ERROR, "Collection Team setup failed", team.getId());
		}
    	
		return new ServiceResult(ServiceResult.MessageType.SUCCESS, "Collection Team successfully set up", team.getId());
	}

}
