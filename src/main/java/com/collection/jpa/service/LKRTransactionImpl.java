package com.collection.jpa.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.TypedQuery;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.collection.jpa.CustomUserDetails;
import com.collection.jpa.dao.DelegationBucketRepository;
import com.collection.jpa.dao.SKPCTransactionRepository;
import com.collection.jpa.dao.LKRTransactionRepository;
import com.collection.jpa.dao.PrintActivityRepository;
import com.collection.jpa.dto.LkrCriteria;
import com.collection.jpa.dto.BucketDetailDTO;
import com.collection.jpa.dto.LkrHeaderDTO;
import com.collection.jpa.dto.LkrPageDTO;
import com.collection.jpa.dto.LkrDataDTO;
import com.collection.jpa.dto.LkrCyclePageDTO;
import com.collection.jpa.dto.LkrPrintDTO;
import com.collection.jpa.dto.LkrPrintPageDTO;
import com.collection.jpa.dto.MemberCriteria;
import com.collection.jpa.dto.RemedialMemberDTO;
import com.collection.jpa.dto.LkrTeamPageDTO;
import com.collection.jpa.dto.PrintCriteria;
import com.collection.jpa.dto.ReclusteringDTO;
import com.collection.jpa.dto.SpecialDelegationDTO;
import com.collection.jpa.dto.StaffCriteria;
import com.collection.jpa.dto.TeamCriteria;
import com.collection.jpa.dto.TeamDTO;
import com.collection.jpa.entity.Bucket;
import com.collection.jpa.entity.RemedialTeam;
import com.collection.jpa.entity.DelegationBucket;
import com.collection.jpa.entity.Employee;
import com.collection.jpa.entity.LKREntryStatus;
import com.collection.jpa.entity.SKPCTransaction;
import com.collection.jpa.entity.Status;
import com.collection.jpa.entity.LKRTransaction;
import com.collection.jpa.entity.PrintActivity;
import com.collection.jpa.util.Page;
import com.collection.jpa.util.ServiceResult;


@Repository
@Transactional(readOnly = true)
public class LKRTransactionImpl implements LKRTransactionService {
	
	Date currentDate = new Date();

	@Autowired
	private LKRTransactionRepository repository;
	
	@Autowired
	private PrintActivityRepository printrepository;

	@Autowired
	private SKPCTransactionRepository skpcrepository;

	@Autowired
	private DelegationBucketRepository bucketrepository;

	@Autowired
	private QueryImpl queryService;

	@Autowired
	private RemedialTeamImpl teamService;

	@Autowired
	private EmployeeImpl employeeService;


    private List<LkrPrintDTO> getPrintList(List<LkrDataDTO> printList) {
		List<LkrPrintDTO> lkrList = new ArrayList<LkrPrintDTO>();

		for (LkrDataDTO m: printList) {
			LkrPrintDTO lkr = new LkrPrintDTO();
			lkr.setId(m.getId());
			lkr.setTeamId(m.getTeamId());
			lkr.setSupervisorId(m.getSupervisorId());
			lkr.setContractNo(m.getContractNo());
			lkr.setContractDate(m.getContractDate());
			lkr.setCustName(m.getCustName());
			lkr.setCustBillingAddress(m.getCustBillingAddress());
			lkr.setPhone1(m.getPhone1());
			lkr.setPhone2(m.getPhone2());
			lkr.setLeastInstallmentNo(m.getLeastInstallmentNo());
			lkr.setLastInstallmentNo(m.getLastInstallmentNo());
			lkr.setTopInstallment(m.getTopInstallment());
			lkr.setLeastDueDate(m.getLeastDueDate());
			lkr.setLastDueDate(m.getLastDueDate());
			lkr.setOverdue(m.getOverdue());
			lkr.setOutstandingPrincipalAmnt(m.getOutstandingPrincipalAmnt());
			lkr.setOutstandingInterestAmnt(m.getOutstandingInterestAmnt());
			lkr.setMonthlyInstallment(m.getMonthlyInstallment());
			lkr.setBusinessUnit(m.getBusinessUnit());
			lkr.setBrand(m.getBrand());
			lkr.setModel(m.getModel());
			lkr.setColor(m.getColor());
			lkr.setFrameNo(m.getFrameNo());
			lkr.setEngineNo(m.getEngineNo());
			lkr.setPoliceNo(m.getPoliceNo());
			lkr.setStnk(m.getStnk());
			lkr.setPrintNo(m.getPrintNo());
			lkr.setPrintDate(m.getPrintDate());
			List<BucketDetailDTO> theDetails = queryService.bucketDetail(m.getContractNo(), m.getLeastInstallmentNo(), m.getLastInstallmentNo()).getResultList();
			lkr.setDetails(theDetails);

			lkrList.add(lkr);
		}

		return lkrList;
	}

    @PreAuthorize("hasRole('ROLE_SUPERADMIN') or hasRole('ROLE_REMEDIAL_HEAD') or hasRole('ROLE_REMEDIAL_SPV')")
    public Page<LkrCyclePageDTO> pageManualAssignment(Pageable pageable) {
        CustomUserDetails user = (CustomUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        LkrCriteria lkrcriteria = new LkrCriteria();
        lkrcriteria.setOfficeCode(user.getOfficeCode());
        
        if (user.getJobCode().equals("REMSPV")) {
            TeamCriteria teamcriteria = new TeamCriteria();
            teamcriteria.setSpvId(user.getEmplId());
            List<TeamDTO> teams = queryService.remedialTeam(teamcriteria).getResultList();
            List<Integer> cycleIdList = new ArrayList<Integer>();
            for (TeamDTO team : teams) {
            	cycleIdList.add(team.getCycleId());
            }
        	lkrcriteria.setCycleIdList(cycleIdList);
    	}

        TypedQuery<LkrCyclePageDTO> query = queryService.lkrRemedialAssignment(lkrcriteria);
		if (query == null) {
    		return new Page<LkrCyclePageDTO>();
        }
		return (Page<LkrCyclePageDTO>) new Page<LkrCyclePageDTO>(query, pageable);
	}
	
    @PreAuthorize("hasRole('ROLE_SUPERADMIN') or hasRole('ROLE_REMEDIAL_HEAD') or hasRole('ROLE_REMEDIAL_SPV')")
	public Page<LkrTeamPageDTO> pageClustering(Pageable pageable) {
        CustomUserDetails user = (CustomUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

    	LkrCriteria lkrcriteria = new LkrCriteria();
        lkrcriteria.setOfficeCode(user.getOfficeCode());

        if (user.getJobCode().equals("REMSPV")) {
            TeamCriteria teamcriteria = new TeamCriteria();
            teamcriteria.setSpvId(user.getEmplId());
            List<TeamDTO> teams = queryService.remedialTeam(teamcriteria).getResultList();
            List<Integer> teamIdList = new ArrayList<Integer>();
            for (TeamDTO team : teams) {
                teamIdList.add(team.getId());
            }
        	lkrcriteria.setTeamIdList(teamIdList);
    	}
    	
		TypedQuery<LkrTeamPageDTO> query = queryService.lkrClustering(lkrcriteria);
		if (query == null) {
    		return new Page<LkrTeamPageDTO>();
        }
		return (Page<LkrTeamPageDTO>) new Page<LkrTeamPageDTO>(query, pageable);
	}
	
    @Transactional
	@Modifying(clearAutomatically = true)
    @PreAuthorize("hasRole('ROLE_REMEDIAL_SPV')")
	public ServiceResult assign(LKRTransaction persisted) {
        CustomUserDetails user = (CustomUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        if (!(user.getJobCode().equals("REMSPV"))) {
			return new ServiceResult(ServiceResult.MessageType.ERROR, "Not authorized user", null);
        }
        
        if (persisted == null || persisted.getId() == null) {
    		return new ServiceResult(ServiceResult.MessageType.ERROR, "Contract not defined", null);
    	}

        LKRTransaction lkr = repository.findOne(persisted.getId());
        if (lkr == null) {
    		return new ServiceResult(ServiceResult.MessageType.ERROR, "Contract not found", null);
        }

        if (lkr.getAssignedTo() != null && lkr.getTeam() != null) {
    		return new ServiceResult(ServiceResult.MessageType.ERROR, "Contract already assigned (to another team)", lkr.getId());
    	}

        if (persisted.getSupervisor() == null || persisted.getSupervisor().getId() != user.getEmplId()) {
			return new ServiceResult(ServiceResult.MessageType.ERROR, "Not valid Supervisor", null);
    	}
    	
        if (persisted.getTeam() == null || persisted.getTeam().getId() == null) {
			return new ServiceResult(ServiceResult.MessageType.ERROR, "Please specify team", null);
        }
        TeamCriteria criteria = new TeamCriteria();
        criteria.setId(persisted.getTeam().getId());
        criteria.setOfficeId(user.getOfficeId());
        criteria.setSpvId(user.getEmplId());
        List<TeamDTO> teamList = queryService.remedialTeam(criteria).getResultList();
        if (teamList.isEmpty()) {
			return new ServiceResult(ServiceResult.MessageType.ERROR, "Not valid Supervisor", null);
        }
        RemedialTeam team = teamService.findOne(teamList.get(0).getId());
    	Employee spv = employeeService.findOne(persisted.getSupervisor().getId());

    	if (persisted.getAssignedTo() == null || persisted.getAssignedTo().getId() == null) {
    		return new ServiceResult(ServiceResult.MessageType.ERROR, "Collector has not been assigned", null);
    	}
    	
    	Employee empl = employeeService.findOne(persisted.getAssignedTo().getId());
    	if (empl == null) {
    		return new ServiceResult(ServiceResult.MessageType.ERROR, "Employee not found", null);
    	}

    	if (queryService.hasMaxLoadReached(empl.getId())) {
    		return new ServiceResult(ServiceResult.MessageType.ERROR, "Has reached the maximum load", lkr.getId());
    	}
    	
    	lkr.setAssignedTo(empl);
    	lkr.setTeam(team);
    	lkr.setSupervisor(spv);
    	
    	try {
    		repository.save(lkr);
		} catch (Exception excp) {
			return new ServiceResult(ServiceResult.MessageType.ERROR, "Contract assignment failed", null);
		}
    	return new ServiceResult(ServiceResult.MessageType.SUCCESS, String.format("Contract #%s successfully assigned", lkr.getBucket().getContractNo()), lkr.getId());
	}

    @PreAuthorize("hasRole('ROLE_REMEDIAL_SPV')")
	public List<LkrPrintDTO> listPrint(StaffCriteria criteria) {
        CustomUserDetails user = (CustomUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        if (!user.getJobCode().equals("REMSPV")) {
        	return null;
    	}

    	if (criteria == null || criteria.getStaffId() == null) {
    		return null;
    	}

    	LkrCriteria lkrcriteria = new LkrCriteria();
        lkrcriteria.setOfficeCode(user.getOfficeCode());
    	lkrcriteria.setSupervisorId(user.getEmplId());
        lkrcriteria.setAssignedTo(criteria.getStaffId());
        lkrcriteria.setPrintId(0);

        TypedQuery<LkrDataDTO> query = queryService.lkrPrint(lkrcriteria);
		if (query == null) {
			return null;
        }

        List<LkrDataDTO> printList = query.getResultList();
        System.out.println(printList);
		return getPrintList(printList);
    }

    @PreAuthorize("hasRole('ROLE_REMEDIAL_SPV')")
	public LkrHeaderDTO headerPrint(StaffCriteria criteria) {
        CustomUserDetails user = (CustomUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        if (!user.getJobCode().equals("REMSPV")) {
        	return null;
    	}

    	if (criteria == null || criteria.getStaffId() == null) {
    		return null;
    	}

    	MemberCriteria membercriteria = new MemberCriteria();
        if (user.getJobCode().equals("REMSPV")) {
        	membercriteria.setSupervisorId(user.getEmplId());
    	}
    	membercriteria.setEmplId(criteria.getStaffId());
    	System.out.println("membercriteria: " + membercriteria);
    	TypedQuery<RemedialMemberDTO> query = queryService.remedialMember(membercriteria);
		
    	RemedialMemberDTO member = new RemedialMemberDTO();
		try {
			member = query.getSingleResult();
		} catch (Exception excp) {
		}

    	if (member.getEmplId() == null) {
    		return null;
    	}

        LkrHeaderDTO header = new LkrHeaderDTO();
		header.setTeamId(member.getTeamId());
		header.setTeamName(member.getTeamName());
		header.setEmployeeId(member.getEmplId());
		header.setEmployeeRegNo(member.getEmplRegNo());
		header.setEmployeeName(member.getEmplName());

		return header;
	}

	@Transactional
	@Modifying(clearAutomatically = true)
    @PreAuthorize("hasRole('ROLE_REMEDIAL_SPV')")
	public ServiceResult setPrinted(LkrHeaderDTO header, List<LkrPrintDTO> listData) {
        CustomUserDetails user = (CustomUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        if (!user.getJobCode().equals("REMSPV")) {
			return new ServiceResult(ServiceResult.MessageType.ERROR, "Not authorized user", null);
    	}

        if (listData.size() == 0) {
        	return new ServiceResult(ServiceResult.MessageType.INFO, "No LKR to print", null);
        }

		PrintActivity print = new PrintActivity();
		print.setTeamId(header.getTeamId());
		print.setEmployeeId(header.getEmployeeId());
        print.setPrintDate(header.getPrintDate());
        print.setPrintNo(header.getPrintNo());
        print.setType("R");

        try {
            PrintActivity result = printrepository.save(print);
            Integer printId = result.getId();
            
            List<Integer> theList = new ArrayList<Integer>();
            for (LkrPrintDTO m: listData) {
                theList.add(m.getId());
            }
            
            queryService.lkrPrinted(theList, printId);
            
		} catch (Exception excp) {
	        return new ServiceResult(ServiceResult.MessageType.ERROR, "Failed to update print activity", null);
		}

        return new ServiceResult(ServiceResult.MessageType.SUCCESS, "Print activity successfully updated", null);
	}
	
    @PreAuthorize("hasRole('ROLE_SUPERADMIN') or hasRole('ROLE_REMEDIAL_HEAD') or hasRole('ROLE_REMEDIAL_SPV')")
	public Page<PrintActivity> pageReprint(StaffCriteria criteria, Pageable pageable) {
    	if (criteria == null || criteria.getStaffId() == null) {
			return new Page<PrintActivity>();
    	}

    	PrintCriteria printcriteria = new PrintCriteria();
    	printcriteria.setStaffId(criteria.getStaffId());
    	printcriteria.setPrintType("R");
		TypedQuery<PrintActivity> query = queryService.printActivity1(printcriteria);
		if (query == null) {
			return new Page<PrintActivity>();
        }
		return (Page<PrintActivity>) new Page<PrintActivity>(query, pageable);
	}

    @PreAuthorize("hasRole('ROLE_SUPERADMIN') or hasRole('ROLE_REMEDIAL_HEAD') or hasRole('ROLE_REMEDIAL_SPV')")
	public Page<LkrPrintPageDTO> pageReprintDetail(PrintCriteria criteria, Pageable pageable) {
    	if (criteria == null || (criteria.getPrintId() == null && criteria.getPrintNo() == null)) {
    		return new Page<LkrPrintPageDTO>();
    	}

        CustomUserDetails user = (CustomUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

    	LkrCriteria lkrcriteria = new LkrCriteria();
        lkrcriteria.setOfficeCode(user.getOfficeCode());

        if (user.getJobCode().equals("REMSPV")) {
        	lkrcriteria.setSupervisorId(user.getEmplId());
    	}

        lkrcriteria.setPrintId(criteria.getPrintId());
		lkrcriteria.setPrintNo(criteria.getPrintNo());

        TypedQuery<LkrPrintPageDTO> query = queryService.lkrPrintPage(lkrcriteria);
		if (query == null) {
    		return new Page<LkrPrintPageDTO>();
        }

		return (Page<LkrPrintPageDTO>) new Page<LkrPrintPageDTO>(query, pageable);
	}

    @PreAuthorize("hasRole('ROLE_SUPERADMIN') or hasRole('ROLE_REMEDIAL_HEAD') or hasRole('ROLE_REMEDIAL_SPV')")
	public List<LkrPrintDTO> listReprint(PrintCriteria criteria) {
    	if (criteria == null || (criteria.getPrintId() == null && criteria.getPrintNo() == null)) {
    		return null;
    	}

        CustomUserDetails user = (CustomUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

    	LkrCriteria lkrcriteria = new LkrCriteria();
        if (user.getJobCode().equals("REMSPV")) {
        	lkrcriteria.setSupervisorId(user.getEmplId());
    	}
        if (criteria.getPrintId() == null || criteria.getPrintId() == 0) {
        	lkrcriteria.setPrintId(-1); // reprint
        }
        else {
        	lkrcriteria.setPrintId(criteria.getPrintId());
        }
    	lkrcriteria.setPrintNo(criteria.getPrintNo());

    	System.out.println("criteria: " + lkrcriteria);
        TypedQuery<LkrDataDTO> query = queryService.lkrPrint(lkrcriteria);
		if (query == null) {
			return null;
        }

        List<LkrDataDTO> printList = query.getResultList();
		return getPrintList(printList);
	}

    @PreAuthorize("hasRole('ROLE_SUPERADMIN') or hasRole('ROLE_REMEDIAL_HEAD') or hasRole('ROLE_REMEDIAL_ADMIN')")
	public LkrPageDTO contract(LkrCriteria criteria) {
		LkrPageDTO result = new LkrPageDTO();
		if (criteria == null || criteria.getContractNo() == null) {
			return result;
        }

        CustomUserDetails user = (CustomUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        criteria.setOfficeCode(user.getOfficeCode());
        criteria.setAssignedTo(-1);
        criteria.setPrintId(-1);
		criteria.setCodeEntryStatus("null");
		System.out.println("LKR Criteria: " + criteria);
        TypedQuery<LkrPageDTO> query = queryService.lkrPage(criteria);
		try {
			result = query.getSingleResult();
		} catch (Exception excp) {
		}
		return result;
	}

    @Transactional
	@Modifying(clearAutomatically = true)
    @PreAuthorize("hasRole('ROLE_REMEDIAL_ADMIN')")
	public ServiceResult entry(LKRTransaction persisted) {
        CustomUserDetails user = (CustomUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        if (!(user.getJobCode().equals("REMADM"))) {
			return new ServiceResult(ServiceResult.MessageType.ERROR, "Not authorized user", null);
        }
        
    	if (persisted == null || persisted.getId() == null) {
    		return new ServiceResult(ServiceResult.MessageType.ERROR, "Contract not defined", null);
    	}

    	if (persisted.getLkrEntryStatus() == null) {
    		return new ServiceResult(ServiceResult.MessageType.ERROR, "Entry status not defined", null);
    	}
    	
    	LKRTransaction lkr = repository.findOne(persisted.getId());

    	if (lkr == null) {
    		return new ServiceResult(ServiceResult.MessageType.ERROR, "Contract not found", null);
    	}

    	if (lkr.getAssignedTo() == null || lkr.getTeam() == null || lkr.getTeam().getId() == null) {
    		return new ServiceResult(ServiceResult.MessageType.ERROR, "Contract has not been assigned yet", lkr.getId());
    	}
    	
    	if (lkr.getPrintActivity() == null) {
    		return new ServiceResult(ServiceResult.MessageType.ERROR, "Contract has not been printed yet", lkr.getId());
    	}

    	if (lkr.getLkrEntryStatus() != null) {
    		return new ServiceResult(ServiceResult.MessageType.ERROR, "Contract already done", lkr.getId());
    	}

    	lkr.setLkrEntryStatus(persisted.getLkrEntryStatus());
    	lkr.setDateRequired(persisted.getDateRequired());
    	lkr.setStrComment(persisted.getStrComment());   
    	
    	try {
    		repository.save(lkr);
		} catch (Exception excp) {
    		return new ServiceResult(ServiceResult.MessageType.ERROR, "Contract result entry failed", null);
		}
    	return new ServiceResult(ServiceResult.MessageType.SUCCESS, String.format("Result entry for contract #%s succeed", lkr.getBucket().getContractNo()), lkr.getId());
    }

    @PreAuthorize("hasRole('ROLE_SUPERADMIN') or hasRole('ROLE_REMEDIAL_HEAD') or hasRole('ROLE_REMEDIAL_SPV')")
	public Page<LkrPrintPageDTO> pageReclustering(StaffCriteria criteria, Pageable pageable) {
    	if (criteria == null || criteria.getStaffId() == null) {
    		return new Page<LkrPrintPageDTO>();
    	}

        CustomUserDetails user = (CustomUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

    	LkrCriteria lkrcriteria = new LkrCriteria();
        lkrcriteria.setOfficeCode(user.getOfficeCode());
    	lkrcriteria.setAssignedTo(criteria.getStaffId());
    	lkrcriteria.setReclustering(true);

        if (user.getJobCode().equals("REMSPV")) {
        	lkrcriteria.setSupervisorId(user.getEmplId());
    	}

    	TypedQuery<LkrPrintPageDTO> query = queryService.lkrPrintPage(lkrcriteria);
		if (query == null) {
    		return new Page<LkrPrintPageDTO>();
        }
		return (Page<LkrPrintPageDTO>) new Page<LkrPrintPageDTO>(query, pageable);
	}
	
    @Transactional
	@Modifying(clearAutomatically = true)
    @PreAuthorize("hasRole('ROLE_REMEDIAL_SPV')")
	public ServiceResult recluster(ReclusteringDTO data) {
        CustomUserDetails user = (CustomUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        if (!(user.getJobCode().equals("REMSPV"))) {
			return new ServiceResult(ServiceResult.MessageType.ERROR, "Not authorized user", null);
        }

    	if (data == null || data.getLkrId() == null) {
    		return new ServiceResult(ServiceResult.MessageType.ERROR, "Contract not defined", null);
    	}

    	LkrCriteria lkrcriteria = new LkrCriteria();
        lkrcriteria.setOfficeCode(user.getOfficeCode());
    	lkrcriteria.setId(data.getLkrId());
    	lkrcriteria.setReclustering(true);

        if (user.getJobCode().equals("REMSPV")) {
        	lkrcriteria.setSupervisorId(user.getEmplId());
        }

        TypedQuery<LkrPageDTO> query = queryService.lkrPage(lkrcriteria);
		if (query == null) {
    		return new ServiceResult(ServiceResult.MessageType.ERROR, "Contract not found", null);
        }
		
        LKRTransaction oldLkr = repository.findOne(data.getLkrId());

    	LKREntryStatus status = new LKREntryStatus();
    	status.setCode("NEW");
    	oldLkr.setLkrEntryStatus(status);
    	
    	LKRTransaction newLkr = new LKRTransaction();
    	newLkr.setBucket(oldLkr.getBucket());
    	newLkr.setRunDate(new Date());
    	newLkr.setTeam(oldLkr.getTeam());
    	newLkr.setSupervisor(oldLkr.getSupervisor());
    	newLkr.setOwnerId(oldLkr.getOwnerId());
    	
    	try {
    		repository.save(oldLkr);
    		repository.save(newLkr);
		} catch (Exception excp) {
			return new ServiceResult(ServiceResult.MessageType.ERROR, "Create new assignment for the contract failed", null);
		}
    	return new ServiceResult(ServiceResult.MessageType.SUCCESS, String.format("Contract #%s new assignment succeed", newLkr.getBucket().getContractNo()), newLkr.getId());
	}

    @PreAuthorize("hasRole('ROLE_SUPERADMIN') or hasRole('ROLE_REMEDIAL_HEAD') or hasRole('ROLE_REMEDIAL_SPV')")
	public Page<LkrPageDTO> pageSpecialDelegation(Pageable pageable) {
        CustomUserDetails user = (CustomUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

    	LkrCriteria lkrcriteria = new LkrCriteria();
        lkrcriteria.setOfficeCode(user.getOfficeCode());
		lkrcriteria.setPrintId(0);

        if (user.getJobCode().equals("REMSPV")) {
        	lkrcriteria.setSupervisorId(user.getEmplId());
        }

        TypedQuery<LkrPageDTO> query = queryService.lkrPage(lkrcriteria);
		if (query == null) {
    		return new Page<LkrPageDTO>();
        }

		return (Page<LkrPageDTO>) new Page<LkrPageDTO>(query, pageable);
	}

    @Transactional
	@Modifying(clearAutomatically = true)
    @PreAuthorize("hasRole('ROLE_REMEDIAL_SPV')")
	public ServiceResult delegate(SpecialDelegationDTO data) {
        CustomUserDetails user = (CustomUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        if (!(user.getJobCode().equals("REMSPV"))) {
			return new ServiceResult(ServiceResult.MessageType.ERROR, "Not authorized user", null);
        }

        if (data == null || data.getContractId() == null) {
    		return new ServiceResult(ServiceResult.MessageType.ERROR, "Contract not defined", null);
    	}

    	LkrCriteria lkrcriteria = new LkrCriteria();
        lkrcriteria.setOfficeCode(user.getOfficeCode());
    	lkrcriteria.setId(data.getContractId());
		lkrcriteria.setPrintId(0);

        if (user.getJobCode().equals("REMSPV")) {
        	lkrcriteria.setSupervisorId(user.getEmplId());
        }

        TypedQuery<LkrPageDTO> query = queryService.lkrPage(lkrcriteria);
		if (query == null) {
    		return new ServiceResult(ServiceResult.MessageType.ERROR, "Contract not found", null);
        }

		LkrPageDTO lkr = new LkrPageDTO();
		try {
			lkr = query.getSingleResult();
		} catch (Exception excp) {
    		return new ServiceResult(ServiceResult.MessageType.ERROR, "Contract not found", null);
		}
		
        LKRTransaction oldLkr = repository.findOne(data.getContractId());
    	Bucket bucket = oldLkr.getBucket();

    	SKPCTransaction skpc = new SKPCTransaction();
    	skpc.setBucket(bucket);
    	skpc.setRunDate(currentDate);
    	skpc.setStatus(new Status(0));

    	DelegationBucket delbucket = new DelegationBucket();
    	delbucket.setMemoNo(data.getMemoNo());
    	delbucket.setMemoDate(data.getMemoDate());
    	delbucket.setRunDate(currentDate);
    	delbucket.setContractNo(bucket.getContractNo());
    	delbucket.setContractDate(bucket.getContractDate());
    	delbucket.setOfficeCode(bucket.getOfficeCode());
    	delbucket.setTopInstallment(bucket.getTopInstallment());
    	delbucket.setOutstandingPrincipalAmnt(bucket.getOutstandingPrincipalAmnt());
    	delbucket.setOutstandingInterestAmnt(bucket.getOutstandingInterestAmnt());
    	delbucket.setMonthlyInstallment(bucket.getMonthlyInstallment());
    	delbucket.setCustName(bucket.getCustName());
    	delbucket.setCustAddress(bucket.getCustAddress());
    	delbucket.setCustBillingAddress(bucket.getCustBillingAddress());
    	delbucket.setZipcode(bucket.getZipcode());
    	delbucket.setSubZipcode(bucket.getSubZipcode());
    	delbucket.setPhone1(bucket.getPhone1());
    	delbucket.setPhone2(bucket.getPhone2());
    	delbucket.setBusinessUnit(bucket.getBusinessUnit());
    	delbucket.setLeastDueDate(bucket.getLeastDueDate());
    	delbucket.setLeastInstallmentNo(bucket.getLeastInstallmentNo());
    	delbucket.setLastDueDate(bucket.getLastDueDate());
    	delbucket.setLastInstallmentNo(bucket.getLastInstallmentNo());
    	
    	try {
        	bucketrepository.save(delbucket);
        	skpcrepository.save(skpc);
        	queryService.lkrDelete(oldLkr.getBucket().getContractNo());
		} catch (Exception excp) {
    		return new ServiceResult(ServiceResult.MessageType.ERROR, "Contract not found", null);
		}
		
    	return new ServiceResult(ServiceResult.MessageType.SUCCESS, String.format("Contract #%s has successfully delegated", delbucket.getContractNo()), delbucket.getId());
	}

}
