package com.collection.jpa.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.collection.jpa.dao.CollectionDormantRepository;
import com.collection.jpa.dto.CollectionMemberDTO;
import com.collection.jpa.dto.TeamDTO;
import com.collection.jpa.entity.CollectionDormant;
import com.collection.jpa.entity.CollectionTeam;
import com.collection.jpa.entity.CollectionWorkingArea;
import com.collection.jpa.entity.Employee;
import com.collection.jpa.entity.Office;
import com.collection.jpa.util.InList;
import com.collection.jpa.util.Page;
import com.collection.jpa.util.ServiceResult;


@Repository
@Transactional(readOnly = true)
public class CollectionDormantImpl implements CollectionDormantService {
	
	@PersistenceContext
	private EntityManager entityManager;
	
	@Autowired
	private CollectionDormantRepository repository;
	
	public CollectionDormant getOne(Integer id) {
		return repository.findOne(id);
	}

	@Override
	@Transactional
	public ServiceResult insert(CollectionDormant persisted) {
		repository.save(persisted);
		return new ServiceResult(ServiceResult.MessageType.SUCCESS, "", persisted.getId());
	}

	@Override
	@Transactional
	public ServiceResult update(CollectionDormant persisted) {
		repository.save(persisted);
		return new ServiceResult(ServiceResult.MessageType.SUCCESS, "", persisted.getId());
	}


}
