package com.collection.jpa.service;

import java.util.List;

import javax.persistence.TypedQuery;

import com.collection.jpa.dto.CollectionMemberDTO;
import com.collection.jpa.dto.OfficeCriteria;
import com.collection.jpa.dto.OfficeCycleCriteria;
import com.collection.jpa.dto.PrintCriteria;
import com.collection.jpa.dto.ProfCollectorCriteria;
import com.collection.jpa.dto.ProfCollectorDTO;
import com.collection.jpa.dto.RemedialMemberDTO;
import com.collection.jpa.dto.SkpcCriteria;
import com.collection.jpa.dto.SkpcPageDTO;
import com.collection.jpa.dto.TeamCriteria;
import com.collection.jpa.dto.TeamDTO;
import com.collection.jpa.dto.ZipDTO;
import com.collection.jpa.entity.Employee;
import com.collection.jpa.entity.PrintActivity;
import com.collection.jpa.dto.AreaCriteria;
import com.collection.jpa.dto.BucketDetailDTO;
import com.collection.jpa.dto.CycleCriteria;
import com.collection.jpa.dto.CycleDTO;
import com.collection.jpa.dto.DeskCallTeamMemberDTO;
import com.collection.jpa.dto.LkpTeamPageDTO;
import com.collection.jpa.dto.EmployeeCriteria;
import com.collection.jpa.dto.EmployeeDTO;
import com.collection.jpa.dto.LkdCriteria;
import com.collection.jpa.dto.LkdDataDTO;
import com.collection.jpa.dto.LkpCriteria;
import com.collection.jpa.dto.LkpDataDTO;
import com.collection.jpa.dto.LkpPageDTO;
import com.collection.jpa.dto.LkpPrintPageDTO;
import com.collection.jpa.dto.LkpCyclePageDTO;
import com.collection.jpa.dto.MemberCriteria;
import com.collection.jpa.dto.LkrTeamPageDTO;
import com.collection.jpa.dto.LkrCriteria;
import com.collection.jpa.dto.LkrDataDTO;
import com.collection.jpa.dto.LkrPageDTO;
import com.collection.jpa.dto.LkrPrintPageDTO;
import com.collection.jpa.dto.LkrCyclePageDTO;


public interface QueryService {

	// Cycle
	TypedQuery<CycleDTO> cycle(CycleCriteria criteria);
	Integer numCollectionTeam(Integer cycleId);
	Integer numRemedialTeam(Integer cycleId);
	Integer numDeskcallTeam(Integer cycleId);
	
	// Zipcode
	TypedQuery<ZipDTO> zipcodeAvailable(OfficeCycleCriteria criteria, List<Integer> theList);
	TypedQuery<ZipDTO> zipcodeCollectionMember(MemberCriteria criteria);
	TypedQuery<ZipDTO> zipcodeRemedialMember(MemberCriteria criteria);
	
	//Employee
	TypedQuery<Employee> employee(EmployeeCriteria criteria);
	
	// Collection
	TypedQuery<TeamDTO> collectionTeam(TeamCriteria criteria);
	TypedQuery<CollectionMemberDTO> collectionMember(MemberCriteria criteria);
	TypedQuery<ZipDTO> collectionArea(AreaCriteria criteria);
	TypedQuery<EmployeeDTO> collectionSpvAvailable(OfficeCriteria criteria);
	TypedQuery<EmployeeDTO> collectionStaffAvailable(OfficeCriteria criteria);
	
	// Remedial
	TypedQuery<TeamDTO> remedialTeam(TeamCriteria criteria);
	TypedQuery<RemedialMemberDTO> remedialMember(MemberCriteria criteria);
	TypedQuery<ZipDTO> remedialArea(AreaCriteria criteria);
	TypedQuery<EmployeeDTO> remedialSpvAvailable(OfficeCriteria criteria);
	TypedQuery<EmployeeDTO> remedialStaffAvailable(OfficeCriteria criteria);

	// Desk Call
	TypedQuery<TeamDTO> deskcallTeam(TeamCriteria criteria);
	TypedQuery<DeskCallTeamMemberDTO> deskcallMember(MemberCriteria criteria);
	TypedQuery<EmployeeDTO> deskcallSpvAvailable(OfficeCriteria criteria);
	TypedQuery<EmployeeDTO> deskcallStaffAvailable(OfficeCriteria criteria);
	
	// Professional Collector
	TypedQuery<ProfCollectorDTO> profCollector(ProfCollectorCriteria criteria);
	
	// Bucket Detail
	TypedQuery<BucketDetailDTO> bucketDetail(String contractNo, Integer from, Integer to);

	// LKP
	TypedQuery<LkpCyclePageDTO> lkpManualAssignment(LkpCriteria criteria);
	TypedQuery<LkpTeamPageDTO> lkpClustering(LkpCriteria criteria);
	TypedQuery<LkpDataDTO> lkpPrint(LkpCriteria criteria);
	TypedQuery<LkpPageDTO> lkpPage(LkpCriteria criteria);
	TypedQuery<PrintActivity> printActivity(PrintCriteria criteria);
	TypedQuery<LkpPrintPageDTO> lkpPrintPage(LkpCriteria criteria);
	void lkpPrinted(List<Integer> theList, Integer printId);
	boolean hasMaxLoadReached(Integer assignedTo);
	void lkpDelete(String contractNo);
	
	// LKR
	TypedQuery<LkrCyclePageDTO> lkrRemedialAssignment(LkrCriteria criteria);
	TypedQuery<LkrTeamPageDTO> lkrClustering(LkrCriteria criteria);
	TypedQuery<LkrDataDTO> lkrPrint(LkrCriteria criteria);
	TypedQuery<LkrPageDTO> lkrPage(LkrCriteria criteria);
	TypedQuery<PrintActivity> printActivity1(PrintCriteria criteria);
	TypedQuery<LkrPrintPageDTO> lkrPrintPage(LkrCriteria criteria);
	void lkrPrinted(List<Integer> theList, Integer printId);
	boolean hasMaxLoadReached1(Integer assignedTo);
	void lkrDelete(String contractNo);

	// LKD
	TypedQuery<LkdDataDTO> lkd(LkdCriteria criteria);
	void lkdPrinted(List<Integer> theList, Integer printId);

	// SKPC
	TypedQuery<SkpcPageDTO> skpc(SkpcCriteria criteria);
	boolean hasPCMaxLoadReached(Integer profcollId);
}
