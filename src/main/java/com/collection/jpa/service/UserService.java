package com.collection.jpa.service;

import org.springframework.data.domain.Pageable;
import com.collection.jpa.dto.UserPageDTO;
import com.collection.jpa.entity.User;
import com.collection.jpa.util.Page;
import com.collection.jpa.util.ServiceResult;

public interface UserService {
	
//	UserDetails findByUsername(String username);

	Page<UserPageDTO> page(Pageable pageable);
	
	ServiceResult create(User persisted);
	
}
