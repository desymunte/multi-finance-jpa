package com.collection.jpa.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.collection.jpa.dao.RoleRepository;
import com.collection.jpa.entity.Role;


@Repository
@Transactional(readOnly = true)
public class RoleImpl implements RoleService {
	
	@Autowired
	private RoleRepository roleRepository;

	public List<Role> listRole() {
		return roleRepository.findAll();
	}
	
}
