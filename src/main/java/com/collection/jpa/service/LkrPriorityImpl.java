package com.collection.jpa.service;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.collection.jpa.dao.LKRPriorityRepository;
import com.collection.jpa.entity.LKRPriority;


@Repository
@Transactional(readOnly = true)
public class LkrPriorityImpl implements LkrPriorityService {
	
	@PersistenceContext
	private EntityManager entityManager;
	
	@Autowired
	private LKRPriorityRepository repository;
	
	@Override
	public List<LKRPriority> findAll() {
		return repository.findAll();
	}
	
	@Override
    @Transactional
	public LKRPriority insert(LKRPriority persisted) {
		return repository.save(persisted);
	}
	
    @Transactional
	@Modifying(clearAutomatically = true)
	public LKRPriority update(LKRPriority persisted) {
		return repository.save(persisted);
	}

    @Transactional
	public void delete(Integer id) {
		// TODO Auto-generated method stub
//		Long delId = Long.parseLong(id.toString());
		repository.delete(id);
	}
	
//    @Transactional
//	@Modifying
//	public void delete(LkrPriority persisted) {
//		// TODO Auto-generated method stub
//		repository.delete(persisted);
//	}
}
