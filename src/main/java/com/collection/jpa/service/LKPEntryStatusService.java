package com.collection.jpa.service;

import java.util.List;

import com.collection.jpa.entity.LKPEntryStatus;


public interface LKPEntryStatusService {

	List<LKPEntryStatus> listStatus();

}
