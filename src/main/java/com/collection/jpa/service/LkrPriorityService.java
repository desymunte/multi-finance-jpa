package com.collection.jpa.service;

import java.util.List;

import com.collection.jpa.entity.LKRPriority;


public interface LkrPriorityService {

	List<LKRPriority> findAll();

	LKRPriority insert(LKRPriority persisted);
	
//	LkrPriority save(LkrPriority persisted);

}
