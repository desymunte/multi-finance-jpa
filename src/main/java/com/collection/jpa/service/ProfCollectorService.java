package com.collection.jpa.service;

import java.util.List;

import org.springframework.data.domain.Pageable;

import com.collection.jpa.dto.ProfCollMemberDTO;
import com.collection.jpa.dto.ProfCollMouPrintDTO;
import com.collection.jpa.dto.ProfCollectorCriteria;
import com.collection.jpa.dto.ProfCollectorDTO;
import com.collection.jpa.entity.ProfCollector;
import com.collection.jpa.util.Page;
import com.collection.jpa.util.ServiceResult;


public interface ProfCollectorService {

	Page<ProfCollectorDTO> page(ProfCollectorCriteria criteria, Pageable pageable);

	ServiceResult setup(ProfCollector persisted);
	
	List<ProfCollMemberDTO> listActiveProfColl(ProfCollectorCriteria criteria);
	
	ServiceResult update(ProfCollector persisted);
	
	ProfCollMouPrintDTO print(ProfCollector criteria);
}
