package com.collection.jpa.service;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.collection.jpa.dto.OfficeCriteria;
import com.collection.jpa.entity.Office;
import com.collection.jpa.util.Page;


@Repository
@Transactional(readOnly = true)
public class OfficeImpl implements OfficeService {
	
	@PersistenceContext
	private EntityManager entityManager;
	
	private TypedQuery<Office> query(OfficeCriteria criteria) {
		final List<Predicate> predicates = new ArrayList<Predicate>();
		CriteriaBuilder cBuilder = entityManager.getCriteriaBuilder();
		CriteriaQuery<Office> cQuery = cBuilder.createQuery(Office.class);
		Root<Office> root = cQuery.from(Office.class);

		if (criteria == null) {
			return null;
		}
        if (criteria.getId() != null) {
            predicates.add(cBuilder.equal(root.get("id"), criteria.getId()));
        }
        if (criteria.getCode() != null) {
            predicates.add(cBuilder.equal(root.get("code"), criteria.getCode()));
        }
        if (criteria.getName() != null) {
        	predicates.add(cBuilder.like(cBuilder.lower(root.get("name")), "%"+criteria.getName().toLowerCase()+"%"));
        }

        cQuery.select(root)
              .where(cBuilder.and(predicates.toArray(new Predicate[predicates.size()])));

		TypedQuery<Office> query = entityManager.createQuery(cQuery);
		
		return query;
	}

    public Page<Office> page(OfficeCriteria criteria, Pageable pageable) {
    	if (criteria == null) {
    		return new Page<Office>();
    	}
		TypedQuery<Office> query = query(criteria);
		return (Page<Office>) new Page<Office>(query, pageable);
	}
	
}
