package com.collection.jpa.service;

import org.springframework.data.domain.Pageable;

import com.collection.jpa.dto.ChangeSpvParamDTO;
import com.collection.jpa.dto.TeamCriteria;
import com.collection.jpa.dto.TeamDTO;
import com.collection.jpa.entity.DeskcallTeam;
import com.collection.jpa.util.Page;
import com.collection.jpa.util.ServiceResult;


public interface DeskcallTeamService {

	Page<TeamDTO> page(TeamCriteria criteria, Pageable pageable);

	TeamDTO content(TeamCriteria criteria);
	
	ServiceResult changeSpv(ChangeSpvParamDTO persisted);
	
	ServiceResult setup(DeskcallTeam persisted);
}
