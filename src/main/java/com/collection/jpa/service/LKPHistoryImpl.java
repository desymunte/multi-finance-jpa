package com.collection.jpa.service;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.collection.jpa.dao.LKPHistoryRepository;
import com.collection.jpa.dto.ContractCriteria;
import com.collection.jpa.entity.LKPHistory;
import com.collection.jpa.util.Page;


@Repository
@Transactional(readOnly = true)
public class LKPHistoryImpl implements LKPHistoryService {
	
	@PersistenceContext
	private EntityManager entityManager;
	
	@Autowired
	private LKPHistoryRepository repository;
	
	public LKPHistory getOne(Integer id) {
		return repository.getOne(id);
	}

	public Page<LKPHistory> pageByContract(ContractCriteria criteria, Pageable pageable) {
		final List<Predicate> predicates = new ArrayList<Predicate>();

		CriteriaBuilder cBuilder = entityManager.getCriteriaBuilder();
		CriteriaQuery<LKPHistory> cQuery = cBuilder.createQuery(LKPHistory.class);
		Root<LKPHistory> root = cQuery.from(LKPHistory.class);

		if (criteria.getContractNo() == null) {
			return null;
        }

        predicates.add(cBuilder.equal(root.get("bucket").get("contractNo"), criteria.getContractNo()));
		cQuery.select(root).where(cBuilder.and(predicates.toArray(new Predicate[predicates.size()])));

		TypedQuery<LKPHistory> query = entityManager.createQuery(cQuery);
		
		return (Page<LKPHistory>) new Page<LKPHistory>(query, pageable);
	}
	
}
