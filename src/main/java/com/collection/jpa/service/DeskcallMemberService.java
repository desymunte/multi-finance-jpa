package com.collection.jpa.service;

import org.springframework.data.domain.Pageable;

import com.collection.jpa.dto.DeskCallTeamMemberDTO;
import com.collection.jpa.entity.DeskcallMember;
import com.collection.jpa.dto.MemberCriteria;
import com.collection.jpa.util.Page;
import com.collection.jpa.util.ServiceResult;


public interface DeskcallMemberService {

	Page<DeskCallTeamMemberDTO> page(MemberCriteria criteria, Pageable pageable);

	ServiceResult insert(DeskcallMember persisted);

}
