package com.collection.jpa.service;

import java.util.List;

import org.springframework.data.domain.Pageable;

import com.collection.jpa.dto.EmployeeCriteria;
import com.collection.jpa.dto.EmployeeDTO;
import com.collection.jpa.entity.Employee;
import com.collection.jpa.util.Page;


public interface EmployeeService {
	
	Employee findOne(Integer id);
	
	List<EmployeeDTO> listCollectionSpv();

	List<EmployeeDTO> listRemedialSpv();

	List<EmployeeDTO> listDeskcallSpv();

	List<EmployeeDTO> listCollectionStaff();

	List<EmployeeDTO> listRemedialStaff();

	List<EmployeeDTO> listDeskcallStaff();

	List<Employee> list();

	Page<Employee> page(EmployeeCriteria criteria, Pageable pageable);

}
