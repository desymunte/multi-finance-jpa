package com.collection.jpa.service;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.collection.jpa.dao.CollectionWorkingAreaRepository;
import com.collection.jpa.dto.CollectionWorkingAreaCriteria;
import com.collection.jpa.entity.CollectionWorkingArea;
import com.collection.jpa.util.ServiceResult;


@Repository
@Transactional(readOnly = true)
public class CollectionWorkingAreaImpl implements CollectionWorkingAreaService {
	
	@PersistenceContext
	private EntityManager entityManager;
	
	@Autowired
	private CollectionWorkingAreaRepository repository;
	
	public List<CollectionWorkingArea> listOccupied(CollectionWorkingAreaCriteria criteria) {
		final List<Predicate> predicates = new ArrayList<Predicate>();

		CriteriaBuilder cBuilder = entityManager.getCriteriaBuilder();
		CriteriaQuery<CollectionWorkingArea> cQuery = cBuilder.createQuery(CollectionWorkingArea.class);
		Root<CollectionWorkingArea> root = cQuery.from(CollectionWorkingArea.class);

        if (criteria == null || criteria.getCollectionMemberId() == null) {
        	return null;
        }
        predicates.add(cBuilder.equal(root.get("collectionMember").get("id"), criteria.getCollectionMemberId()));
		cQuery.select(root).where(cBuilder.and(predicates.toArray(new Predicate[predicates.size()])));
		TypedQuery<CollectionWorkingArea> query = entityManager.createQuery(cQuery);
		
		return query.getResultList();
	}

/*	public List<Zipcode> listAvailable(ZipcodeAvailableCriteria criteria) {
		final List<Predicate> predicates = new ArrayList<Predicate>();

		CriteriaBuilder cBuilder = entityManager.getCriteriaBuilder();
		CriteriaQuery<Zipcode> cQuery = cBuilder.createQuery(Zipcode.class);
		Root<Zipcode> root = cQuery.from(Zipcode.class);

        if (criteria == null || criteria.getOfficeId() == null || criteria.getZipcodeId() == null || criteria.getCycleId() == null ) {
        	return null;
        }
        predicates.add(cBuilder.equal(root.get("office").get("id"), criteria.getOfficeId()));
        //Filtered by zipcodeId
        //Filtered by cycleId
		cQuery.select(root).where(cBuilder.and(predicates.toArray(new Predicate[predicates.size()])));
		TypedQuery<Zipcode> query = entityManager.createQuery(cQuery);
		
		return query.getResultList();
	}
*/
    @Transactional
	private ServiceResult save(CollectionWorkingArea persisted) {
		return new ServiceResult(ServiceResult.MessageType.SUCCESS, "", repository.save(persisted));
	}
	
    @Transactional
	public ServiceResult insert(CollectionWorkingArea persisted) {
		return this.save(persisted);
	}
	
    @Transactional
	public ServiceResult delete(Integer id) {
		repository.delete(id);
		return new ServiceResult(ServiceResult.MessageType.SUCCESS, "", null);
	}
	
}
