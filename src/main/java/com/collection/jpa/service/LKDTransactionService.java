package com.collection.jpa.service;

import java.util.List;

import com.collection.jpa.dto.LkdCriteria;
import com.collection.jpa.dto.LkdDataDTO;
import com.collection.jpa.dto.LkdPrintDTO;
import com.collection.jpa.dto.LkdHeaderDTO;
import com.collection.jpa.entity.LKDEntryStatus;
import com.collection.jpa.entity.LKDTransaction;
import com.collection.jpa.util.ServiceResult;


public interface LKDTransactionService {

	List<LkdPrintDTO> listPrint();
	
	LkdHeaderDTO headerPrint();
	
	ServiceResult setPrinted(LkdHeaderDTO header, List<LkdPrintDTO> listPersisted);
	
	ServiceResult entry(LKDTransaction persisted);

	LkdDataDTO contract(LkdCriteria criteria);
	
	List<LKDEntryStatus> listStatus();
}
