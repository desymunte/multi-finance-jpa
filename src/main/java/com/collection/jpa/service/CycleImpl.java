package com.collection.jpa.service;

import java.util.List;

import javax.persistence.TypedQuery;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.collection.jpa.dao.CycleRepository;
import com.collection.jpa.dto.CycleCriteria;
import com.collection.jpa.dto.CycleDTO;
import com.collection.jpa.entity.Cycle;
import com.collection.jpa.entity.CycleType;
import com.collection.jpa.util.Page;
import com.collection.jpa.util.ServiceResult;


@Repository
@Transactional(readOnly = true)
public class CycleImpl implements CycleService {
	
	@Autowired
	private CycleRepository repository;
	
	@Autowired
	private QueryImpl queryService;

	
    public Page<CycleDTO> page(CycleCriteria criteria, Pageable pageable) {
    	if (criteria == null) {
    		return new Page<CycleDTO>();
    	}
		TypedQuery<CycleDTO> query = queryService.cycle(criteria);
		return (Page<CycleDTO>) new Page<CycleDTO>(query, pageable);
	}

    public Cycle content(CycleCriteria criteria) {
    	if (criteria == null || criteria.getId() == null) {
    		return null;
    	}
		Cycle cont = repository.findOne(criteria.getId());
		return cont;
	}

	@Transactional
	@Modifying(clearAutomatically = true)
    @PreAuthorize("hasRole('ROLE_SUPERADMIN')")
    public ServiceResult update(Cycle persisted) {
//        CustomUserDetails user = (CustomUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
//        if (!user.hasRole("ROLE_SUPERADMIN")) {
//        	return new ServiceResult(ServiceResult.MessageType.ALERT, "Not authorized user", null);
//        }

		if (persisted == null || persisted.getId() == null) {
			return new ServiceResult(ServiceResult.MessageType.ERROR, "Cycle not found", null);
		}
		Cycle cycle = repository.findOne(persisted.getId());
        if (cycle == null) {
			return new ServiceResult(ServiceResult.MessageType.ERROR, "Cycle not found", null);
        }

        if (persisted.getOverdueStart() == null) {
        	persisted.setOverdueStart(cycle.getOverdueStart());
        }
        if (persisted.getOverdueEnd() == null) {
        	persisted.setOverdueEnd(cycle.getOverdueEnd());
        }
        if (persisted.getOverdueStart() > persisted.getOverdueEnd()) {
			return new ServiceResult(ServiceResult.MessageType.ERROR, "Please arrange overdue (days) correctly", null);
        }

        if (persisted.getCode() != null && !persisted.getCode().isEmpty()) {
        	if (!(cycle.getType().getType().equalsIgnoreCase("R") || cycle.getType().getType().equalsIgnoreCase("D"))) {
            	cycle.setCode(persisted.getCode());
        	}
        }
        if (persisted.getName() != null && !persisted.getName().isEmpty()) {
        	cycle.setName(persisted.getName());
        }
        if (persisted.getDescription() != null && !persisted.getDescription().isEmpty()) {
        	cycle.setDescription(persisted.getDescription());
        }
        if (persisted.getOverdueStart() != null) {
        	cycle.setOverdueStart(persisted.getOverdueStart());
        }
        if (persisted.getOverdueEnd() != null) {
        	cycle.setOverdueEnd(persisted.getOverdueEnd());
        }
        
        //leave Cycle Type as is; cannot be updated

        try {
			repository.save(cycle);
		} catch (Exception e) {
    		return new ServiceResult(ServiceResult.MessageType.ERROR, "Fail to update Cycle parameter", null);
		}

		return new ServiceResult(ServiceResult.MessageType.SUCCESS, "Cycle has been successfully modified", cycle.getId());
	}

	@Transactional
	@Modifying(clearAutomatically = true)
    @PreAuthorize("hasRole('ROLE_SUPERADMIN')")
    public ServiceResult insert(Cycle persisted) {
//        CustomUserDetails user = (CustomUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
//        if (!user.hasRole("ROLE_SUPERADMIN")) {
//        	return new ServiceResult(ServiceResult.MessageType.ALERT, "Not authorized user", null);
//        }

        if (persisted.getCode() == null || persisted.getCode().isEmpty()) {
        	return new ServiceResult(ServiceResult.MessageType.ERROR, "Please fill Cycle Code", null);
        }
        if (persisted.getName() == null || persisted.getName().isEmpty()) {
        	return new ServiceResult(ServiceResult.MessageType.ERROR, "Please fill Cycle Name", null);
        }
        if (persisted.getDescription() == null || persisted.getDescription().isEmpty()) {
        	return new ServiceResult(ServiceResult.MessageType.ERROR, "Please fill Cycle Description", null);
        }
        if (persisted.getOverdueStart() == null) {
        	return new ServiceResult(ServiceResult.MessageType.ERROR, "Please fill Cycle Start Overdue (D-day)", null);
        }
        if (persisted.getOverdueEnd() == null) {
        	return new ServiceResult(ServiceResult.MessageType.ERROR, "Please fill Cycle End Overdue (D-day)", null);
        }
        if (persisted.getOverdueStart() > persisted.getOverdueEnd()) {
			return new ServiceResult(ServiceResult.MessageType.ERROR, "Please arrange overdue (days) correctly", null);
        }
        CycleType type = new CycleType();
        type.setType("C");
        persisted.setType(type);
/*
        if (persisted.getType().getType() == null) {
			return new ServiceResult(ServiceResult.MessageType.ERROR, "Please select cycle type", null);
        }
*/
		try {
			persisted.setId(null);
			repository.save(persisted);
		} catch (Exception e) {
    		return new ServiceResult(ServiceResult.MessageType.ERROR, "Fail to insert new Cycle parameter", null);
		}

		return new ServiceResult(ServiceResult.MessageType.SUCCESS, "Cycle has been successfully inserted", null);
	}

	@Transactional
	@Modifying(clearAutomatically = true)
    @PreAuthorize("hasRole('ROLE_SUPERADMIN')")
    public ServiceResult delete(Cycle persisted) {
//        CustomUserDetails user = (CustomUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
//        if (!user.hasRole("ROLE_SUPERADMIN")) {
//        	return new ServiceResult(ServiceResult.MessageType.ALERT, "Not authorized user", null);
//        }

		Cycle cycle = repository.findOne(persisted.getId());
        if (cycle == null) {
			return new ServiceResult(ServiceResult.MessageType.ERROR, "Cycle not found", null);
        }
        switch (cycle.getType().getType()) {
        case "C":
        	Integer numCollectionTeam = queryService.numCollectionTeam(cycle.getId());
        	if (numCollectionTeam > 0) {
        		return new ServiceResult(ServiceResult.MessageType.ERROR, "Cycle cannot be deleted because still has team", null);
        	}
        	break;
/*
        case "R":
        	Integer numRemedialTeam = queryService.numRemedialTeam(cycle.getId());
        	if (numRemedialTeam > 0) {
        		return new ServiceResult(ServiceResult.MessageType.ERROR, "Cycle cannot be deleted because still has team", null);
        	}
        	break;

        case "D":
        	Integer numDeskcallTeam = queryService.numDeskcallTeam(cycle.getId());
        	if (numDeskcallTeam > 0) {
        		return new ServiceResult(ServiceResult.MessageType.ERROR, "Cycle cannot be deleted because still has team", null);
        	}
        	break;
*/
        default:
    		return new ServiceResult(ServiceResult.MessageType.INFO, "Cycle can be deleted for Collection cycle only", null);
        }

		try {
			repository.delete(cycle);
		} catch (Exception e) {
    		return new ServiceResult(ServiceResult.MessageType.ERROR, "Fail to delete cycle", null);
		}

		return new ServiceResult(ServiceResult.MessageType.SUCCESS, "Cycle has been successfully deleted", null);
	}

	public List<CycleDTO> listCollection() {
		CycleCriteria criteria = new CycleCriteria();
		criteria.setType("C");
		TypedQuery<CycleDTO> query = queryService.cycle(criteria);
		return query.getResultList();
	}

	public List<CycleDTO> listRemedial() {
		CycleCriteria criteria = new CycleCriteria();
		criteria.setCode("RMD");
		criteria.setType("R");
		TypedQuery<CycleDTO> query = queryService.cycle(criteria);
		return query.getResultList();
	}

	public List<CycleDTO> listDeskcall() {
		CycleCriteria criteria = new CycleCriteria();
		criteria.setCode("DSC");
		criteria.setType("D");
		TypedQuery<CycleDTO> query = queryService.cycle(criteria);
		return query.getResultList();
	}

}
