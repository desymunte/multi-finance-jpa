package com.collection.jpa.service;

import org.springframework.data.domain.Pageable;

import com.collection.jpa.dto.DormantDTO;
import com.collection.jpa.dto.OfficeCriteria;
import com.collection.jpa.entity.CollectionDormant;
import com.collection.jpa.entity.CollectionMember;
import com.collection.jpa.util.Page;
import com.collection.jpa.util.ServiceResult;


public interface ChangeCollectorService {

	ServiceResult assign(CollectionDormant dormant);
	
	ServiceResult revoke(CollectionMember member);
	
//	Page<CollectionDormant> pageDormant(OfficeCriteria criteria, Pageable pageable);
	
	Page<DormantDTO> pageDormant(OfficeCriteria criteria, Pageable pageable);
	
}
