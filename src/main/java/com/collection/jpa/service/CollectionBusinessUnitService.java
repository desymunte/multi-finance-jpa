package com.collection.jpa.service;

import java.util.List;

import com.collection.jpa.entity.CollectionBusinessUnit;


public interface CollectionBusinessUnitService {

	List<CollectionBusinessUnit> findAll();

	CollectionBusinessUnit insert(CollectionBusinessUnit persisted);
	

}
