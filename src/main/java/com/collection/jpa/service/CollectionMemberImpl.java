package com.collection.jpa.service;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.persistence.TypedQuery;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.collection.jpa.CustomUserDetails;
import com.collection.jpa.dao.CollectionMemberRepository;
import com.collection.jpa.dao.CollectionTeamRepository;
import com.collection.jpa.dto.CollectionMemberDTO;
import com.collection.jpa.dto.TeamCriteria;
import com.collection.jpa.dto.TeamDTO;
import com.collection.jpa.dto.MemberCriteria;
import com.collection.jpa.entity.CollectionMember;
import com.collection.jpa.entity.CollectionTeam;
import com.collection.jpa.entity.CollectionWorkingArea;
import com.collection.jpa.util.Page;
import com.collection.jpa.util.ServiceResult;


@Repository
@Transactional(readOnly = true)
public class CollectionMemberImpl implements CollectionMemberService {
	
	@Autowired
	private CollectionMemberRepository repository;
	
	@Autowired
	private CollectionTeamRepository teamrepository;
	
	@Autowired
	private QueryImpl queryService;
	

    @PreAuthorize("hasRole('ROLE_SUPERADMIN') or hasRole('ROLE_COLLECTION_HEAD') or hasRole('ROLE_COLLECTION_SPV')")
	public Page<CollectionMemberDTO> page(MemberCriteria criteria, Pageable pageable) {
        CustomUserDetails user = (CustomUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        if (criteria == null) {
        	return new Page<CollectionMemberDTO>();
        }
        criteria.setOfficeId(user.getOfficeId());

        if (user.getJobCode().equals("COLLSPV")) {
            TeamCriteria teamcriteria = new TeamCriteria();
            teamcriteria.setSpvId(user.getEmplId());
            List<TeamDTO> teams = queryService.collectionTeam(teamcriteria).getResultList();
            List<Integer> teamIdList = new ArrayList<Integer>();
            for (TeamDTO team : teams) {
                teamIdList.add(team.getId());
            }
        	criteria.setTeamIdList(teamIdList);
    	}

        TypedQuery<CollectionMemberDTO> query = queryService.collectionMember(criteria);
        
		return (Page<CollectionMemberDTO>) new Page<CollectionMemberDTO>(query, pageable);
	}

	@Transactional
    @PreAuthorize("hasRole('ROLE_COLLECTION_HEAD') or hasRole('ROLE_COLLECTION_SPV')")
	public ServiceResult insert(CollectionMember persisted) {
        CustomUserDetails user = (CustomUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

    	if (persisted == null ||
    		persisted.getId() != null ||
    		persisted.getCollectionTeam().getId() == null ||
    		persisted.getEmployee().getId() == null) {
    		return new ServiceResult(ServiceResult.MessageType.ERROR, "Invalid Collection Member data", null);
    	}
    	
    	CollectionTeam team = teamrepository.findOne(persisted.getCollectionTeam().getId());
    	if (team == null ||
    		team.getSupervisor().getId() != user.getEmplId()) {
    		return new ServiceResult(ServiceResult.MessageType.ERROR, "Invalid Collection Team data", null);
    	}

    	MemberCriteria criteria = new MemberCriteria(); 
    	criteria.setEmplId(persisted.getEmployee().getId());
		TypedQuery<CollectionMemberDTO> query = queryService.collectionMember(criteria);
		List<CollectionMemberDTO> results = query.getResultList();
		boolean exist = !results.isEmpty();
		if (exist) {
			return new ServiceResult(ServiceResult.MessageType.ERROR, "Staff already assigned as a Collector", null);
		}
		
		CollectionMember member = new CollectionMember();
		member.setCollectionTeam(team);
		member.setEmployee(persisted.getEmployee());
		member.setDailyLoad(persisted.getDailyLoad());
		member.setMonthlyLoad(persisted.getMonthlyLoad());

		for (Iterator<CollectionWorkingArea> itr = persisted.getCollectionWorkingAreas().iterator(); itr.hasNext();) { 
			CollectionWorkingArea area = itr.next();
			member.addCollectionWorkingArea(area);
    	}

		try {
			repository.save(member);
		} catch (Exception excp) {
    		return new ServiceResult(ServiceResult.MessageType.ERROR, "Fail to add Collection Member", null);
		}

		return new ServiceResult(ServiceResult.MessageType.SUCCESS, "New Collection Member successfully added", member.getId());
	}
	
//    @PreAuthorize("hasRole('ROLE_COLLECTION_SPV')")
//	public MemberDTO member(StaffCriteria criteria) {
//        CustomUserDetails user = (CustomUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
//
//    	MemberDTO result = new MemberDTO();
//
//    	if (criteria.getStaffId() != null) {
//            MemberCriteria newcriteria = new MemberCriteria(); 
//            newcriteria.setEmplId(criteria.getStaffId());
//            newcriteria.setSupervisorId(user.getEmplId());
//
//        	TypedQuery<MemberDTO> query = queryService.collectionMember(newcriteria);
//
//        	try {
//    			result = query.getSingleResult();
//    		} catch (Exception excp) {
//    		}
//        }
//
//    	return result;
//	}

}
