package com.collection.jpa.service;

import java.util.List;

import com.collection.jpa.dto.CycleCriteria;
import com.collection.jpa.dto.MemberCriteria;
import com.collection.jpa.dto.ZipDTO;


public interface ZipcodeService {
	
	List<ZipDTO> listCollection(CycleCriteria criteria);
	
	List<ZipDTO> listRemedial(CycleCriteria criteria);

	List<ZipDTO> listCollectionMember(MemberCriteria criteria);
	
}
