package com.collection.jpa.service;

import java.util.List;

import com.collection.jpa.entity.LKREntryStatus;


public interface LKREntryStatusService {

	List<LKREntryStatus> listStatus();

}
