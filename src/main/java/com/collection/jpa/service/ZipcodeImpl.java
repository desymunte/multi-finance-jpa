package com.collection.jpa.service;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.TypedQuery;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.collection.jpa.CustomUserDetails;
import com.collection.jpa.dto.AreaCriteria;
import com.collection.jpa.dto.CycleCriteria;
import com.collection.jpa.dto.MemberCriteria;
import com.collection.jpa.dto.OfficeCycleCriteria;
import com.collection.jpa.dto.ZipDTO;


@Repository
@Transactional(readOnly = true)
public class ZipcodeImpl implements ZipcodeService {
	
	@Autowired
	private QueryImpl queryService;

	public List<ZipDTO> listCollection(CycleCriteria criteria) {
        CustomUserDetails user = (CustomUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        
        if (criteria.getId() == null) {
        	return null;
        }
        OfficeCycleCriteria newcriteria = new OfficeCycleCriteria();
        newcriteria.setOfficeId(user.getOfficeId());
        newcriteria.setCycleId(criteria.getId());
        
		AreaCriteria areacriteria = new AreaCriteria();
		areacriteria.setOfficeId(newcriteria.getOfficeId());
		areacriteria.setCycleId(newcriteria.getCycleId());
		
		List<ZipDTO> listData = queryService.collectionArea(areacriteria).getResultList();
		List<Integer> theList = new ArrayList<Integer>();
        for (ZipDTO zip: listData) {
            theList.add(zip.getId());
        }
		
		TypedQuery<ZipDTO> query = queryService.zipcodeAvailable(newcriteria, theList);
		return query.getResultList();
	}
	
	public List<ZipDTO> listRemedial(CycleCriteria criteria) {
        CustomUserDetails user = (CustomUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        
        if (criteria.getId() == null) {
        	return null;
        }
        OfficeCycleCriteria newcriteria = new OfficeCycleCriteria();
        newcriteria.setOfficeId(user.getOfficeId());
        newcriteria.setCycleId(criteria.getId());
        
		AreaCriteria areacriteria = new AreaCriteria();
		areacriteria.setOfficeId(newcriteria.getOfficeId());
		areacriteria.setCycleId(newcriteria.getCycleId());
		
		List<ZipDTO> listData = queryService.remedialArea(areacriteria).getResultList();
		List<Integer> theList = new ArrayList<Integer>();
        for (ZipDTO zip: listData) {
            theList.add(zip.getId());
        }
		
		TypedQuery<ZipDTO> query = queryService.zipcodeAvailable(newcriteria, theList);
		return query.getResultList();
	}
	
	public List<ZipDTO> listCollectionMember(MemberCriteria criteria) {
        CustomUserDetails user = (CustomUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        
        if (criteria.getMemberId() == null) {
        	return null;
        }
        criteria.setOfficeId(user.getOfficeId());
        
		TypedQuery<ZipDTO> query = queryService.zipcodeCollectionMember(criteria);
		return query.getResultList();
	}
	
}
