package com.collection.jpa.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.collection.jpa.dao.GeneralParameterRepository;
import com.collection.jpa.entity.GeneralParameter;
import com.collection.jpa.util.ServiceResult;


@Repository
@Transactional(readOnly = true)
public class GeneralParameterImpl implements GeneralParameterService {
	
	@Autowired
	private GeneralParameterRepository repository;
	

	public GeneralParameter content() {
        GeneralParameter genparam = repository.findOne(1);
		return genparam;
	}
	
    @Transactional
	@Modifying(clearAutomatically = true)
    @PreAuthorize("hasRole('ROLE_SUPERADMIN')")
	public ServiceResult update(GeneralParameter persisted) {
        persisted.setId(1);
		if (persisted.getCompany_name() == null || persisted.getCompany_name().isEmpty()) {
	    	return new ServiceResult(ServiceResult.MessageType.ERROR, "Please fill Company Name", null);
		}
		if (persisted.getCompany_address() == null || persisted.getCompany_address().isEmpty()) {
	    	return new ServiceResult(ServiceResult.MessageType.ERROR, "Please fill Company Address", null);
		}
		if (persisted.getLkp_max_entry_days() == null) {
	    	return new ServiceResult(ServiceResult.MessageType.ERROR, "Please fill LKP Maximum Entry Days", null);
		}
		if (persisted.getSkpc_validity_period() == null) {
	    	return new ServiceResult(ServiceResult.MessageType.ERROR, "Please fill SKPC Validity Period in Days", null);
		}
		if (persisted.getSkpc_extended_validity_period() == null) {
	    	return new ServiceResult(ServiceResult.MessageType.ERROR, "Please fill Extended SKPC Validity Period in Days", null);
		}
		if (persisted.getSkpc_max_idle_days() == null) {
	    	return new ServiceResult(ServiceResult.MessageType.ERROR, "Please fill SKPC Maximum Idle Days", null);
		}
		if (persisted.getSkpc_max_extend() == null) {
	    	return new ServiceResult(ServiceResult.MessageType.ERROR, "Please fill SKPC Maximum Extend", null);
		}
		if (persisted.getSkpc_max_outstanding_load() == null) {
	    	return new ServiceResult(ServiceResult.MessageType.ERROR, "Please fill SKPC Maximum Outstanding Load", null);
		}
		if (persisted.getMou_min_performance() == null) {
	    	return new ServiceResult(ServiceResult.MessageType.ERROR, "Please fill MOU Minimum Performance (0-100) %", null);
		}

		try {
			repository.save(persisted);
		} catch (Exception e) {
    		return new ServiceResult(ServiceResult.MessageType.ERROR, "Fail to update General Parameter", null);
		}
    	return new ServiceResult(ServiceResult.MessageType.SUCCESS, "General Parameter successfully updated", null);
	}
}
