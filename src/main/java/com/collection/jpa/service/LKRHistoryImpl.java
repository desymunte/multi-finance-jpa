package com.collection.jpa.service;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.collection.jpa.dao.LKRHistoryRepository;
import com.collection.jpa.dto.ContractCriteria;
import com.collection.jpa.entity.LKRHistory;
import com.collection.jpa.util.Page;


@Repository
@Transactional(readOnly = true)
public class LKRHistoryImpl implements LKRHistoryService {
	
	@PersistenceContext
	private EntityManager entityManager;
	
	@Autowired
	private LKRHistoryRepository repository;
	
	public LKRHistory getOne(Integer id) {
		return repository.getOne(id);
	}

	public Page<LKRHistory> pageByContract(ContractCriteria criteria, Pageable pageable) {
		final List<Predicate> predicates = new ArrayList<Predicate>();

		CriteriaBuilder cBuilder = entityManager.getCriteriaBuilder();
		CriteriaQuery<LKRHistory> cQuery = cBuilder.createQuery(LKRHistory.class);
		Root<LKRHistory> root = cQuery.from(LKRHistory.class);

		if (criteria.getContractNo() == null) {
			return null;
        }

        predicates.add(cBuilder.equal(root.get("bucket").get("contractNo"), criteria.getContractNo()));
		cQuery.select(root).where(cBuilder.and(predicates.toArray(new Predicate[predicates.size()])));

		TypedQuery<LKRHistory> query = entityManager.createQuery(cQuery);
		
		return (Page<LKRHistory>) new Page<LKRHistory>(query, pageable);
	}
	
}
