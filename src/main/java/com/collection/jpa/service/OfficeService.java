package com.collection.jpa.service;

import org.springframework.data.domain.Pageable;

import com.collection.jpa.dto.OfficeCriteria;
import com.collection.jpa.entity.Office;
import com.collection.jpa.util.Page;


public interface OfficeService {

	Page<Office> page(OfficeCriteria criteria, Pageable pageable);

}
