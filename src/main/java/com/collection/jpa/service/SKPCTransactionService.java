package com.collection.jpa.service;

import org.springframework.data.domain.Pageable;

import com.collection.jpa.dto.SkpcCriteria;
import com.collection.jpa.dto.SkpcPageDTO;
import com.collection.jpa.dto.SkpcPrintDTO;
import com.collection.jpa.entity.SKPCTransaction;
import com.collection.jpa.util.Page;
import com.collection.jpa.util.ServiceResult;


public interface SKPCTransactionService {

	Page<SkpcPageDTO> page(SkpcCriteria criteria, Pageable pageable);
	
	ServiceResult insert(SkpcCriteria criteria);
	
	SkpcPrintDTO getContent(SkpcCriteria criteria);
	
	SkpcPageDTO getSkpcContract(SkpcCriteria criteria);

	ServiceResult updateSKPCEntry(SKPCTransaction persisted);
	
}
