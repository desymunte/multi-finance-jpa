package com.collection.jpa.service;

import java.time.LocalDate;
import java.util.Calendar;
import java.util.Date;

import javax.persistence.TypedQuery;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.collection.jpa.CustomUserDetails;
import com.collection.jpa.dao.GeneralParameterRepository;
import com.collection.jpa.dao.OfficeRepository;
import com.collection.jpa.dao.PrintActivityRepository;
import com.collection.jpa.dao.ProfCollectionMemberRepository;
import com.collection.jpa.dao.SKPCTransactionRepository;
import com.collection.jpa.dao.StatusRepository;
import com.collection.jpa.dto.SkpcCriteria;
import com.collection.jpa.dto.SkpcPageDTO;
import com.collection.jpa.dto.SkpcPrintDTO;
import com.collection.jpa.entity.SKPCTransaction;
import com.collection.jpa.entity.SKPCTransactionDetail;
import com.collection.jpa.entity.GeneralParameter;
import com.collection.jpa.entity.Office;
import com.collection.jpa.entity.PrintActivity;
import com.collection.jpa.entity.ProfCollector;
import com.collection.jpa.entity.ProfCollMember;
import com.collection.jpa.util.FPrint;
import com.collection.jpa.util.Page;
import com.collection.jpa.util.ServiceResult;


@Repository
@Transactional(readOnly = true)
public class SKPCTransactionImpl implements SKPCTransactionService {
	
	@Autowired
	private SKPCTransactionRepository repository;
	
	@Autowired
	private GeneralParameterRepository paramrepository;
	
	@Autowired
	private ProfCollectionMemberRepository pcrepository;
	
	@Autowired
	private PrintActivityRepository printrepository;
	
	@Autowired
	private StatusRepository statusrepository;
	
	@Autowired
	private OfficeRepository offrepository;
	
	@Autowired
	private QueryImpl queryService;

	@Autowired
	private FPrint fprint;

    @PreAuthorize("hasRole('ROLE_SUPERADMIN') or hasRole('ROLE_REMEDIAL_HEAD') or hasRole('ROLE_REMEDIAL_SPV')")
	public Page<SkpcPageDTO> page(SkpcCriteria criteria, Pageable pageable) {
        CustomUserDetails user = (CustomUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

		if (criteria.getStaffId() == null) {
			return new Page<SkpcPageDTO>();
	    }
		criteria.setOfficeCode(user.getOfficeCode());
		
		TypedQuery<SkpcPageDTO> query = queryService.skpc(criteria);
		return (Page<SkpcPageDTO>) new Page<SkpcPageDTO>(query, pageable);
	}
	
    @Transactional
	@Modifying(clearAutomatically = true)
    @PreAuthorize("hasRole('ROLE_REMEDIAL_SPV')")
	public ServiceResult insert(SkpcCriteria criteria) {
        CustomUserDetails user = (CustomUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        if (!(user.getJobCode().equals("REMSPV"))) {
			return new ServiceResult(ServiceResult.MessageType.ERROR, "Not authorized user", null);
        }

        if (criteria.getStaffId() == null) {
    		return new ServiceResult(ServiceResult.MessageType.ERROR, "Proffesional Collector required", null);
		}
		if (criteria.getId() == null) {
			return new ServiceResult(ServiceResult.MessageType.ERROR, "SKPC required", null);
    	}
		criteria.setOfficeCode(user.getOfficeCode());
		criteria.setType(null);
		criteria.setPrintNo(null);
    	TypedQuery<SkpcPageDTO> query = queryService.skpc(criteria);

		SkpcPageDTO skpc = new SkpcPageDTO();
		try {
			skpc = query.getSingleResult();
		} catch (Exception e) {
    		return new ServiceResult(ServiceResult.MessageType.ERROR, "SKPC not found", null);
		}

		if (skpc.getStatus() == 2) {
    		return new ServiceResult(ServiceResult.MessageType.ERROR, "SKPC not active / expired", null);
    	}
    	if (skpc.getStatus() == 4) {
    		return new ServiceResult(ServiceResult.MessageType.ERROR, "SKPC close", null);
    	}

		LocalDate today = LocalDate.now();
    	Date startDate = java.sql.Date.valueOf(today);
    	Date endDate = startDate;

    	ProfCollMember staf = pcrepository.findOne(criteria.getStaffId());
		if (staf == null) {
    		return new ServiceResult(ServiceResult.MessageType.ERROR, "Proffesional Collector not found", null);
		}
		if (staf.getProfCollection().getOffice().getCode().compareTo(user.getOfficeCode()) != 0) {
    		return new ServiceResult(ServiceResult.MessageType.ERROR, "Proffesional Collector office is not appropriate with SKPC office", null);
		}

		ProfCollector profcoll = staf.getProfCollection();

    	if (profcoll == null) {
    		return new ServiceResult(ServiceResult.MessageType.ERROR, "MOU not found", null);
    	}

    	if (profcoll.getMouStatus().getId() != 1 ||
    		profcoll.getMouStartDate().compareTo(startDate) == 1 ||
    		profcoll.getMouEndDate().compareTo(startDate) == -1) {
        	return new ServiceResult(ServiceResult.MessageType.ERROR, "MOU expired / inactive", null);
        }
        	
    	GeneralParameter params = paramrepository.findOne(1);

    	Integer sequenceNo = 0;
    	if (skpc.getMouNo() != null) { // EXTEND
//    		if (skpc.getMouId() != mou.getId()) {
//	    		return new ServiceResult(ServiceResult.MessageType.ERROR, "Data integration error, please check", null);
//    		}
    		if (skpc.getAssignedTo() != criteria.getStaffId()) { // different professional collector, allowed / not?
	    		return new ServiceResult(ServiceResult.MessageType.ERROR, "Data integration error, please check", null);
    		}

    		if (skpc.getSkpcSeqNo() >= params.getSkpc_max_extend()) {
    			return new ServiceResult(ServiceResult.MessageType.ERROR, "Reach maximum extend", null);
    		}
    		
    		if (!(skpc.getSkpcEndDate().compareTo(startDate) == -1)) {
	    		return new ServiceResult(ServiceResult.MessageType.ERROR, "Current SKPC still active", null);
    		}

        	Calendar calendar = Calendar.getInstance();
        	calendar.setTime(skpc.getSkpcEndDate());
        	calendar.add(Calendar.DATE, params.getSkpc_max_idle_days());
        	Date idleDate = calendar.getTime();
			if (idleDate.compareTo(startDate) == -1) {
	    		return new ServiceResult(ServiceResult.MessageType.ERROR, "Already exceeded idle days", null);
    		}

    		sequenceNo = skpc.getSkpcSeqNo() + 1;
        	endDate = java.sql.Date.valueOf(today.plusDays(params.getSkpc_extended_validity_period()-1));
    	}

    	else { // NEW
        	if (queryService.hasPCMaxLoadReached(profcoll.getId())) {
        		return new ServiceResult(ServiceResult.MessageType.ERROR, "Has reached the maximum load", null);
        	}
        	
    		endDate = java.sql.Date.valueOf(today.plusDays(params.getSkpc_validity_period()-1));
    	}

    	SKPCTransaction master = repository.findOne(criteria.getId());
    	master.setAssignedTo(staf.getId());
    	master.setProfcollId(profcoll.getId());
    	master.setStatus(statusrepository.findOne(1)); // active
 
    	if (sequenceNo == 0) {
        	master.setMouNo(profcoll.getMouNo());

    		Date printDate = new Date();
            String printNo = fprint.getNextPrintNo("SKPC", String.format("%05d", criteria.getStaffId()));

            PrintActivity print = new PrintActivity();
        	print.setPrintNo(printNo);
        	print.setPrintDate(printDate);
        	print.setEmployeeId(criteria.getStaffId());
        	print.setTeamId(profcoll.getId());
        	print.setType("P");
            PrintActivity result = printrepository.save(print);

            master.setPrintActivity(result);
    	}
    	
    	SKPCTransactionDetail detail = new SKPCTransactionDetail();
    	detail.setSequenceNo(sequenceNo);
    	detail.setStartDate(startDate);
    	detail.setEndDate(endDate);
    	master.setSequenceNo(sequenceNo);
    	master.addDetail(detail);

    	repository.save(master);
    	return new ServiceResult(ServiceResult.MessageType.SUCCESS, String.format("SKPC for Contract #%s created successfully", master.getBucket().getContractNo()), master.getId());
	}

    @PreAuthorize("hasRole('ROLE_REMEDIAL_SPV')")
	public SkpcPrintDTO getContent(SkpcCriteria criteria) {
        CustomUserDetails user = (CustomUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        if (criteria.getId() == null) {
			return null;
		}
		criteria.setOfficeCode(user.getOfficeCode());
		criteria.setStaffId(null);
		criteria.setType(null);
		criteria.setPrintNo(null);
 		TypedQuery<SkpcPageDTO> query = queryService.skpc(criteria);

		SkpcPageDTO skpc = new SkpcPageDTO();
		try {
			skpc = query.getSingleResult();
		} catch (Exception e) {
    		return null;
		}
		SkpcPrintDTO content = new SkpcPrintDTO();
		content.setSkpcPrintNo(skpc.getPrintNo());
		content.setMouNo(skpc.getMouNo());
		Integer seqNo = skpc.getSkpcSeqNo();
		if (seqNo > 0) {
			content.setSkpcPrintNo(skpc.getPrintNo() + "-" + String.format("%02d", seqNo));
		}

		content.setSkpcDate(skpc.getSkpcDate());
		content.setSkpcStartDate(skpc.getSkpcStartDate());
		content.setSkpcEndDate(skpc.getSkpcEndDate());
		
		ProfCollMember staf = pcrepository.findOne(skpc.getAssignedTo());
		ProfCollector profcoll = staf.getProfCollection();
		String profcollName = staf.getName();
		String profcollAddress = staf.getAddress();
		if (profcoll.getType() == 1) {
			profcollName = profcollName + " / " + profcoll.getName();
			profcollAddress = profcoll.getAddress();
		}
		content.setProfcollName(profcollName);
		content.setProfcollAddress(profcollAddress);
		content.setProfcollIdCardNo(staf.getIdCardNo());

		SKPCTransaction trx = repository.findOne(skpc.getId());
		content.setContractNo(skpc.getContractNo());
		content.setContractDate(trx.getSkpcTransDetails().get(0).getStartDate());
		
		content.setCustName(skpc.getCustName());
		content.setCustBillingAddress(skpc.getCustBillingAddress());

		content.setBrand(skpc.getBrand());
		content.setModel(skpc.getModel());
		content.setColor(skpc.getColor());
		content.setFrameNo(skpc.getFrameNo());
		content.setEngineNo(skpc.getEngineNo());
		content.setPoliceNo(skpc.getPoliceNo());
		content.setStnk(skpc.getStnk());

		Office office = offrepository.findOne(user.getOfficeId());
		content.setOfficeName(office.getName());
		content.setOfficeAddress(office.getAddress());
		content.setOfficePicName(office.getPicName());
		
		return content;
	}

    @PreAuthorize("hasRole('ROLE_SUPERADMIN') or hasRole('ROLE_REMEDIAL_HEAD') or hasRole('ROLE_REMEDIAL_SPV')")
	public SkpcPageDTO getSkpcContract(SkpcCriteria criteria) {
        CustomUserDetails user = (CustomUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

		if (criteria.getPrintNo() == null) {
			return null;
		}
		criteria.setOfficeCode(user.getOfficeCode());
		criteria.setId(null);
		criteria.setOfficeCode(null);
		criteria.setStaffId(null);
		criteria.setType(null);
		TypedQuery<SkpcPageDTO> query = queryService.skpc(criteria);

		SkpcPageDTO skpc = new SkpcPageDTO();
		try {
			skpc = query.getSingleResult();
		} catch (Exception e) {
    		return null;
		}
		Integer seqNo = skpc.getSkpcSeqNo();
		if (seqNo > 0) {
			skpc.setPrintNo(skpc.getPrintNo() + "-" + String.format("%02d", seqNo));
		}
		return skpc;
	}

    @Transactional
	@Modifying(clearAutomatically = true)
	public ServiceResult updateSKPCEntry(SKPCTransaction persisted) {
        CustomUserDetails user = (CustomUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

    	SKPCTransaction old = repository.findOne(persisted.getId());
    	
    	if (old == null) {
    		return new ServiceResult(ServiceResult.MessageType.ERROR, "SKPC not found", null);
    	}

    	if (old.getBucket().getOfficeCode() != user.getOfficeCode()) {
			return new ServiceResult(ServiceResult.MessageType.ERROR, "Not authorized user", null);
    	}

    	if (old.getPrintActivity() == null) {
    		return new ServiceResult(ServiceResult.MessageType.ERROR, "SKPC has not been printed yet", old.getId());
    	}

    	if (old.getResult() != null) {
    		return new ServiceResult(ServiceResult.MessageType.ERROR, "SKPC already done", old.getId());
    	}
    	old.setResult(persisted.getResult());
    	old.setStatus(statusrepository.findOne(4)); // close
    	if (persisted.getResult() == 0) { // fail, create available SKPC
        	SKPCTransaction newSkpc = new SKPCTransaction();
        	newSkpc.setBucket(old.getBucket());
        	newSkpc.setRunDate(new Date());
        	newSkpc.setStatus(statusrepository.findOne(0));
    		repository.save(newSkpc);
    	} 
    	else { // result = 1 = success
    		old.setRefNo(persisted.getRefNo());
    	}
    	
    	repository.save(old);
    	return new ServiceResult(ServiceResult.MessageType.SUCCESS, "SKPC result entry saved", null);
	}

}