package com.collection.jpa.service;

import java.util.List;

import org.springframework.data.domain.Pageable;

import com.collection.jpa.dto.LkpTeamPageDTO;
import com.collection.jpa.dto.SpecialDelegationDTO;
import com.collection.jpa.dto.LkpCriteria;
import com.collection.jpa.dto.LkpHeaderDTO;
import com.collection.jpa.dto.LkpPageDTO;
import com.collection.jpa.dto.LkpPrintDTO;
import com.collection.jpa.dto.LkpPrintPageDTO;
import com.collection.jpa.dto.LkpCyclePageDTO;
import com.collection.jpa.dto.PrintCriteria;
import com.collection.jpa.dto.ReclusteringDTO;
import com.collection.jpa.dto.StaffCriteria;
import com.collection.jpa.entity.LKPTransaction;
import com.collection.jpa.entity.PrintActivity;
import com.collection.jpa.util.Page;
import com.collection.jpa.util.ServiceResult;


public interface LKPTransactionService {

	Page<LkpCyclePageDTO> pageManualAssignment(Pageable pageable);

	Page<LkpTeamPageDTO> pageClustering(Pageable pageable);

	ServiceResult assign(LKPTransaction persisted);
	
	List<LkpPrintDTO> listPrint(StaffCriteria criteria);

	LkpHeaderDTO headerPrint(StaffCriteria criteria);
	
	ServiceResult setPrinted(LkpHeaderDTO header, List<LkpPrintDTO> listData);

	Page<PrintActivity> pageReprint(StaffCriteria criteria, Pageable pageable);
	
	Page<LkpPrintPageDTO> pageReprintDetail(PrintCriteria criteria, Pageable pageable);
	
	List<LkpPrintDTO> listReprint(PrintCriteria criteria);
	
	LkpPageDTO contract(LkpCriteria criteria);
	
	ServiceResult entry(LKPTransaction persisted);

	Page<LkpPrintPageDTO> pageReclustering(StaffCriteria criteria, Pageable pageable);
	
	ServiceResult recluster(ReclusteringDTO data);
	
	Page<LkpPageDTO> pageSpecialDelegation(Pageable pageable);

	ServiceResult delegate(SpecialDelegationDTO data);
}
