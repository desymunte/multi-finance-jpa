package com.collection.jpa.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.collection.jpa.dao.CollectionDormantRepository;
import com.collection.jpa.dao.CollectionMemberRepository;
import com.collection.jpa.dto.TeamDTO;
import com.collection.jpa.dto.DormantDTO;
import com.collection.jpa.dto.OfficeCriteria;
import com.collection.jpa.entity.CollectionDormant;
import com.collection.jpa.entity.CollectionMember;
import com.collection.jpa.entity.CollectionTeam;
import com.collection.jpa.entity.Cycle;
import com.collection.jpa.entity.Employee;
import com.collection.jpa.entity.Office;
import com.collection.jpa.util.Page;
import com.collection.jpa.util.ServiceResult;


@Repository
@Transactional(readOnly = true)
public class ChangeCollectorImpl implements ChangeCollectorService {
	
	@PersistenceContext
	private EntityManager entityManager;
	
	@Autowired
	private CollectionMemberRepository repository;
	
	@Autowired
	private CollectionDormantImpl repositoryDormant;
	

//	public Page<CollectionDormant> pageDormant(OfficeCriteria criteria, Pageable pageable) {
//	final List<Predicate> predicates = new ArrayList<Predicate>();
//
//	CriteriaBuilder cBuilder = entityManager.getCriteriaBuilder();
//	CriteriaQuery<CollectionDormant> cQuery = cBuilder.createQuery(CollectionDormant.class);
//	Root<CollectionDormant> root = cQuery.from(CollectionDormant.class);
//
//    predicates.add(cBuilder.equal(root.get("employee").get("office").get("id"), criteria.getId()));
//    predicates.add(cBuilder.isNull(root.get("nextMember")));
//	cQuery.select(root).where(cBuilder.and(predicates.toArray(new Predicate[predicates.size()])));
//
//	TypedQuery<CollectionDormant> query = entityManager.createQuery(cQuery);
//	
//	return (Page<CollectionDormant>) new Page<CollectionDormant>(query, pageable);
//}

	@PreAuthorize("hasRole('ROLE_SUPERADMIN') or hasRole('ROLE_COLLECTION_HEAD') or hasRole('ROLE_COLLECTION_SPV')")
	public Page<DormantDTO> pageDormant(OfficeCriteria criteria, Pageable pageable) {
		final List<Predicate> predicates = new ArrayList<Predicate>();
	
		CriteriaBuilder cBuilder = entityManager.getCriteriaBuilder();
		CriteriaQuery<DormantDTO> cQuery = cBuilder.createQuery(DormantDTO.class);
		Root<CollectionDormant> root = cQuery.from(CollectionDormant.class);
		Join<CollectionDormant,Employee> e = root.join("employee", JoinType.LEFT);
		Join<CollectionDormant,CollectionMember> pm = root.join("previousMember", JoinType.LEFT);
		Join<CollectionMember,Employee> pmE = pm.join("employee", JoinType.LEFT);
		Join<CollectionDormant,CollectionMember> nm = root.join("nextMember", JoinType.LEFT);
		Join<CollectionMember,Employee> nmE = nm.join("employee", JoinType.LEFT);
	
	    predicates.add(cBuilder.equal(root.get("employee").get("office").get("id"), criteria.getId()));
	    predicates.add(cBuilder.isNull(root.get("nextMember")));
	    
	    cQuery.select(cBuilder.construct(DormantDTO.class, 
				root.get("id"), 
				root.get("dateIn"), 
				root.get("dateOut"),
				e.get("id"), 
				e.get("name"), 
				pm.get("id"), 
				pmE.get("name"), 
				nm.get("id"), 
				nmE.get("name")
			));
		cQuery.where(cBuilder.and(predicates.toArray(new Predicate[predicates.size()])));
	
		TypedQuery<DormantDTO> query = entityManager.createQuery(cQuery);
		
		return (Page<DormantDTO>) new Page<DormantDTO>(query, pageable);
	}

	@Transactional
	@Modifying(clearAutomatically = true)
    @PreAuthorize("hasRole('ROLE_COLLECTION_HEAD') or hasRole('ROLE_COLLECTION_SPV')")
    public ServiceResult assign(CollectionDormant dormant) {
    	CollectionMember member = repository.findOne(dormant.getNextMember().getId());

    	if (member.getEmployee() != null) {
    		return new ServiceResult(ServiceResult.MessageType.ERROR, "Assignment not allowed, collector should be revoked first", member.getId());
    	}
//    	CollectionDormant d = repository.findOne(dormant.getNextMember().getId());

    	member.setEmployee(dormant.getEmployee());
//    	entityManager.persist(member);
    	repository.save(member);


    	CollectionDormant d = repositoryDormant.getOne(dormant.getId());
    	d.setNextMember(dormant.getNextMember());
    	d.setDateOut(new Date());
//    	entityManager.persist(dormant);
//    	entityManager.flush();
    	repositoryDormant.update(d);
    	
    	return new ServiceResult(ServiceResult.MessageType.SUCCESS, "", null);
	}

    @Transactional
	@Modifying(clearAutomatically = true)
    @PreAuthorize("hasRole('ROLE_COLLECTION_HEAD') or hasRole('ROLE_COLLECTION_SPV')")
    public ServiceResult revoke(CollectionMember member) {
    	CollectionMember cm = repository.findOne(member.getId());

    	CollectionDormant dormant = new CollectionDormant();
    	dormant.setPreviousMember(cm);
    	dormant.setEmployee(cm.getEmployee());
    	dormant.setDateIn(new Date());
//    	entityManager.persist(dormant);
    	repositoryDormant.insert(dormant);
    	
    	cm.setEmployee(null);
    	repository.save(cm);
    	
    	return new ServiceResult(ServiceResult.MessageType.SUCCESS, "", null);
	}

    @Transactional
	@Modifying(clearAutomatically = true)
    @PreAuthorize("hasRole('ROLE_COLLECTION_HEAD') or hasRole('ROLE_COLLECTION_SPV')")
    public ServiceResult assignBaru(CollectionMember cm) {
    	CollectionMember member = repository.findOne(cm.getId());

    	if (member.getEmployee() != null) {
    		return new ServiceResult(ServiceResult.MessageType.ERROR, "Assignment not allowed, collector should be revoked first", member.getId());
    	}
//    	CollectionDormant d = repository.findOne(dormant.getNextMember().getId());

    	member.setEmployee(cm.getEmployee());
//    	entityManager.persist(member);
    	repository.save(member);
    	
    	return new ServiceResult(ServiceResult.MessageType.SUCCESS, "", member.getId());
	}
	
}
