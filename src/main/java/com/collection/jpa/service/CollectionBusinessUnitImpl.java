package com.collection.jpa.service;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.collection.jpa.dao.CollectionBusinessUnitRepository;
import com.collection.jpa.entity.CollectionBusinessUnit;


@Repository
@Transactional(readOnly = true)
public class CollectionBusinessUnitImpl implements CollectionBusinessUnitService {
	
	@PersistenceContext
	private EntityManager entityManager;
	
	@Autowired
	private CollectionBusinessUnitRepository repository;
	
	@Override
	public List<CollectionBusinessUnit> findAll() {
		return repository.findAll();
	}
	
	@Override
    @Transactional
	public CollectionBusinessUnit insert(CollectionBusinessUnit persisted) {
		return repository.save(persisted);
	}
	
    @Transactional
	@Modifying(clearAutomatically = true)
	public CollectionBusinessUnit update(CollectionBusinessUnit persisted) {
		return repository.save(persisted);
	}

    @Transactional
	public void delete(Integer id) {
		repository.delete(id);
	}
	
}
