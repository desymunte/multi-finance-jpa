package com.collection.jpa.service;

import com.collection.jpa.entity.GeneralParameter;
import com.collection.jpa.util.ServiceResult;


public interface GeneralParameterService {

	GeneralParameter content();

	ServiceResult update(GeneralParameter persisted);

}
