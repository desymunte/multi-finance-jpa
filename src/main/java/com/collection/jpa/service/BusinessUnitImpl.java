package com.collection.jpa.service;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.collection.jpa.dao.BusinessUnitRepository;
import com.collection.jpa.dto.BusinessUnitCriteria;
import com.collection.jpa.entity.BusinessUnit;
import com.collection.jpa.util.Page;


@Repository
@Transactional(readOnly = true)
public class BusinessUnitImpl implements BusinessUnitService {
	
	@PersistenceContext
	private EntityManager entityManager;
	
	@Autowired
	private BusinessUnitRepository repository;
	
	public List<BusinessUnit> findAll() {
		return repository.findAll();
	}

	public Page<BusinessUnit> findAll(BusinessUnitCriteria criteria) {
		return findAll(criteria, null);
	}
	
	public Page<BusinessUnit> findAll(BusinessUnitCriteria criteria, PageRequest pageRequest) {
		final List<Predicate> predicates = new ArrayList<Predicate>();

		CriteriaBuilder cBuilder = entityManager.getCriteriaBuilder();
		CriteriaQuery<BusinessUnit> cQuery = cBuilder.createQuery(BusinessUnit.class);
		Root<BusinessUnit> root = cQuery.from(BusinessUnit.class);

        if(criteria.getCode()!=null) {
            predicates.add(cBuilder.equal(root.get("code"), criteria.getCode()));
        }
        if(criteria.getName()!=null) {
            predicates.add(cBuilder.like(cBuilder.lower(root.get("name")), "%"+criteria.getName().toLowerCase()+"%"));
        }

		cQuery.select(root).where(cBuilder.and(predicates.toArray(new Predicate[predicates.size()])));

		TypedQuery<BusinessUnit> query = entityManager.createQuery(cQuery);
		
		return (Page<BusinessUnit>) new Page<BusinessUnit>(query, pageRequest);
	}
	
	public BusinessUnit getOne(Integer businessUnitId) {
		return repository.getOne(businessUnitId);
	}

/*	@Override
    @Transactional
	public BusinessUnit insert(BusinessUnit persisted) {
		return repository.save(persisted);
	}
	
    @Transactional
	@Modifying(clearAutomatically = true)
	public BusinessUnit update(BusinessUnit persisted) {
    	
		return repository.save(persisted);
	}

    @Transactional
	public void delete(Integer id) {
		repository.delete(id);
	}
*/	
}
