package com.collection.jpa.service;

import java.util.List;

import com.collection.jpa.entity.RemedialWorkingArea;


public interface RemedialWorkingAreaService {

	List<RemedialWorkingArea> findAll();

	RemedialWorkingArea insert(RemedialWorkingArea persisted);
	public void deleteList(Integer empId);
	
//	RemedialWorkingArea save(RemedialWorkingArea persisted);

}
