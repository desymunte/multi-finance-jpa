package com.collection.jpa.service;

import java.util.List;

import org.springframework.data.domain.PageRequest;

import com.collection.jpa.dto.BusinessUnitCriteria;
import com.collection.jpa.entity.BusinessUnit;
import com.collection.jpa.util.Page;


public interface BusinessUnitService {

	List<BusinessUnit> findAll();

	Page<BusinessUnit> findAll(BusinessUnitCriteria criteria);

	Page<BusinessUnit> findAll(BusinessUnitCriteria criteria, PageRequest pageRequest);

	BusinessUnit getOne(Integer businessUnitId);
	
/*	BusinessUnit insert(BusinessUnit persisted);
	
	BusinessUnit update(BusinessUnit persisted);

	void delete(Integer cycleId);
*/
}
