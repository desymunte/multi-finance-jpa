package com.collection.jpa.service;

import java.util.List;

import org.springframework.data.domain.Pageable;

import com.collection.jpa.dto.CycleCriteria;
import com.collection.jpa.dto.CycleDTO;
import com.collection.jpa.entity.Cycle;
import com.collection.jpa.util.Page;
import com.collection.jpa.util.ServiceResult;


public interface CycleService {

	Page<CycleDTO> page(CycleCriteria criteria, Pageable pageable);
	
	Cycle content(CycleCriteria persisted);
	
	ServiceResult update(Cycle persisted);
	
	ServiceResult insert(Cycle persisted);
	
	ServiceResult delete(Cycle persisted);
	
	List<CycleDTO> listCollection();
	
	List<CycleDTO> listRemedial();

	List<CycleDTO> listDeskcall();

}
