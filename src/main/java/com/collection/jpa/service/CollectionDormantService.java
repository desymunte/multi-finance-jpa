package com.collection.jpa.service;

import com.collection.jpa.entity.CollectionDormant;
import com.collection.jpa.util.ServiceResult;


public interface CollectionDormantService {

	CollectionDormant getOne(Integer id);
	ServiceResult insert(CollectionDormant persisted);
	
	ServiceResult update(CollectionDormant persisted);
}
