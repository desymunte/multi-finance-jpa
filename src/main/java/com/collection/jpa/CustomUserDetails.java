package com.collection.jpa;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import com.collection.jpa.entity.Role;
import com.collection.jpa.entity.User;

public class CustomUserDetails implements UserDetails {
	
	private Integer id;

	private String username;
	
	private String password;
	
	private Integer emplId;
	
	private String jobCode;
	
	private Integer jobLevel;
	
	private String jobName;
	
	private Integer officeId;
	
	private String officeCode;
	

	Collection<? extends GrantedAuthority> authorities;
	
	public CustomUserDetails(User findByUsername) {
		this.id = findByUsername.getId();
		this.username = findByUsername.getUsername();
		this.password = findByUsername.getPassword();
		this.emplId = findByUsername.getEmployee().getId();
		this.jobCode = findByUsername.getEmployee().getJobTitle().getCode();
		this.jobLevel = findByUsername.getEmployee().getJobTitle().getLevel();
		this.jobName = findByUsername.getEmployee().getJobTitle().getName();
		this.officeId = findByUsername.getEmployee().getOffice().getId();
		this.officeCode = findByUsername.getEmployee().getOffice().getCode();
		
		List<GrantedAuthority> auths = new ArrayList<>();
		for(Role role: findByUsername.getRoles()) {
			auths.add(new SimpleGrantedAuthority(role.getName().toUpperCase()));
		}
		this.authorities = auths;
	}
	
	public CustomUserDetails() {
		super();
	}

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		// TODO Auto-generated method stub
		return authorities;
	}

	@Override
	public boolean isAccountNonExpired() {
		// TODO Auto-generated method stub
//		return false;
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		// TODO Auto-generated method stub
//		return false;
		return true;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		// TODO Auto-generated method stub
//		return false;
		return true;
	}

	@Override
	public boolean isEnabled() {
		// TODO Auto-generated method stub
//		return false;
		return true;
	}

	@Override
	public String getUsername() {
		// TODO Auto-generated method stub
		return username;
	}

	@Override
	public String getPassword() {
		// TODO Auto-generated method stub
		return password;
	}

	public void setPassword(String pass) {
		this.password = pass;
	}
		
	public Integer getEmplId() {
		return emplId;
	}

	public void setEmplId(Integer emplId) {
		this.emplId = emplId;
	}

	public Integer getOfficeId() {
		return officeId;
	}

	public void setOfficeId(Integer officeId) {
		this.officeId = officeId;
	}

	public String getOfficeCode() {
		return officeCode;
	}

	public void setOfficeCode(String officeCode) {
		this.officeCode = officeCode;
	}

	public String getJobCode() {
		return jobCode;
	}

	public void setJobCode(String jobCode) {
		this.jobCode = jobCode;
	}
	
	public Integer getJobLevel() {
		return jobLevel;
	}

	public void setJobLevel(Integer jobLevel) {
		this.jobLevel = jobLevel;
	}

	public String getJobName() {
		return jobName;
	}

	public void setJobName(String jobName) {
		this.jobName = jobName;
	}

	public boolean hasRole(String roleName) {
        SimpleGrantedAuthority role = new SimpleGrantedAuthority(roleName);
		return this.authorities.contains(role);
	}

}
