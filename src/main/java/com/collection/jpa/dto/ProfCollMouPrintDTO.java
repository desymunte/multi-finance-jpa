package com.collection.jpa.dto;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Temporal;
import javax.persistence.TemporalType;

public class ProfCollMouPrintDTO implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
    private Integer id;      
	private String mouNo;
    @Temporal(TemporalType.DATE)
	private Date mouDate;  
    @Temporal(TemporalType.DATE)
	private Date startDate;  
    @Temporal(TemporalType.DATE)
	private Date endDate;  
	private String pcName;  
	private String pcCode;
	private String pcIdNo;
	private String pcAddress;  
	private String pcPhone;  
	private String pcEmail; 
	private String officeName; 
	private String officeCode;
	private String officeAddress;
	private Double skpcFee;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getMouNo() {
		return mouNo;
	}

	public void setMouNo(String mouNo) {
		this.mouNo = mouNo;
	}

	public Date getMouDate() {
		return mouDate;
	}

	public void setMouDate(Date mouDate) {
		this.mouDate = mouDate;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public String getPcName() {
		return pcName;
	}

	public void setPcName(String pcName) {
		this.pcName = pcName;
	}

	public String getPcCode() {
		return pcCode;
	}

	public void setPcCode(String pcCode) {
		this.pcCode = pcCode;
	}

	public String getPcIdNo() {
		return pcIdNo;
	}

	public void setPcIdNo(String pcIdNo) {
		this.pcIdNo = pcIdNo;
	}

	public String getPcAddress() {
		return pcAddress;
	}

	public void setPcAddress(String pcAddress) {
		this.pcAddress = pcAddress;
	}

	public String getPcPhone() {
		return pcPhone;
	}

	public void setPcPhone(String pcPhone) {
		this.pcPhone = pcPhone;
	}

	public String getPcEmail() {
		return pcEmail;
	}

	public void setPcEmail(String pcEmail) {
		this.pcEmail = pcEmail;
	}

	public String getOfficeName() {
		return officeName;
	}

	public void setOfficeName(String officeName) {
		this.officeName = officeName;
	}

	public String getOfficeCode() {
		return officeCode;
	}

	public void setOfficeCode(String officeCode) {
		this.officeCode = officeCode;
	}

	public String getOfficeAddress() {
		return officeAddress;
	}

	public void setOfficeAddress(String officeAddress) {
		this.officeAddress = officeAddress;
	}

	public Double getSkpcFee() {
		return skpcFee;
	}

	public void setSkpcFee(Double skpcFee) {
		this.skpcFee = skpcFee;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public ProfCollMouPrintDTO() {
		super();
		// TODO Auto-generated constructor stub
	}

	public ProfCollMouPrintDTO(Integer id, String mouNo, Date mouDate, Date startDate, Date endDate, String pcName,
			String pcCode, String pcIdNo, String pcAddress, String pcPhone, String pcEmail, String officeName,
			String officeCode, String officeAddress, Double skpcFee) {
		super();
		this.id = id;
		this.mouNo = mouNo;
		this.mouDate = mouDate;
		this.startDate = startDate;
		this.endDate = endDate;
		this.pcName = pcName;
		this.pcCode = pcCode;
		this.pcIdNo = pcIdNo;
		this.pcAddress = pcAddress;
		this.pcPhone = pcPhone;
		this.pcEmail = pcEmail;
		this.officeName = officeName;
		this.officeCode = officeCode;
		this.officeAddress = officeAddress;
		this.skpcFee = skpcFee;
	}

	@Override
	public String toString() {
		return "ProfCollMouPrintDTO [id=" + id + ", mouNo=" + mouNo + ", mouDate=" + mouDate + ", startDate="
				+ startDate + ", endDate=" + endDate + ", pcName=" + pcName + ", pcCode=" + pcCode + ", pcIdNo="
				+ pcIdNo + ", pcAddress=" + pcAddress + ", pcPhone=" + pcPhone + ", pcEmail=" + pcEmail
				+ ", officeName=" + officeName + ", officeCode=" + officeCode + ", officeAddress=" + officeAddress
				+ ", skpcFee=" + skpcFee + "]";
	}

}
