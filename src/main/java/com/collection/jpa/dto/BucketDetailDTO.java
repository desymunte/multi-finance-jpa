package com.collection.jpa.dto;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Temporal;
import javax.persistence.TemporalType;

public class BucketDetailDTO implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private Integer id;
	private int installmentNo;
	private double principalAmnt;
	private double interestAmnt;
    @Temporal(TemporalType.DATE)
    private Date dueDate;
    private Integer overdue;
	private double principalAmntPaid;
	private double interestAmntPaid;
    @Temporal(TemporalType.DATE)
	private Date paymentDate;
	private double penaltyAmnt;
	private double penaltyAmntPaid;
    @Temporal(TemporalType.DATE)
	private Date penaltyPaymentDate;
	private double collectionFee;
	private double collectionFeePaid;
    @Temporal(TemporalType.DATE)
	private Date collectionFeePaymentDate;

    public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public int getInstallmentNo() {
		return installmentNo;
	}

	public void setInstallmentNo(int installmentNo) {
		this.installmentNo = installmentNo;
	}

	public double getPrincipalAmnt() {
		return principalAmnt;
	}

	public void setPrincipalAmnt(double principalAmnt) {
		this.principalAmnt = principalAmnt;
	}

	public double getInterestAmnt() {
		return interestAmnt;
	}

	public void setInterestAmnt(double interestAmnt) {
		this.interestAmnt = interestAmnt;
	}

	public Date getDueDate() {
		return dueDate;
	}

	public void setDueDate(Date dueDate) {
		this.dueDate = dueDate;
	}

	public Integer getOverdue() {
		return overdue;
	}

	public void setOverdue(Integer overdue) {
		this.overdue = overdue;
	}

	public double getPrincipalAmntPaid() {
		return principalAmntPaid;
	}

	public void setPrincipalAmntPaid(double principalAmntPaid) {
		this.principalAmntPaid = principalAmntPaid;
	}

	public double getInterestAmntPaid() {
		return interestAmntPaid;
	}

	public void setInterestAmntPaid(double interestAmntPaid) {
		this.interestAmntPaid = interestAmntPaid;
	}

	public Date getPaymentDate() {
		return paymentDate;
	}

	public void setPaymentDate(Date paymentDate) {
		this.paymentDate = paymentDate;
	}

	public double getPenaltyAmnt() {
		return penaltyAmnt;
	}

	public void setPenaltyAmnt(double penaltyAmnt) {
		this.penaltyAmnt = penaltyAmnt;
	}

	public double getPenaltyAmntPaid() {
		return penaltyAmntPaid;
	}

	public void setPenaltyAmntPaid(double penaltyAmntPaid) {
		this.penaltyAmntPaid = penaltyAmntPaid;
	}

	public Date getPenaltyPaymentDate() {
		return penaltyPaymentDate;
	}

	public void setPenaltyPaymentDate(Date penaltyPaymentDate) {
		this.penaltyPaymentDate = penaltyPaymentDate;
	}

	public double getCollectionFee() {
		return collectionFee;
	}

	public void setCollectionFee(double collectionFee) {
		this.collectionFee = collectionFee;
	}

	public double getCollectionFeePaid() {
		return collectionFeePaid;
	}

	public void setCollectionFeePaid(double collectionFeePaid) {
		this.collectionFeePaid = collectionFeePaid;
	}

	public Date getCollectionFeePaymentDate() {
		return collectionFeePaymentDate;
	}

	public void setCollectionFeePaymentDate(Date collectionFeePaymentDate) {
		this.collectionFeePaymentDate = collectionFeePaymentDate;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public BucketDetailDTO() {
		super();
		// TODO Auto-generated constructor stub
	}

	public BucketDetailDTO(Integer id, int installmentNo, double principalAmnt, double interestAmnt, Date dueDate,
			Integer overdue, double principalAmntPaid, double interestAmntPaid, Date paymentDate, double penaltyAmnt,
			double penaltyAmntPaid, Date penaltyPaymentDate, double collectionFee, double collectionFeePaid,
			Date collectionFeePaymentDate) {
		super();
		this.id = id;
		this.installmentNo = installmentNo;
		this.principalAmnt = principalAmnt;
		this.interestAmnt = interestAmnt;
		this.dueDate = dueDate;
		this.overdue = overdue;
		this.principalAmntPaid = principalAmntPaid;
		this.interestAmntPaid = interestAmntPaid;
		this.paymentDate = paymentDate;
		this.penaltyAmnt = penaltyAmnt;
		this.penaltyAmntPaid = penaltyAmntPaid;
		this.penaltyPaymentDate = penaltyPaymentDate;
		this.collectionFee = collectionFee;
		this.collectionFeePaid = collectionFeePaid;
		this.collectionFeePaymentDate = collectionFeePaymentDate;
	}

	@Override
	public String toString() {
		return "BucketDetailDTO [id=" + id + ", installmentNo=" + installmentNo + ", principalAmnt=" + principalAmnt
				+ ", interestAmnt=" + interestAmnt + ", dueDate=" + dueDate + ", overdue=" + overdue
				+ ", principalAmntPaid=" + principalAmntPaid + ", interestAmntPaid=" + interestAmntPaid
				+ ", paymentDate=" + paymentDate + ", penaltyAmnt=" + penaltyAmnt + ", penaltyAmntPaid="
				+ penaltyAmntPaid + ", penaltyPaymentDate=" + penaltyPaymentDate + ", collectionFee=" + collectionFee
				+ ", collectionFeePaid=" + collectionFeePaid + ", collectionFeePaymentDate=" + collectionFeePaymentDate
				+ "]";
	}

}
