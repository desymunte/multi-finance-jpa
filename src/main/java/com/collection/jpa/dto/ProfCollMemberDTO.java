package com.collection.jpa.dto;

import java.io.Serializable;

public class ProfCollMemberDTO implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
    private Integer id;
    private Integer type;
    private String profcollName;  
    private Integer staffId;
    private String staffName;

    public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public String getProfcollName() {
		return profcollName;
	}

	public void setProfcollName(String profcollName) {
		this.profcollName = profcollName;
	}

	public Integer getStaffId() {
		return staffId;
	}

	public void setStaffId(Integer staffId) {
		this.staffId = staffId;
	}

	public String getStaffName() {
		return staffName;
	}

	public void setStaffName(String staffName) {
		this.staffName = staffName;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public ProfCollMemberDTO() {
		super();
		// TODO Auto-generated constructor stub
	}

	public ProfCollMemberDTO(Integer id, Integer type, String profcollName, Integer staffId, String staffName) {
		super();
		this.id = id;
		this.type = type;
		this.profcollName = profcollName;
		this.staffId = staffId;
		this.staffName = staffName;
	}

	@Override
	public String toString() {
		return "ProfCollMemberDTO [id=" + id + ", type=" + type + ", profcollName=" + profcollName + ", staffId="
				+ staffId + ", staffName=" + staffName + "]";
	}

}
