package com.collection.jpa.dto;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.collection.jpa.entity.Bucket;
import com.collection.jpa.entity.SKPCTransaction;
import com.collection.jpa.entity.SKPCTransactionDetail;
import com.collection.jpa.entity.Status;
import com.collection.jpa.entity.Unit;

public class SkpcPageDTO implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private Integer id;
    private Integer assignedTo;
    private String contractNo;
    private String custName;
    private String custBillingAddress;
    private String phone1;
    private String phone2;
    private int topInstallment;
    private double outstandingPrincipalAmnt;
    private double outstandingInterestAmnt;
    private double monthlyInstallment;
    @Temporal(TemporalType.DATE)
    private Date leastDueDate;
    private int leastInstallmentNo;
    @Temporal(TemporalType.DATE)
    private Date lastDueDate;
    private int lastInstallmentNo;
    private int overdue;
    private String businessUnit;
    private String brand;
    private String model;
    private String color;
    private String frameNo;
    private String engineNo;
    private String policeNo;
    private String stnk;
    private Integer result;
    private Integer status;
    private String statusDescr;
	private String mouNo;  
	private Integer skpcSeqNo;
	private Date skpcDate;
	private Date skpcStartDate;
	private Date skpcEndDate;
	private Integer printId;
	private String printNo;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getAssignedTo() {
		return assignedTo;
	}

	public void setAssignedTo(Integer assignedTo) {
		this.assignedTo = assignedTo;
	}

	public String getContractNo() {
		return contractNo;
	}

	public void setContractNo(String contractNo) {
		this.contractNo = contractNo;
	}

	public String getCustName() {
		return custName;
	}

	public void setCustName(String custName) {
		this.custName = custName;
	}

	public String getCustBillingAddress() {
		return custBillingAddress;
	}

	public void setCustBillingAddress(String custBillingAddress) {
		this.custBillingAddress = custBillingAddress;
	}

	public String getPhone1() {
		return phone1;
	}

	public void setPhone1(String phone1) {
		this.phone1 = phone1;
	}

	public String getPhone2() {
		return phone2;
	}

	public void setPhone2(String phone2) {
		this.phone2 = phone2;
	}

	public int getTopInstallment() {
		return topInstallment;
	}

	public void setTopInstallment(int topInstallment) {
		this.topInstallment = topInstallment;
	}

	public double getOutstandingPrincipalAmnt() {
		return outstandingPrincipalAmnt;
	}

	public void setOutstandingPrincipalAmnt(double outstandingPrincipalAmnt) {
		this.outstandingPrincipalAmnt = outstandingPrincipalAmnt;
	}

	public double getOutstandingInterestAmnt() {
		return outstandingInterestAmnt;
	}

	public void setOutstandingInterestAmnt(double outstandingInterestAmnt) {
		this.outstandingInterestAmnt = outstandingInterestAmnt;
	}

	public double getMonthlyInstallment() {
		return monthlyInstallment;
	}

	public void setMonthlyInstallment(double monthlyInstallment) {
		this.monthlyInstallment = monthlyInstallment;
	}

	public Date getLeastDueDate() {
		return leastDueDate;
	}

	public void setLeastDueDate(Date leastDueDate) {
		this.leastDueDate = leastDueDate;
	}

	public int getLeastInstallmentNo() {
		return leastInstallmentNo;
	}

	public void setLeastInstallmentNo(int leastInstallmentNo) {
		this.leastInstallmentNo = leastInstallmentNo;
	}

	public Date getLastDueDate() {
		return lastDueDate;
	}

	public void setLastDueDate(Date lastDueDate) {
		this.lastDueDate = lastDueDate;
	}

	public int getLastInstallmentNo() {
		return lastInstallmentNo;
	}

	public void setLastInstallmentNo(int lastInstallmentNo) {
		this.lastInstallmentNo = lastInstallmentNo;
	}

	public int getOverdue() {
		return overdue;
	}

	public void setOverdue(int overdue) {
		this.overdue = overdue;
	}

	public String getBusinessUnit() {
		return businessUnit;
	}

	public void setBusinessUnit(String businessUnit) {
		this.businessUnit = businessUnit;
	}

	public String getBrand() {
		return brand;
	}

	public void setBrand(String brand) {
		this.brand = brand;
	}

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public String getFrameNo() {
		return frameNo;
	}

	public void setFrameNo(String frameNo) {
		this.frameNo = frameNo;
	}

	public String getEngineNo() {
		return engineNo;
	}

	public void setEngineNo(String engineNo) {
		this.engineNo = engineNo;
	}

	public String getPoliceNo() {
		return policeNo;
	}

	public void setPoliceNo(String policeNo) {
		this.policeNo = policeNo;
	}

	public String getStnk() {
		return stnk;
	}

	public void setStnk(String stnk) {
		this.stnk = stnk;
	}

	public Integer getResult() {
		return result;
	}

	public void setResult(Integer result) {
		this.result = result;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getStatusDescr() {
		return statusDescr;
	}

	public void setStatusDescr(String statusDescr) {
		this.statusDescr = statusDescr;
	}

	public String getMouNo() {
		return mouNo;
	}

	public void setMouNo(String mouNo) {
		this.mouNo = mouNo;
	}

	public Integer getSkpcSeqNo() {
		return skpcSeqNo;
	}

	public void setSkpcSeqNo(Integer skpcSeqNo) {
		this.skpcSeqNo = skpcSeqNo;
	}

	public Date getSkpcDate() {
		return skpcDate;
	}

	public void setSkpcDate(Date skpcDate) {
		this.skpcDate = skpcDate;
	}

	public Date getSkpcStartDate() {
		return skpcStartDate;
	}

	public void setSkpcStartDate(Date skpcStartDate) {
		this.skpcStartDate = skpcStartDate;
	}

	public Date getSkpcEndDate() {
		return skpcEndDate;
	}

	public void setSkpcEndDate(Date skpcEndDate) {
		this.skpcEndDate = skpcEndDate;
	}

	public Integer getPrintId() {
		return printId;
	}

	public void setPrintId(Integer printId) {
		this.printId = printId;
	}

	public String getPrintNo() {
		return printNo;
	}

	public void setPrintNo(String printNo) {
		this.printNo = printNo;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public SkpcPageDTO() {
		super();
		// TODO Auto-generated constructor stub
	}

	public SkpcPageDTO(Integer id, Integer assignedTo, String contractNo, String custName, String custBillingAddress,
			String phone1, String phone2, int topInstallment, double outstandingPrincipalAmnt,
			double outstandingInterestAmnt, double monthlyInstallment, Date leastDueDate, int leastInstallmentNo,
			Date lastDueDate, int lastInstallmentNo, int overdue, String businessUnit, String brand, String model,
			String color, String frameNo, String engineNo, String policeNo, String stnk, Integer result, Integer status,
			String statusDescr, String mouNo, Integer skpcSeqNo, Date skpcDate, Date skpcStartDate, Date skpcEndDate,
			Integer printId, String printNo) {
		super();
		this.id = id;
		this.assignedTo = assignedTo;
		this.contractNo = contractNo;
		this.custName = custName;
		this.custBillingAddress = custBillingAddress;
		this.phone1 = phone1;
		this.phone2 = phone2;
		this.topInstallment = topInstallment;
		this.outstandingPrincipalAmnt = outstandingPrincipalAmnt;
		this.outstandingInterestAmnt = outstandingInterestAmnt;
		this.monthlyInstallment = monthlyInstallment;
		this.leastDueDate = leastDueDate;
		this.leastInstallmentNo = leastInstallmentNo;
		this.lastDueDate = lastDueDate;
		this.lastInstallmentNo = lastInstallmentNo;
		this.overdue = overdue;
		this.businessUnit = businessUnit;
		this.brand = brand;
		this.model = model;
		this.color = color;
		this.frameNo = frameNo;
		this.engineNo = engineNo;
		this.policeNo = policeNo;
		this.stnk = stnk;
		this.result = result;
		this.status = status;
		this.statusDescr = statusDescr;
		this.mouNo = mouNo;
		this.skpcSeqNo = skpcSeqNo;
		this.skpcDate = skpcDate;
		this.skpcStartDate = skpcStartDate;
		this.skpcEndDate = skpcEndDate;
		this.printId = printId;
		this.printNo = printNo;
	}

}
