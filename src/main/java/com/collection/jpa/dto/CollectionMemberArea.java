package com.collection.jpa.dto;

import java.util.List;

import com.collection.jpa.entity.CollectionMember;
import com.collection.jpa.entity.CollectionWorkingArea;

public class CollectionMemberArea {
	
    private CollectionMember member;    
    private List<CollectionWorkingArea> areaList;

    public CollectionMember getMember() {
		return member;
	}

	public void setMember(CollectionMember member) {
		this.member = member;
	}

	public List<CollectionWorkingArea> getAreaList() {
		return areaList;
	}

	public void setAreaList(List<CollectionWorkingArea> areaList) {
		this.areaList = areaList;
	}

	public CollectionMemberArea() {
		super();
		// TODO Auto-generated constructor stub
	}

	public CollectionMemberArea(CollectionMember member, List<CollectionWorkingArea> areaList) {
		super();
		this.member = member;
		this.areaList = areaList;
	}

	@Override
	public String toString() {
		return "CollectionMemberDTO [member=" + member + ", areaList=" + areaList + "]";
	}    

}
