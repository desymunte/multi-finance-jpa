package com.collection.jpa.dto;

public class ContractCriteria {
	
    private String contractNo;

	public String getContractNo() {
		return contractNo;
	}

	public void setContractNo(String contractNo) {
		this.contractNo = contractNo;
	}

	public ContractCriteria() {
		super();
		// TODO Auto-generated constructor stub
	}

	public ContractCriteria(String contractNo) {
		super();
		this.contractNo = contractNo;
	}

	@Override
	public String toString() {
		return "LKPTrxByContractCriteria [contractNo=" + contractNo + "]";
	}

}
