package com.collection.jpa.dto;

import java.io.Serializable;

public class LkdCriteria implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private Integer id;
    private Integer teamId;
    private Integer supervisorId;
    private String contractNo;
    private Integer printId;
    private String printNo;
    private String codeEntryStatus;

    public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getTeamId() {
		return teamId;
	}

	public void setTeamId(Integer teamId) {
		this.teamId = teamId;
	}

	public Integer getSupervisorId() {
		return supervisorId;
	}

	public void setSupervisorId(Integer supervisorId) {
		this.supervisorId = supervisorId;
	}

	public String getContractNo() {
		return contractNo;
	}

	public void setContractNo(String contractNo) {
		this.contractNo = contractNo;
	}

	public Integer getPrintId() {
		return printId;
	}

	public void setPrintId(Integer printId) {
		this.printId = printId;
	}

	public String getPrintNo() {
		return printNo;
	}

	public void setPrintNo(String printNo) {
		this.printNo = printNo;
	}

	public String getCodeEntryStatus() {
		return codeEntryStatus;
	}

	public void setCodeEntryStatus(String codeEntryStatus) {
		this.codeEntryStatus = codeEntryStatus;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public LkdCriteria() {
		super();
		// TODO Auto-generated constructor stub
	}

	public LkdCriteria(Integer id, Integer teamId, Integer supervisorId, String contractNo, Integer printId,
			String printNo, String codeEntryStatus) {
		super();
		this.id = id;
		this.teamId = teamId;
		this.supervisorId = supervisorId;
		this.contractNo = contractNo;
		this.printId = printId;
		this.printNo = printNo;
		this.codeEntryStatus = codeEntryStatus;
	}

	@Override
	public String toString() {
		return "LkdCriteria [id=" + id + ", teamId=" + teamId + ", supervisorId=" + supervisorId + ", contractNo="
				+ contractNo + ", printId=" + printId + ", printNo=" + printNo + ", codeEntryStatus=" + codeEntryStatus
				+ "]";
	}

}
