package com.collection.jpa.dto;

import java.io.Serializable;
import java.util.List;

public class LkrCriteria implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private Integer id;
    private Integer teamId;
    private List<Integer>teamIdList;
    private List<Integer>cycleIdList;
    private Integer supervisorId;
    private Integer assignedTo;
    private String contractNo;
    private String officeCode;
    private Integer printId;
    private String printNo;
    private String codeEntryStatus;
    private boolean reclustering;

    public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getTeamId() {
		return teamId;
	}

	public void setTeamId(Integer teamId) {
		this.teamId = teamId;
	}

	public List<Integer> getTeamIdList() {
		return teamIdList;
	}

	public void setTeamIdList(List<Integer> teamIdList) {
		this.teamIdList = teamIdList;
	}

	public List<Integer> getCycleIdList() {
		return cycleIdList;
	}

	public void setCycleIdList(List<Integer> cycleIdList) {
		this.cycleIdList = cycleIdList;
	}

	public Integer getSupervisorId() {
		return supervisorId;
	}

	public void setSupervisorId(Integer supervisorId) {
		this.supervisorId = supervisorId;
	}

	public Integer getAssignedTo() {
		return assignedTo;
	}

	public void setAssignedTo(Integer assignedTo) {
		this.assignedTo = assignedTo;
	}

	public String getContractNo() {
		return contractNo;
	}

	public void setContractNo(String contractNo) {
		this.contractNo = contractNo;
	}

	public String getOfficeCode() {
		return officeCode;
	}

	public void setOfficeCode(String officeCode) {
		this.officeCode = officeCode;
	}

	public Integer getPrintId() {
		return printId;
	}

	public void setPrintId(Integer printId) {
		this.printId = printId;
	}

	public String getPrintNo() {
		return printNo;
	}

	public void setPrintNo(String printNo) {
		this.printNo = printNo;
	}

	public String getCodeEntryStatus() {
		return codeEntryStatus;
	}

	public void setCodeEntryStatus(String codeEntryStatus) {
		this.codeEntryStatus = codeEntryStatus;
	}

	public boolean isReclustering() {
		return reclustering;
	}

	public void setReclustering(boolean reclustering) {
		this.reclustering = reclustering;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public LkrCriteria() {
		super();
		// TODO Auto-generated constructor stub
	}

	public LkrCriteria(Integer id, Integer teamId, List<Integer> teamIdList, List<Integer> cycleIdList,
			Integer supervisorId, Integer assignedTo, String contractNo, String officeCode, Integer printId,
			String printNo, String codeEntryStatus, boolean reclustering) {
		super();
		this.id = id;
		this.teamId = teamId;
		this.teamIdList = teamIdList;
		this.cycleIdList = cycleIdList;
		this.supervisorId = supervisorId;
		this.assignedTo = assignedTo;
		this.contractNo = contractNo;
		this.officeCode = officeCode;
		this.printId = printId;
		this.printNo = printNo;
		this.codeEntryStatus = codeEntryStatus;
		this.reclustering = reclustering;
	}

	@Override
	public String toString() {
		return "LkrCriteria [id=" + id + ", teamId=" + teamId + ", teamIdList=" + teamIdList + ", cycleIdList="
				+ cycleIdList + ", supervisorId=" + supervisorId + ", assignedTo=" + assignedTo + ", contractNo="
				+ contractNo + ", officeCode=" + officeCode + ", printId=" + printId + ", printNo=" + printNo
				+ ", codeEntryStatus=" + codeEntryStatus + ", reclustering=" + reclustering + "]";
	}

}
