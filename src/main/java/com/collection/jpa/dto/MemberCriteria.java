package com.collection.jpa.dto;

import java.util.List;

public class MemberCriteria {
	
    private Integer memberId;       
    private Integer emplId;       
	private String emplName;
    private Integer teamId;
    private List<Integer> teamIdList;
    private Integer supervisorId;
    private Integer monthlyLoad;
    private Integer dailyLoad;
    private Integer officeId;
    private Integer cycleId;

    public Integer getMemberId() {
		return memberId;
	}

	public void setMemberId(Integer memberId) {
		this.memberId = memberId;
	}

	public Integer getEmplId() {
		return emplId;
	}

	public void setEmplId(Integer emplId) {
		this.emplId = emplId;
	}

	public String getEmplName() {
		return emplName;
	}

	public void setEmplName(String emplName) {
		this.emplName = emplName;
	}

	public Integer getTeamId() {
		return teamId;
	}

	public void setTeamId(Integer teamId) {
		this.teamId = teamId;
	}

	public List<Integer> getTeamIdList() {
		return teamIdList;
	}

	public void setTeamIdList(List<Integer> teamIdList) {
		this.teamIdList = teamIdList;
	}

	public Integer getSupervisorId() {
		return supervisorId;
	}

	public void setSupervisorId(Integer supervisorId) {
		this.supervisorId = supervisorId;
	}

	public Integer getMonthlyLoad() {
		return monthlyLoad;
	}

	public void setMonthlyLoad(Integer monthlyLoad) {
		this.monthlyLoad = monthlyLoad;
	}

	public Integer getDailyLoad() {
		return dailyLoad;
	}

	public void setDailyLoad(Integer dailyLoad) {
		this.dailyLoad = dailyLoad;
	}

	public Integer getOfficeId() {
		return officeId;
	}

	public void setOfficeId(Integer officeId) {
		this.officeId = officeId;
	}

	public Integer getCycleId() {
		return cycleId;
	}

	public void setCycleId(Integer cycleId) {
		this.cycleId = cycleId;
	}

	public MemberCriteria() {
		super();
		// TODO Auto-generated constructor stub
	}

	public MemberCriteria(Integer memberId, Integer emplId, String emplName, Integer teamId, List<Integer> teamIdList,
			Integer supervisorId, Integer monthlyLoad, Integer dailyLoad, Integer officeId, Integer cycleId) {
		super();
		this.memberId = memberId;
		this.emplId = emplId;
		this.emplName = emplName;
		this.teamId = teamId;
		this.teamIdList = teamIdList;
		this.supervisorId = supervisorId;
		this.monthlyLoad = monthlyLoad;
		this.dailyLoad = dailyLoad;
		this.officeId = officeId;
		this.cycleId = cycleId;
	}

	@Override
	public String toString() {
		return "MemberCriteria [memberId=" + memberId + ", emplId=" + emplId + ", emplName=" + emplName + ", teamId="
				+ teamId + ", teamIdList=" + teamIdList + ", supervisorId=" + supervisorId + ", monthlyLoad="
				+ monthlyLoad + ", dailyLoad=" + dailyLoad + ", officeId=" + officeId + ", cycleId=" + cycleId + "]";
	}

}
