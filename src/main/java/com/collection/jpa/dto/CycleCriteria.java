package com.collection.jpa.dto;


public class CycleCriteria {
	
    private Integer id;
    private String code;
    private Integer overdue;
    private String type;

    public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}

    public void setCode(String code) {
		this.code = code;
	}

    public Integer getOverdue() {
		return overdue;
	}

    public void setOverdue(Integer overdue) {
		this.overdue = overdue;
	}

    public String getType() {
		return type;
	}

    public void setType(String type) {
		this.type = type;
	}

    public CycleCriteria() {
		super();
		// TODO Auto-generated constructor stub
	}

	public CycleCriteria(Integer id, String code, Integer overdue, String type) {
		super();
		this.id = id;
		this.code = code;
		this.overdue = overdue;
		this.type = type;
	}

	@Override
	public String toString() {
		return "CycleCriteria [id=" + id + ", code=" + code + ", overdue=" + overdue + ", type=" + type + "]";
	}

}
