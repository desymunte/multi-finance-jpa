package com.collection.jpa.dto;

import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

public class DormantDTO implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private Integer id;
	@JsonFormat(pattern = "dd-MMMM-yyyy")
	private Date dateIn;
	@JsonFormat(pattern = "dd-MMMM-yyyy")
	private Date dateOut;
	private Integer emplId;
	private String emplName;
	private Integer prevMemberId;
	private String prevMemberName;
	private Integer nextMemberId;
	private String nextMemberName;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Date getDateIn() {
		return dateIn;
	}
	public void setDateIn(Date dateIn) {
		this.dateIn = dateIn;
	}
	public Date getDateOut() {
		return dateOut;
	}
	public void setDateOut(Date dateOut) {
		this.dateOut = dateOut;
	}
	public Integer getEmplId() {
		return emplId;
	}
	public void setEmplId(Integer emplId) {
		this.emplId = emplId;
	}
	public String getEmplName() {
		return emplName;
	}
	public void setEmplName(String emplName) {
		this.emplName = emplName;
	}
	public Integer getPrevMemberId() {
		return prevMemberId;
	}
	public void setPrevMemberId(Integer prevMemberId) {
		this.prevMemberId = prevMemberId;
	}
	public String getPrevMemberName() {
		return prevMemberName;
	}
	public void setPrevMemberName(String prevMemberName) {
		this.prevMemberName = prevMemberName;
	}
	public Integer getNextMemberId() {
		return nextMemberId;
	}
	public void setNextMemberId(Integer nextMemberId) {
		this.nextMemberId = nextMemberId;
	}
	public String getNextMemberName() {
		return nextMemberName;
	}
	public void setNextMemberName(String nextMemberName) {
		this.nextMemberName = nextMemberName;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	public DormantDTO(Integer id, Date dateIn, Date dateOut, Integer emplId, String emplName, Integer prevMemberId,
			String prevMemberName, Integer nextMemberId, String nextMemberName) {
		super();
		this.id = id;
		this.dateIn = dateIn;
		this.dateOut = dateOut;
		this.emplId = emplId;
		this.emplName = emplName;
		this.prevMemberId = prevMemberId;
		this.prevMemberName = prevMemberName;
		this.nextMemberId = nextMemberId;
		this.nextMemberName = nextMemberName;
	}
	public DormantDTO() {
		super();
		// TODO Auto-generated constructor stub
	}
	@Override
	public String toString() {
		return "DormantDTO [id=" + id + ", dateIn=" + dateIn + ", dateOut=" + dateOut + ", emplId=" + emplId
				+ ", emplName=" + emplName + ", prevMemberId=" + prevMemberId + ", prevMemberName=" + prevMemberName
				+ ", nextMemberId=" + nextMemberId + ", nextMemberName=" + nextMemberName + "]";
	}
	
}
