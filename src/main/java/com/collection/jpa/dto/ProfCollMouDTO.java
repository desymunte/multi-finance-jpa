package com.collection.jpa.dto;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

public class ProfCollMouDTO implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
    private Integer id;      
	private String mouNo;
    @Temporal(TemporalType.DATE)
	private Date mouDate;  
    @Temporal(TemporalType.DATE)
	private Date startDate;  
    @Temporal(TemporalType.DATE)
	private Date endDate;  
    private Integer status;

    public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getMouNo() {
		return mouNo;
	}

	public void setMouNo(String mouNo) {
		this.mouNo = mouNo;
	}

	public Date getMouDate() {
		return mouDate;
	}

	public void setMouDate(Date mouDate) {
		this.mouDate = mouDate;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public ProfCollMouDTO() {
		super();
		// TODO Auto-generated constructor stub
	}

	public ProfCollMouDTO(Integer id, String mouNo, Date mouDate, Date startDate, Date endDate, Integer status) {
		super();
		this.id = id;
		this.mouNo = mouNo;
		this.mouDate = mouDate;
		this.startDate = startDate;
		this.endDate = endDate;
		this.status = status;
	}

	@Override
	public String toString() {
		return "ProfCollMouDTO [id=" + id + ", mouNo=" + mouNo + ", mouDate=" + mouDate + ", startDate=" + startDate
				+ ", endDate=" + endDate + ", status=" + status + "]";
	}

}
