package com.collection.jpa.dto;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

public class LkdHeaderDTO {
	
	private String printNo;
	@JsonFormat(pattern = "dd-MMMM-yyyy HH:mm", timezone = "GMT+7")
	private Date printDate;
    private Integer teamId;
    private String teamName;

    public String getPrintNo() {
		return printNo;
	}

	public void setPrintNo(String printNo) {
		this.printNo = printNo;
	}

	public Date getPrintDate() {
		return printDate;
	}

	public void setPrintDate(Date printDate) {
		this.printDate = printDate;
	}

	public Integer getTeamId() {
		return teamId;
	}

	public void setTeamId(Integer teamId) {
		this.teamId = teamId;
	}

	public String getTeamName() {
		return teamName;
	}

	public void setTeamName(String teamName) {
		this.teamName = teamName;
	}

	public LkdHeaderDTO() {
		super();
		// TODO Auto-generated constructor stub
	}

	public LkdHeaderDTO(String printNo, Date printDate, Integer teamId, String teamName) {
		super();
		this.printNo = printNo;
		this.printDate = printDate;
		this.teamId = teamId;
		this.teamName = teamName;
	}

	@Override
	public String toString() {
		return "LkpHeaderDTO [printNo=" + printNo + ", printDate=" + printDate + ", teamId=" + teamId + ", teamName="
				+ teamName + "]";
	}

}
