package com.collection.jpa.dto;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

public class LkpHeaderDTO {
	
	private String printNo;
	@JsonFormat(pattern = "dd-MMMM-yyyy")
	private Date printDate;
    private Integer teamId;
    private String teamName;
    private Integer employeeId;
    private String employeeRegNo;
    private String employeeName;

    public String getPrintNo() {
		return printNo;
	}

	public void setPrintNo(String printNo) {
		this.printNo = printNo;
	}

	public Date getPrintDate() {
		return printDate;
	}

	public void setPrintDate(Date printDate) {
		this.printDate = printDate;
	}

	public Integer getTeamId() {
		return teamId;
	}

	public void setTeamId(Integer teamId) {
		this.teamId = teamId;
	}

	public String getTeamName() {
		return teamName;
	}

	public void setTeamName(String teamName) {
		this.teamName = teamName;
	}

	public Integer getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(Integer employeeId) {
		this.employeeId = employeeId;
	}

	public String getEmployeeRegNo() {
		return employeeRegNo;
	}

	public void setEmployeeRegNo(String employeeRegNo) {
		this.employeeRegNo = employeeRegNo;
	}

	public String getEmployeeName() {
		return employeeName;
	}

	public void setEmployeeName(String employeeName) {
		this.employeeName = employeeName;
	}

	public LkpHeaderDTO() {
		super();
		// TODO Auto-generated constructor stub
	}

	public LkpHeaderDTO(String printNo, Date printDate, Integer teamId, String teamName, Integer employeeId,
			String employeeRegNo, String employeeName) {
		super();
		this.printNo = printNo;
		this.printDate = printDate;
		this.teamId = teamId;
		this.teamName = teamName;
		this.employeeId = employeeId;
		this.employeeRegNo = employeeRegNo;
		this.employeeName = employeeName;
	}

	@Override
	public String toString() {
		return "LkpHeaderDTO [printNo=" + printNo + ", printDate=" + printDate + ", teamId=" + teamId + ", teamName="
				+ teamName + ", employeeId=" + employeeId + ", employeeRegNo=" + employeeRegNo + ", employeeName="
				+ employeeName + "]";
	}

}
