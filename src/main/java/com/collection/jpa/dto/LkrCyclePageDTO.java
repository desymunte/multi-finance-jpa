package com.collection.jpa.dto;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.collection.jpa.entity.Bucket;
import com.collection.jpa.entity.Cycle;

public class LkrCyclePageDTO implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private Integer id;
    private String contractNo;
	private Date contractDate;
    private String custName;
    private String custBillingAddress;
    private String phone1;
    private String phone2;
    private Integer leastInstallmentNo;
    private Integer lastInstallmentNo;
    private int topInstallment;
    @Temporal(TemporalType.DATE)
    private Date leastDueDate;
    @Temporal(TemporalType.DATE)
    private Date lastDueDate;
    private Integer overdue;
    private double outstandingPrincipalAmnt;
    private double outstandingInterestAmnt;
    private double monthlyInstallment;
    private String businessUnit;
	private Integer cycleId;
	private String cycleCode;

    public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getContractNo() {
		return contractNo;
	}

	public void setContractNo(String contractNo) {
		this.contractNo = contractNo;
	}

	public Date getContractDate() {
		return contractDate;
	}

	public void setContractDate(Date contractDate) {
		this.contractDate = contractDate;
	}

	public String getCustName() {
		return custName;
	}

	public void setCustName(String custName) {
		this.custName = custName;
	}

	public String getCustBillingAddress() {
		return custBillingAddress;
	}

	public void setCustBillingAddress(String custBillingAddress) {
		this.custBillingAddress = custBillingAddress;
	}

	public String getPhone1() {
		return phone1;
	}

	public void setPhone1(String phone1) {
		this.phone1 = phone1;
	}

	public String getPhone2() {
		return phone2;
	}

	public void setPhone2(String phone2) {
		this.phone2 = phone2;
	}

	public Integer getLeastInstallmentNo() {
		return leastInstallmentNo;
	}

	public void setLeastInstallmentNo(Integer leastInstallmentNo) {
		this.leastInstallmentNo = leastInstallmentNo;
	}

	public Integer getLastInstallmentNo() {
		return lastInstallmentNo;
	}

	public void setLastInstallmentNo(Integer lastInstallmentNo) {
		this.lastInstallmentNo = lastInstallmentNo;
	}

	public int getTopInstallment() {
		return topInstallment;
	}

	public void setTopInstallment(int topInstallment) {
		this.topInstallment = topInstallment;
	}

	public Date getLeastDueDate() {
		return leastDueDate;
	}

	public void setLeastDueDate(Date leastDueDate) {
		this.leastDueDate = leastDueDate;
	}

	public Date getLastDueDate() {
		return lastDueDate;
	}

	public void setLastDueDate(Date lastDueDate) {
		this.lastDueDate = lastDueDate;
	}

	public Integer getOverdue() {
		return overdue;
	}

	public void setOverdue(Integer overdue) {
		this.overdue = overdue;
	}

	public double getOutstandingPrincipalAmnt() {
		return outstandingPrincipalAmnt;
	}

	public void setOutstandingPrincipalAmnt(double outstandingPrincipalAmnt) {
		this.outstandingPrincipalAmnt = outstandingPrincipalAmnt;
	}

	public double getOutstandingInterestAmnt() {
		return outstandingInterestAmnt;
	}

	public void setOutstandingInterestAmnt(double outstandingInterestAmnt) {
		this.outstandingInterestAmnt = outstandingInterestAmnt;
	}

	public double getMonthlyInstallment() {
		return monthlyInstallment;
	}

	public void setMonthlyInstallment(double monthlyInstallment) {
		this.monthlyInstallment = monthlyInstallment;
	}

	public String getBusinessUnit() {
		return businessUnit;
	}

	public void setBusinessUnit(String businessUnit) {
		this.businessUnit = businessUnit;
	}

	public Integer getCycleId() {
		return cycleId;
	}

	public void setCycleId(Integer cycleId) {
		this.cycleId = cycleId;
	}

	public String getCycleCode() {
		return cycleCode;
	}

	public void setCycleCode(String cycleCode) {
		this.cycleCode = cycleCode;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public LkrCyclePageDTO() {
		super();
		// TODO Auto-generated constructor stub
	}

	public LkrCyclePageDTO(Integer id, Bucket bucket, Cycle cycle) {
		super();
		this.id = id;
		this.contractNo = bucket.getContractNo();
		this.contractDate = bucket.getContractDate();
		this.custName = bucket.getCustName();
		this.custBillingAddress = bucket.getCustBillingAddress();
		this.phone1 = bucket.getPhone1();
		this.phone2 = bucket.getPhone2();
		this.leastInstallmentNo = bucket.getLeastInstallmentNo();
		this.lastInstallmentNo = bucket.getLastInstallmentNo();
		this.topInstallment = bucket.getTopInstallment();
		this.leastDueDate = bucket.getLeastDueDate();
		this.lastDueDate = bucket.getLastDueDate();
		this.overdue = bucket.getOverdue();
		this.outstandingPrincipalAmnt = bucket.getOutstandingPrincipalAmnt();
		this.outstandingInterestAmnt = bucket.getOutstandingInterestAmnt();
		this.monthlyInstallment = bucket.getMonthlyInstallment();
		this.businessUnit = bucket.getBusinessUnit();
		this.cycleId = cycle.getId();
		this.cycleCode = cycle.getCode();
	}

}
