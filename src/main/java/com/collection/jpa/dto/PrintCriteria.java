package com.collection.jpa.dto;

public class PrintCriteria {
	
    private Integer staffId;
    private Integer printId;
    private String printNo;
    private String printType;

    public Integer getStaffId() {
		return staffId;
	}

	public void setStaffId(Integer staffId) {
		this.staffId = staffId;
	}

	public Integer getPrintId() {
		return printId;
	}

	public void setPrintId(Integer printId) {
		this.printId = printId;
	}

	public String getPrintNo() {
		return printNo;
	}

	public void setPrintNo(String printNo) {
		this.printNo = printNo;
	}

	public String getPrintType() {
		return printType;
	}

	public void setPrintType(String printType) {
		this.printType = printType;
	}

	public PrintCriteria() {
		super();
		// TODO Auto-generated constructor stub
	}

	public PrintCriteria(Integer staffId, Integer printId, String printNo, String printType) {
		super();
		this.staffId = staffId;
		this.printId = printId;
		this.printNo = printNo;
		this.printType = printType;
	}

	@Override
	public String toString() {
		return "PrintCriteria [staffId=" + staffId + ", printId=" + printId + ", printNo=" + printNo + ", printType="
				+ printType + "]";
	}

}
