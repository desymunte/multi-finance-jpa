package com.collection.jpa.dto;

public class AreaCriteria {
	
    private Integer zipId;       
	private String zipcode;
	private String subzipcode;
    private Integer officeId;
    private Integer cycleId;

    public Integer getZipId() {
		return zipId;
	}

	public void setZipId(Integer zipId) {
		this.zipId = zipId;
	}

	public String getZipcode() {
		return zipcode;
	}

	public void setZipcode(String zipcode) {
		this.zipcode = zipcode;
	}

	public String getSubzipcode() {
		return subzipcode;
	}

	public void setSubzipcode(String subzipcode) {
		this.subzipcode = subzipcode;
	}

	public Integer getOfficeId() {
		return officeId;
	}

	public void setOfficeId(Integer officeId) {
		this.officeId = officeId;
	}

	public Integer getCycleId() {
		return cycleId;
	}

	public void setCycleId(Integer cycleId) {
		this.cycleId = cycleId;
	}

	public AreaCriteria() {
		super();
		// TODO Auto-generated constructor stub
	}

	public AreaCriteria(Integer zipId, String zipcode, String subzipcode, Integer officeId, Integer cycleId) {
		super();
		this.zipId = zipId;
		this.zipcode = zipcode;
		this.subzipcode = subzipcode;
		this.officeId = officeId;
		this.cycleId = cycleId;
	}

	@Override
	public String toString() {
		return "AreaCriteria [zipId=" + zipId + ", zipcode=" + zipcode + ", subzipcode=" + subzipcode + ", officeId="
				+ officeId + ", cycleId=" + cycleId + "]";
	}
    
}
