package com.collection.jpa.dto;

public class ReclusteringDTO {
	
    private Integer lkpId;
    private Integer lkrId;

	public Integer getLkpId() {
		return lkpId;
	}

	public void setLkrId(Integer lkpId) {
		this.lkpId = lkpId;
	}
	
	public Integer getLkrId() {
		return lkrId;
	}

	public void setLkpId(Integer lkrId) {
		this.lkrId = lkrId;
	}

	public ReclusteringDTO() {
		super();
		// TODO Auto-generated constructor stub
	}

	public ReclusteringDTO(Integer lkpId, Integer lkrId) {
		super();
		this.lkpId = lkpId;
		this.lkrId = lkrId;
	}

	@Override
	public String toString() {
		return "ReclusteringDTO [lkpId=" + lkpId + "lkrId=" + lkrId + "]";
	}

}
