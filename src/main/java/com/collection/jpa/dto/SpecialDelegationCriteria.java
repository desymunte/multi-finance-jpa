package com.collection.jpa.dto;

public class SpecialDelegationCriteria {
	
    private String officeCode;
    private Integer teamId;

    public String getOfficeCode() {
		return officeCode;
	}

	public void setOfficeCode(String officeCode) {
		this.officeCode = officeCode;
	}

	public Integer getTeamId() {
		return teamId;
	}

	public void setTeamId(Integer teamId) {
		this.teamId = teamId;
	}

	public SpecialDelegationCriteria() {
		super();
		// TODO Auto-generated constructor stub
	}

	public SpecialDelegationCriteria(String officeCode, Integer teamId) {
		super();
		this.officeCode = officeCode;
		this.teamId = teamId;
	}

	@Override
	public String toString() {
		return "SpecialDelegationCriteria [officeCode=" + officeCode + ", teamId=" + teamId + "]";
	}

}
