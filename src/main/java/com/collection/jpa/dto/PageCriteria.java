package com.collection.jpa.dto;

public class PageCriteria {
	
    private String page;

	public String getPage() {
		return page;
	}

	public void setPage(String page) {
		this.page = page;
	}

	public PageCriteria() {
		super();
		// TODO Auto-generated constructor stub
	}

	public PageCriteria(String page) {
		super();
		this.page = page;
	}

	@Override
	public String toString() {
		return "PageCriteria [page=" + page + "]";
	}

}
