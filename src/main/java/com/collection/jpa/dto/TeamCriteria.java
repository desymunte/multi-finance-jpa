package com.collection.jpa.dto;

public class TeamCriteria {
	
    private Integer id;       
	private String name;
    private Integer officeId;       
    private Integer spvId;
    private Integer cycleId;
    private String cycleType;

    public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getOfficeId() {
		return officeId;
	}

	public void setOfficeId(Integer officeId) {
		this.officeId = officeId;
	}

	public Integer getSpvId() {
		return spvId;
	}

	public void setSpvId(Integer spvId) {
		this.spvId = spvId;
	}

	public Integer getCycleId() {
		return cycleId;
	}

	public void setCycleId(Integer cycleId) {
		this.cycleId = cycleId;
	}

	public String getCycleType() {
		return cycleType;
	}

	public void setCycleType(String cycleType) {
		this.cycleType = cycleType;
	}

	public TeamCriteria() {
		super();
		// TODO Auto-generated constructor stub
	}

	public TeamCriteria(Integer id, String name, Integer officeId, Integer spvId, Integer cycleId, String cycleType) {
		super();
		this.id = id;
		this.name = name;
		this.officeId = officeId;
		this.spvId = spvId;
		this.cycleId = cycleId;
		this.cycleType = cycleType;
	}

	@Override
	public String toString() {
		return "TeamCriteria [id=" + id + ", name=" + name + ", officeId=" + officeId + ", spvId=" + spvId
				+ ", cycleId=" + cycleId + ", cycleType=" + cycleType + "]";
	}

}
