package com.collection.jpa.dto;

public class ProfCollMouCriteria {
	
	private Integer profcollId;

	public Integer getProfcollId() {
		return profcollId;
	}

	public void setProfcollId(Integer profcollId) {
		this.profcollId = profcollId;
	}

	public ProfCollMouCriteria() {
		super();
		// TODO Auto-generated constructor stub
	}

	public ProfCollMouCriteria(Integer profcollId) {
		super();
		this.profcollId = profcollId;
	}

	@Override
	public String toString() {
		return "ProfCollMouCriteria [profcollId=" + profcollId + "]";
	}

}
