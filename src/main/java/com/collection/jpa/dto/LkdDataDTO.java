package com.collection.jpa.dto;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Temporal;
import javax.persistence.TemporalType;

public class LkdDataDTO implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private Integer id;
    private Integer teamId;
    private Integer supervisorId;
    private String contractNo;
    private String custName;
    private String custBillingAddress;
    private String phone1;
    private String phone2;
    private Integer leastInstallmentNo;
    private Integer lastInstallmentNo;
    private int topInstallment;
    @Temporal(TemporalType.DATE)
    private Date leastDueDate;
    @Temporal(TemporalType.DATE)
    private Date lastDueDate;
    private Integer overdue;
    private double outstandingPrincipalAmnt;
    private double outstandingInterestAmnt;
    private double monthlyInstallment;
    private String businessUnit;

    public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getTeamId() {
		return teamId;
	}

	public void setTeamId(Integer teamId) {
		this.teamId = teamId;
	}

	public Integer getSupervisorId() {
		return supervisorId;
	}

	public void setSupervisorId(Integer supervisorId) {
		this.supervisorId = supervisorId;
	}

	public String getContractNo() {
		return contractNo;
	}

	public void setContractNo(String contractNo) {
		this.contractNo = contractNo;
	}

	public String getCustName() {
		return custName;
	}

	public void setCustName(String custName) {
		this.custName = custName;
	}

	public String getCustBillingAddress() {
		return custBillingAddress;
	}

	public void setCustBillingAddress(String custBillingAddress) {
		this.custBillingAddress = custBillingAddress;
	}

	public String getPhone1() {
		return phone1;
	}

	public void setPhone1(String phone1) {
		this.phone1 = phone1;
	}

	public String getPhone2() {
		return phone2;
	}

	public void setPhone2(String phone2) {
		this.phone2 = phone2;
	}

	public Integer getLeastInstallmentNo() {
		return leastInstallmentNo;
	}

	public void setLeastInstallmentNo(Integer leastInstallmentNo) {
		this.leastInstallmentNo = leastInstallmentNo;
	}

	public Integer getLastInstallmentNo() {
		return lastInstallmentNo;
	}

	public void setLastInstallmentNo(Integer lastInstallmentNo) {
		this.lastInstallmentNo = lastInstallmentNo;
	}

	public int getTopInstallment() {
		return topInstallment;
	}

	public void setTopInstallment(int topInstallment) {
		this.topInstallment = topInstallment;
	}

	public Date getLeastDueDate() {
		return leastDueDate;
	}

	public void setLeastDueDate(Date leastDueDate) {
		this.leastDueDate = leastDueDate;
	}

	public Date getLastDueDate() {
		return lastDueDate;
	}

	public void setLastDueDate(Date lastDueDate) {
		this.lastDueDate = lastDueDate;
	}

	public Integer getOverdue() {
		return overdue;
	}

	public void setOverdue(Integer overdue) {
		this.overdue = overdue;
	}

	public double getOutstandingPrincipalAmnt() {
		return outstandingPrincipalAmnt;
	}

	public void setOutstandingPrincipalAmnt(double outstandingPrincipalAmnt) {
		this.outstandingPrincipalAmnt = outstandingPrincipalAmnt;
	}

	public double getOutstandingInterestAmnt() {
		return outstandingInterestAmnt;
	}

	public void setOutstandingInterestAmnt(double outstandingInterestAmnt) {
		this.outstandingInterestAmnt = outstandingInterestAmnt;
	}

	public double getMonthlyInstallment() {
		return monthlyInstallment;
	}

	public void setMonthlyInstallment(double monthlyInstallment) {
		this.monthlyInstallment = monthlyInstallment;
	}

	public String getBusinessUnit() {
		return businessUnit;
	}

	public void setBusinessUnit(String businessUnit) {
		this.businessUnit = businessUnit;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public LkdDataDTO() {
		super();
		// TODO Auto-generated constructor stub
	}

	public LkdDataDTO(Integer id, Integer teamId, Integer supervisorId, String contractNo, String custName,
			String custBillingAddress, String phone1, String phone2, Integer leastInstallmentNo,
			Integer lastInstallmentNo, int topInstallment, Date leastDueDate, Date lastDueDate, Integer overdue,
			double outstandingPrincipalAmnt, double outstandingInterestAmnt, double monthlyInstallment,
			String businessUnit) {
		super();
		this.id = id;
		this.teamId = teamId;
		this.supervisorId = supervisorId;
		this.contractNo = contractNo;
		this.custName = custName;
		this.custBillingAddress = custBillingAddress;
		this.phone1 = phone1;
		this.phone2 = phone2;
		this.leastInstallmentNo = leastInstallmentNo;
		this.lastInstallmentNo = lastInstallmentNo;
		this.topInstallment = topInstallment;
		this.leastDueDate = leastDueDate;
		this.lastDueDate = lastDueDate;
		this.overdue = overdue;
		this.outstandingPrincipalAmnt = outstandingPrincipalAmnt;
		this.outstandingInterestAmnt = outstandingInterestAmnt;
		this.monthlyInstallment = monthlyInstallment;
		this.businessUnit = businessUnit;
	}

	@Override
	public String toString() {
		return "LkdPageDTO [id=" + id + ", teamId=" + teamId + ", supervisorId=" + supervisorId + ", contractNo="
				+ contractNo + ", custName=" + custName + ", custBillingAddress=" + custBillingAddress + ", phone1="
				+ phone1 + ", phone2=" + phone2 + ", leastInstallmentNo=" + leastInstallmentNo + ", lastInstallmentNo="
				+ lastInstallmentNo + ", topInstallment=" + topInstallment + ", leastDueDate=" + leastDueDate
				+ ", lastDueDate=" + lastDueDate + ", overdue=" + overdue + ", outstandingPrincipalAmnt="
				+ outstandingPrincipalAmnt + ", outstandingInterestAmnt=" + outstandingInterestAmnt
				+ ", monthlyInstallment=" + monthlyInstallment + ", businessUnit=" + businessUnit + "]";
	}

}
