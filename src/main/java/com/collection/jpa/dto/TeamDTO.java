package com.collection.jpa.dto;

import java.io.Serializable;

public class TeamDTO implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private Integer id;
	private String name;
	private Integer officeId;
	private String officeCode;
	private String officeName;
	private Integer spvId;
	private String spvName;
	private String spvRegNo;
	private Integer cycleId;
	private String cycleName;
	private String cycleCode;
	private String cycleType;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getOfficeId() {
		return officeId;
	}

	public void setOfficeId(Integer officeId) {
		this.officeId = officeId;
	}

	public String getOfficeCode() {
		return officeCode;
	}

	public void setOfficeCode(String officeCode) {
		this.officeCode = officeCode;
	}

	public String getOfficeName() {
		return officeName;
	}

	public void setOfficeName(String officeName) {
		this.officeName = officeName;
	}

	public Integer getSpvId() {
		return spvId;
	}

	public void setSpvId(Integer spvId) {
		this.spvId = spvId;
	}

	public String getSpvName() {
		return spvName;
	}

	public void setSpvName(String spvName) {
		this.spvName = spvName;
	}

	public String getSpvRegNo() {
		return spvRegNo;
	}

	public void setSpvRegNo(String spvRegNo) {
		this.spvRegNo = spvRegNo;
	}

	public Integer getCycleId() {
		return cycleId;
	}

	public void setCycleId(Integer cycleId) {
		this.cycleId = cycleId;
	}

	public String getCycleName() {
		return cycleName;
	}

	public void setCycleName(String cycleName) {
		this.cycleName = cycleName;
	}

	public String getCycleCode() {
		return cycleCode;
	}

	public void setCycleCode(String cycleCode) {
		this.cycleCode = cycleCode;
	}

	public String getCycleType() {
		return cycleType;
	}

	public void setCycleType(String cycleType) {
		this.cycleType = cycleType;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public TeamDTO() {
		super();
		// TODO Auto-generated constructor stub
	}

	public TeamDTO(Integer id, String name, Integer officeId, String officeCode, String officeName, Integer spvId,
			String spvName, String spvRegNo, Integer cycleId, String cycleName, String cycleCode, String cycleType) {
		super();
		this.id = id;
		this.name = name;
		this.officeId = officeId;
		this.officeCode = officeCode;
		this.officeName = officeName;
		this.spvId = spvId;
		this.spvName = spvName;
		this.spvRegNo = spvRegNo;
		this.cycleId = cycleId;
		this.cycleName = cycleName;
		this.cycleCode = cycleCode;
		this.cycleType = cycleType;
	}

	@Override
	public String toString() {
		return "TeamDTO [id=" + id + ", name=" + name + ", officeId=" + officeId + ", officeCode=" + officeCode
				+ ", officeName=" + officeName + ", spvId=" + spvId + ", spvName=" + spvName + ", spvRegNo=" + spvRegNo
				+ ", cycleId=" + cycleId + ", cycleName=" + cycleName + ", cycleCode=" + cycleCode + ", cycleType="
				+ cycleType + "]";
	}

}
