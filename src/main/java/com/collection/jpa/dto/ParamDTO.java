package com.collection.jpa.dto;

public class ParamDTO {

	private Integer id;
	private String code;
	private String desc;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getDesc() {
		return desc;
	}
	public void setDesc(String desc) {
		this.desc = desc;
	}
	public ParamDTO(Integer id, String code, String desc) {
		super();
		this.id = id;
		this.code = code;
		this.desc = desc;
	}
	public ParamDTO() {
		super();
		// TODO Auto-generated constructor stub
	}
	@Override
	public String toString() {
		return "ParamDTO [id=" + id + ", code=" + code + ", desc=" + desc + "]";
	}
	
}
