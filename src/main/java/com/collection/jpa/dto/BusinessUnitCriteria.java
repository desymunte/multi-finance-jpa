package com.collection.jpa.dto;


public class BusinessUnitCriteria {
	
    private String code;      
    private String name;

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public BusinessUnitCriteria() {
		super();
		// TODO Auto-generated constructor stub
	}

	public BusinessUnitCriteria(String code, String name) {
		super();
		this.code = code;
		this.name = name;
	}

	@Override
	public String toString() {
		return "BusinessUnitCriteria [code=" + code + ", name=" + name + "]";
	}

}
