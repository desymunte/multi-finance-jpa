package com.collection.jpa.dto;

public class SkpcCriteria {
	
    private String officeCode;
    private Integer id;
    private Integer type; // 0: NEW, 1: EXTEND
    private Integer staffId;
    private String printNo;

    public String getOfficeCode() {
		return officeCode;
	}

	public void setOfficeCode(String officeCode) {
		this.officeCode = officeCode;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public Integer getStaffId() {
		return staffId;
	}

	public void setStaffId(Integer staffId) {
		this.staffId = staffId;
	}

	public String getPrintNo() {
		return printNo;
	}

	public void setPrintNo(String printNo) {
		this.printNo = printNo;
	}

	public SkpcCriteria() {
		super();
		// TODO Auto-generated constructor stub
	}

	public SkpcCriteria(String officeCode, Integer id, Integer type, Integer staffId, String printNo) {
		super();
		this.officeCode = officeCode;
		this.id = id;
		this.type = type;
		this.staffId = staffId;
		this.printNo = printNo;
	}

	@Override
	public String toString() {
		return "SkpcCriteria [officeCode=" + officeCode + ", id=" + id + ", type=" + type + ", staffId=" + staffId
				+ ", printNo=" + printNo + "]";
	}

}
