package com.collection.jpa.dto;


public class ZoneCriteria {
	
    private String code;      
    private String name;
    private Integer officeId;       

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getOfficeId() {
		return officeId;
	}

	public void setOfficeId(Integer officeId) {
		this.officeId = officeId;
	}

	public ZoneCriteria() {
		super();
		// TODO Auto-generated constructor stub
	}

	public ZoneCriteria(String code, String name, Integer officeId) {
		super();
		this.code = code;
		this.name = name;
		this.officeId = officeId;
	}

	@Override
	public String toString() {
		return "ZoneParamDTO [code=" + code + ", name=" + name + ", officeId=" + officeId + "]";
	}

}
