package com.collection.jpa.dto;

import java.io.Serializable;

public class EmployeeDTO implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private Integer id;
	private String regNo;
	private String name;
	private Integer officeId;
	private String jobCode;
	private String jobName;
	private Integer jobLevel;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getRegNo() {
		return regNo;
	}

	public void setRegNo(String regNo) {
		this.regNo = regNo;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getOfficeId() {
		return officeId;
	}

	public void setOfficeId(Integer officeId) {
		this.officeId = officeId;
	}

	public String getJobCode() {
		return jobCode;
	}

	public void setJobCode(String jobCode) {
		this.jobCode = jobCode;
	}

	public String getJobName() {
		return jobName;
	}

	public void setJobName(String jobName) {
		this.jobName = jobName;
	}

	public Integer getJobLevel() {
		return jobLevel;
	}

	public void setJobLevel(Integer jobLevel) {
		this.jobLevel = jobLevel;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public EmployeeDTO() {
		super();
		// TODO Auto-generated constructor stub
	}

	public EmployeeDTO(Integer id, String regNo, String name, Integer officeId, String jobCode, String jobName,
			Integer jobLevel) {
		super();
		this.id = id;
		this.regNo = regNo;
		this.name = name;
		this.officeId = officeId;
		this.jobCode = jobCode;
		this.jobName = jobName;
		this.jobLevel = jobLevel;
	}

	@Override
	public String toString() {
		return "EmployeeDTO [id=" + id + ", regNo=" + regNo + ", name=" + name + ", officeId=" + officeId + ", jobCode="
				+ jobCode + ", jobName=" + jobName + ", jobLevel=" + jobLevel + "]";
	}
	
}
