package com.collection.jpa.dto;

import java.util.ArrayList;
import java.util.List;
import java.io.Serializable;
import com.collection.jpa.entity.DeskcallMember;

public class DeskCallTeamMemberDTO {
	
	private static final long serialVersionUID = 1L;
	private Integer memberId;
	private Integer emplId;
	private String emplName;
	private String emplRegNo;
	private Integer teamId;
	private String teamName;
	private Integer spvId;
	private Integer monthlyLoad;
	private Integer dailyLoad;

	public Integer getMemberId() {
		return memberId;
	}

	public void setMemberId(Integer memberId) {
		this.memberId = memberId;
	}

	public Integer getEmplId() {
		return emplId;
	}

	public void setEmplId(Integer emplId) {
		this.emplId = emplId;
	}

	public String getEmplName() {
		return emplName;
	}

	public void setEmplName(String emplName) {
		this.emplName = emplName;
	}

	public String getEmplRegNo() {
		return emplRegNo;
	}

	public void setEmplRegNo(String emplRegNo) {
		this.emplRegNo = emplRegNo;
	}

	public Integer getTeamId() {
		return teamId;
	}

	public void setTeamId(Integer teamId) {
		this.teamId = teamId;
	}

	public String getTeamName() {
		return teamName;
	}

	public void setTeamName(String teamName) {
		this.teamName = teamName;
	}

	public Integer getSpvId() {
		return spvId;
	}

	public void setSpvId(Integer spvId) {
		this.spvId = spvId;
	}

	public Integer getMonthlyLoad() {
		return monthlyLoad;
	}

	public void setMonthlyLoad(Integer monthlyLoad) {
		this.monthlyLoad = monthlyLoad;
	}

	public Integer getDailyLoad() {
		return dailyLoad;
	}

	public void setDailyLoad(Integer dailyLoad) {
		this.dailyLoad = dailyLoad;
	}

	public DeskCallTeamMemberDTO() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public DeskCallTeamMemberDTO(DeskcallMember member, Integer emplId, String emplName, String emplRegNo, Integer teamId,
			String teamName, Integer spvId) {
		super();
		this.memberId = member.getId();
		this.monthlyLoad = member.getMonthlyLoad();
		this.dailyLoad = member.getDailyLoad();
		this.emplId = emplId;
		this.emplName = emplName;
		this.emplRegNo = emplRegNo;
		this.teamId = teamId;
		this.teamName = teamName;
		this.spvId = spvId;
	}

	@Override
	public String toString() {
		return "MemberDTO [memberId=" + memberId + ", emplId=" + emplId + ", emplName=" + emplName + ", emplRegNo="
				+ emplRegNo + ", teamId=" + teamId + ", teamName=" + teamName + ", spvId=" + spvId + ", monthlyLoad="
				+ monthlyLoad + ", dailyLoad=" + dailyLoad + "]";
	}
}
