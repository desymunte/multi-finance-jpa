package com.collection.jpa.dto;

import java.util.List;

import com.collection.jpa.entity.CollectionTeam;
import com.collection.jpa.entity.User;

public class Collection {
	
	private User user;
    private CollectionTeam team;
    private List<CollectionMemberArea> memberList;

    public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public CollectionTeam getTeam() {
		return team;
	}

	public void setTeam(CollectionTeam team) {
		this.team = team;
	}

	public List<CollectionMemberArea> getMemberList() {
		return memberList;
	}

	public void setMemberList(List<CollectionMemberArea> memberList) {
		this.memberList = memberList;
	}

	public Collection() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Collection(User user, CollectionTeam team, List<CollectionMemberArea> memberList) {
		super();
		this.user = user;
		this.team = team;
		this.memberList = memberList;
	}

	@Override
	public String toString() {
		return "Collection [user=" + user + ", team=" + team + ", memberList=" + memberList + "]";
	}

}
