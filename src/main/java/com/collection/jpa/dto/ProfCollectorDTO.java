package com.collection.jpa.dto;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Temporal;
import javax.persistence.TemporalType;

public class ProfCollectorDTO implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
    private Integer id;      
    private Integer officeId;
    private String officeCode;
    private String officeName;
    private Double officeSKPCFee;
	private Integer type;
	private String profcollName;  
	private String profcollIdNo;  
	private String profcollAddress;  
	private String profcollPhone;  
	private String profcollEmail;
    private Integer staffId;      
	private String staffName;
	private String staffIdCardNo;
	private String staffAddress;
	private String staffPhone;  
	private String staffEmail;
	private String mouNo;
    @Temporal(TemporalType.DATE)
	private Date mouDate;  
    @Temporal(TemporalType.DATE)
	private Date mouStartDate;  
    @Temporal(TemporalType.DATE)
	private Date mouEndDate;  
    private Integer mouStatus;
	private String mouStatusDescr;
	private Double mouPerformance;
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getOfficeId() {
		return officeId;
	}

	public void setOfficeId(Integer officeId) {
		this.officeId = officeId;
	}

	public String getOfficeCode() {
		return officeCode;
	}

	public void setOfficeCode(String officeCode) {
		this.officeCode = officeCode;
	}

	public String getOfficeName() {
		return officeName;
	}

	public void setOfficeName(String officeName) {
		this.officeName = officeName;
	}

	public Double getOfficeSKPCFee() {
		return officeSKPCFee;
	}

	public void setOfficeSKPCFee(Double officeSKPCFee) {
		this.officeSKPCFee = officeSKPCFee;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public String getProfcollName() {
		return profcollName;
	}

	public void setProfcollName(String profcollName) {
		this.profcollName = profcollName;
	}

	public String getProfcollIdNo() {
		return profcollIdNo;
	}

	public void setProfcollIdNo(String profcollIdNo) {
		this.profcollIdNo = profcollIdNo;
	}

	public String getProfcollAddress() {
		return profcollAddress;
	}

	public void setProfcollAddress(String profcollAddress) {
		this.profcollAddress = profcollAddress;
	}

	public String getProfcollPhone() {
		return profcollPhone;
	}

	public void setProfcollPhone(String profcollPhone) {
		this.profcollPhone = profcollPhone;
	}

	public String getProfcollEmail() {
		return profcollEmail;
	}

	public void setProfcollEmail(String profcollEmail) {
		this.profcollEmail = profcollEmail;
	}

	public Integer getStaffId() {
		return staffId;
	}

	public void setStaffId(Integer staffId) {
		this.staffId = staffId;
	}

	public String getStaffName() {
		return staffName;
	}

	public void setStaffName(String staffName) {
		this.staffName = staffName;
	}

	public String getStaffIdCardNo() {
		return staffIdCardNo;
	}

	public void setStaffIdCardNo(String staffIdCardNo) {
		this.staffIdCardNo = staffIdCardNo;
	}

	public String getStaffAddress() {
		return staffAddress;
	}

	public void setStaffAddress(String staffAddress) {
		this.staffAddress = staffAddress;
	}

	public String getStaffPhone() {
		return staffPhone;
	}

	public void setStaffPhone(String staffPhone) {
		this.staffPhone = staffPhone;
	}

	public String getStaffEmail() {
		return staffEmail;
	}

	public void setStaffEmail(String staffEmail) {
		this.staffEmail = staffEmail;
	}

	public String getMouNo() {
		return mouNo;
	}

	public void setMouNo(String mouNo) {
		this.mouNo = mouNo;
	}

	public Date getMouDate() {
		return mouDate;
	}

	public void setMouDate(Date mouDate) {
		this.mouDate = mouDate;
	}

	public Date getMouStartDate() {
		return mouStartDate;
	}

	public void setMouStartDate(Date mouStartDate) {
		this.mouStartDate = mouStartDate;
	}

	public Date getMouEndDate() {
		return mouEndDate;
	}

	public void setMouEndDate(Date mouEndDate) {
		this.mouEndDate = mouEndDate;
	}

	public Integer getMouStatus() {
		return mouStatus;
	}

	public void setMouStatus(Integer mouStatus) {
		this.mouStatus = mouStatus;
	}

	public String getMouStatusDescr() {
		return mouStatusDescr;
	}

	public void setMouStatusDescr(String mouStatusDescr) {
		this.mouStatusDescr = mouStatusDescr;
	}

	public Double getMouPerformance() {
		return mouPerformance;
	}

	public void setMouPerformance(Double mouPerformance) {
		this.mouPerformance = mouPerformance;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public ProfCollectorDTO() {
		super();
		// TODO Auto-generated constructor stub
	}

	public ProfCollectorDTO(Integer id, Integer officeId, String officeCode, String officeName, Double officeSKPCFee,
			Integer type, String profcollName, String profcollIdNo, String profcollAddress,
			String profcollPhone, String profcollEmail, Integer staffId, String staffName, String staffIdCardNo,
			String staffAddress, String staffPhone, String staffEmail, String mouNo, Date mouDate, Date mouStartDate,
			Date mouEndDate, Integer mouStatus, String mouStatusDescr, Double mouPerformance) {
		super();
		this.id = id;
		this.officeId = officeId;
		this.officeCode = officeCode;
		this.officeName = officeName;
		this.officeSKPCFee = officeSKPCFee;
		this.type = type;
		this.profcollName = profcollName;
		this.profcollIdNo = profcollIdNo;
		this.profcollAddress = profcollAddress;
		this.profcollPhone = profcollPhone;
		this.profcollEmail = profcollEmail;
		this.staffId = staffId;
		this.staffName = staffName;
		this.staffIdCardNo = staffIdCardNo;
		this.staffAddress = staffAddress;
		this.staffPhone = staffPhone;
		this.staffEmail = staffEmail;
		this.mouNo = mouNo;
		this.mouDate = mouDate;
		this.mouStartDate = mouStartDate;
		this.mouEndDate = mouEndDate;
		this.mouStatus = mouStatus;
		this.mouStatusDescr = mouStatusDescr;
		this.mouPerformance = mouPerformance;
	}

	@Override
	public String toString() {
		return "ProfCollectionDTO [id=" + id + ", officeId=" + officeId + ", officeCode=" + officeCode + ", officeName="
				+ officeName + ", officeSKPCFee=" + officeSKPCFee + ", type=" + type 
				+ ", profcollName=" + profcollName + ", profcollIdNo=" + profcollIdNo + ", profcollAddress="
				+ profcollAddress + ", profcollPhone=" + profcollPhone + ", profcollEmail=" + profcollEmail
				+ ", staffId=" + staffId + ", staffName=" + staffName + ", staffIdCardNo=" + staffIdCardNo
				+ ", staffAddress=" + staffAddress + ", staffPhone=" + staffPhone + ", staffEmail=" + staffEmail
				+ ", mouNo=" + mouNo + ", mouDate=" + mouDate + ", mouStartDate=" + mouStartDate + ", mouEndDate="
				+ mouEndDate + ", mouStatus=" + mouStatus + ", mouStatusDescr=" + mouStatusDescr + ", mouPerformance="
				+ mouPerformance + "]";
	}

}
