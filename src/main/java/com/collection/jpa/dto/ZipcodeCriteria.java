package com.collection.jpa.dto;


public class ZipcodeCriteria {
	
    private String zipcode;      
    private String subzipcode;
    private String name;
    private Integer officeId;       
    private Integer zoneId;       

	public String getZipcode() {
		return zipcode;
	}

	public void setZipcode(String zipcode) {
		this.zipcode = zipcode;
	}

	public String getSubzipcode() {
		return subzipcode;
	}

	public void setSubzipcode(String subzipcode) {
		this.subzipcode = subzipcode;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getOfficeId() {
		return officeId;
	}

	public void setOfficeId(Integer officeId) {
		this.officeId = officeId;
	}

	public Integer getZoneId() {
		return zoneId;
	}

	public void setZoneId(Integer zoneId) {
		this.zoneId = zoneId;
	}

	public ZipcodeCriteria() {
		super();
		// TODO Auto-generated constructor stub
	}

	public ZipcodeCriteria(String zipcode, String subzipcode, String name, Integer officeId, Integer zoneId) {
		super();
		this.zipcode = zipcode;
		this.subzipcode = subzipcode;
		this.name = name;
		this.officeId = officeId;
		this.zoneId = zoneId;
	}

	@Override
	public String toString() {
		return "ZipcodeParamDTO [zipcode=" + zipcode + ", subzipcode=" + subzipcode + ", name=" + name + ", officeId="
				+ officeId + ", zoneId=" + zoneId + "]";
	}

}
