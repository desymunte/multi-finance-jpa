package com.collection.jpa.dto;

public class ChangeSpvParamDTO {
	
    private Integer teamId;       
    private Integer spvId;

    public Integer getTeamId() {
		return teamId;
	}

	public void setTeamId(Integer teamId) {
		this.teamId = teamId;
	}

	public Integer getSpvId() {
		return spvId;
	}

	public void setSpvId(Integer spvId) {
		this.spvId = spvId;
	}

	public ChangeSpvParamDTO() {
		super();
		// TODO Auto-generated constructor stub
	}

	public ChangeSpvParamDTO(Integer teamId, Integer spvId) {
		super();
		this.teamId = teamId;
		this.spvId = spvId;
	}

	@Override
	public String toString() {
		return "ChangeSpvDTO [teamId=" + teamId + ", spvId=" + spvId + "]";
	}

}
