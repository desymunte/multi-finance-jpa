package com.collection.jpa.dto;

public class CollectionWorkingAreaCriteria {
	
    public Integer getCollectionMemberId() {
		return collectionMemberId;
	}

	public void setCollectionMemberId(Integer collectionMemberId) {
		this.collectionMemberId = collectionMemberId;
	}

	public CollectionWorkingAreaCriteria() {
		super();
		// TODO Auto-generated constructor stub
	}

	public CollectionWorkingAreaCriteria(Integer collectionMemberId) {
		super();
		this.collectionMemberId = collectionMemberId;
	}

	private Integer collectionMemberId;

	@Override
	public String toString() {
		return "CollectionWorkingAreaCriteria [collectionMemberId=" + collectionMemberId + "]";
	}       

}
