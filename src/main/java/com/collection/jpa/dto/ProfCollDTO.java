package com.collection.jpa.dto;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Temporal;
import javax.persistence.TemporalType;

public class ProfCollDTO implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
    private Integer id;      
    private Integer officeId;
    private String officeCode;
    private String officeName;
	private Integer type;
	private String corpName;  
	private String corpId;
	private String corpAddress;  
	private String corpPhone;  
	private String corpEmail;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getOfficeId() {
		return officeId;
	}

	public void setOfficeId(Integer officeId) {
		this.officeId = officeId;
	}

	public String getOfficeCode() {
		return officeCode;
	}

	public void setOfficeCode(String officeCode) {
		this.officeCode = officeCode;
	}

	public String getOfficeName() {
		return officeName;
	}

	public void setOfficeName(String officeName) {
		this.officeName = officeName;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public String getCorpName() {
		return corpName;
	}

	public void setCorpName(String corpName) {
		this.corpName = corpName;
	}

	public String getCorpId() {
		return corpId;
	}

	public void setCorpId(String corpId) {
		this.corpId = corpId;
	}

	public String getCorpAddress() {
		return corpAddress;
	}

	public void setCorpAddress(String corpAddress) {
		this.corpAddress = corpAddress;
	}

	public String getCorpPhone() {
		return corpPhone;
	}

	public void setCorpPhone(String corpPhone) {
		this.corpPhone = corpPhone;
	}

	public String getCorpEmail() {
		return corpEmail;
	}

	public void setCorpEmail(String corpEmail) {
		this.corpEmail = corpEmail;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public ProfCollDTO() {
		super();
		// TODO Auto-generated constructor stub
	}

	public ProfCollDTO(Integer id, Integer officeId, String officeCode, String officeName, Integer type,
			String corpName, String corpId, String corpAddress, String corpPhone, String corpEmail) {
		super();
		this.id = id;
		this.officeId = officeId;
		this.officeCode = officeCode;
		this.officeName = officeName;
		this.type = type;
		this.corpName = corpName;
		this.corpId = corpId;
		this.corpAddress = corpAddress;
		this.corpPhone = corpPhone;
		this.corpEmail = corpEmail;
	}

	@Override
	public String toString() {
		return "ProfCollDTO [id=" + id + ", officeId=" + officeId + ", officeCode=" + officeCode + ", officeName="
				+ officeName + ", type=" + type + ", corpName=" + corpName + ", corpId=" + corpId + ", corpAddress="
				+ corpAddress + ", corpPhone=" + corpPhone + ", corpEmail=" + corpEmail + "]";
	}

}
