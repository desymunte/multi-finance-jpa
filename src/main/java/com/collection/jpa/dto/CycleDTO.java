package com.collection.jpa.dto;

import java.io.Serializable;

public class CycleDTO implements Serializable {
 
    private static final long serialVersionUID = 1L;
        
    private Integer id;      
	private String code;  
	private String name;
	private String description;
	private Integer overdueStart;
	private Integer overdueEnd;
	private String cycleType;
	private String cycleDescription;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Integer getOverdueStart() {
		return overdueStart;
	}

	public void setOverdueStart(Integer overdueStart) {
		this.overdueStart = overdueStart;
	}

	public Integer getOverdueEnd() {
		return overdueEnd;
	}

	public void setOverdueEnd(Integer overdueEnd) {
		this.overdueEnd = overdueEnd;
	}

	public String getCycleType() {
		return cycleType;
	}

	public void setCycleType(String cycleType) {
		this.cycleType = cycleType;
	}

	public String getCycleDescription() {
		return cycleDescription;
	}

	public void setCycleDescription(String cycleDescription) {
		this.cycleDescription = cycleDescription;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public CycleDTO() {
		super();
		// TODO Auto-generated constructor stub
	}

	public CycleDTO(Integer id, String code, String name, String description, Integer overdueStart, Integer overdueEnd,
			String cycleType, String cycleDescription) {
		super();
		this.id = id;
		this.code = code;
		this.name = name;
		this.description = description;
		this.overdueStart = overdueStart;
		this.overdueEnd = overdueEnd;
		this.cycleType = cycleType;
		this.cycleDescription = cycleDescription;
	}

	@Override
	public String toString() {
		return "CycleDTO [id=" + id + ", code=" + code + ", name=" + name + ", description=" + description
				+ ", overdueStart=" + overdueStart + ", overdueEnd=" + overdueEnd + ", cycleType=" + cycleType
				+ ", cycleDescription=" + cycleDescription + "]";
	}

}
