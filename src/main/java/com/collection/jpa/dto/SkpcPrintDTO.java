package com.collection.jpa.dto;

import java.io.Serializable;
import java.util.Date;

public class SkpcPrintDTO implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private String skpcPrintNo;
	private Date skpcDate;
	private Date skpcStartDate;
	private Date skpcEndDate;
	private String officePicName;
	private String officeName;
	private String officeAddress;
	private String mouNo;
	private String profcollName;
	private String profcollAddress;
    private String profcollIdCardNo;
    private String contractNo;
	private Date contractDate;
    private String custName;
    private String custBillingAddress;
    private String phone1;
    private String phone2;
//    private int topInstallment;
//    private double outstandingPrincipalAmnt;
//    private double outstandingInterestAmnt;
//    private double monthlyInstallment;
//    @Temporal(TemporalType.DATE)
//    private Date leastDueDate;
//    private int leastInstallmentNo;
//    @Temporal(TemporalType.DATE)
//    private Date lastDueDate;
//    private int lastInstallmentNo;
//    private int overdue;
//    private String businessUnit;
    private String brand;
    private String model;
    private String color;
    private String frameNo;
    private String engineNo;
    private String policeNo;
    private String stnk;

    public String getSkpcPrintNo() {
		return skpcPrintNo;
	}

	public void setSkpcPrintNo(String skpcPrintNo) {
		this.skpcPrintNo = skpcPrintNo;
	}

	public Date getSkpcDate() {
		return skpcDate;
	}

	public void setSkpcDate(Date skpcDate) {
		this.skpcDate = skpcDate;
	}

	public Date getSkpcStartDate() {
		return skpcStartDate;
	}

	public void setSkpcStartDate(Date skpcStartDate) {
		this.skpcStartDate = skpcStartDate;
	}

	public Date getSkpcEndDate() {
		return skpcEndDate;
	}

	public void setSkpcEndDate(Date skpcEndDate) {
		this.skpcEndDate = skpcEndDate;
	}

	public String getOfficePicName() {
		return officePicName;
	}

	public void setOfficePicName(String officePicName) {
		this.officePicName = officePicName;
	}

	public String getOfficeName() {
		return officeName;
	}

	public void setOfficeName(String officeName) {
		this.officeName = officeName;
	}

	public String getOfficeAddress() {
		return officeAddress;
	}

	public void setOfficeAddress(String officeAddress) {
		this.officeAddress = officeAddress;
	}

	public String getMouNo() {
		return mouNo;
	}

	public void setMouNo(String mouNo) {
		this.mouNo = mouNo;
	}

	public String getProfcollName() {
		return profcollName;
	}

	public void setProfcollName(String profcollName) {
		this.profcollName = profcollName;
	}

	public String getProfcollAddress() {
		return profcollAddress;
	}

	public void setProfcollAddress(String profcollAddress) {
		this.profcollAddress = profcollAddress;
	}

	public String getProfcollIdCardNo() {
		return profcollIdCardNo;
	}

	public void setProfcollIdCardNo(String profcollIdCardNo) {
		this.profcollIdCardNo = profcollIdCardNo;
	}

	public String getContractNo() {
		return contractNo;
	}

	public void setContractNo(String contractNo) {
		this.contractNo = contractNo;
	}

	public Date getContractDate() {
		return contractDate;
	}

	public void setContractDate(Date contractDate) {
		this.contractDate = contractDate;
	}

	public String getCustName() {
		return custName;
	}

	public void setCustName(String custName) {
		this.custName = custName;
	}

	public String getCustBillingAddress() {
		return custBillingAddress;
	}

	public void setCustBillingAddress(String custBillingAddress) {
		this.custBillingAddress = custBillingAddress;
	}

	public String getPhone1() {
		return phone1;
	}

	public void setPhone1(String phone1) {
		this.phone1 = phone1;
	}

	public String getPhone2() {
		return phone2;
	}

	public void setPhone2(String phone2) {
		this.phone2 = phone2;
	}

	public String getBrand() {
		return brand;
	}

	public void setBrand(String brand) {
		this.brand = brand;
	}

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public String getFrameNo() {
		return frameNo;
	}

	public void setFrameNo(String frameNo) {
		this.frameNo = frameNo;
	}

	public String getEngineNo() {
		return engineNo;
	}

	public void setEngineNo(String engineNo) {
		this.engineNo = engineNo;
	}

	public String getPoliceNo() {
		return policeNo;
	}

	public void setPoliceNo(String policeNo) {
		this.policeNo = policeNo;
	}

	public String getStnk() {
		return stnk;
	}

	public void setStnk(String stnk) {
		this.stnk = stnk;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public SkpcPrintDTO() {
		super();
		// TODO Auto-generated constructor stub
	}

	public SkpcPrintDTO(String skpcPrintNo, Date skpcDate, Date skpcStartDate, Date skpcEndDate, String officePicName,
			String officeName, String officeAddress, String mouNo, String profcollName, String profcollAddress,
			String profcollIdCardNo, String contractNo, Date contractDate, String custName, String custBillingAddress,
			String phone1, String phone2, String brand, String model, String color, String frameNo, String engineNo,
			String policeNo, String stnk) {
		super();
		this.skpcPrintNo = skpcPrintNo;
		this.skpcDate = skpcDate;
		this.skpcStartDate = skpcStartDate;
		this.skpcEndDate = skpcEndDate;
		this.officePicName = officePicName;
		this.officeName = officeName;
		this.officeAddress = officeAddress;
		this.mouNo = mouNo;
		this.profcollName = profcollName;
		this.profcollAddress = profcollAddress;
		this.profcollIdCardNo = profcollIdCardNo;
		this.contractNo = contractNo;
		this.contractDate = contractDate;
		this.custName = custName;
		this.custBillingAddress = custBillingAddress;
		this.phone1 = phone1;
		this.phone2 = phone2;
		this.brand = brand;
		this.model = model;
		this.color = color;
		this.frameNo = frameNo;
		this.engineNo = engineNo;
		this.policeNo = policeNo;
		this.stnk = stnk;
	}

	@Override
	public String toString() {
		return "SkpcPrintDTO [skpcPrintNo=" + skpcPrintNo + ", skpcDate=" + skpcDate + ", skpcStartDate="
				+ skpcStartDate + ", skpcEndDate=" + skpcEndDate + ", officePicName=" + officePicName + ", officeName="
				+ officeName + ", officeAddress=" + officeAddress + ", mouNo=" + mouNo + ", profcollName="
				+ profcollName + ", profcollAddress=" + profcollAddress + ", profcollIdCardNo=" + profcollIdCardNo
				+ ", contractNo=" + contractNo + ", contractDate=" + contractDate + ", custName=" + custName
				+ ", custBillingAddress=" + custBillingAddress + ", phone1=" + phone1 + ", phone2=" + phone2
				+ ", brand=" + brand + ", model=" + model + ", color=" + color + ", frameNo=" + frameNo + ", engineNo="
				+ engineNo + ", policeNo=" + policeNo + ", stnk=" + stnk + "]";
	}

}
