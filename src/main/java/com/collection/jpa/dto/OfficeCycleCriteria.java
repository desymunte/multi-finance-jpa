package com.collection.jpa.dto;


public class OfficeCycleCriteria {
	
    private Integer officeId;      
    private Integer cycleId;

    public Integer getOfficeId() {
		return officeId;
	}

	public void setOfficeId(Integer officeId) {
		this.officeId = officeId;
	}

	public Integer getCycleId() {
		return cycleId;
	}

	public void setCycleId(Integer cycleId) {
		this.cycleId = cycleId;
	}

	public OfficeCycleCriteria() {
		super();
		// TODO Auto-generated constructor stub
	}

	public OfficeCycleCriteria(Integer officeId, Integer cycleId) {
		super();
		this.officeId = officeId;
		this.cycleId = cycleId;
	}

	@Override
	public String toString() {
		return "OfficeCycleCriteria [officeId=" + officeId + ", cycleId=" + cycleId + "]";
	}      

}
