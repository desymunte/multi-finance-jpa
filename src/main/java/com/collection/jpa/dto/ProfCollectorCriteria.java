package com.collection.jpa.dto;

public class ProfCollectorCriteria {
	
	private String name;
    private Integer officeId;
	private String idNo;
    private Integer type;

    public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getOfficeId() {
		return officeId;
	}

	public void setOfficeId(Integer officeId) {
		this.officeId = officeId;
	}

	public String getIdNo() {
		return idNo;
	}

	public void setIdNo(String idNo) {
		this.idNo = idNo;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public ProfCollectorCriteria() {
		super();
		// TODO Auto-generated constructor stub
	}

	public ProfCollectorCriteria(String name, Integer officeId, String idNo, Integer type) {
		super();
		this.name = name;
		this.officeId = officeId;
		this.idNo = idNo;
		this.type = type;
	}

	@Override
	public String toString() {
		return "ProfCollectionCriteria [name=" + name + ", officeId=" + officeId + ", idNo=" + idNo + ", type=" + type
				+ "]";
	}

}
