package com.collection.jpa.dto;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Temporal;
import javax.persistence.TemporalType;

public class LkrPrintDTO implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private Integer id;
    private Integer teamId;
    private Integer supervisorId;
    private Integer assignedTo;
    private String contractNo;
	private Date contractDate;
    private String custName;
    private String custBillingAddress;
    private String phone1;
    private String phone2;
    private Integer leastInstallmentNo;
    private Integer lastInstallmentNo;
    private int topInstallment;
    @Temporal(TemporalType.DATE)
    private Date leastDueDate;
    @Temporal(TemporalType.DATE)
    private Date lastDueDate;
    private Integer overdue;
    private double outstandingPrincipalAmnt;
    private double outstandingInterestAmnt;
    private double monthlyInstallment;
    private String businessUnit;
    private String brand;
    private String model;
    private String color;
    private String frameNo;
    private String engineNo;
    private String policeNo;
    private String stnk;
    private String printNo;
    private Date printDate;
    private List<BucketDetailDTO> details;

    public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getTeamId() {
		return teamId;
	}

	public void setTeamId(Integer teamId) {
		this.teamId = teamId;
	}

	public Integer getSupervisorId() {
		return supervisorId;
	}

	public void setSupervisorId(Integer supervisorId) {
		this.supervisorId = supervisorId;
	}

	public Integer getAssignedTo() {
		return assignedTo;
	}

	public void setAssignedTo(Integer assignedTo) {
		this.assignedTo = assignedTo;
	}

	public String getContractNo() {
		return contractNo;
	}

	public void setContractNo(String contractNo) {
		this.contractNo = contractNo;
	}

	public Date getContractDate() {
		return contractDate;
	}

	public void setContractDate(Date contractDate) {
		this.contractDate = contractDate;
	}

	public String getCustName() {
		return custName;
	}

	public void setCustName(String custName) {
		this.custName = custName;
	}

	public String getCustBillingAddress() {
		return custBillingAddress;
	}

	public void setCustBillingAddress(String custBillingAddress) {
		this.custBillingAddress = custBillingAddress;
	}

	public String getPhone1() {
		return phone1;
	}

	public void setPhone1(String phone1) {
		this.phone1 = phone1;
	}

	public String getPhone2() {
		return phone2;
	}

	public void setPhone2(String phone2) {
		this.phone2 = phone2;
	}

	public Integer getLeastInstallmentNo() {
		return leastInstallmentNo;
	}

	public void setLeastInstallmentNo(Integer leastInstallmentNo) {
		this.leastInstallmentNo = leastInstallmentNo;
	}

	public Integer getLastInstallmentNo() {
		return lastInstallmentNo;
	}

	public void setLastInstallmentNo(Integer lastInstallmentNo) {
		this.lastInstallmentNo = lastInstallmentNo;
	}

	public int getTopInstallment() {
		return topInstallment;
	}

	public void setTopInstallment(int topInstallment) {
		this.topInstallment = topInstallment;
	}

	public Date getLeastDueDate() {
		return leastDueDate;
	}

	public void setLeastDueDate(Date leastDueDate) {
		this.leastDueDate = leastDueDate;
	}

	public Date getLastDueDate() {
		return lastDueDate;
	}

	public void setLastDueDate(Date lastDueDate) {
		this.lastDueDate = lastDueDate;
	}

	public Integer getOverdue() {
		return overdue;
	}

	public void setOverdue(Integer overdue) {
		this.overdue = overdue;
	}

	public double getOutstandingPrincipalAmnt() {
		return outstandingPrincipalAmnt;
	}

	public void setOutstandingPrincipalAmnt(double outstandingPrincipalAmnt) {
		this.outstandingPrincipalAmnt = outstandingPrincipalAmnt;
	}

	public double getOutstandingInterestAmnt() {
		return outstandingInterestAmnt;
	}

	public void setOutstandingInterestAmnt(double outstandingInterestAmnt) {
		this.outstandingInterestAmnt = outstandingInterestAmnt;
	}

	public double getMonthlyInstallment() {
		return monthlyInstallment;
	}

	public void setMonthlyInstallment(double monthlyInstallment) {
		this.monthlyInstallment = monthlyInstallment;
	}

	public String getBusinessUnit() {
		return businessUnit;
	}

	public void setBusinessUnit(String businessUnit) {
		this.businessUnit = businessUnit;
	}

	public String getBrand() {
		return brand;
	}

	public void setBrand(String brand) {
		this.brand = brand;
	}

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public String getFrameNo() {
		return frameNo;
	}

	public void setFrameNo(String frameNo) {
		this.frameNo = frameNo;
	}

	public String getEngineNo() {
		return engineNo;
	}

	public void setEngineNo(String engineNo) {
		this.engineNo = engineNo;
	}

	public String getPoliceNo() {
		return policeNo;
	}

	public void setPoliceNo(String policeNo) {
		this.policeNo = policeNo;
	}

	public String getStnk() {
		return stnk;
	}

	public void setStnk(String stnk) {
		this.stnk = stnk;
	}

	public String getPrintNo() {
		return printNo;
	}

	public void setPrintNo(String printNo) {
		this.printNo = printNo;
	}
	
	public Date getPrintDate() {
		return printDate;
	}

	public void setPrintDate(Date printDate) {
		this.printDate = printDate;
	}

	public List<BucketDetailDTO> getDetails() {
		return details;
	}

	public void setDetails(List<BucketDetailDTO> details) {
		this.details = details;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public LkrPrintDTO() {
		super();
		// TODO Auto-generated constructor stub
	}

	public LkrPrintDTO(Integer id, Integer teamId, Integer supervisorId, Integer assignedTo, String contractNo,
			Date contractDate, String custName, String custBillingAddress, String phone1, String phone2,
			Integer leastInstallmentNo, Integer lastInstallmentNo, int topInstallment, Date leastDueDate,
			Date lastDueDate, Integer overdue, double outstandingPrincipalAmnt, double outstandingInterestAmnt,
			double monthlyInstallment, String businessUnit, String brand, String model, String color, String frameNo,
			String engineNo, String policeNo, String stnk, String printNo, Date printDate, List<BucketDetailDTO> details) {
		super();
		this.id = id;
		this.teamId = teamId;
		this.supervisorId = supervisorId;
		this.assignedTo = assignedTo;
		this.contractNo = contractNo;
		this.contractDate = contractDate;
		this.custName = custName;
		this.custBillingAddress = custBillingAddress;
		this.phone1 = phone1;
		this.phone2 = phone2;
		this.leastInstallmentNo = leastInstallmentNo;
		this.lastInstallmentNo = lastInstallmentNo;
		this.topInstallment = topInstallment;
		this.leastDueDate = leastDueDate;
		this.lastDueDate = lastDueDate;
		this.overdue = overdue;
		this.outstandingPrincipalAmnt = outstandingPrincipalAmnt;
		this.outstandingInterestAmnt = outstandingInterestAmnt;
		this.monthlyInstallment = monthlyInstallment;
		this.businessUnit = businessUnit;
		this.brand = brand;
		this.model = model;
		this.color = color;
		this.frameNo = frameNo;
		this.engineNo = engineNo;
		this.policeNo = policeNo;
		this.stnk = stnk;
		this.printNo = printNo;
		this.printDate = printDate;
		this.details = details;
	}

}
