package com.collection.jpa.dto;

import java.io.Serializable;

public class ZipDTO implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private Integer id;       
    private String zipcode;      
    private String subzipcode;      
	private String name;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getZipcode() {
		return zipcode;
	}
	public void setZipcode(String zipcode) {
		this.zipcode = zipcode;
	}
	public String getSubzipcode() {
		return subzipcode;
	}
	public void setSubzipcode(String subzipcode) {
		this.subzipcode = subzipcode;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	public ZipDTO(Integer id, String zipcode, String subzipcode, String name) {
		super();
		this.id = id;
		this.zipcode = zipcode;
		this.subzipcode = subzipcode;
		this.name = name;
	}
	@Override
	public String toString() {
		return "ZipDTO [id=" + id + ", zipcode=" + zipcode + ", subzipcode=" + subzipcode + ", name=" + name + "]";
	}
	
}
