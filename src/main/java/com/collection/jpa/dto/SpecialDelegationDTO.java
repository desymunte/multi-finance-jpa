package com.collection.jpa.dto;

import java.util.Date;

import javax.persistence.Temporal;
import javax.persistence.TemporalType;

public class SpecialDelegationDTO {
	
    private String memoNo;
    @Temporal(TemporalType.DATE)
	private Date memoDate;
    private Integer contractId;

    public String getMemoNo() {
		return memoNo;
	}

	public void setMemoNo(String memoNo) {
		this.memoNo = memoNo;
	}

	public Date getMemoDate() {
		return memoDate;
	}

	public void setMemoDate(Date memoDate) {
		this.memoDate = memoDate;
	}

	public Integer getContractId() {
		return contractId;
	}

	public void setContractId(Integer contractId) {
		this.contractId = contractId;
	}

	public SpecialDelegationDTO() {
		super();
		// TODO Auto-generated constructor stub
	}

	public SpecialDelegationDTO(String memoNo, Date memoDate, Integer contractId) {
		super();
		this.memoNo = memoNo;
		this.memoDate = memoDate;
		this.contractId = contractId;
	}

	@Override
	public String toString() {
		return "DelegationCriteria [memoNo=" + memoNo + ", memoDate=" + memoDate + ", contractId=" + contractId + "]";
	}

}
