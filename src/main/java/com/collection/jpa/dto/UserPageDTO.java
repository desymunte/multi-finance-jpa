package com.collection.jpa.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import com.collection.jpa.entity.Role;
import com.collection.jpa.entity.User;

public class UserPageDTO implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private Integer id;
	private String username;
	private List<GrantedAuthority> roles;
	private Integer emplId;
	private String emplRegNo;
	private String emplName;
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	public Integer getId() {
		return id;
	}
	public String getUsername() {
		return username;
	}
	public Integer getEmplId() {
		return emplId;
	}
	public String getEmplRegNo() {
		return emplRegNo;
	}
	public String getEmplName() {
		return emplName;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public void setEmplId(Integer emplId) {
		this.emplId = emplId;
	}
	public void setEmplRegNo(String emplRegNo) {
		this.emplRegNo = emplRegNo;
	}
	public void setEmplName(String emplName) {
		this.emplName = emplName;
	}
	public List<GrantedAuthority> getRoles() {
		return roles;
	}
	public void setRoles(List<GrantedAuthority> roles) {
		this.roles = roles;
	}
	
	
	public UserPageDTO() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public UserPageDTO(User user, Integer emplId, String emplRegNo, String emplName) {
		super();
		// TODO Auto-generated constructor stub
		this.id = user.getId();
		this.username = user.getUsername();
		this.emplId = emplId;
		this.emplRegNo = emplRegNo;
		this.emplName = emplName;
		List<GrantedAuthority> auths = new ArrayList<>();
		for(Role role: user.getRoles()) {
			auths.add(new SimpleGrantedAuthority(role.getName().toUpperCase()));
		}
		this.roles = auths;
	}
	
	
	@Override
	public String toString() {
		return "UserPageDTO [id=" + id + ", username=" + username + ", roles=" + roles + "]";
	}
	
	
	
}
