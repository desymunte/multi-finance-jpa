package com.collection.jpa.dto;


public class OfficeCriteria {
	
    private Integer id;      
    private String code;      
    private String name;

    public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public OfficeCriteria() {
		super();
		// TODO Auto-generated constructor stub
	}

	public OfficeCriteria(Integer id, String code, String name) {
		super();
		this.id = id;
		this.code = code;
		this.name = name;
	}

	@Override
	public String toString() {
		return "OfficeCriteria [id=" + id + ", code=" + code + ", name=" + name + "]";
	}

}
