package com.collection.jpa.dto;


public class ZipcodeAvailableCriteria {
	
    private Integer officeId;       
    private Integer cycleId;

    public Integer getOfficeId() {
		return officeId;
	}

	public void setOfficeId(Integer officeId) {
		this.officeId = officeId;
	}

	public Integer getCycleId() {
		return cycleId;
	}

	public void setCycleId(Integer cycleId) {
		this.cycleId = cycleId;
	}

	public ZipcodeAvailableCriteria() {
		super();
		// TODO Auto-generated constructor stub
	}

	public ZipcodeAvailableCriteria(Integer officeId, Integer cycleId) {
		super();
		this.officeId = officeId;
		this.cycleId = cycleId;
	}

	@Override
	public String toString() {
		return "ZipcodeAvailableCriteria [officeId=" + officeId + ", cycleId=" + cycleId + "]";
	}

}
