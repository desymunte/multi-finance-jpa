package com.collection.jpa.dto;


public class EmployeeCriteria {
	
    private Integer id;       
    private String regno;
    private String name;
    private Integer officeId;       
    private String jobTitle;
    private Integer jobLevel;

    public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getRegno() {
		return regno;
	}

	public void setRegno(String regno) {
		this.regno = regno;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getOfficeId() {
		return officeId;
	}

	public void setOfficeId(Integer officeId) {
		this.officeId = officeId;
	}

	public String getJobTitle() {
		return jobTitle;
	}

	public void setJobTitle(String jobTitle) {
		this.jobTitle = jobTitle;
	}

	public Integer getJobLevel() {
		return jobLevel;
	}

	public void setJobLevel(Integer jobLevel) {
		this.jobLevel = jobLevel;
	}

	public EmployeeCriteria() {
		super();
		// TODO Auto-generated constructor stub
	}

	public EmployeeCriteria(Integer id, String regno, String name, Integer officeId, String jobTitle,
			Integer jobLevel) {
		super();
		this.id = id;
		this.regno = regno;
		this.name = name;
		this.officeId = officeId;
		this.jobTitle = jobTitle;
		this.jobLevel = jobLevel;
	}

	@Override
	public String toString() {
		return "EmployeeCriteria [id=" + id + ", regno=" + regno + ", name=" + name + ", officeId=" + officeId
				+ ", jobTitle=" + jobTitle + ", jobLevel=" + jobLevel + "]";
	}       

}
