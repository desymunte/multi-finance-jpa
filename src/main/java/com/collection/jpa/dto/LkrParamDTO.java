package com.collection.jpa.dto;

import java.io.Serializable;

public class LkrParamDTO implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private Integer id;
    private Integer assignedTo;
    private String entryCode;

    public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getAssignedTo() {
		return assignedTo;
	}

	public void setAssignedTo(Integer assignedTo) {
		this.assignedTo = assignedTo;
	}

	public String getEntryCode() {
		return entryCode;
	}

	public void setEntryCode(String entryCode) {
		this.entryCode = entryCode;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public LkrParamDTO() {
		super();
		// TODO Auto-generated constructor stub
	}

	public LkrParamDTO(Integer id, Integer assignedTo, String entryCode) {
		super();
		this.id = id;
		this.assignedTo = assignedTo;
		this.entryCode = entryCode;
	}

	@Override
	public String toString() {
		return "LkrParamDTO [id=" + id + ", assignedTo=" + assignedTo + ", entryCode=" + entryCode + "]";
	}

}
