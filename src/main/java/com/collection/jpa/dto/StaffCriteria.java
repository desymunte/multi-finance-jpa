package com.collection.jpa.dto;

public class StaffCriteria {
	
    private Integer staffId;
    private Integer teamId;

    public Integer getStaffId() {
		return staffId;
	}

	public void setStaffId(Integer staffId) {
		this.staffId = staffId;
	}

	public Integer getTeamId() {
		return teamId;
	}

	public void setTeamId(Integer teamId) {
		this.teamId = teamId;
	}

	public StaffCriteria() {
		super();
		// TODO Auto-generated constructor stub
	}

	public StaffCriteria(Integer staffId, Integer teamId) {
		super();
		this.staffId = staffId;
		this.teamId = teamId;
	}

	@Override
	public String toString() {
		return "StaffCriteria [staffId=" + staffId + ", teamId=" + teamId + "]";
	}

}
