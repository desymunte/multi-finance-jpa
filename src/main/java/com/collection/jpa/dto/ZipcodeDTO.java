package com.collection.jpa.dto;


public class ZipcodeDTO {
	
    private Integer memberId;
    private String zipcode;      

    public Integer getMemberId() {
		return memberId;
	}

	public void setMemberId(Integer memberId) {
		this.memberId = memberId;
	}

	public String getZipcode() {
		return zipcode;
	}

	public void setZipcode(String zipcode) {
		this.zipcode = zipcode;
	}

	public ZipcodeDTO() {
		super();
		// TODO Auto-generated constructor stub
	}

	public ZipcodeDTO(Integer memberId, String zipcode) {
		super();
		this.memberId = memberId;
		this.zipcode = zipcode;
	}

	@Override
	public String toString() {
		return "ZipcodeDTO [memberId=" + memberId + ", zipcode=" + zipcode + "]";
	}

}
