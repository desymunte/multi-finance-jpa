package com.collection.jpa.dto;


public class OfficeDTO {
	
    private Integer id;       
    
    private String code;      
	private String name;
	private String address;
	private String addressAreaCode;
	private String phone;
	private String type;
	private String picName;
	private String picAddress;
	private String picAddressAreaCode;

	private String picPhone;

	private String picEmail;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getAddressAreaCode() {
		return addressAreaCode;
	}

	public void setAddressAreaCode(String addressAreaCode) {
		this.addressAreaCode = addressAreaCode;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getPicName() {
		return picName;
	}

	public void setPicName(String picName) {
		this.picName = picName;
	}

	public String getPicAddress() {
		return picAddress;
	}

	public void setPicAddress(String picAddress) {
		this.picAddress = picAddress;
	}

	public String getPicAddressAreaCode() {
		return picAddressAreaCode;
	}

	public void setPicAddressAreaCode(String picAddressAreaCode) {
		this.picAddressAreaCode = picAddressAreaCode;
	}

	public String getPicPhone() {
		return picPhone;
	}

	public void setPicPhone(String picPhone) {
		this.picPhone = picPhone;
	}

	public String getPicEmail() {
		return picEmail;
	}

	public void setPicEmail(String picEmail) {
		this.picEmail = picEmail;
	}

	public OfficeDTO(Integer id, String code, String name, String address, String addressAreaCode, String phone,
			String type, String picName, String picAddress, String picAddressAreaCode, String picPhone,
			String picEmail) {
		super();
		this.id = id;
		this.code = code;
		this.name = name;
		this.address = address;
		this.addressAreaCode = addressAreaCode;
		this.phone = phone;
		this.type = type;
		this.picName = picName;
		this.picAddress = picAddress;
		this.picAddressAreaCode = picAddressAreaCode;
		this.picPhone = picPhone;
		this.picEmail = picEmail;
	}

	public OfficeDTO() {
		super();
		// TODO Auto-generated constructor stub
	}

	@Override
	public String toString() {
		return "OfficeDTO [id=" + id + ", code=" + code + ", name=" + name + ", address=" + address
				+ ", addressAreaCode=" + addressAreaCode + ", phone=" + phone + ", type=" + type + ", picName="
				+ picName + ", picAddress=" + picAddress + ", picAddressAreaCode=" + picAddressAreaCode + ", picPhone="
				+ picPhone + ", picEmail=" + picEmail + "]";
	}
}
