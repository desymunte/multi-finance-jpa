package com.collection.jpa.entity;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Entity
@Table(name="coll_zip", schema="param")
//@Table(name="coll_zip", schema="param", uniqueConstraints={@UniqueConstraint(columnNames={"zip_id", "cycle_id"}, name="coll_zip_ukey")})
public class CollectionWorkingArea implements Serializable {
 
    private static final long serialVersionUID = 1L;
        
    @Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id", nullable=false, updatable=false)
    private Integer id;      
    
    @ManyToOne(optional=false, cascade=CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(name="coll_empl_id", referencedColumnName="id", foreignKey=@ForeignKey(name="collzip_collempl_fkey"), nullable=false)
    private CollectionMember collectionMember;
    
    @ManyToOne(optional=false, cascade=CascadeType.MERGE, fetch = FetchType.LAZY)
    @JoinColumn(name="zip_id", referencedColumnName="id", foreignKey=@ForeignKey(name="collzip_zip_fkey"), nullable=false)
    private Zipcode zipcode;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public CollectionMember getCollectionMember() {
		return collectionMember;
	}

	public void setCollectionMember(CollectionMember collectionMember) {
		this.collectionMember = collectionMember;
	}

	public Zipcode getZipcode() {
		return zipcode;
	}

	public void setZipcode(Zipcode zipcode) {
		this.zipcode = zipcode;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public CollectionWorkingArea() {
		super();
		// TODO Auto-generated constructor stub
	}

	public CollectionWorkingArea(Integer id, CollectionMember collectionMember, Zipcode zipcode) {
		super();
		this.id = id;
		this.collectionMember = collectionMember;
		this.zipcode = zipcode;
	}

	@Override
	public String toString() {
		return "CollectionWorkingArea [id=" + id + ", collectionMember=" + collectionMember + ", zipcode=" + zipcode
				+ "]";
	}

}
