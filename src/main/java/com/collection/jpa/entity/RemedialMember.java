package com.collection.jpa.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Entity
@Table(name="rem_empl", schema="param", uniqueConstraints = {@UniqueConstraint(columnNames={"empl_id"}, name="rem_empl_ukey")})
public class RemedialMember implements Serializable {
 
    private static final long serialVersionUID = 1L;
        
    @Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id", nullable=false, updatable=false)  
    private Integer id;      
    
    @OneToMany(mappedBy="remedialMember", cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = true)
    private List<RemedialWorkingArea> remedialWorkingAreas = new ArrayList<RemedialWorkingArea>();
    
	@ManyToMany(cascade= {CascadeType.MERGE},fetch=FetchType.EAGER)
	@JoinTable(name="rem_zip", schema="param",
	    joinColumns = {
			@JoinColumn(name="rem_empl_id", nullable=false)
		},
	    inverseJoinColumns = {
			@JoinColumn(name="zip_id", nullable=false)
		}
	)
    private List<Zipcode> zipcodes;
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="rem_id", referencedColumnName="id", foreignKey=@ForeignKey(name="remempl_rem_fkey"), nullable=false)
    private RemedialTeam remedialTeam;
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="empl_id", foreignKey=@ForeignKey(name="remempl_empl_fkey"), nullable=true)
    private Employee employee;
    
    @Column(name="mload", nullable=false)
	private Integer monthlyLoad = 0;
    
    @Column(name="dload", nullable=false)
	private Integer dailyLoad = 0;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public List<RemedialWorkingArea> getRemedialWorkingAreas() {
		return remedialWorkingAreas;
	}

	public void setRemedialWorkingAreas(List<RemedialWorkingArea> remedialWorkingAreas) {
		this.remedialWorkingAreas = remedialWorkingAreas;
	}

	public List<Zipcode> getZipcodes() {
		return zipcodes;
	}

	public void setZipcodes(List<Zipcode> zipcodes) {
		this.zipcodes = zipcodes;
	}

	public RemedialTeam getRemedialTeam() {
		return remedialTeam;
	}

	public void setRemedialTeam(RemedialTeam remedialTeam) {
		this.remedialTeam = remedialTeam;
	}

	public Employee getEmployee() {
		return employee;
	}

	public void setEmployee(Employee employee) {
		this.employee = employee;
	}

	public Integer getMonthlyLoad() {
		return monthlyLoad;
	}

	public void setMonthlyLoad(Integer monthlyLoad) {
		this.monthlyLoad = monthlyLoad;
	}

	public Integer getDailyLoad() {
		return dailyLoad;
	}

	public void setDailyLoad(Integer dailyLoad) {
		this.dailyLoad = dailyLoad;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public RemedialMember() {
		super();
		// TODO Auto-generated constructor stub
	}

	public RemedialMember(Integer id, List<RemedialWorkingArea> remedialWorkingAreas, List<Zipcode> zipcodes,
			RemedialTeam remedialTeam, Employee employee, Integer monthlyLoad, Integer dailyLoad) {
		super();
		this.id = id;
		this.remedialWorkingAreas = remedialWorkingAreas;
		this.zipcodes = zipcodes;
		this.remedialTeam = remedialTeam;
		this.employee = employee;
		this.monthlyLoad = monthlyLoad;
		this.dailyLoad = dailyLoad;
	}

	@Override
	public String toString() {
		return "RemedialMember [id=" + id + ", remedialWorkingAreas=" + remedialWorkingAreas + ", zipcodes=" + zipcodes
				+ ", employee=" + employee + ", monthlyLoad=" + monthlyLoad + ", dailyLoad=" + dailyLoad + "]";
	}

	public void addRemedialWorkingArea(RemedialWorkingArea remedialWorkingArea) {
		if (remedialWorkingArea != null) {
			this.remedialWorkingAreas.add(remedialWorkingArea);
			remedialWorkingArea.setRemedialMember(this);
	    }        
    }

}
