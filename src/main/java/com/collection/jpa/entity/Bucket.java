package com.collection.jpa.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="bucket", schema="public")
//@Table(name="bucket", schema="tran")
public class Bucket implements Serializable {
 
    private static final long serialVersionUID = 1L;
        
    @Id
    @Column(name = "cntrno", nullable=false, length=12)    
    private String contractNo;

    @Temporal(TemporalType.DATE)
    @Column(name="cntrdate", nullable=false)
	private Date contractDate;
	
    @Column(name = "officd", nullable=false, length=5)    
    private String officeCode;

    @Column(name="topinstl", nullable=false)
	private int topInstallment;

    @Column(name="outsdpriamnt")
	private double outstandingPrincipalAmnt;

    @Column(name="outsdintamnt")
	private double outstandingInterestAmnt;

    @Column(name="minstl")
	private double monthlyInstallment;

    @Column(name = "custname", length=50)
    private String custName;

    @Column(name = "custaddr", length=100)    
    private String custAddress;

    @Column(name = "custbilladdr", length=100)
    private String custBillingAddress;

    @Column(name = "zipcd", nullable=false, length=5)    
    private String zipcode;

    @Column(name = "subzipcd", length=2)    
    private String SubZipcode;

    @Column(name = "phone1", length=100)
    private String phone1;

    @Column(name = "phone2", length=100)
    private String phone2;

    @Column(name = "bizu", length=2)
    private String businessUnit;

    @Column(name="srcid")
	private int sourceAR_Id;

    @Temporal(TemporalType.DATE)
    @Column(name="leastduedate")
	private Date leastDueDate;

    @Column(name="leastinstlno")
	private int leastInstallmentNo;

    @Temporal(TemporalType.DATE)
    @Column(name="lastduedate")
	private Date lastDueDate;

    @Column(name="lastinstlno")
	private int lastInstallmentNo;

    @Column(name="ovd")
	private int overdue;

    @ManyToOne
    @JoinColumn(name="cycle_id", foreignKey=@ForeignKey(name="bucket_cycle_fkey"))
	private Cycle cycle;

    @OneToMany(mappedBy="bucket", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Unit> units = new ArrayList<Unit>();

    @OneToMany(mappedBy="bucket", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<BucketDetail> details = new ArrayList<BucketDetail>();

	public String getContractNo() {
		return contractNo;
	}

	public void setContractNo(String contractNo) {
		this.contractNo = contractNo;
	}

	public Date getContractDate() {
		return contractDate;
	}

	public void setContractDate(Date contractDate) {
		this.contractDate = contractDate;
	}

	public String getOfficeCode() {
		return officeCode;
	}

	public void setOfficeCode(String officeCode) {
		this.officeCode = officeCode;
	}

	public int getTopInstallment() {
		return topInstallment;
	}

	public void setTopInstallment(int topInstallment) {
		this.topInstallment = topInstallment;
	}

	public double getOutstandingPrincipalAmnt() {
		return outstandingPrincipalAmnt;
	}

	public void setOutstandingPrincipalAmnt(double outstandingPrincipalAmnt) {
		this.outstandingPrincipalAmnt = outstandingPrincipalAmnt;
	}

	public double getOutstandingInterestAmnt() {
		return outstandingInterestAmnt;
	}

	public void setOutstandingInterestAmnt(double outstandingInterestAmnt) {
		this.outstandingInterestAmnt = outstandingInterestAmnt;
	}

	public double getMonthlyInstallment() {
		return monthlyInstallment;
	}

	public void setMonthlyInstallment(double monthlyInstallment) {
		this.monthlyInstallment = monthlyInstallment;
	}

	public String getCustName() {
		return custName;
	}

	public void setCustName(String custName) {
		this.custName = custName;
	}

	public String getCustAddress() {
		return custAddress;
	}

	public void setCustAddress(String custAddress) {
		this.custAddress = custAddress;
	}

	public String getCustBillingAddress() {
		return custBillingAddress;
	}

	public void setCustBillingAddress(String custBillingAddress) {
		this.custBillingAddress = custBillingAddress;
	}

	public String getZipcode() {
		return zipcode;
	}

	public void setZipcode(String zipcode) {
		this.zipcode = zipcode;
	}

	public String getSubZipcode() {
		return SubZipcode;
	}

	public void setSubZipcode(String subZipcode) {
		SubZipcode = subZipcode;
	}

	public String getPhone1() {
		return phone1;
	}

	public void setPhone1(String phone1) {
		this.phone1 = phone1;
	}

	public String getPhone2() {
		return phone2;
	}

	public void setPhone2(String phone2) {
		this.phone2 = phone2;
	}

	public String getBusinessUnit() {
		return businessUnit;
	}

	public void setBusinessUnit(String businessUnit) {
		this.businessUnit = businessUnit;
	}

	public int getSourceAR_Id() {
		return sourceAR_Id;
	}

	public void setSourceAR_Id(int sourceAR_Id) {
		this.sourceAR_Id = sourceAR_Id;
	}

	public Date getLeastDueDate() {
		return leastDueDate;
	}

	public void setLeastDueDate(Date leastDueDate) {
		this.leastDueDate = leastDueDate;
	}

	public int getLeastInstallmentNo() {
		return leastInstallmentNo;
	}

	public void setLeastInstallmentNo(int leastInstallmentNo) {
		this.leastInstallmentNo = leastInstallmentNo;
	}

	public Date getLastDueDate() {
		return lastDueDate;
	}

	public void setLastDueDate(Date lastDueDate) {
		this.lastDueDate = lastDueDate;
	}

	public int getLastInstallmentNo() {
		return lastInstallmentNo;
	}

	public void setLastInstallmentNo(int lastInstallmentNo) {
		this.lastInstallmentNo = lastInstallmentNo;
	}

	public int getOverdue() {
		return overdue;
	}

	public void setOverdue(int overdue) {
		this.overdue = overdue;
	}

	public Cycle getCycle() {
		return cycle;
	}

	public void setCycle(Cycle cycle) {
		this.cycle = cycle;
	}

	public List<Unit> getUnits() {
		return units;
	}

	public void setUnits(List<Unit> units) {
		this.units = units;
	}

	public List<BucketDetail> getDetails() {
		return details;
	}

	public void setDetails(List<BucketDetail> details) {
		this.details = details;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public Bucket() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Bucket(String contractNo, Date contractDate, String officeCode, int topInstallment,
			double outstandingPrincipalAmnt, double outstandingInterestAmnt, double monthlyInstallment, String custName,
			String custAddress, String custBillingAddress, String zipcode, String subZipcode, String phone1,
			String phone2, String businessUnit, int sourceAR_Id, Date leastDueDate, int leastInstallmentNo,
			Date lastDueDate, int lastInstallmentNo, int overdue, Cycle cycle, List<Unit> units,
			List<BucketDetail> details) {
		super();
		this.contractNo = contractNo;
		this.contractDate = contractDate;
		this.officeCode = officeCode;
		this.topInstallment = topInstallment;
		this.outstandingPrincipalAmnt = outstandingPrincipalAmnt;
		this.outstandingInterestAmnt = outstandingInterestAmnt;
		this.monthlyInstallment = monthlyInstallment;
		this.custName = custName;
		this.custAddress = custAddress;
		this.custBillingAddress = custBillingAddress;
		this.zipcode = zipcode;
		SubZipcode = subZipcode;
		this.phone1 = phone1;
		this.phone2 = phone2;
		this.businessUnit = businessUnit;
		this.sourceAR_Id = sourceAR_Id;
		this.leastDueDate = leastDueDate;
		this.leastInstallmentNo = leastInstallmentNo;
		this.lastDueDate = lastDueDate;
		this.lastInstallmentNo = lastInstallmentNo;
		this.overdue = overdue;
		this.cycle = cycle;
		this.units = units;
		this.details = details;
	}

	@Override
	public String toString() {
		return "Bucket [contractNo=" + contractNo + ", contractDate=" + contractDate + ", officeCode=" + officeCode
				+ ", topInstallment=" + topInstallment + ", outstandingPrincipalAmnt=" + outstandingPrincipalAmnt
				+ ", outstandingInterestAmnt=" + outstandingInterestAmnt + ", monthlyInstallment=" + monthlyInstallment
				+ ", custName=" + custName + ", custAddress=" + custAddress + ", custBillingAddress="
				+ custBillingAddress + ", zipcode=" + zipcode + ", SubZipcode=" + SubZipcode + ", phone1=" + phone1
				+ ", phone2=" + phone2 + ", businessUnit=" + businessUnit + ", sourceAR_Id=" + sourceAR_Id
				+ ", leastDueDate=" + leastDueDate + ", leastInstallmentNo=" + leastInstallmentNo + ", lastDueDate="
				+ lastDueDate + ", lastInstallmentNo=" + lastInstallmentNo + ", overdue=" + overdue + ", cycle=" + cycle
				+ ", units=" + units + ", details=" + details + "]";
	}

}
