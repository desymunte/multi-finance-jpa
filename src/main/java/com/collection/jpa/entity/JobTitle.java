package com.collection.jpa.entity;
import javax.persistence.Table;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;


@Entity
@Table(name="job", schema="param")
public class JobTitle implements Serializable {
	
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="code", nullable=false, length=7)
	private String code;

    @Column(name="name", length=50)
	private String name;

	@Column(name="level", nullable=false)
	private Integer level;

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getLevel() {
		return level;
	}

	public void setLevel(Integer level) {
		this.level = level;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public JobTitle() {
		super();
		// TODO Auto-generated constructor stub
	}

	public JobTitle(String code, String name, Integer level) {
		super();
		this.code = code;
		this.name = name;
		this.level = level;
	}

	@Override
	public String toString() {
		return "JobTitle [code=" + code + ", name=" + name + ", level=" + level + "]";
	}

}
