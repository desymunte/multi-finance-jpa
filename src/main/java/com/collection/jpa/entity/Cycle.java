package com.collection.jpa.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Entity
@Table(name="cycle", schema="param", uniqueConstraints = {@UniqueConstraint(columnNames={"code"}, name="cycle_ukey")})
public class Cycle implements Serializable {
 
    private static final long serialVersionUID = 1L;
        
    @Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id", nullable=false, updatable=false)  
    private Integer id;      
    
    @Column(name="code", nullable=false, length=3)
	private String code;  
    
    @Column(name="name", nullable=false, length=30)
	private String name;
    
    @Column(name="descr")
	private String description;
    
    @Column(name="ovdstart", nullable=false)
	private Integer overdueStart;
    
    @Column(name="ovdend", nullable=false)
	private Integer overdueEnd;
    
    @ManyToOne
    @JoinColumn(name="type", foreignKey=@ForeignKey(name="cycle_type_fkey"), nullable=false)
	private CycleType type;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Integer getOverdueStart() {
		return overdueStart;
	}

	public void setOverdueStart(Integer overdueStart) {
		this.overdueStart = overdueStart;
	}

	public Integer getOverdueEnd() {
		return overdueEnd;
	}

	public void setOverdueEnd(Integer overdueEnd) {
		this.overdueEnd = overdueEnd;
	}

	public CycleType getType() {
		return type;
	}

	public void setType(CycleType type) {
		this.type = type;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public Cycle() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Cycle(Integer id, String code, String name, String description, Integer overdueStart, Integer overdueEnd,
			CycleType type) {
		super();
		this.id = id;
		this.code = code;
		this.name = name;
		this.description = description;
		this.overdueStart = overdueStart;
		this.overdueEnd = overdueEnd;
		this.type = type;
	}

	@Override
	public String toString() {
		return "Cycle [id=" + id + ", code=" + code + ", name=" + name + ", description=" + description
				+ ", overdueStart=" + overdueStart + ", overdueEnd=" + overdueEnd + ", type=" + type + "]";
	}

}
