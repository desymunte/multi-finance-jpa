package com.collection.jpa.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="lkdtrx", schema="public")
public class LKDTransaction implements Serializable {
 
    private static final long serialVersionUID = 1L;
        
    @Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id", nullable=false, updatable=false)  
    private Integer id;       
    
    @ManyToOne
    @JoinColumn(name="cntrno", foreignKey=@ForeignKey(name="lkdtrx_bucket_fkey"), nullable=false)
    private Bucket bucket;

    @Column(name="rundate", nullable=false)
	private Date runDate;
	
    @Column(name="team_id")
    private Integer teamId;

    @Column(name="spv_id")
    private Integer supervisorId;

    @ManyToOne
    @JoinColumn(name="print_id", foreignKey=@ForeignKey(name="lkdtrx_print_fkey"), nullable=true)
    private PrintActivity printActivity;

    @ManyToOne
    @JoinColumn(name="entry_cd", foreignKey=@ForeignKey(name="lkdtrx_lkdentry_fkey"), nullable=true)
    private LKDEntryStatus lkdEntryStatus;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Bucket getBucket() {
		return bucket;
	}

	public void setBucket(Bucket bucket) {
		this.bucket = bucket;
	}

	public Date getRunDate() {
		return runDate;
	}

	public void setRunDate(Date runDate) {
		this.runDate = runDate;
	}

	public Integer getTeamId() {
		return teamId;
	}

	public void setTeamId(Integer teamId) {
		this.teamId = teamId;
	}

	public Integer getSupervisorId() {
		return supervisorId;
	}

	public void setSupervisorId(Integer supervisorId) {
		this.supervisorId = supervisorId;
	}

	public PrintActivity getPrintActivity() {
		return printActivity;
	}

	public void setPrintActivity(PrintActivity printActivity) {
		this.printActivity = printActivity;
	}

	public LKDEntryStatus getLkdEntryStatus() {
		return lkdEntryStatus;
	}

	public void setLkdEntryStatus(LKDEntryStatus lkdEntryStatus) {
		this.lkdEntryStatus = lkdEntryStatus;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public LKDTransaction() {
		super();
		// TODO Auto-generated constructor stub
	}

	public LKDTransaction(Integer id, Bucket bucket, Date runDate, Integer teamId, Integer supervisorId,
			PrintActivity printActivity, LKDEntryStatus lkdEntryStatus) {
		super();
		this.id = id;
		this.bucket = bucket;
		this.runDate = runDate;
		this.teamId = teamId;
		this.supervisorId = supervisorId;
		this.printActivity = printActivity;
		this.lkdEntryStatus = lkdEntryStatus;
	}

	@Override
	public String toString() {
		return "LKDTransaction [id=" + id + ", bucket=" + bucket + ", runDate=" + runDate + ", teamId=" + teamId
				+ ", supervisorId=" + supervisorId + ", printActivity=" + printActivity + ", lkdEntryStatus="
				+ lkdEntryStatus + "]";
	}

}
