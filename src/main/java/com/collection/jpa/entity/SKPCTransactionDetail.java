package com.collection.jpa.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="skpctrxdet", schema="public")
public class SKPCTransactionDetail implements Serializable {
 
    private static final long serialVersionUID = 1L;
        
    @Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id", nullable=false, updatable=false)  
    private Integer id;       
    
    @ManyToOne
    @JoinColumn(name="skpctrx_id", foreignKey=@ForeignKey(name="skpctrxdet_skpctrx_fkey"), nullable=false)
    private SKPCTransaction skpcTransaction;

    @Column(name="seqno")
    private Integer sequenceNo;

    @Temporal(TemporalType.DATE)
    @Column(name="startdate", nullable=false)
	private Date startDate;
	
    @Temporal(TemporalType.DATE)
    @Column(name="enddate", nullable=false)
	private Date endDate;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public SKPCTransaction getSkpcTransaction() {
		return skpcTransaction;
	}

	public void setSkpcTransaction(SKPCTransaction skpcTransaction) {
		this.skpcTransaction = skpcTransaction;
	}

	public Integer getSequenceNo() {
		return sequenceNo;
	}

	public void setSequenceNo(Integer sequenceNo) {
		this.sequenceNo = sequenceNo;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public SKPCTransactionDetail() {
		super();
		// TODO Auto-generated constructor stub
	}

	public SKPCTransactionDetail(Integer id, SKPCTransaction skpcTransaction, Integer sequenceNo, Date startDate,
			Date endDate) {
		super();
		this.id = id;
		this.skpcTransaction = skpcTransaction;
		this.sequenceNo = sequenceNo;
		this.startDate = startDate;
		this.endDate = endDate;
	}

	@Override
	public String toString() {
		return "SKPCTransactionDetail [id=" + id + ", skpcTransaction=" + skpcTransaction + ", sequenceNo=" + sequenceNo
				+ ", startDate=" + startDate + ", endDate=" + endDate + "]";
	}
	
}
