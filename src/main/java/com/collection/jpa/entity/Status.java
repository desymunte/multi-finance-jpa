package com.collection.jpa.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Entity
@Table(name="stts", schema="param", uniqueConstraints = {@UniqueConstraint(columnNames={"descr"}, name="status_ukey")})
public class Status implements Serializable {
 
    private static final long serialVersionUID = 1L;
        
    @Id
    @Column(name="id", nullable=false, updatable=false)  
    private Integer id;      
    
    @Column(name="descr")
	private String description;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public Status() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Status(Integer id) {
		super();
		this.id = id;
	}

	public Status(Integer id, String description) {
		super();
		this.id = id;
		this.description = description;
	}

	@Override
	public String toString() {
		return "Status [id=" + id + ", description=" + description + "]";
	}

}
