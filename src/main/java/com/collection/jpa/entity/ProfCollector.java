package com.collection.jpa.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "prof", schema="param")
public class ProfCollector implements Serializable {
 
    private static final long serialVersionUID = 1L;
        
    @Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id", nullable=false, updatable=false)  
    private Integer id;      
    
    @OneToMany(mappedBy="profCollection", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<ProfCollMember> profcollMembers = new ArrayList<ProfCollMember>();
    
    @Column(name="name", nullable=false, length=50)
	private String name;  
    
    @Column(name="code", nullable=false, length=5)
	private String code;  
    
    @ManyToOne
    @JoinColumn(name="off_id", foreignKey=@ForeignKey(name="prof_off_fkey"), nullable=false)
    private Office office;
    
    @Column(name="idno", nullable=false, length=20)
	private String idNo;
    
    @Column(name="addr", length=100)
	private String address;  
    
    @Column(name="phone", length=20)
	private String phone;  
    
    @Column(name="email", length=50)
	private String email;

    @Column(name="type", nullable=false)
	private Integer type;

    @Column(name="mouno", length=20)
	private String mouNo;  
    
    @Temporal(TemporalType.DATE)
    @Column(name="moudate")
	private Date mouDate;  
    
    @Temporal(TemporalType.DATE)
    @Column(name="moustart")
	private Date mouStartDate;  
    
    @Temporal(TemporalType.DATE)
    @Column(name="mouend")
	private Date mouEndDate;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="status", foreignKey=@ForeignKey(name="prof_stts_fkey"), nullable=true)
	private Status mouStatus;

    @Column(name="perf")
	private Double performance;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public List<ProfCollMember> getProfcollMembers() {
		return profcollMembers;
	}

	public void setProfcollMembers(List<ProfCollMember> profcollMembers) {
		this.profcollMembers = profcollMembers;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Office getOffice() {
		return office;
	}

	public void setOffice(Office office) {
		this.office = office;
	}

	public String getIdNo() {
		return idNo;
	}

	public void setIdNo(String idNo) {
		this.idNo = idNo;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public String getMouNo() {
		return mouNo;
	}

	public void setMouNo(String mouNo) {
		this.mouNo = mouNo;
	}

	public Date getMouDate() {
		return mouDate;
	}

	public void setMouDate(Date mouDate) {
		this.mouDate = mouDate;
	}

	public Date getMouStartDate() {
		return mouStartDate;
	}

	public void setMouStartDate(Date mouStartDate) {
		this.mouStartDate = mouStartDate;
	}

	public Date getMouEndDate() {
		return mouEndDate;
	}

	public void setMouEndDate(Date mouEndDate) {
		this.mouEndDate = mouEndDate;
	}

	public Status getMouStatus() {
		return mouStatus;
	}

	public void setMouStatus(Status mouStatus) {
		this.mouStatus = mouStatus;
	}

	public Double getPerformance() {
		return performance;
	}

	public void setPerformance(Double performance) {
		this.performance = performance;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public ProfCollector() {
		super();
		// TODO Auto-generated constructor stub
	}

	public ProfCollector(Integer id, List<ProfCollMember> profcollMembers, String name, String code,
			Office office, String idNo, String address, String phone, String email, Integer type, String mouNo,
			Date mouDate, Date mouStartDate, Date mouEndDate, Status mouStatus, Double performance) {
		super();
		this.id = id;
		this.profcollMembers = profcollMembers;
		this.name = name;
		this.code = code;
		this.office = office;
		this.idNo = idNo;
		this.address = address;
		this.phone = phone;
		this.email = email;
		this.type = type;
		this.mouNo = mouNo;
		this.mouDate = mouDate;
		this.mouStartDate = mouStartDate;
		this.mouEndDate = mouEndDate;
		this.mouStatus = mouStatus;
		this.performance = performance;
	}

	@Override
	public String toString() {
		return "ProfCollection [id=" + id + ", profcollMembers=" + profcollMembers + ", name=" + name
				+ ", office=" + office + ", idNo=" + idNo + ", address=" + address + ", phone=" + phone + ", email="
				+ email + ", type=" + type + ", mouNo=" + mouNo + ", mouDate=" + mouDate + ", mouStartDate="
				+ mouStartDate + ", mouEndDate=" + mouEndDate + ", mouStatus=" + mouStatus + ", performance="
				+ performance + "]";
	}

	public void addProfCollMember(ProfCollMember profcollMember) {
		if (profcollMember != null) {
			this.profcollMembers.add(profcollMember);
			profcollMember.setProfCollection(this);
	    }
    }

}
