package com.collection.jpa.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="lkrtrx", schema="public")
//@Table(name="lkrtrx", schema="tran")
public class LKRTransaction implements Serializable {
 
    private static final long serialVersionUID = 1L;
        
    @Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id", nullable=false, updatable=false)  
    private Integer id;       
    
    @ManyToOne
    @JoinColumn(name="cntrno", foreignKey=@ForeignKey(name="lkrtrx_bucket_fkey"), nullable=false)
    private Bucket bucket;

    @Column(name="rundate", nullable=false)
	private Date runDate;
	
    @ManyToOne
    @JoinColumn(name="team_id", foreignKey=@ForeignKey(name="lkrtrx_coll_fkey"), nullable=true)
    //@Column(name="team_id")
    private RemedialTeam team;

    @ManyToOne
    @JoinColumn(name="spv_id", foreignKey=@ForeignKey(name="lkrtrxspv_empl_fkey"), nullable=true)
    //@Column(name="spv_id")
    private Employee supervisor;

    @ManyToOne
    @JoinColumn(name="asgnd_to", foreignKey=@ForeignKey(name="lkrtrx_empl_fkey"), nullable=true)
    //@Column(name="asgnd_to")
    private Employee assignedTo;

    @Column(name="owner_id")
    private Integer ownerId;

    @ManyToOne
    @JoinColumn(name="print_id", foreignKey=@ForeignKey(name="lkrtrx_print_fkey"), nullable=true)
    private PrintActivity printActivity;

    @ManyToOne
    @JoinColumn(name="entry_cd", foreignKey=@ForeignKey(name="lkrtrx_lkrentry_fkey"), nullable=true)
    private LKREntryStatus lkrEntryStatus;

    @Column(name="cmnt_id")
    private Integer lkrEntryCommentId;

    @Temporal(TemporalType.DATE)
    @Column(name="cmntdate")
	private Date dateRequired;

    @Column(name="cmnttext")
    private String strComment;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Bucket getBucket() {
		return bucket;
	}

	public void setBucket(Bucket bucket) {
		this.bucket = bucket;
	}

	public Date getRunDate() {
		return runDate;
	}

	public void setRunDate(Date runDate) {
		this.runDate = runDate;
	}

	public RemedialTeam getTeam() {
		return team;
	}

	public void setTeam(RemedialTeam team) {
		this.team = team;
	}

	public Employee getSupervisor() {
		return supervisor;
	}

	public void setSupervisor(Employee supervisor) {
		this.supervisor = supervisor;
	}

	public Employee getAssignedTo() {
		return assignedTo;
	}

	public void setAssignedTo(Employee assignedTo) {
		this.assignedTo = assignedTo;
	}

	public Integer getOwnerId() {
		return ownerId;
	}

	public void setOwnerId(Integer ownerId) {
		this.ownerId = ownerId;
	}

	public PrintActivity getPrintActivity() {
		return printActivity;
	}

	public void setPrintActivity(PrintActivity printActivity) {
		this.printActivity = printActivity;
	}

	public LKREntryStatus getLkrEntryStatus() {
		return lkrEntryStatus;
	}

	public void setLkrEntryStatus(LKREntryStatus lkrEntryStatus) {
		this.lkrEntryStatus = lkrEntryStatus;
	}

	public Integer getLkrEntryCommentId() {
		return lkrEntryCommentId;
	}

	public void setLkrEntryCommentId(Integer lkrEntryCommentId) {
		this.lkrEntryCommentId = lkrEntryCommentId;
	}

	public Date getDateRequired() {
		return dateRequired;
	}

	public void setDateRequired(Date dateRequired) {
		this.dateRequired = dateRequired;
	}

	public String getStrComment() {
		return strComment;
	}

	public void setStrComment(String strComment) {
		this.strComment = strComment;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public LKRTransaction() {
		super();
		// TODO Auto-generated constructor stub
	}

	public LKRTransaction(Integer id, Bucket bucket, Date runDate, RemedialTeam team, Employee supervisor,
			Employee assignedTo, Integer ownerId, PrintActivity printActivity, LKREntryStatus lkrEntryStatus,
			Integer lkrEntryCommentId, Date dateRequired, String strComment) {
		super();
		this.id = id;
		this.bucket = bucket;
		this.runDate = runDate;
		this.team = team;
		this.supervisor = supervisor;
		this.assignedTo = assignedTo;
		this.ownerId = ownerId;
		this.printActivity = printActivity;
		this.lkrEntryStatus = lkrEntryStatus;
		this.lkrEntryCommentId = lkrEntryCommentId;
		this.dateRequired = dateRequired;
		this.strComment = strComment;
	}

	@Override
	public String toString() {
		return "LKRTransaction [id=" + id + ", bucket=" + bucket + ", runDate=" + runDate + ", team=" + team
				+ ", supervisor=" + supervisor + ", assignedTo=" + assignedTo + ", ownerId=" + ownerId
				+ ", printActivity=" + printActivity + ", lkrEntryStatus=" + lkrEntryStatus + ", lkrEntryCommentId="
				+ lkrEntryCommentId + ", dateRequired=" + dateRequired + ", strComment=" + strComment + "]";
	}

}
