package com.collection.jpa.entity;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="rem_zip", schema="param")
//@NamedNativeQuery(name = "RemedialWorkingArea.deleteList", query = "delete from RemedialWorkingArea u where u.remedialMember.id = ?1")
//@Table(name="rem_zip", schema="param", uniqueConstraints={@UniqueConstraint(columnNames={"zip_id", "cycle_id"}, name="rem_zip_ukey")})
public class RemedialWorkingArea implements Serializable {
 
    private static final long serialVersionUID = 1L;
        
    @Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id", nullable=false, updatable=false)
    private Integer id;      
    
    @ManyToOne(optional=false, cascade=CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(name="rem_empl_id", referencedColumnName="id", foreignKey=@ForeignKey(name="remzip_remempl_fkey"), nullable=false)
    private RemedialMember remedialMember;
    
    @ManyToOne(optional=false, cascade=CascadeType.MERGE, fetch = FetchType.LAZY)
    @JoinColumn(name="zip_id", referencedColumnName="id", foreignKey=@ForeignKey(name="remzip_zip_fkey"), nullable=false)
    private Zipcode zipcode;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public RemedialMember getRemedialMember() {
		return remedialMember;
	}

	public void setRemedialMember(RemedialMember remedialMember) {
		this.remedialMember = remedialMember;
	}

	public Zipcode getZipcode() {
		return zipcode;
	}

	public void setZipcode(Zipcode zipcode) {
		this.zipcode = zipcode;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public RemedialWorkingArea() {
		super();
		// TODO Auto-generated constructor stub
	}

	public RemedialWorkingArea(Integer id, RemedialMember remedialMember, Zipcode zipcode) {
		super();
		this.id = id;
		this.remedialMember = remedialMember;
		this.zipcode = zipcode;
	}

	@Override
	public String toString() {
		return "RemedialWorkingArea [id=" + id + ", remedialMember=" + remedialMember + ", zipcode=" + zipcode
				+ "]";
	}

}
