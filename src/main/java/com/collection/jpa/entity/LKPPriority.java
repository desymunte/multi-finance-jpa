package com.collection.jpa.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Entity
@Table(name="lkpprio", schema="param", 
	uniqueConstraints = {
			@UniqueConstraint(columnNames={"code"}, name="lkpprio_ukey"), 
			@UniqueConstraint(columnNames={"type", "seqno"}, name="lkpprio_seqno_ukey")})
public class LKPPriority implements Serializable {
 
    private static final long serialVersionUID = 1L;
        
    @Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id", nullable=false, updatable=false)  
    private Integer id;      

    @Column(name="code", nullable=false, length=3)
	private String code;  
    
    @Column(name="script", nullable=false, columnDefinition="TEXT")
    private String script;
    
    @Column(name="descr", length=256)
	private String description;
    
    @Column(name="seqno")
	private Integer seqNo;
    
    @Column(name="type", nullable=false, length=1)
	private String type;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getScript() {
		return script;
	}

	public void setScript(String script) {
		this.script = script;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Integer getSeqNo() {
		return seqNo;
	}

	public void setSeqNo(Integer seqNo) {
		this.seqNo = seqNo;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public LKPPriority() {
		super();
		// TODO Auto-generated constructor stub
	}

	public LKPPriority(Integer id, String code, String script, String description, Integer seqNo, String type) {
		super();
		this.id = id;
		this.code = code;
		this.script = script;
		this.description = description;
		this.seqNo = seqNo;
		this.type = type;
	}

	@Override
	public String toString() {
		return "LKPPriority [id=" + id + ", code=" + code + ", script=" + script + ", description=" + description
				+ ", seqNo=" + seqNo + ", type=" + type + "]";
	}

}
