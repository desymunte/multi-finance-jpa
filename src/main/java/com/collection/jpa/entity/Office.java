package com.collection.jpa.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Entity
@Table(name="offi", schema="param", uniqueConstraints={@UniqueConstraint(columnNames={"code"}, name="off_ukey")}
)
public class Office implements Serializable {
 
    private static final long serialVersionUID = 1L;
        
    @Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id", nullable=false, updatable=false)  
    private Integer id;       
    
    @Column(name = "code", nullable=false, length=5)    
    private String code;      

    @Column(name="name", nullable=false, length=100)
	private String name;
	
    @Column(name="addr", length=100)
	private String address;

    @Column(name="phone", length=20)
	private String phone;

    @Column(name="type", length=1)
	private String type;

    @Column(name="pic_name", length=50)
	private String picName;

    @Column(name="pic_addr", length=100)
	private String picAddress;

    @Column(name="pic_phone", length=20)
	private String picPhone;

    @Column(name="pic_email", length=50)
	private String picEmail;

    @Column(name="skpc_fee", nullable=false)
	private Double skpcFee;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getPicName() {
		return picName;
	}

	public void setPicName(String picName) {
		this.picName = picName;
	}

	public String getPicAddress() {
		return picAddress;
	}

	public void setPicAddress(String picAddress) {
		this.picAddress = picAddress;
	}

	public String getPicPhone() {
		return picPhone;
	}

	public void setPicPhone(String picPhone) {
		this.picPhone = picPhone;
	}

	public String getPicEmail() {
		return picEmail;
	}

	public void setPicEmail(String picEmail) {
		this.picEmail = picEmail;
	}

	public Double getSkpcFee() {
		return skpcFee;
	}

	public void setSkpcFee(Double skpcFee) {
		this.skpcFee = skpcFee;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public Office() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Office(Integer id, String code, String name, String address, String phone, String type, String picName,
			String picAddress, String picPhone, String picEmail, Double skpcFee) {
		super();
		this.id = id;
		this.code = code;
		this.name = name;
		this.address = address;
		this.phone = phone;
		this.type = type;
		this.picName = picName;
		this.picAddress = picAddress;
		this.picPhone = picPhone;
		this.picEmail = picEmail;
		this.skpcFee = skpcFee;
	}

	@Override
	public String toString() {
		return "Office [id=" + id + ", code=" + code + ", name=" + name + ", address=" + address + ", phone=" + phone
				+ ", type=" + type + ", picName=" + picName + ", picAddress=" + picAddress + ", picPhone=" + picPhone
				+ ", picEmail=" + picEmail + ", skpcFee=" + skpcFee + "]";
	}

}
