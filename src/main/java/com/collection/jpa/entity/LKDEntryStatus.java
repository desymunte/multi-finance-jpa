package com.collection.jpa.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="lkdentry", schema="param")
public class LKDEntryStatus implements Serializable {
 
    private static final long serialVersionUID = 1L;
        
    @Id
    @Column(name="code", nullable=false, length=3)
	private String code;  
    
    @Column(name="descr", length=256)
	private String description;

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public LKDEntryStatus() {
		super();
		// TODO Auto-generated constructor stub
	}

	public LKDEntryStatus(String code, String description) {
		super();
		this.code = code;
		this.description = description;
	}

	@Override
	public String toString() {
		return "LKDEntryStatus [code=" + code + ", description=" + description + "]";
	}
    
}
