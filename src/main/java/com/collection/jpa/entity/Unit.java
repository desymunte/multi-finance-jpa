package com.collection.jpa.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="unit", schema="public")
//@Table(name="object", schema="tran")
public class Unit implements Serializable {
 
    private static final long serialVersionUID = 1L;
        
    @Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id", nullable=false, updatable=false)  
    private Integer id;      

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="cntrno", foreignKey=@ForeignKey(name="unit_bucket_fkey"), nullable=false)
    private Bucket bucket;

    @Column(name = "brand", length=50)    
    private String brand;

    @Column(name = "model", length=50)    
    private String model;

    @Column(name = "color", length=50)    
    private String color;

    @Column(name = "frameno", length=50)    
    private String frameNo;

    @Column(name = "engineno", length=50)    
    private String engineNo;

    @Column(name = "polno", length=50)    
    private String policeNo;

    @Column(name = "stnk", length=50)    
    private String stnk;

    @Column(name="addinfo", columnDefinition="TEXT")
    private String additionalInfo;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Bucket getBucket() {
		return bucket;
	}

	public void setBucket(Bucket bucket) {
		this.bucket = bucket;
	}

	public String getBrand() {
		return brand;
	}

	public void setBrand(String brand) {
		this.brand = brand;
	}

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public String getFrameNo() {
		return frameNo;
	}

	public void setFrameNo(String frameNo) {
		this.frameNo = frameNo;
	}

	public String getEngineNo() {
		return engineNo;
	}

	public void setEngineNo(String engineNo) {
		this.engineNo = engineNo;
	}

	public String getPoliceNo() {
		return policeNo;
	}

	public void setPoliceNo(String policeNo) {
		this.policeNo = policeNo;
	}

	public String getStnk() {
		return stnk;
	}

	public void setStnk(String stnk) {
		this.stnk = stnk;
	}

	public String getAdditionalInfo() {
		return additionalInfo;
	}

	public void setAdditionalInfo(String additionalInfo) {
		this.additionalInfo = additionalInfo;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public Unit() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Unit(Integer id, Bucket bucket, String brand, String model, String color, String frameNo, String engineNo,
			String policeNo, String stnk, String additionalInfo) {
		super();
		this.id = id;
		this.bucket = bucket;
		this.brand = brand;
		this.model = model;
		this.color = color;
		this.frameNo = frameNo;
		this.engineNo = engineNo;
		this.policeNo = policeNo;
		this.stnk = stnk;
		this.additionalInfo = additionalInfo;
	}

	@Override
	public String toString() {
		return "Unit [id=" + id + ", bucket=" + bucket + ", brand=" + brand + ", model=" + model + ", color=" + color
				+ ", frameNo=" + frameNo + ", engineNo=" + engineNo + ", policeNo=" + policeNo + ", stnk=" + stnk
				+ ", additionalInfo=" + additionalInfo + "]";
	}

}
