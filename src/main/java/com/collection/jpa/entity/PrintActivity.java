package com.collection.jpa.entity;
import javax.persistence.Table;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;


@Entity
@Table(name="print", schema="public")
public class PrintActivity implements Serializable {
	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id", nullable=false, updatable=false)  
    private Integer id;
    
    @Column(name="printno", nullable=false, updatable=false, length=20)
	private String printNo;

    @Column(name="team_id")
	private Integer teamId;

/*    @ManyToOne
    @JoinColumn(name="empl", foreignKey=@ForeignKey(name="print_empl_fkey"), nullable=false)
    private Employee employee;
*/
    @Column(name="empl_id")
    private Integer employeeId;

    @Column(name="printdate")
	private Date printDate;

    @Column(name="type", nullable=false, updatable=false, length=1)
	private String type;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getPrintNo() {
		return printNo;
	}

	public void setPrintNo(String printNo) {
		this.printNo = printNo;
	}

	public Integer getTeamId() {
		return teamId;
	}

	public void setTeamId(Integer teamId) {
		this.teamId = teamId;
	}

	public Integer getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(Integer employeeId) {
		this.employeeId = employeeId;
	}

	public Date getPrintDate() {
		return printDate;
	}

	public void setPrintDate(Date printDate) {
		this.printDate = printDate;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public PrintActivity() {
		super();
		// TODO Auto-generated constructor stub
	}

	public PrintActivity(Integer id, String printNo, Integer teamId, Integer employeeId, Date printDate, String type) {
		super();
		this.id = id;
		this.printNo = printNo;
		this.teamId = teamId;
		this.employeeId = employeeId;
		this.printDate = printDate;
		this.type = type;
	}

	@Override
	public String toString() {
		return "PrintActivity [id=" + id + ", printNo=" + printNo + ", teamId=" + teamId + ", employeeId=" + employeeId
				+ ", printDate=" + printDate + ", type=" + type + "]";
	}

}
