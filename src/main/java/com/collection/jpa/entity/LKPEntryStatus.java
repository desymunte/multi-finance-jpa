package com.collection.jpa.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="lkpentry", schema="param")
public class LKPEntryStatus implements Serializable {
 
    private static final long serialVersionUID = 1L;
        
    @Id
    @Column(name="code", nullable=false, length=3)
	private String code;  
    
    @Column(name="name", nullable=false, length=30)
    private String name;
    
    @Column(name="descr", length=256)
	private String description;
    
    @Column(name="reason", nullable=false)
	private Boolean isReasonRequired = false;
    
    @Column(name="date", nullable=false)
	private Boolean isDateRequired = false;

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Boolean getIsReasonRequired() {
		return isReasonRequired;
	}

	public void setIsReasonRequired(Boolean isReasonRequired) {
		this.isReasonRequired = isReasonRequired;
	}

	public Boolean getIsDateRequired() {
		return isDateRequired;
	}

	public void setIsDateRequired(Boolean isDateRequired) {
		this.isDateRequired = isDateRequired;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public LKPEntryStatus() {
		super();
		// TODO Auto-generated constructor stub
	}

	public LKPEntryStatus(String code, String name, String description, Boolean isReasonRequired,
			Boolean isDateRequired) {
		super();
		this.code = code;
		this.name = name;
		this.description = description;
		this.isReasonRequired = isReasonRequired;
		this.isDateRequired = isDateRequired;
	}

	@Override
	public String toString() {
		return "LKPEntryStatus [code=" + code + ", name=" + name + ", description=" + description
				+ ", isReasonRequired=" + isReasonRequired + ", isDateRequired=" + isDateRequired + "]";
	}

}
