package com.collection.jpa.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="bucketdetail", schema="public")
//@Table(name="bucketdetail", schema="tran")
public class BucketDetail implements Serializable {
 
    private static final long serialVersionUID = 1L;
        
    @Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id", nullable=false, updatable=false)  
    private Integer id;       
    
    @ManyToOne
    @JoinColumn(name="cntrno", foreignKey=@ForeignKey(name="detail_bucket_fkey"), nullable=false)
    private Bucket bucket;

    @Column(name="instlno", nullable=false)
	private int installmentNo;

    @Column(name="priamnt", nullable=false)
	private double principalAmnt;

    @Column(name="intamnt")
	private double interestAmnt;

    @Temporal(TemporalType.DATE)
    @Column(name="duedate", nullable=false)
	private Date dueDate;

    @Column(name="ovd", nullable=false)
	private int overdue;

    @Column(name="priamntpaid")
	private double principalAmntPaid;

    @Column(name="intamntpaid")
	private double interestAmntPaid;

    @Temporal(TemporalType.DATE)
    @Column(name="paydate")
	private Date paymentDate;

    @Column(name="pnltamnt")
	private double penaltyAmnt;

    @Column(name="pnltamntpaid")
	private double penaltyAmntPaid;

    @Temporal(TemporalType.DATE)
    @Column(name="pnltpaydate")
	private Date penaltyPaymentDate;

    @Column(name="collfee")
	private double collectionFee;

    @Column(name="collfeepaid")
	private double collectionFeePaid;

    @Temporal(TemporalType.DATE)
    @Column(name="collfeepaydate")
	private Date collectionFeePaymentDate;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Bucket getBucket() {
		return bucket;
	}

	public void setBucket(Bucket bucket) {
		this.bucket = bucket;
	}

	public int getInstallmentNo() {
		return installmentNo;
	}

	public void setInstallmentNo(int installmentNo) {
		this.installmentNo = installmentNo;
	}

	public double getPrincipalAmnt() {
		return principalAmnt;
	}

	public void setPrincipalAmnt(double principalAmnt) {
		this.principalAmnt = principalAmnt;
	}

	public double getInterestAmnt() {
		return interestAmnt;
	}

	public void setInterestAmnt(double interestAmnt) {
		this.interestAmnt = interestAmnt;
	}

	public Date getDueDate() {
		return dueDate;
	}

	public void setDueDate(Date dueDate) {
		this.dueDate = dueDate;
	}

	public int getOverdue() {
		return overdue;
	}

	public void setOverdue(int overdue) {
		this.overdue = overdue;
	}

	public double getPrincipalAmntPaid() {
		return principalAmntPaid;
	}

	public void setPrincipalAmntPaid(double principalAmntPaid) {
		this.principalAmntPaid = principalAmntPaid;
	}

	public double getInterestAmntPaid() {
		return interestAmntPaid;
	}

	public void setInterestAmntPaid(double interestAmntPaid) {
		this.interestAmntPaid = interestAmntPaid;
	}

	public Date getPaymentDate() {
		return paymentDate;
	}

	public void setPaymentDate(Date paymentDate) {
		this.paymentDate = paymentDate;
	}

	public double getPenaltyAmnt() {
		return penaltyAmnt;
	}

	public void setPenaltyAmnt(double penaltyAmnt) {
		this.penaltyAmnt = penaltyAmnt;
	}

	public double getPenaltyAmntPaid() {
		return penaltyAmntPaid;
	}

	public void setPenaltyAmntPaid(double penaltyAmntPaid) {
		this.penaltyAmntPaid = penaltyAmntPaid;
	}

	public Date getPenaltyPaymentDate() {
		return penaltyPaymentDate;
	}

	public void setPenaltyPaymentDate(Date penaltyPaymentDate) {
		this.penaltyPaymentDate = penaltyPaymentDate;
	}

	public double getCollectionFee() {
		return collectionFee;
	}

	public void setCollectionFee(double collectionFee) {
		this.collectionFee = collectionFee;
	}

	public double getCollectionFeePaid() {
		return collectionFeePaid;
	}

	public void setCollectionFeePaid(double collectionFeePaid) {
		this.collectionFeePaid = collectionFeePaid;
	}

	public Date getCollectionFeePaymentDate() {
		return collectionFeePaymentDate;
	}

	public void setCollectionFeePaymentDate(Date collectionFeePaymentDate) {
		this.collectionFeePaymentDate = collectionFeePaymentDate;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public BucketDetail() {
		super();
		// TODO Auto-generated constructor stub
	}

	public BucketDetail(Integer id, Bucket bucket, int installmentNo, double principalAmnt, double interestAmnt,
			Date dueDate, int overdue, double principalAmntPaid, double interestAmntPaid, Date paymentDate,
			double penaltyAmnt, double penaltyAmntPaid, Date penaltyPaymentDate, double collectionFee,
			double collectionFeePaid, Date collectionFeePaymentDate) {
		super();
		this.id = id;
		this.bucket = bucket;
		this.installmentNo = installmentNo;
		this.principalAmnt = principalAmnt;
		this.interestAmnt = interestAmnt;
		this.dueDate = dueDate;
		this.overdue = overdue;
		this.principalAmntPaid = principalAmntPaid;
		this.interestAmntPaid = interestAmntPaid;
		this.paymentDate = paymentDate;
		this.penaltyAmnt = penaltyAmnt;
		this.penaltyAmntPaid = penaltyAmntPaid;
		this.penaltyPaymentDate = penaltyPaymentDate;
		this.collectionFee = collectionFee;
		this.collectionFeePaid = collectionFeePaid;
		this.collectionFeePaymentDate = collectionFeePaymentDate;
	}

	@Override
	public String toString() {
		return "BucketDetail [id=" + id + ", bucket=" + bucket + ", installmentNo=" + installmentNo + ", principalAmnt="
				+ principalAmnt + ", interestAmnt=" + interestAmnt + ", dueDate=" + dueDate + ", overdue=" + overdue
				+ ", principalAmntPaid=" + principalAmntPaid + ", interestAmntPaid=" + interestAmntPaid
				+ ", paymentDate=" + paymentDate + ", penaltyAmnt=" + penaltyAmnt + ", penaltyAmntPaid="
				+ penaltyAmntPaid + ", penaltyPaymentDate=" + penaltyPaymentDate + ", collectionFee=" + collectionFee
				+ ", collectionFeePaid=" + collectionFeePaid + ", collectionFeePaymentDate=" + collectionFeePaymentDate
				+ "]";
	}

}
