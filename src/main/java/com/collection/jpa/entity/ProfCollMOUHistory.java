package com.collection.jpa.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "mou", schema="hist")
public class ProfCollMOUHistory implements Serializable {
 
    private static final long serialVersionUID = 1L;
        
    @Id
    @Column(name="mouno", nullable=false, length=20)
	private String mouNo;  
    
    @ManyToOne
    @JoinColumn(name="prof_id", foreignKey=@ForeignKey(name="profmou_prof_fkey"), nullable=false)
    private ProfCollector profCollection;
    
    @Temporal(TemporalType.DATE)
    @Column(name="moudate", nullable=false)
	private Date mouDate;  
    
    @Temporal(TemporalType.DATE)
    @Column(name="moustart", nullable=false)
	private Date startDate;  
    
    @Temporal(TemporalType.DATE)
    @Column(name="mouend", nullable=false)
	private Date endDate;

    @Column(name="status", nullable=false)
	private Integer lastStatus;

    @Column(name="perf", nullable=false)
	private Double performance;

	public String getMouNo() {
		return mouNo;
	}

	public void setMouNo(String mouNo) {
		this.mouNo = mouNo;
	}

	public ProfCollector getProfCollection() {
		return profCollection;
	}

	public void setProfCollection(ProfCollector profCollection) {
		this.profCollection = profCollection;
	}

	public Date getMouDate() {
		return mouDate;
	}

	public void setMouDate(Date mouDate) {
		this.mouDate = mouDate;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public Integer getLastStatus() {
		return lastStatus;
	}

	public void setLastStatus(Integer lastStatus) {
		this.lastStatus = lastStatus;
	}

	public Double getPerformance() {
		return performance;
	}

	public void setPerformance(Double performance) {
		this.performance = performance;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public ProfCollMOUHistory() {
		super();
		// TODO Auto-generated constructor stub
	}

	public ProfCollMOUHistory(String mouNo, ProfCollector profCollection, Date mouDate, Date startDate,
			Date endDate, Integer lastStatus, Double performance) {
		super();
		this.mouNo = mouNo;
		this.profCollection = profCollection;
		this.mouDate = mouDate;
		this.startDate = startDate;
		this.endDate = endDate;
		this.lastStatus = lastStatus;
		this.performance = performance;
	}

	@Override
	public String toString() {
		return "ProfCollectionMOUHistory [mouNo=" + mouNo + ", profCollection=" + profCollection + ", mouDate="
				+ mouDate + ", startDate=" + startDate + ", endDate=" + endDate + ", lastStatus=" + lastStatus
				+ ", performance=" + performance + "]";
	}

}
