package com.collection.jpa.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="coll", schema="param")
public class CollectionTeam implements Serializable {
 
    private static final long serialVersionUID = 1L;
        
    @Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id", nullable=false, updatable=false)
    private Integer id;      
    
    @OneToMany(mappedBy="collectionTeam", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<CollectionMember> collectionMembers = new ArrayList<CollectionMember>();
    
    @Column(name="name", nullable=false, length=50)
	private String name;
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="off_id", foreignKey=@ForeignKey(name="coll_off_fkey"), nullable=false)
    private Office office;
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="cycle_id", foreignKey=@ForeignKey(name="coll_cycle_fkey"), nullable=false)
    private Cycle cycle;
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="spv_id", foreignKey=@ForeignKey(name="coll_empl_fkey"), nullable=true)
    private Employee supervisor;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public List<CollectionMember> getCollectionMembers() {
		return collectionMembers;
	}

	public void setCollectionMembers(List<CollectionMember> collectionMembers) {
		this.collectionMembers = collectionMembers;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Office getOffice() {
		return office;
	}

	public void setOffice(Office office) {
		this.office = office;
	}

	public Cycle getCycle() {
		return cycle;
	}

	public void setCycle(Cycle cycle) {
		this.cycle = cycle;
	}

	public Employee getSupervisor() {
		return supervisor;
	}

	public void setSupervisor(Employee supervisor) {
		this.supervisor = supervisor;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public CollectionTeam(Integer id, List<CollectionMember> collectionMembers, String name, Office office,
			Cycle cycle, Employee supervisor) {
		super();
		this.id = id;
		this.collectionMembers = collectionMembers;
		this.name = name;
		this.office = office;
		this.cycle = cycle;
		this.supervisor = supervisor;
	}

	public CollectionTeam() {
		super();
		// TODO Auto-generated constructor stub
	}

	@Override
	public String toString() {
		return "CollectionTeam [id=" + id + ", collectionMembers=" + collectionMembers + ", name=" + name
				+ ", office=" + office + ", cycle=" + cycle + ", supervisor=" + supervisor + "]";
	}

	public void addCollectionMember(CollectionMember collectionMember) {
		if (collectionMember != null) {
			this.collectionMembers.add(collectionMember);
	        collectionMember.setCollectionTeam(this);
	    }
    }

}
