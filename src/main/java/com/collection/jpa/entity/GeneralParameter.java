package com.collection.jpa.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="genparam", schema="param")
public class GeneralParameter implements Serializable {
 
    private static final long serialVersionUID = 1L;
        
    @Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id", nullable=false, updatable=false)  
    private Integer id;      

    @Column(name="company_name", length=50, nullable=false)
	private String company_name = "PT. Serasi Raya Autofinance";
    
    @Column(name="company_address", length=100, nullable=false)
	private String company_address = "Jl. Raya No. 1";
    
    @Column(name="lkp_max_entry_days", nullable=false)
	private Integer lkp_max_entry_days = 1;
    
    @Column(name="skpc_validity_period", nullable=false)
	private Integer skpc_validity_period = 14;
    
    @Column(name="skpc_extended_validity_period", nullable=false)
	private Integer skpc_extended_validity_period = 7;
    
    @Column(name="skpc_max_idle_days", nullable=false)
	private Integer skpc_max_idle_days = 3;
    
    @Column(name="skpc_max_extend", nullable=false)
	private Integer skpc_max_extend = 2;
    
    @Column(name="skpc_max_outstanding_load", nullable=false)
	private Integer skpc_max_outstanding_load = 15;
    
    @Column(name="mou_min_performance", nullable=false)
	private Double mou_min_performance = 60.00;

    @Column(name="report_dir", nullable=false)
	private String report_dir = "D:\\Projects\\Multifinance\\Design\\App\\JPAEngga\\20180119\\MFCollectionJPAServer\\src\\main\\resources\\report\\";

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getCompany_name() {
		return company_name;
	}

	public void setCompany_name(String company_name) {
		this.company_name = company_name;
	}

	public String getCompany_address() {
		return company_address;
	}

	public void setCompany_address(String company_address) {
		this.company_address = company_address;
	}

	public Integer getLkp_max_entry_days() {
		return lkp_max_entry_days;
	}

	public void setLkp_max_entry_days(Integer lkp_max_entry_days) {
		this.lkp_max_entry_days = lkp_max_entry_days;
	}

	public Integer getSkpc_validity_period() {
		return skpc_validity_period;
	}

	public void setSkpc_validity_period(Integer skpc_validity_period) {
		this.skpc_validity_period = skpc_validity_period;
	}

	public Integer getSkpc_extended_validity_period() {
		return skpc_extended_validity_period;
	}

	public void setSkpc_extended_validity_period(Integer skpc_extended_validity_period) {
		this.skpc_extended_validity_period = skpc_extended_validity_period;
	}

	public Integer getSkpc_max_idle_days() {
		return skpc_max_idle_days;
	}

	public void setSkpc_max_idle_days(Integer skpc_max_idle_days) {
		this.skpc_max_idle_days = skpc_max_idle_days;
	}

	public Integer getSkpc_max_extend() {
		return skpc_max_extend;
	}

	public void setSkpc_max_extend(Integer skpc_max_extend) {
		this.skpc_max_extend = skpc_max_extend;
	}

	public Integer getSkpc_max_outstanding_load() {
		return skpc_max_outstanding_load;
	}

	public void setSkpc_max_outstanding_load(Integer skpc_max_outstanding_load) {
		this.skpc_max_outstanding_load = skpc_max_outstanding_load;
	}

	public Double getMou_min_performance() {
		return mou_min_performance;
	}

	public void setMou_min_performance(Double mou_min_performance) {
		this.mou_min_performance = mou_min_performance;
	}

	public String getReport_dir() {
		return report_dir;
	}

	public void setReport_dir(String report_dir) {
		this.report_dir = report_dir;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public GeneralParameter() {
		super();
		// TODO Auto-generated constructor stub
	}

	public GeneralParameter(Integer id, String company_name, String company_address, Integer lkp_max_entry_days,
			Integer skpc_validity_period, Integer skpc_extended_validity_period, Integer skpc_max_idle_days,
			Integer skpc_max_extend, Integer skpc_max_outstanding_load, Double mou_min_performance, String report_dir) {
		super();
		this.id = id;
		this.company_name = company_name;
		this.company_address = company_address;
		this.lkp_max_entry_days = lkp_max_entry_days;
		this.skpc_validity_period = skpc_validity_period;
		this.skpc_extended_validity_period = skpc_extended_validity_period;
		this.skpc_max_idle_days = skpc_max_idle_days;
		this.skpc_max_extend = skpc_max_extend;
		this.skpc_max_outstanding_load = skpc_max_outstanding_load;
		this.mou_min_performance = mou_min_performance;
		this.report_dir = report_dir;
	}

	@Override
	public String toString() {
		return "GeneralParameter [id=" + id + ", company_name=" + company_name + ", company_address=" + company_address
				+ ", lkp_max_entry_days=" + lkp_max_entry_days + ", skpc_validity_period=" + skpc_validity_period
				+ ", skpc_extended_validity_period=" + skpc_extended_validity_period + ", skpc_max_idle_days="
				+ skpc_max_idle_days + ", skpc_max_extend=" + skpc_max_extend + ", skpc_max_outstanding_load="
				+ skpc_max_outstanding_load + ", mou_min_performance=" + mou_min_performance + ", report_dir="
				+ report_dir + "]";
	}

}
