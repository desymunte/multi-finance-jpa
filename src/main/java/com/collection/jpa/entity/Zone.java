package com.collection.jpa.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Entity
@Table(name = "zone", schema="param", uniqueConstraints={@UniqueConstraint(columnNames={"off_id", "code"}, name="zone_ukey")})
public class Zone implements Serializable {
 
    private static final long serialVersionUID = 1L;
    
    @Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id", nullable=false, updatable=false)  
    private Integer id;      
    
    @Column(name="code", nullable=false, length=2)
	private String code;
    
    @Column(name="name", length=50)
	private String name;

    @ManyToOne
    @JoinColumn(name="off_id", foreignKey=@ForeignKey(name="zone_off_fkey"), nullable=false)
    private Office office;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Office getOffice() {
		return office;
	}

	public void setOffice(Office office) {
		this.office = office;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public Zone() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Zone(Integer id, String code, String name, Office office) {
		super();
		this.id = id;
		this.code = code;
		this.name = name;
		this.office = office;
	}

	@Override
	public String toString() {
		return "Zone [id=" + id + ", code=" + code + ", name=" + name + ", office=" + office + "]";
	}

}
