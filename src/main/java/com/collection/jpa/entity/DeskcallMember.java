package com.collection.jpa.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Entity
@Table(name="dcall_empl", schema="param", uniqueConstraints = {@UniqueConstraint(columnNames={"empl_id"}, name="dcall_empl_ukey")})
public class DeskcallMember implements Serializable {
 
    private static final long serialVersionUID = 1L;
        
    @Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id", nullable=false, updatable=false)  
    private Integer id;      
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="dcall_id", referencedColumnName="id", foreignKey=@ForeignKey(name="dcallempl_dcall_fkey"), nullable=false)
    private DeskcallTeam deskcallTeam;
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="empl_id", foreignKey=@ForeignKey(name="dcallempl_empl_fkey"), nullable=true)
    private Employee employee;
    
    @Column(name="mload", nullable=false)
	private Integer monthlyLoad = 0;
    
    @Column(name="dload", nullable=false)
	private Integer dailyLoad = 0;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public DeskcallTeam getDeskcallTeam() {
		return deskcallTeam;
	}

	public void setDeskcallTeam(DeskcallTeam deskcallTeam) {
		this.deskcallTeam = deskcallTeam;
	}

	public Employee getEmployee() {
		return employee;
	}

	public void setEmployee(Employee employee) {
		this.employee = employee;
	}

	public Integer getMonthlyLoad() {
		return monthlyLoad;
	}

	public void setMonthlyLoad(Integer monthlyLoad) {
		this.monthlyLoad = monthlyLoad;
	}

	public Integer getDailyLoad() {
		return dailyLoad;
	}

	public void setDailyLoad(Integer dailyLoad) {
		this.dailyLoad = dailyLoad;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public DeskcallMember() {
		super();
		// TODO Auto-generated constructor stub
	}

	public DeskcallMember(Integer id, DeskcallTeam deskcallTeam, Employee employee, Integer monthlyLoad,
			Integer dailyLoad) {
		super();
		this.id = id;
		this.deskcallTeam = deskcallTeam;
		this.employee = employee;
		this.monthlyLoad = monthlyLoad;
		this.dailyLoad = dailyLoad;
	}

	@Override
	public String toString() {
		return "DeskcallMember [id=" + id + ", deskcallTeam=" + deskcallTeam + ", employee=" + employee
				+ ", monthlyLoad=" + monthlyLoad + ", dailyLoad=" + dailyLoad + "]";
	}

}
