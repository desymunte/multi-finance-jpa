package com.collection.jpa.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Entity
@Table(name="coll_empl", schema="param", uniqueConstraints = {@UniqueConstraint(columnNames={"empl_id"}, name="coll_empl_ukey")})
public class CollectionMember implements Serializable {
 
    private static final long serialVersionUID = 1L;
        
    @Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id", nullable=false, updatable=false)  
    private Integer id;      
    
    @OneToMany(mappedBy="collectionMember", cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = true)
    private List<CollectionWorkingArea> collectionWorkingAreas = new ArrayList<CollectionWorkingArea>();
    
	@ManyToMany(cascade= {CascadeType.MERGE},fetch=FetchType.EAGER)
	@JoinTable(name="coll_zip", schema="param",
        joinColumns = {
    		@JoinColumn(name="coll_empl_id", nullable=false)
		},
        inverseJoinColumns = {
    		@JoinColumn(name="zip_id", nullable=false)
		}
    )
    private List<Zipcode> zipcodes;
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="coll_id", referencedColumnName="id", foreignKey=@ForeignKey(name="collempl_coll_fkey"), nullable=false)
    private CollectionTeam collectionTeam;
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="empl_id", foreignKey=@ForeignKey(name="collempl_empl_fkey"), nullable=true)
    private Employee employee;
    
    @Column(name="mload", nullable=false)
	private Integer monthlyLoad = 0;
    
    @Column(name="dload", nullable=false)
	private Integer dailyLoad = 0;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public List<CollectionWorkingArea> getCollectionWorkingAreas() {
		return collectionWorkingAreas;
	}

	public void setCollectionWorkingAreas(List<CollectionWorkingArea> collectionWorkingAreas) {
		this.collectionWorkingAreas = collectionWorkingAreas;
	}

	public List<Zipcode> getZipcodes() {
		return zipcodes;
	}

	public void setZipcodes(List<Zipcode> zipcodes) {
		this.zipcodes = zipcodes;
	}

	public CollectionTeam getCollectionTeam() {
		return collectionTeam;
	}

	public void setCollectionTeam(CollectionTeam collectionTeam) {
		this.collectionTeam = collectionTeam;
	}

	public Employee getEmployee() {
		return employee;
	}

	public void setEmployee(Employee employee) {
		this.employee = employee;
	}

	public Integer getMonthlyLoad() {
		return monthlyLoad;
	}

	public void setMonthlyLoad(Integer monthlyLoad) {
		this.monthlyLoad = monthlyLoad;
	}

	public Integer getDailyLoad() {
		return dailyLoad;
	}

	public void setDailyLoad(Integer dailyLoad) {
		this.dailyLoad = dailyLoad;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public CollectionMember() {
		super();
		// TODO Auto-generated constructor stub
	}

	public CollectionMember(Integer id, List<CollectionWorkingArea> collectionWorkingAreas, List<Zipcode> zipcodes,
			CollectionTeam collectionTeam, Employee employee, Integer monthlyLoad, Integer dailyLoad) {
		super();
		this.id = id;
		this.collectionWorkingAreas = collectionWorkingAreas;
		this.zipcodes = zipcodes;
		this.collectionTeam = collectionTeam;
		this.employee = employee;
		this.monthlyLoad = monthlyLoad;
		this.dailyLoad = dailyLoad;
	}

	@Override
	public String toString() {
		return "CollectionMember [id=" + id + ", collectionWorkingAreas=" + collectionWorkingAreas + ", zipcodes="
				+ zipcodes + ", collectionTeam=" + collectionTeam + ", employee=" + employee + ", monthlyLoad="
				+ monthlyLoad + ", dailyLoad=" + dailyLoad + "]";
	}

	public void addCollectionWorkingArea(CollectionWorkingArea collectionWorkingArea) {
		if (collectionWorkingArea != null) {
			this.collectionWorkingAreas.add(collectionWorkingArea);
			collectionWorkingArea.setCollectionMember(this);
	    }        
    }

}
