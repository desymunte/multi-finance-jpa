package com.collection.jpa.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="lkrtrx", schema="hist")
public class LKRHistory implements Serializable {
 
    private static final long serialVersionUID = 1L;
        
    @Id
	// @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id", nullable=false, updatable=false)  
    private Integer id;       
    
    // @ManyToOne
    // @JoinColumn(name="cntrno", foreignKey=@ForeignKey(name="lkrhist_bucket_fkey"), nullable=false)
		// private Bucket bucket;
		
		@Column(name="cntrno", nullable=false)
    private String contractNo;

    @Column(name="rundate", nullable=false)
	private Date runDate;
	
    @Column(name="team_id")
    private Integer teamId;

    @Column(name="spv_id")
    private Integer supervisorId;

    @Column(name="asgnd_to")
    private Integer assignedTo;

    @Column(name="owner_id")
    private Integer ownerId;

    @ManyToOne
    @JoinColumn(name="print_id", foreignKey=@ForeignKey(name="lkrhist_print_fkey"), nullable=true)
    private PrintActivity printActivity;

    @ManyToOne
    @JoinColumn(name="entry_cd", foreignKey=@ForeignKey(name="lkrhist_lkrentry_fkey"), nullable=true)
    private LKREntryStatus lkrEntryStatus;

    @Column(name="cmnt_id")
    private Integer lkrEntryCommentId;

    @Temporal(TemporalType.DATE)
    @Column(name="cmntdate")
	private Date dateRequired;

    @Column(name="cmnttext")
    private String strComment;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	// public Bucket getBucket() {
	// 	return bucket;
	// }

	// public void setBucket(Bucket bucket) {
	// 	this.bucket = bucket;
	// }

	public String getContractNo() {
		return contractNo;
	}

	public void setContractNo(String contractNo) {
		this.contractNo = contractNo;
	}

	public Date getRunDate() {
		return runDate;
	}

	public void setRunDate(Date runDate) {
		this.runDate = runDate;
	}

	public Integer getTeamId() {
		return teamId;
	}

	public void setTeamId(Integer teamId) {
		this.teamId = teamId;
	}

	public Integer getSupervisorId() {
		return supervisorId;
	}

	public void setSupervisorId(Integer supervisorId) {
		this.supervisorId = supervisorId;
	}

	public Integer getAssignedTo() {
		return assignedTo;
	}

	public void setAssignedTo(Integer assignedTo) {
		this.assignedTo = assignedTo;
	}

	public Integer getOwnerId() {
		return ownerId;
	}

	public void setOwnerId(Integer ownerId) {
		this.ownerId = ownerId;
	}

	public PrintActivity getPrintActivity() {
		return printActivity;
	}

	public void setPrintActivity(PrintActivity printActivity) {
		this.printActivity = printActivity;
	}

	public LKREntryStatus getLkrEntryStatus() {
		return lkrEntryStatus;
	}

	public void setLkrEntryStatus(LKREntryStatus lkrEntryStatus) {
		this.lkrEntryStatus = lkrEntryStatus;
	}

	public Integer getLkrEntryCommentId() {
		return lkrEntryCommentId;
	}

	public void setLkrEntryCommentId(Integer lkrEntryCommentId) {
		this.lkrEntryCommentId = lkrEntryCommentId;
	}

	public Date getDateRequired() {
		return dateRequired;
	}

	public void setDateRequired(Date dateRequired) {
		this.dateRequired = dateRequired;
	}

	public String getStrComment() {
		return strComment;
	}

	public void setStrComment(String strComment) {
		this.strComment = strComment;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public LKRHistory() {
		super();
		// TODO Auto-generated constructor stub
	}

	public LKRHistory(Integer id, String contractNo, Date runDate, Integer teamId, Integer supervisorId, Integer assignedTo,
			Integer ownerId, PrintActivity printActivity, LKREntryStatus lkrEntryStatus, Integer lkrEntryCommentId,
			Date dateRequired, String strComment) {
		super();
		this.id = id;
		this.contractNo = contractNo;
		this.runDate = runDate;
		this.teamId = teamId;
		this.supervisorId = supervisorId;
		this.assignedTo = assignedTo;
		this.ownerId = ownerId;
		this.printActivity = printActivity;
		this.lkrEntryStatus = lkrEntryStatus;
		this.lkrEntryCommentId = lkrEntryCommentId;
		this.dateRequired = dateRequired;
		this.strComment = strComment;
	}

	@Override
	public String toString() {
		return "LKRHistory [id=" + id + ", contractNo=" + contractNo + ", runDate=" + runDate + ", teamId=" + teamId
				+ ", supervisorId=" + supervisorId + ", assignedTo=" + assignedTo + ", ownerId=" + ownerId
				+ ", printActivity=" + printActivity + ", lkrEntryStatus=" + lkrEntryStatus + ", lkrEntryCommentId="
				+ lkrEntryCommentId + ", dateRequired=" + dateRequired + ", strComment=" + strComment + "]";
	}

}
