package com.collection.jpa.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import com.collection.jpa.entity.Office;
import com.collection.jpa.entity.Zone;

@Entity
@Table(name="zip", schema="param", uniqueConstraints={@UniqueConstraint(columnNames={"zipcd", "subzipcd"}, name="zip_ukey")})
public class Zipcode implements Serializable {
 
    private static final long serialVersionUID = 1L;
    
    @Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name="id", nullable=false, updatable=false)
    private Integer id;      
    
    @OneToMany(mappedBy="zipcode", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<CollectionWorkingArea> collectionWorkingAreas = new ArrayList<CollectionWorkingArea>();
    
    @OneToMany(mappedBy="zipcode", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<RemedialWorkingArea> remedialWorkingAreas = new ArrayList<RemedialWorkingArea>();
    
    @Column(name="zipcd", nullable=false, length=5, updatable=false)
	private String zipcode;
	
    @Column(name="subzipcd", nullable=false, length=2)
	private String subzipcode = "";
    
    @Column(name="name", length=50)
	private String name;
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="off_id", foreignKey=@ForeignKey(name="zip_off_fkey"))
    private Office office;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="zone_id", foreignKey=@ForeignKey(name="zip_zone_fkey"))
    private Zone zone;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public List<CollectionWorkingArea> getCollectionWorkingAreas() {
		return collectionWorkingAreas;
	}

	public void setCollectionWorkingAreas(List<CollectionWorkingArea> collectionWorkingAreas) {
		this.collectionWorkingAreas = collectionWorkingAreas;
	}

	public List<RemedialWorkingArea> getRemedialWorkingAreas() {
		return remedialWorkingAreas;
	}

	public void setRemedialWorkingAreas(List<RemedialWorkingArea> remedialWorkingAreas) {
		this.remedialWorkingAreas = remedialWorkingAreas;
	}

	public String getZipcode() {
		return zipcode;
	}

	public void setZipcode(String zipcode) {
		this.zipcode = zipcode;
	}

	public String getSubzipcode() {
		return subzipcode;
	}

	public void setSubzipcode(String subzipcode) {
		this.subzipcode = subzipcode;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Office getOffice() {
		return office;
	}

	public void setOffice(Office office) {
		this.office = office;
	}

	public Zone getZone() {
		return zone;
	}

	public void setZone(Zone zone) {
		this.zone = zone;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public Zipcode() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Zipcode(Integer id, List<CollectionWorkingArea> collectionWorkingAreas,
			List<RemedialWorkingArea> remedialWorkingAreas, String zipcode, String subzipcode, String name,
			Office office, Zone zone) {
		super();
		this.id = id;
		this.collectionWorkingAreas = collectionWorkingAreas;
		this.remedialWorkingAreas = remedialWorkingAreas;
		this.zipcode = zipcode;
		this.subzipcode = subzipcode;
		this.name = name;
		this.office = office;
		this.zone = zone;
	}

	@Override
	public String toString() {
		return "Zipcode [id=" + id + ", collectionWorkingAreas=" + collectionWorkingAreas + ", remedialWorkingAreas="
				+ remedialWorkingAreas + ", zipcode=" + zipcode + ", subzipcode=" + subzipcode + ", name=" + name
				+ ", office=" + office + ", zone=" + zone + "]";
	}

	public void addCollectionWorkingArea(CollectionWorkingArea cwa) {
		if (cwa != null) {
			this.collectionWorkingAreas.add(cwa);
			cwa.setZipcode(this);
	    }        
    }
	
	public void addRemedialWorkingArea(RemedialWorkingArea cwa) {
		if (cwa != null) {
			this.remedialWorkingAreas.add(cwa);
			cwa.setZipcode(this);
	    }        
    }
	
}
