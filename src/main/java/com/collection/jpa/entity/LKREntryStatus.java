package com.collection.jpa.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="lkrentry", schema="param")
public class LKREntryStatus implements Serializable {
 
    private static final long serialVersionUID = 1L;
        
    @Id
    @Column(name="code", nullable=false, length=3)
	private String code;  
    
    @Column(name="name", nullable=false, length=30)
    private String name;
    
    @Column(name="descr", length=256)
	private String description;

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public LKREntryStatus() {
		super();
		// TODO Auto-generated constructor stub
	}

	public LKREntryStatus(String code, String name, String description) {
		super();
		this.code = code;
		this.name = name;
		this.description = description;
	}

	@Override
	public String toString() {
		return "LKREntryStatus [code=" + code + ", name=" + name + ", description=" + description+ "]";
	}

}
