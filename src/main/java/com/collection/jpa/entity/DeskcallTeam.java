package com.collection.jpa.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="dcall", schema="param")
public class DeskcallTeam implements Serializable {
 
    private static final long serialVersionUID = 1L;
        
    @Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id", nullable=false, updatable=false)
    private Integer id;      
    
    @OneToMany(mappedBy="deskcallTeam", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<DeskcallMember> deskcallMembers = new ArrayList<DeskcallMember>();
    
    @Column(name="name", nullable=false, length=50)
	private String name;
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="off_id", foreignKey=@ForeignKey(name="dcall_off_fkey"), nullable=false)
    private Office office;
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="cycle_id", foreignKey=@ForeignKey(name="dcall_cycle_fkey"), nullable=false)
    private Cycle cycle;
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="spv_id", foreignKey=@ForeignKey(name="dcall_empl_fkey"), nullable=true)
    private Employee supervisor;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public List<DeskcallMember> getDeskcallMembers() {
		return deskcallMembers;
	}

	public void setDeskcallMembers(List<DeskcallMember> deskcallMembers) {
		this.deskcallMembers = deskcallMembers;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Office getOffice() {
		return office;
	}

	public void setOffice(Office office) {
		this.office = office;
	}

	public Cycle getCycle() {
		return cycle;
	}

	public void setCycle(Cycle cycle) {
		this.cycle = cycle;
	}

	public Employee getSupervisor() {
		return supervisor;
	}

	public void setSupervisor(Employee supervisor) {
		this.supervisor = supervisor;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public DeskcallTeam() {
		super();
		// TODO Auto-generated constructor stub
	}

	public DeskcallTeam(Integer id, List<DeskcallMember> deskcallMembers, String name, Office office, Cycle cycle,
			Employee supervisor) {
		super();
		this.id = id;
		this.deskcallMembers = deskcallMembers;
		this.name = name;
		this.office = office;
		this.cycle = cycle;
		this.supervisor = supervisor;
	}

	@Override
	public String toString() {
		return "DeskcallTeam [id=" + id + ", deskcallMembers=" + deskcallMembers + ", name=" + name + ", office="
				+ office + ", cycle=" + cycle + ", supervisor=" + supervisor + "]";
	}

	public void addDeskcallMember(DeskcallMember deskcallMember) {
		if (deskcallMember != null) {
			this.deskcallMembers.add(deskcallMember);
	        deskcallMember.setDeskcallTeam(this);
	    }
    }

}
