package com.collection.jpa.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="coll_drmt", schema="param")
public class CollectionDormant implements Serializable {
 
    private static final long serialVersionUID = 1L;
        
    @Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id", nullable=false, updatable=false)  
    private Integer id;      
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="empl_id", foreignKey=@ForeignKey(name="colldrmt_empl_fkey"), nullable=false)
    private Employee employee;
    
    @Column(name="datein", nullable=false)
    private Date dateIn;
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="prev_member_id", foreignKey=@ForeignKey(name="colldrmt_prev_fkey"), nullable=false)
	private CollectionMember previousMember;

    @Column(name="dateout", nullable=true)
    private Date dateOut;
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="next_member_id", foreignKey=@ForeignKey(name="colldrmt_next_fkey"), nullable=true)
	private CollectionMember nextMember;
    
    @Column(name="mload", nullable=false)
	private Integer monthlyLoad = 0;

	@Column(name="dload", nullable=false)
	private Integer dailyLoad = 0;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getMonthlyLoad() {
		return monthlyLoad;
	}

	public Integer getDailyLoad() {
		return dailyLoad;
	}

	public void setMonthlyLoad(Integer monthlyLoad) {
		this.monthlyLoad = monthlyLoad;
	}

	public void setDailyLoad(Integer dailyLoad) {
		this.dailyLoad = dailyLoad;
	}

	public Employee getEmployee() {
		return employee;
	}

	public void setEmployee(Employee employee) {
		this.employee = employee;
	}

	public Date getDateIn() {
		return dateIn;
	}

	public void setDateIn(Date dateIn) {
		this.dateIn = dateIn;
	}

	public CollectionMember getPreviousMember() {
		return previousMember;
	}

	public void setPreviousMember(CollectionMember previousMember) {
		this.previousMember = previousMember;
	}

	public Date getDateOut() {
		return dateOut;
	}

	public void setDateOut(Date dateOut) {
		this.dateOut = dateOut;
	}

	public CollectionMember getNextMember() {
		return nextMember;
	}

	public void setNextMember(CollectionMember nextMember) {
		this.nextMember = nextMember;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public CollectionDormant() {
		super();
		// TODO Auto-generated constructor stub
	}

	public CollectionDormant(Integer id, Employee employee, Date dateIn, CollectionMember previousMember, Date dateOut,
			CollectionMember nextMember, Integer monthlyLoad, Integer dailyLoad) {
		super();
		this.id = id;
		this.employee = employee;
		this.dateIn = dateIn;
		this.previousMember = previousMember;
		this.dateOut = dateOut;
		this.nextMember = nextMember;
		this.monthlyLoad = monthlyLoad;
		this.dailyLoad = dailyLoad;
	}

	@Override
	public String toString() {
		return "CollectionDormant [id=" + id + ", employee=" + employee + ", dateIn=" + dateIn + ", previousMember="
				+ previousMember + ", dateOut=" + dateOut + ", nextMember=" + nextMember + ", monthlyLoad="
				+ monthlyLoad + ", dailyLoad=" + dailyLoad + "]";
	}
	

}
