package com.collection.jpa.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="skpctrx", schema="public")
public class SKPCTransaction implements Serializable {
 
    private static final long serialVersionUID = 1L;
        
    @Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id", nullable=false, updatable=false)  
    private Integer id;       
    
    @ManyToOne
    @JoinColumn(name="cntrno", foreignKey=@ForeignKey(name="lkptrx_bucket_fkey"), nullable=false)
    private Bucket bucket;

    @Column(name="rundate", nullable=false)
	private Date runDate;
	
    @Column(name="profcoll_id")
    private Integer profcollId;

    @Column(name="asgnd_to")
    private Integer assignedTo;

    @ManyToOne
    @JoinColumn(name="print_id", foreignKey=@ForeignKey(name="skpctrx_print_fkey"), nullable=true)
    private PrintActivity printActivity;

    @ManyToOne
    @JoinColumn(name="status", foreignKey=@ForeignKey(name="skpctrx_status_fkey"), nullable=false)
    private Status status;

    @Column(name="result")
    private Integer result;

    @Column(name="refno", length=20)
    private String refNo;

    @Column(name="mouno", length=20)
    private String mouNo;
    
    @Column(name="seqno")
    private Integer sequenceNo;

    @OneToMany(mappedBy="skpcTransaction", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<SKPCTransactionDetail> skpcTransDetails = new ArrayList<SKPCTransactionDetail>();
    
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Bucket getBucket() {
		return bucket;
	}

	public void setBucket(Bucket bucket) {
		this.bucket = bucket;
	}

	public Date getRunDate() {
		return runDate;
	}

	public void setRunDate(Date runDate) {
		this.runDate = runDate;
	}

	public Integer getProfcollId() {
		return profcollId;
	}

	public void setProfcollId(Integer profcollId) {
		this.profcollId = profcollId;
	}

	public Integer getAssignedTo() {
		return assignedTo;
	}

	public void setAssignedTo(Integer assignedTo) {
		this.assignedTo = assignedTo;
	}

	public PrintActivity getPrintActivity() {
		return printActivity;
	}

	public void setPrintActivity(PrintActivity printActivity) {
		this.printActivity = printActivity;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	public Integer getResult() {
		return result;
	}

	public void setResult(Integer result) {
		this.result = result;
	}

	public String getRefNo() {
		return refNo;
	}

	public void setRefNo(String refNo) {
		this.refNo = refNo;
	}

	public String getMouNo() {
		return mouNo;
	}

	public void setMouNo(String mouNo) {
		this.mouNo = mouNo;
	}

	public Integer getSequenceNo() {
		return sequenceNo;
	}

	public void setSequenceNo(Integer sequenceNo) {
		this.sequenceNo = sequenceNo;
	}

	public List<SKPCTransactionDetail> getSkpcTransDetails() {
		return skpcTransDetails;
	}

	public void setSkpcTransDetails(List<SKPCTransactionDetail> skpcTransDetails) {
		this.skpcTransDetails = skpcTransDetails;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public SKPCTransaction() {
		super();
		// TODO Auto-generated constructor stub
	}

	public SKPCTransaction(Integer id, Bucket bucket, Date runDate, Integer profcollId, Integer assignedTo,
			PrintActivity printActivity, Status status, Integer result, String refNo, String mouNo, Integer sequenceNo,
			List<SKPCTransactionDetail> skpcTransDetails) {
		super();
		this.id = id;
		this.bucket = bucket;
		this.runDate = runDate;
		this.profcollId = profcollId;
		this.assignedTo = assignedTo;
		this.printActivity = printActivity;
		this.status = status;
		this.result = result;
		this.refNo = refNo;
		this.mouNo = mouNo;
		this.sequenceNo = sequenceNo;
		this.skpcTransDetails = skpcTransDetails;
	}

	@Override
	public String toString() {
		return "SKPCTransaction [id=" + id + ", bucket=" + bucket + ", runDate=" + runDate + ", profcollId="
				+ profcollId + ", assignedTo=" + assignedTo + ", printActivity=" + printActivity + ", status=" + status
				+ ", result=" + result + ", refNo=" + refNo + ", mouNo=" + mouNo + ", sequenceNo=" + sequenceNo
				+ ", skpcTransDetails=" + skpcTransDetails + "]";
	}

	public void addDetail(SKPCTransactionDetail detail) {
		if (detail != null) {
			this.skpcTransDetails.add(detail);
			detail.setSkpcTransaction(this);
	    }
    }

}
