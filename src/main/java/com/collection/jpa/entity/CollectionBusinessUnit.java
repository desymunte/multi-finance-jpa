package com.collection.jpa.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="coll_bizu", schema="param")
public class CollectionBusinessUnit implements Serializable {
 
    private static final long serialVersionUID = 1L;
        
    @Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id", nullable=false, updatable=false)  
    private Integer id;      
    
    @ManyToOne
    @JoinColumn(name="coll_id", foreignKey=@ForeignKey(name="collbizu_coll_fkey"), nullable=false)
    private CollectionTeam collectionTeam;
    
    @ManyToOne
    @JoinColumn(name="bizu_id", foreignKey=@ForeignKey(name="collbizu_bizu_fkey"), nullable=false)
    private BusinessUnit businessUnit;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public CollectionTeam getCollectionTeam() {
		return collectionTeam;
	}

	public void setCollectionTeam(CollectionTeam collectionTeam) {
		this.collectionTeam = collectionTeam;
	}

	public BusinessUnit getBusinessUnit() {
		return businessUnit;
	}

	public void setBusinessUnit(BusinessUnit businessUnit) {
		this.businessUnit = businessUnit;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public CollectionBusinessUnit() {
		super();
		// TODO Auto-generated constructor stub
	}

	public CollectionBusinessUnit(Integer id, CollectionTeam collectionTeam, BusinessUnit businessUnit) {
		super();
		this.id = id;
		this.collectionTeam = collectionTeam;
		this.businessUnit = businessUnit;
	}

	@Override
	public String toString() {
		return "CollectionBusinessUnit [id=" + id + ", collectionTeam=" + collectionTeam + ", businessUnit="
				+ businessUnit + "]";
	}

}
