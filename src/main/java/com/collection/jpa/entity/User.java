package com.collection.jpa.entity;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import java.io.Serializable;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;


@Entity
@Table(name="user", schema="auth", uniqueConstraints = {@UniqueConstraint(columnNames={"uname"}, name="user_ukey")})
public class User implements Serializable {
	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id")  
    private Integer id;    
	
	@ManyToMany(cascade= {CascadeType.MERGE},fetch=FetchType.EAGER)
	@JoinTable(name="user_roles", schema="auth",
        joinColumns = {
    		@JoinColumn(name="user_id", nullable=false)
		},
        inverseJoinColumns = {
    		@JoinColumn(name="role_id", nullable=false)
		}
    )
    private List<Role> roles;
    
	@Column(name="uname", nullable=false, length=20)
	private String username;

	@Column(name="pwd", nullable=false, length=20)
	private String password;

    @ManyToOne
    @JoinColumn(name="empl_id", foreignKey=@ForeignKey(name="user_empl_fkey"), nullable=true)
    private Employee employee;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Employee getEmployee() {
		return employee;
	}

	public void setEmployee(Employee employee) {
		this.employee = employee;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}	

	public List<Role> getRoles() {
		return roles;
	}

	public void setRoles(List<Role> roles) {
		this.roles = roles;
	}

	public User(List<Role> roles, String username, String password, Employee employee) {
		super();
		this.roles = roles;
		this.username = username;
		this.password = password;
		this.employee = employee;
	}

	public User() {
		super();
		// TODO Auto-generated constructor stub
	}

//	@Override
//	public String toString() {
//		return "User [id=" + id + ", roles=" + roles + ", username=" + username + ", password=" + password
//				+ ", employee=" + employee + "]";
//	}
	
	
	
//	public void addRole(Role role) {
//		if (role != null) {
//			role.getUsers().add(this);
//			this.roles.add(role);
//	    }        
//    }

	@Override
	public String toString() {
		return "User [id=" + id + ", roles=" + roles + ", username=" + username + ", password=" + password
				+ ", employee=" + employee + "]";
	}
    
}
