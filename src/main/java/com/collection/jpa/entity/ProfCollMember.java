package com.collection.jpa.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "prof_staf", schema="param")
public class ProfCollMember implements Serializable {
 
    private static final long serialVersionUID = 1L;
        
    @Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id", nullable=false, updatable=false)  
    private Integer id;      
    
    @ManyToOne
    @JoinColumn(name="prof_id", foreignKey=@ForeignKey(name="profstaf_prof_fkey"), nullable=false)
    private ProfCollector profCollection;
    
    @Column(name="name", nullable=false, length=50)
	private String name;
    
//    @ManyToOne
//    @JoinColumn(name="idcard", foreignKey=@ForeignKey(name="profstaf_idcard_fkey"), nullable=false)
//    private IdCard idCard;
    
    @Column(name="idcard_no", nullable=false, length=20)
	private String idCardNo;
    
    @Column(name="address", length=100)
	private String address; 
    
    @Column(name="phone", length=20)
	private String phone; 
    
    @Column(name="email", length=50)
	private String email;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public ProfCollector getProfCollection() {
		return profCollection;
	}

	public void setProfCollection(ProfCollector profCollection) {
		this.profCollection = profCollection;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

//	public IdCard getIdCard() {
//		return idCard;
//	}
//
//	public void setIdCard(IdCard idCard) {
//		this.idCard = idCard;
//	}

	public String getIdCardNo() {
		return idCardNo;
	}

	public void setIdCardNo(String idCardNo) {
		this.idCardNo = idCardNo;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public ProfCollMember() {
		super();
		// TODO Auto-generated constructor stub
	}

	public ProfCollMember(Integer id, ProfCollector profCollection, String name, IdCard idCard, String idCardNo,
			String address, String phone, String email) {
		super();
		this.id = id;
		this.profCollection = profCollection;
		this.name = name;
//		this.idCard = idCard;
		this.idCardNo = idCardNo;
		this.address = address;
		this.phone = phone;
		this.email = email;
	}

	@Override
	public String toString() {
		return "ProfCollectionMember [id=" + id + ", profCollection=" + profCollection + ", name=" + name
				+ ", idCardNo=" + idCardNo + ", address=" + address + ", phone=" + phone + ", email=" + email + "]";
	}

}
