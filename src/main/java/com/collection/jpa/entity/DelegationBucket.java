package com.collection.jpa.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="delegbucket", schema="public")
public class DelegationBucket implements Serializable {
 
    private static final long serialVersionUID = 1L;
        
    @Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id", nullable=false, updatable=false)  
    private Integer id;      
    
    @Column(name = "memono", nullable=false, length=12)    
    private String memoNo;

    @Temporal(TemporalType.DATE)
    @Column(name="memodate", nullable=false)
	private Date memoDate;

    @Temporal(TemporalType.DATE)
    @Column(name="rundate", nullable=false)
	private Date runDate;

    @Column(name = "cntrno", nullable=false, length=12)    
    private String contractNo;

    @Temporal(TemporalType.DATE)
    @Column(name="cntrdate", nullable=false)
	private Date contractDate;
	
    @Column(name = "officd", nullable=false, length=5)    
    private String officeCode;

    @Column(name="topinstl", nullable=false)
	private int topInstallment;

    @Column(name="outsdpriamnt")
	private double outstandingPrincipalAmnt;

    @Column(name="outsdintamnt")
	private double outstandingInterestAmnt;

    @Column(name="minstl")
	private double monthlyInstallment;

    @Column(name = "custname", length=50)
    private String custName;

    @Column(name = "custaddr", length=100)    
    private String custAddress;

    @Column(name = "custbilladdr", length=100)
    private String custBillingAddress;

    @Column(name = "zipcd", nullable=false, length=5)    
    private String zipcode;

    @Column(name = "subzipcd", length=2)    
    private String SubZipcode;

    @Column(name = "phone1", length=100)
    private String phone1;

    @Column(name = "phone2", length=100)
    private String phone2;

    @Column(name = "bizu", length=2)
    private String businessUnit;

    @Temporal(TemporalType.DATE)
    @Column(name="leastduedate")
	private Date leastDueDate;

    @Column(name="leastinstlno")
	private int leastInstallmentNo;

    @Temporal(TemporalType.DATE)
    @Column(name="lastduedate")
	private Date lastDueDate;

    @Column(name="lastinstlno")
	private int lastInstallmentNo;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getMemoNo() {
		return memoNo;
	}

	public void setMemoNo(String memoNo) {
		this.memoNo = memoNo;
	}

	public Date getMemoDate() {
		return memoDate;
	}

	public void setMemoDate(Date memoDate) {
		this.memoDate = memoDate;
	}

	public Date getRunDate() {
		return runDate;
	}

	public void setRunDate(Date runDate) {
		this.runDate = runDate;
	}

	public String getContractNo() {
		return contractNo;
	}

	public void setContractNo(String contractNo) {
		this.contractNo = contractNo;
	}

	public Date getContractDate() {
		return contractDate;
	}

	public void setContractDate(Date contractDate) {
		this.contractDate = contractDate;
	}

	public String getOfficeCode() {
		return officeCode;
	}

	public void setOfficeCode(String officeCode) {
		this.officeCode = officeCode;
	}

	public int getTopInstallment() {
		return topInstallment;
	}

	public void setTopInstallment(int topInstallment) {
		this.topInstallment = topInstallment;
	}

	public double getOutstandingPrincipalAmnt() {
		return outstandingPrincipalAmnt;
	}

	public void setOutstandingPrincipalAmnt(double outstandingPrincipalAmnt) {
		this.outstandingPrincipalAmnt = outstandingPrincipalAmnt;
	}

	public double getOutstandingInterestAmnt() {
		return outstandingInterestAmnt;
	}

	public void setOutstandingInterestAmnt(double outstandingInterestAmnt) {
		this.outstandingInterestAmnt = outstandingInterestAmnt;
	}

	public double getMonthlyInstallment() {
		return monthlyInstallment;
	}

	public void setMonthlyInstallment(double monthlyInstallment) {
		this.monthlyInstallment = monthlyInstallment;
	}

	public String getCustName() {
		return custName;
	}

	public void setCustName(String custName) {
		this.custName = custName;
	}

	public String getCustAddress() {
		return custAddress;
	}

	public void setCustAddress(String custAddress) {
		this.custAddress = custAddress;
	}

	public String getCustBillingAddress() {
		return custBillingAddress;
	}

	public void setCustBillingAddress(String custBillingAddress) {
		this.custBillingAddress = custBillingAddress;
	}

	public String getZipcode() {
		return zipcode;
	}

	public void setZipcode(String zipcode) {
		this.zipcode = zipcode;
	}

	public String getSubZipcode() {
		return SubZipcode;
	}

	public void setSubZipcode(String subZipcode) {
		SubZipcode = subZipcode;
	}

	public String getPhone1() {
		return phone1;
	}

	public void setPhone1(String phone1) {
		this.phone1 = phone1;
	}

	public String getPhone2() {
		return phone2;
	}

	public void setPhone2(String phone2) {
		this.phone2 = phone2;
	}

	public String getBusinessUnit() {
		return businessUnit;
	}

	public void setBusinessUnit(String businessUnit) {
		this.businessUnit = businessUnit;
	}

	public Date getLeastDueDate() {
		return leastDueDate;
	}

	public void setLeastDueDate(Date leastDueDate) {
		this.leastDueDate = leastDueDate;
	}

	public int getLeastInstallmentNo() {
		return leastInstallmentNo;
	}

	public void setLeastInstallmentNo(int leastInstallmentNo) {
		this.leastInstallmentNo = leastInstallmentNo;
	}

	public Date getLastDueDate() {
		return lastDueDate;
	}

	public void setLastDueDate(Date lastDueDate) {
		this.lastDueDate = lastDueDate;
	}

	public int getLastInstallmentNo() {
		return lastInstallmentNo;
	}

	public void setLastInstallmentNo(int lastInstallmentNo) {
		this.lastInstallmentNo = lastInstallmentNo;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public DelegationBucket() {
		super();
		// TODO Auto-generated constructor stub
	}

	public DelegationBucket(Integer id, String memoNo, Date memoDate, Date runDate, String contractNo,
			Date contractDate, String officeCode, int topInstallment, double outstandingPrincipalAmnt,
			double outstandingInterestAmnt, double monthlyInstallment, String custName, String custAddress,
			String custBillingAddress, String zipcode, String subZipcode, String phone1, String phone2,
			String businessUnit, Date leastDueDate, int leastInstallmentNo, Date lastDueDate, int lastInstallmentNo) {
		super();
		this.id = id;
		this.memoNo = memoNo;
		this.memoDate = memoDate;
		this.runDate = runDate;
		this.contractNo = contractNo;
		this.contractDate = contractDate;
		this.officeCode = officeCode;
		this.topInstallment = topInstallment;
		this.outstandingPrincipalAmnt = outstandingPrincipalAmnt;
		this.outstandingInterestAmnt = outstandingInterestAmnt;
		this.monthlyInstallment = monthlyInstallment;
		this.custName = custName;
		this.custAddress = custAddress;
		this.custBillingAddress = custBillingAddress;
		this.zipcode = zipcode;
		SubZipcode = subZipcode;
		this.phone1 = phone1;
		this.phone2 = phone2;
		this.businessUnit = businessUnit;
		this.leastDueDate = leastDueDate;
		this.leastInstallmentNo = leastInstallmentNo;
		this.lastDueDate = lastDueDate;
		this.lastInstallmentNo = lastInstallmentNo;
	}

	@Override
	public String toString() {
		return "DelegationBucket [id=" + id + ", memoNo=" + memoNo + ", memoDate=" + memoDate + ", runDate=" + runDate
				+ ", contractNo=" + contractNo + ", contractDate=" + contractDate + ", officeCode=" + officeCode
				+ ", topInstallment=" + topInstallment + ", outstandingPrincipalAmnt=" + outstandingPrincipalAmnt
				+ ", outstandingInterestAmnt=" + outstandingInterestAmnt + ", monthlyInstallment=" + monthlyInstallment
				+ ", custName=" + custName + ", custAddress=" + custAddress + ", custBillingAddress="
				+ custBillingAddress + ", zipcode=" + zipcode + ", SubZipcode=" + SubZipcode + ", phone1=" + phone1
				+ ", phone2=" + phone2 + ", businessUnit=" + businessUnit + ", leastDueDate=" + leastDueDate
				+ ", leastInstallmentNo=" + leastInstallmentNo + ", lastDueDate=" + lastDueDate + ", lastInstallmentNo="
				+ lastInstallmentNo + "]";
	}

}
