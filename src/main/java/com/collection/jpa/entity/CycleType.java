package com.collection.jpa.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="cycletype", schema="param")
public class CycleType implements Serializable {
 
    private static final long serialVersionUID = 1L;
        
    @Id
    @Column(name="type", nullable=false, updatable=false, length=1)  
    private String type;      
    
    @Column(name="descr", nullable=false, length=10)
	private String description;

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public CycleType() {
		super();
		// TODO Auto-generated constructor stub
	}

	public CycleType(String type, String description) {
		super();
		this.type = type;
		this.description = description;
	}

	@Override
	public String toString() {
		return "CycleType [type=" + type + ", description=" + description + "]";
	}

}
