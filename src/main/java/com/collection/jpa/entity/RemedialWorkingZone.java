package com.collection.jpa.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Entity
@Table(name = "rmdl_zone", schema="param", uniqueConstraints={@UniqueConstraint(columnNames={"zone_id"}, name="rmdl_zone_ukey")})
public class RemedialWorkingZone implements Serializable {
 
    private static final long serialVersionUID = 1L;
        
    @Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)  
    private Integer id;      
    
    @ManyToOne
    @JoinColumn(name="rmdl_empl_id", foreignKey=@ForeignKey(name="rmdlzone_rmdlempl_fkey"), nullable=false)
    private RemedialMember remedialMember;
    
    @ManyToOne
    @JoinColumn(name="zone_id", foreignKey=@ForeignKey(name="rmdlzone_zone_fkey"), nullable=false)
    private Zone zone;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public RemedialMember getRemedialMember() {
		return remedialMember;
	}

	public void setRemedialMember(RemedialMember remedialMember) {
		this.remedialMember = remedialMember;
	}

	public Zone getZone() {
		return zone;
	}

	public void setZone(Zone zone) {
		this.zone = zone;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public RemedialWorkingZone() {
		super();
		// TODO Auto-generated constructor stub
	}

	public RemedialWorkingZone(Integer id, RemedialMember remedialMember, Zone zone) {
		super();
		this.id = id;
		this.remedialMember = remedialMember;
		this.zone = zone;
	}

	@Override
	public String toString() {
		return "RemedialWorkingZone [id=" + id + ", remedialMember=" + remedialMember + ", zone=" + zone + "]";
	}

}
