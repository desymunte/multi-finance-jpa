package com.collection.jpa.entity;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;


@Entity
@Table(name="empl", schema="param", uniqueConstraints = {@UniqueConstraint(columnNames={"regno"}, name="empl_ukey")})
public class Employee implements Serializable {
	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id", nullable=false, updatable=false)  
    private Integer id;    
    
    @OneToMany(mappedBy="employee", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<CollectionMember> collectionMembers = new ArrayList<CollectionMember>();
    
    @OneToMany(mappedBy="employee", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<RemedialMember> remedialMembers = new ArrayList<RemedialMember>();
    
    @OneToMany(mappedBy="employee", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<DeskcallMember> deskcallMembers = new ArrayList<DeskcallMember>();
    
    @OneToMany(mappedBy="supervisor", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<CollectionTeam> collectionSupervisor = new ArrayList<CollectionTeam>();
    
    @OneToMany(mappedBy="supervisor", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<RemedialTeam> remedialSupervisor = new ArrayList<RemedialTeam>();
    
    @OneToMany(mappedBy="supervisor", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<DeskcallTeam> deskcallSupervisor = new ArrayList<DeskcallTeam>();
    
	@Column(name="regno", nullable=false, length=5)
	private String regNo;

    @Column(name="name", nullable=false, length=50)
	private String name;

    @ManyToOne
    @JoinColumn(name="off_id", foreignKey=@ForeignKey(name="empl_off_fkey"), nullable=false)
    private Office office;

    @ManyToOne
    @JoinColumn(name="job_cd", foreignKey=@ForeignKey(name="empl_job_fkey"), nullable=false)
    private JobTitle jobTitle;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getRegNo() {
		return regNo;
	}

	public void setRegNo(String regNo) {
		this.regNo = regNo;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Office getOffice() {
		return office;
	}

	public void setOffice(Office office) {
		this.office = office;
	}

	public JobTitle getJobTitle() {
		return jobTitle;
	}

	public void setJobTitle(JobTitle jobTitle) {
		this.jobTitle = jobTitle;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public Employee() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Employee(Integer id, String regNo, String name, Office office, JobTitle jobTitle) {
		super();
		this.id = id;
		this.regNo = regNo;
		this.name = name;
		this.office = office;
		this.jobTitle = jobTitle;
	}

	@Override
	public String toString() {
		return "Employee [id=" + id + ", regNo=" + regNo + ", name=" + name + ", office=" + office + ", jobTitle="
				+ jobTitle + "]";
	}

/*	public void addCollectionMember(CollectionMember cm) {
		if (cm != null) {
			this.collectionMembers.add(cm);
			cm.setEmployee(this);
	    }        
    }
	
	public void addRemedialMember(RemedialMember cm) {
		if (cm != null) {
			this.remedialMembers.add(cm);
			cm.setEmployee(this);
	    }        
    }
	
	public void addDeskcallMember(DeskcallMember cm) {
		if (cm != null) {
			this.deskcallMembers.add(cm);
			cm.setEmployee(this);
	    }        
    }
*/	
}
