package com.collection.jpa.config;
//package com.collection.data.config;
//
//import javax.persistence.EntityManagerFactory;
//import javax.persistence.PersistenceUnit;
//
//import org.springframework.beans.factory.config.ConfigurableBeanFactory;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.context.annotation.Lazy;
//import org.springframework.context.annotation.Scope;
//
//import com.blazebit.persistence.Criteria;
//import com.blazebit.persistence.CriteriaBuilderFactory;
//import com.blazebit.persistence.spi.CriteriaBuilderConfiguration;
//import com.blazebit.persistence.view.EntityViewManager;
//import com.blazebit.persistence.view.spi.EntityViewConfiguration;
//
//@Configuration
//public class BlazePersistenceConfiguration {
//
//	@PersistenceUnit
//    private EntityManagerFactory entityManagerFactory;
//
//    @Bean
//    @Scope(ConfigurableBeanFactory.SCOPE_SINGLETON)
//    @Lazy(false)
//    public CriteriaBuilderFactory createCriteriaBuilderFactory() {
//        CriteriaBuilderConfiguration config = Criteria.getDefault();
//        // do some configuration
//        return config.createCriteriaBuilderFactory(entityManagerFactory);
//    }
//    
//    @Bean
//    @Scope(ConfigurableBeanFactory.SCOPE_SINGLETON)
//    @Lazy(false)
//    // inject the criteria builder factory which will be used along with the entity view manager
//    public EntityViewManager createEntityViewManager(CriteriaBuilderFactory cbf, EntityViewConfiguration entityViewConfiguration) {
//        return entityViewConfiguration.createEntityViewManager(cbf);
//    }
//}
