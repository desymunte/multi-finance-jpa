package com.collection.jpa.config;

import javax.servlet.http.HttpServletResponse;

import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableOAuth2Client;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;

@Configuration
@EnableResourceServer
//@EnableOAuth2Client
public class ResourceServerConfig extends ResourceServerConfigurerAdapter {

	@Override
	public void configure(HttpSecurity http) throws Exception {
		// TODO Auto-generated method stub
//		super.configure(http);
//		http
//			.authorizeRequests()
//			.antMatchers("/").permitAll()
//			.antMatchers("/collection/**").authenticated();
		
		http
        .csrf().disable()
        .exceptionHandling()
        .authenticationEntryPoint((request, response, authException) -> response.sendError(HttpServletResponse.SC_UNAUTHORIZED))
        .and()
        .authorizeRequests()
        .antMatchers(HttpMethod.OPTIONS).permitAll()
		.antMatchers("/").permitAll()
		.antMatchers("/collection/**").authenticated()
		.antMatchers("/api/**").authenticated()
//        .antMatchers("/**").authenticated()
        .and()
        .httpBasic();
	}

//	@Override
//	public void configure(ResourceServerSecurityConfigurer resources) throws Exception {
//		// TODO Auto-generated method stub
//		super.configure(resources);
//	}
	
}

//public class ResourceServerConfig extends ResourceServerConfigurerAdapter {
//
//	private static final String RESOURCE_ID = "protected_content";
//	
//	@Configuration
//	protected static class ResourceServer extends ResourceServerConfigurerAdapter {
//		
//		@Override
//		public void configure(ResourceServerSecurityConfigurer resources)
//				throws Exception {
//			resources.resourceId(RESOURCE_ID);
//		}
//		
//		@Override
//		public void configure(HttpSecurity http) throws Exception {
//			http
//				.authorizeRequests()
//				.antMatchers("/listCycle/**").permitAll()
//				.antMatchers("/cycle/**").permitAll()
//				.antMatchers("/**").permitAll()
////				.antMatchers("/listObjectMerks/**").permitAll()
//				.anyRequest().authenticated();
//		}
//	}
//}
