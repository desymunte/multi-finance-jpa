package com.collection.jpa.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

import com.collection.jpa.entity.LKRHistory;

@Transactional(readOnly = true)
public interface LKRHistoryRepository extends JpaRepository<LKRHistory, Integer> {

}
