package com.collection.jpa.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

import com.collection.jpa.entity.CycleType;

@Transactional(readOnly = true)
public interface CycleTypeRepository extends JpaRepository<CycleType, Integer> {

}
