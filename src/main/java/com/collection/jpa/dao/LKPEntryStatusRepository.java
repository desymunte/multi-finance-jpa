
package com.collection.jpa.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

import com.collection.jpa.entity.LKPEntryStatus;

@Transactional(readOnly = true)
public interface LKPEntryStatusRepository extends JpaRepository<LKPEntryStatus, Integer> {

}
