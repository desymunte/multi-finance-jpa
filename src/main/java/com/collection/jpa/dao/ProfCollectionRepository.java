package com.collection.jpa.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

import com.collection.jpa.entity.ProfCollector;

@Transactional(readOnly = true)
public interface ProfCollectionRepository extends JpaRepository<ProfCollector, Integer> {

}
