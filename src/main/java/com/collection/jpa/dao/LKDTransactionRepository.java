package com.collection.jpa.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

import com.collection.jpa.entity.LKDTransaction;

@Transactional(readOnly = true)
public interface LKDTransactionRepository extends JpaRepository<LKDTransaction, Integer> {

}
