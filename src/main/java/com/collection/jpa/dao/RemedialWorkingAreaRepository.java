package com.collection.jpa.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import com.collection.jpa.entity.RemedialWorkingArea;

@Transactional(readOnly = true)
public interface RemedialWorkingAreaRepository extends JpaRepository<RemedialWorkingArea, Integer> {
	@Modifying
	@Transactional
	@Query("delete from RemedialWorkingArea u where u.remedialMember.id = ?1")
	public void deleteList(Integer empId);
}
