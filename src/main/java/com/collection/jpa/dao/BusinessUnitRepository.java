package com.collection.jpa.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

import com.collection.jpa.entity.BusinessUnit;

@Transactional(readOnly = true)
public interface BusinessUnitRepository extends JpaRepository<BusinessUnit, Integer> {

}
