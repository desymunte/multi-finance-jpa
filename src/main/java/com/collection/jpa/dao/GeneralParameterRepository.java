package com.collection.jpa.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

import com.collection.jpa.entity.GeneralParameter;

@Transactional(readOnly = true)
public interface GeneralParameterRepository extends JpaRepository<GeneralParameter, Integer> {

}
