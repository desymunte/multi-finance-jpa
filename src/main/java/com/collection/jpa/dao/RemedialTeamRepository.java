package com.collection.jpa.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

import com.collection.jpa.entity.RemedialTeam;

@Transactional(readOnly = true)
public interface RemedialTeamRepository extends JpaRepository<RemedialTeam, Integer> {

}
