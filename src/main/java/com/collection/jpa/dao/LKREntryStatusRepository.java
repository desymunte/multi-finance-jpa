
package com.collection.jpa.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

import com.collection.jpa.entity.LKREntryStatus;

@Transactional(readOnly = true)
public interface LKREntryStatusRepository extends JpaRepository<LKREntryStatus, Integer> {

}
