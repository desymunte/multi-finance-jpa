package com.collection.jpa.dao;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

import com.collection.jpa.entity.CollectionBusinessUnit;

@Transactional(readOnly = true)
public interface CollectionBusinessUnitRepository extends JpaRepository<CollectionBusinessUnit, Integer> {

}
