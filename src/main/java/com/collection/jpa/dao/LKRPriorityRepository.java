package com.collection.jpa.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

import com.collection.jpa.entity.LKRPriority;

@Transactional(readOnly = true)
public interface LKRPriorityRepository extends JpaRepository<LKRPriority, Integer> {

}
