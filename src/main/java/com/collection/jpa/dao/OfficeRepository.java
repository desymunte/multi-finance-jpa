
package com.collection.jpa.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

import com.collection.jpa.entity.Office;

@Transactional(readOnly = true)
public interface OfficeRepository extends JpaRepository<Office, Integer> {

}
