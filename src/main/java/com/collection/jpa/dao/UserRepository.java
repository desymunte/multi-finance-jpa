package com.collection.jpa.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

import com.collection.jpa.entity.User;

@Transactional(readOnly = true)
public interface UserRepository extends JpaRepository<User, Integer> {

	User findByUsername(String username);
}
