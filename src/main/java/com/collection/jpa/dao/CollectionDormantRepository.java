package com.collection.jpa.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

import com.collection.jpa.entity.CollectionDormant;

@Transactional(readOnly = true)
public interface CollectionDormantRepository extends JpaRepository<CollectionDormant, Integer> {


}
