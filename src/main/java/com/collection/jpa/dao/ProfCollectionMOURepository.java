package com.collection.jpa.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

import com.collection.jpa.entity.ProfCollMOUHistory;

@Transactional(readOnly = true)
public interface ProfCollectionMOURepository extends JpaRepository<ProfCollMOUHistory, Integer> {

}
