package com.collection.jpa.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

import com.collection.jpa.entity.SKPCTransaction;

@Transactional(readOnly = true)
public interface SKPCTransactionRepository extends JpaRepository<SKPCTransaction, Integer> {

}
