package com.collection.jpa.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

import com.collection.jpa.entity.LKPPriority;

@Transactional(readOnly = true)
public interface LKPPriorityRepository extends JpaRepository<LKPPriority, Integer> {

}
