package com.collection.jpa.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

import com.collection.jpa.entity.LKPTransaction;

@Transactional(readOnly = true)
public interface LKPTransactionRepository extends JpaRepository<LKPTransaction, Integer> {

}
