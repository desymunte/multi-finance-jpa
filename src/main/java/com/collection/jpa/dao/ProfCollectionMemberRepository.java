package com.collection.jpa.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

import com.collection.jpa.entity.ProfCollMember;

@Transactional(readOnly = true)
public interface ProfCollectionMemberRepository extends JpaRepository<ProfCollMember, Integer> {

}
