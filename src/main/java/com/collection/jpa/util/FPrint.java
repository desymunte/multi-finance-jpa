package com.collection.jpa.util;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import com.collection.jpa.CustomUserDetails;

@Service
public class FPrint {
	
	@Temporal(TemporalType.DATE)
	Date currentDate = new Date();

    @PersistenceContext
	private EntityManager entityManager;

	private String getNextSerialNo(String printNo) {
		String nextSerialNo = 
				(String) entityManager.createNativeQuery("select fnGet_NextSeq(:printno)")
	    			.setParameter("printno", printNo)
	    			.getSingleResult();
		return nextSerialNo;
	}

	public String getNextPrintNo(String printType, String actor) {
        CustomUserDetails user = (CustomUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		String printNo = "";
		String type = "";
		
		switch (printType) {
		case "LKP" : type = "CO"; break;
		case "LKD" : type = "DC"; break;
		case "LKR" : type = "RM"; break;
		case "SKPC": type = "PC"; break;
		case "MOU" : type = "MU"; break;
		default    : type = "__"; break;
		}

		Date today = new Date();
        printNo = user.getOfficeCode()
        		.concat(new SimpleDateFormat("yyMMdd").format(currentDate))
        		.concat(type)
        		.concat(String.format("%5s", actor));
System.out.println("fprint: " + currentDate);
System.out.println("fprint: " + new SimpleDateFormat("yyMMdd").format(currentDate));
System.out.println("fprint: " + today);
System.out.println("fprint: " + new SimpleDateFormat("yyMMdd").format(today));

        if (!(printType.compareToIgnoreCase("MOU") == 0)) {
            printNo = this.getNextSerialNo(printNo);
        }

        System.out.println("fprint: " + printNo);
        return printNo;
	}

}
