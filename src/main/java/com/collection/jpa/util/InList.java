package com.collection.jpa.util;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

public class InList<T> {

	@PersistenceContext
	private EntityManager entityManager;

	private List<T> content;
	boolean exist;

	public List<T> getContent() {
		return content;
	}

	public void setContent(List<T> content) {
		this.content = content;
	}

	public boolean exists() {
		return exist;
	}

	public void setExist(boolean exist) {
		this.exist = exist;
	}

	public InList() {
		super();
		// TODO Auto-generated constructor stub
	}

	public InList(List<T> content, boolean exist) {
		super();
		this.content = content;
		this.exist = exist;
	}

	public InList(Class<T> tClass, String entity, Object criteria) {
		super();

		final List<Predicate> predicates = new ArrayList<Predicate>();
		CriteriaBuilder cBuilder = entityManager.getCriteriaBuilder();
		CriteriaQuery<T> cQuery = cBuilder.createQuery(tClass);
		Root<T> root = cQuery.from(tClass);

        predicates.add(cBuilder.equal(root.get(entity), criteria));
		cQuery.select(root).where(cBuilder.and(predicates.toArray(new Predicate[predicates.size()])));
		TypedQuery<T> query = entityManager.createQuery(cQuery);
		
		List<T> list = query.getResultList();
		this.content = list;
		this.exist = !list.isEmpty();
	}

}
