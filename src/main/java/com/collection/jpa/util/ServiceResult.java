package com.collection.jpa.util;


public class ServiceResult {
	
    public enum MessageType {SUCCESS, INFO, ALERT, WARNING, ERROR};
    private MessageType messagetype;
    private String message;
    private Object object;

    public MessageType getMessagetype() {
		return messagetype;
	}

	public void setMessagetype(MessageType messagetype) {
		this.messagetype = messagetype;
	}

	public String getMessage() {
		return message;
	}

    public void setMessage(String message) {
		this.message = message;
	}

    public Object getObject() {
		return object;
	}

    public void setObject(Object object) {
		this.object = object;
	}

	public ServiceResult() {
		super();
		// TODO Auto-generated constructor stub
	}

	public ServiceResult(MessageType messagetype, String message, Object object) {
		super();
		this.messagetype = messagetype;
		this.message = message;
		this.object = object;
	}

	@Override
	public String toString() {
		return "ServiceResult [messagetype=" + messagetype + ", message=" + message + ", object=" + object + "]";
	}

}
