package com.collection.jpa.util;

import java.util.List;

import javax.persistence.TypedQuery;

import org.springframework.data.domain.Pageable;

public class Page<T> {

	private List<T> content;
	private int page;
	private int pageNumber;
	private int pageSize;
	private int totalResult;
	private int totalPages;
	private int startElement;
	private int endElement;
	private int totalElements;
	boolean isFirst;
	boolean isLast;
	boolean hasNext;
	boolean hasPrevious;
	boolean hasContent;

	public List<T> getContent() {
		return content;
	}

	public void setContent(List<T> content) {
		this.content = content;
	}

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public int getPageNumber() {
		return pageNumber;
	}

	public void setPageNumber(int pageNumber) {
		this.pageNumber = pageNumber;
	}

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public int getTotalResult() {
		return totalResult;
	}

	public void setTotalResult(int totalResult) {
		this.totalResult = totalResult;
	}

	public int getTotalPages() {
		return totalPages;
	}

	public void setTotalPages(int totalPages) {
		this.totalPages = totalPages;
	}

	public int getStartElement() {
		return startElement;
	}

	public void setStartElement(int startElement) {
		this.startElement = startElement;
	}

	public int getEndElement() {
		return endElement;
	}

	public void setEndElement(int endElement) {
		this.endElement = endElement;
	}

	public int getTotalElements() {
		return totalElements;
	}

	public void setTotalElements(int totalElements) {
		this.totalElements = totalElements;
	}

	public boolean isFirst() {
		return isFirst;
	}

	public void setFirst(boolean isFirst) {
		this.isFirst = isFirst;
	}

	public boolean isLast() {
		return isLast;
	}

	public void setLast(boolean isLast) {
		this.isLast = isLast;
	}

	public boolean hasNext() {
		return hasNext;
	}

	public void setHasNext(boolean hasNext) {
		this.hasNext = hasNext;
	}

	public boolean hasPrevious() {
		return hasPrevious;
	}

	public void setHasPrevious(boolean hasPrevious) {
		this.hasPrevious = hasPrevious;
	}

	public boolean hasContent() {
		return hasContent;
	}

	public void setHasContent(boolean hasContent) {
		this.hasContent = hasContent;
	}

	public Page() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Page(TypedQuery<T> typedQuery, Pageable pageable) {
		super();

		int pageNumber = pageable.getPageNumber();
		int pageSize = pageable.getPageSize();
		int totalResult = typedQuery.getResultList().size();
		int totalPages = (int)Math.ceil(totalResult / (double)pageSize);

		if (pageNumber < 1 || pageNumber > totalPages) {
			pageNumber = 1;
		}
		if (pageSize < 1 || pageSize > 100) {
			pageSize = 10;
		}

		int offset = (pageNumber - 1) * pageSize;
		int startElement = offset + 1;
		int totalElements = pageNumber < totalPages ? pageSize : totalResult - (pageNumber - 1) * pageSize;
		int endElement = offset + totalElements;

		typedQuery = typedQuery
					.setFirstResult(offset)
					.setMaxResults(pageSize);
	
		List<T> list = typedQuery.getResultList();
	
		this.content = list;
		this.page = pageNumber;
		this.pageNumber = pageNumber;
		this.pageSize = pageSize;
		this.totalResult = totalResult;
		this.totalPages = totalPages;
		this.startElement = startElement;
		this.endElement = endElement;
		this.totalElements = totalElements;
		this.isFirst = pageNumber == 1;
		this.isLast = pageNumber == totalPages;
		this.hasNext = pageNumber < totalPages;
		this.hasPrevious = pageNumber > 1;
		this.hasContent = !list.isEmpty();
	}

}
