package com.collection.jpa;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableOAuth2Client;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;

import com.collection.jpa.dao.UserRepository;

@SpringBootApplication
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class CollectionJPAApplication {

	public static void main(String[] args) {
		SpringApplication.run(CollectionJPAApplication.class, args);
	}
	
	@Autowired
	public void authenticationManager(AuthenticationManagerBuilder builder, UserRepository repo) throws Exception {
		builder.userDetailsService(new UserDetailsService() {
			public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
				return new CustomUserDetails(repo.findByUsername(s));
			}
		});
	}
	
//	@Autowired
//	public void authenticationManager(AuthenticationManagerBuilder builder) throws Exception {
//		UserDetailsService userDetailsService = null;
//		builder.userDetailsService(userDetailsService);
//	}
}
