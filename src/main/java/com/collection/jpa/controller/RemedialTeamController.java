package com.collection.jpa.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.collection.jpa.dto.ChangeSpvParamDTO;
import com.collection.jpa.dto.TeamCriteria;
import com.collection.jpa.dto.TeamDTO;
import com.collection.jpa.entity.RemedialTeam;
import com.collection.jpa.service.RemedialTeamImpl;
import com.collection.jpa.util.Page;
import com.collection.jpa.util.ServiceResult;

@RestController
@EnableResourceServer
public class RemedialTeamController {

	@Autowired
	private RemedialTeamImpl remedialTeamService;

    @PreAuthorize("hasRole('ROLE_SUPERADMIN') or hasRole('ROLE_REMEDIAL_HEAD') or hasRole('ROLE_REMEDIAL_SPV')")
	@RequestMapping(value="/api/collection/rem/team/list", method=RequestMethod.POST)
	public @ResponseBody List<TeamDTO> getList() {
		List<TeamDTO> results = remedialTeamService.list();
		return results;
	}
	
    @PreAuthorize("hasRole('ROLE_SUPERADMIN') or hasRole('ROLE_REMEDIAL_HEAD') or hasRole('ROLE_REMEDIAL_SPV')")
	@RequestMapping(value="/api/collection/rem/team/page", method=RequestMethod.POST)
	public @ResponseBody Page<TeamDTO> getPage(@RequestBody TeamCriteria criteria, @PageableDefault(value=5, sort={"id"}) Pageable pageable) {
		Page<TeamDTO> results = remedialTeamService.page(criteria, pageable);
		return results;
	}
	
    @PreAuthorize("hasRole('ROLE_SUPERADMIN') or hasRole('ROLE_REMEDIAL_HEAD') or hasRole('ROLE_REMEDIAL_SPV')")
	@RequestMapping(value="/api/collection/rem/team", method=RequestMethod.POST)
	public @ResponseBody TeamDTO getContent(@RequestBody TeamCriteria criteria) {
		TeamDTO results = remedialTeamService.content(criteria);
		return results;
	}
	
	@PreAuthorize("hasRole('ROLE_REMEDIAL_HEAD')")
	@RequestMapping(value="/api/collection/rem/team/changespv", method=RequestMethod.POST)
	public @ResponseBody ServiceResult changeSpv(@RequestBody ChangeSpvParamDTO data) {
		ServiceResult results = remedialTeamService.changeSpv(data);
		return results;
	}
	
	@PreAuthorize("hasRole('ROLE_REMEDIAL_HEAD')")
	@RequestMapping(value="/api/collection/rem/team/setup", method=RequestMethod.POST)
    public @ResponseBody ServiceResult setup(@RequestBody RemedialTeam data) {
		return remedialTeamService.setup(data);
    }
	
}
