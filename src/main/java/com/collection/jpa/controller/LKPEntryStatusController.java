package com.collection.jpa.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.collection.jpa.entity.LKPEntryStatus;
import com.collection.jpa.service.LKPEntryStatusImpl;

@RestController
@EnableResourceServer
public class LKPEntryStatusController {

	@Autowired
	private LKPEntryStatusImpl lkpEntryStatusService;


	@RequestMapping(value="/api/collection/lkpentry/list", method=RequestMethod.POST)
	public @ResponseBody List<LKPEntryStatus> getStatusList() {
		long start = System.currentTimeMillis();
		List<LKPEntryStatus> results = lkpEntryStatusService.listStatus();
		long timing = System.currentTimeMillis() - start;
		System.out.println(timing);
		
		return results;
	}
	
}
