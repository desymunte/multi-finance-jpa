package com.collection.jpa.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.collection.jpa.dto.EmployeeCriteria;
import com.collection.jpa.dto.EmployeeDTO;
import com.collection.jpa.entity.Employee;
import com.collection.jpa.service.EmployeeImpl;
import com.collection.jpa.util.Page;

@RestController
@EnableResourceServer
public class EmployeeController {

	@Autowired
	private EmployeeImpl employeeService;
	
	@RequestMapping(value="/api/collection/coll/spv/list/available", method=RequestMethod.POST)
	public @ResponseBody List<EmployeeDTO> getCollectionSpvAvailableList() {
		List<EmployeeDTO> results = employeeService.listCollectionSpv();
		return results;
	}
	
	@RequestMapping(value="/api/collection/rem/spv/list/available", method=RequestMethod.POST)
	public @ResponseBody List<EmployeeDTO> getRemedialSpvAvailableList() {
		List<EmployeeDTO> results = employeeService.listRemedialSpv();
		return results;
	}

	@RequestMapping(value="/api/collection/dcall/spv/list/available", method=RequestMethod.POST)
	public @ResponseBody List<EmployeeDTO> getDeskcallSpvAvailableList() {
		List<EmployeeDTO> results = employeeService.listDeskcallSpv();
		return results;
	}
	
	@RequestMapping(value="/api/collection/coll/staff/list/available", method=RequestMethod.POST)
	public @ResponseBody List<EmployeeDTO> getCollectionStaffList() {
		List<EmployeeDTO> results = employeeService.listCollectionStaff();
		return results;
	}
	
	@RequestMapping(value="/api/collection/rem/staff/list/available", method=RequestMethod.POST)
	public @ResponseBody List<EmployeeDTO> getRemedialStaffList() {
		List<EmployeeDTO> results = employeeService.listRemedialStaff();
		return results;
	}
	
	@RequestMapping(value="/api/collection/dcall/staff/list/available", method=RequestMethod.POST)
	public @ResponseBody List<EmployeeDTO> getDeskcallStaffList() {
		List<EmployeeDTO> results = employeeService.listDeskcallStaff();
		return results;
	}
	
	@RequestMapping(value="/api/collection/employee/list/available", method=RequestMethod.POST)
	public @ResponseBody List<Employee> getList() {
		List<Employee> results = employeeService.list();
		return results;
	}
	
	@RequestMapping(value="/api/collection/employee/page/available", method=RequestMethod.POST)
	public @ResponseBody Page<Employee> getPage(@RequestBody EmployeeCriteria criteria, @PageableDefault(value=5, sort={"id"}) Pageable pageable) {
		Page<Employee> results = employeeService.page(criteria, pageable);
		return results;
	}
	
}
