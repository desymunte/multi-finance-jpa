package com.collection.jpa.controller;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.collection.jpa.dto.CollectionWorkingAreaCriteria;
import com.collection.jpa.dto.ZipcodeAvailableCriteria;
import com.collection.jpa.entity.CollectionWorkingArea;
import com.collection.jpa.entity.Zipcode;
import com.collection.jpa.service.CollectionWorkingAreaImpl;
import com.collection.jpa.util.ServiceResult;

@RestController
@EnableResourceServer
public class CollectionWorkingAreaController {

	@Autowired
	private CollectionWorkingAreaImpl collectionWorkingAreaService;

	@RequestMapping(value="/api/collection/zipcode/occupied/list", method=RequestMethod.POST)
	public @ResponseBody List<CollectionWorkingArea> getListOccupied(@RequestBody CollectionWorkingAreaCriteria criteria) {
		long start = System.currentTimeMillis();
		List<CollectionWorkingArea> results = collectionWorkingAreaService.listOccupied(criteria);
		long timing = System.currentTimeMillis() - start;
		System.out.println(timing);
		
		return results;
	}
	
/*	@RequestMapping(value="/collection/zipcode/available/list", method=RequestMethod.POST)
	public @ResponseBody List<Zipcode> getListAvailable(@RequestBody ZipcodeAvailableCriteria criteria) {
		long start = System.currentTimeMillis();
		List<Zipcode> results = collectionWorkingAreaService.listAvailable(criteria);
		long timing = System.currentTimeMillis() - start;
		System.out.println(timing);
		
		return results;
	}
*/		
	@RequestMapping(value="/api/collection/zipcode/insert", method=RequestMethod.POST)
    public @ResponseBody ServiceResult insert(@RequestBody CollectionWorkingArea persisted) {
		long start = System.currentTimeMillis();
		ServiceResult results = collectionWorkingAreaService.insert(persisted);
		long timing = System.currentTimeMillis() - start;
		System.out.println(timing);
		
		return results;
    }
	
/*	@RequestMapping(value="/api/collection/zipcode/delete/{id}", method=RequestMethod.GET)
    public @ResponseBody ServiceResult delete(@PathVariable Integer id) {
		long start = System.currentTimeMillis();
		ServiceResult results = collectionWorkingAreaService.delete(id);
		long timing = System.currentTimeMillis() - start;
		System.out.println(timing);
		
		return results;
    }
*/	

}
