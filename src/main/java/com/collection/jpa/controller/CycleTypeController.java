package com.collection.jpa.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.collection.jpa.entity.CycleType;
import com.collection.jpa.service.CycleTypeImpl;

@RestController
@EnableResourceServer
public class CycleTypeController {

	@Autowired
	private CycleTypeImpl cycleTypeService;


	@RequestMapping(value="/api/collection/cycletype/list", method=RequestMethod.POST)
	public @ResponseBody List<CycleType> getCycleTypeList() {
		List<CycleType> results = cycleTypeService.listCycleType();
		return results;
	}
	
}
