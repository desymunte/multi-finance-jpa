package com.collection.jpa.controller;

import java.security.Principal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.collection.jpa.dto.DormantDTO;
import com.collection.jpa.dto.OfficeCriteria;
import com.collection.jpa.entity.CollectionDormant;
import com.collection.jpa.entity.CollectionMember;
import com.collection.jpa.service.ChangeCollectorImpl;
import com.collection.jpa.util.Page;
import com.collection.jpa.util.ServiceResult;

@RestController
@EnableResourceServer
public class ChangeCollectorController {

	@Autowired
	private ChangeCollectorImpl changeCollectorService;

    @PreAuthorize("hasRole('ROLE_SUPERADMIN') or hasRole('ROLE_COLLECTION_HEAD') or hasRole('ROLE_COLLECTION_SPV')")
	@RequestMapping(value="/api/collection/changecollector/page", method=RequestMethod.POST)
    public @ResponseBody Page<DormantDTO> getPage(@RequestBody OfficeCriteria criteria, @PageableDefault(value=5, sort={"id"}) Pageable pageable, Principal principal) {
		return changeCollectorService.pageDormant(criteria, pageable);
    }
	
    @PreAuthorize("hasRole('ROLE_COLLECTION_HEAD') or hasRole('ROLE_COLLECTION_SPV')")
	@RequestMapping(value="/api/collection/changecollector/assign", method=RequestMethod.POST)
    public @ResponseBody ServiceResult assign(@RequestBody CollectionDormant dormant) {
		return changeCollectorService.assign(dormant);
    }
	
    @PreAuthorize("hasRole('ROLE_COLLECTION_HEAD') or hasRole('ROLE_COLLECTION_SPV')")
	@RequestMapping(value="/api/collection/changecollector/revoke", method=RequestMethod.POST)
    public @ResponseBody ServiceResult revoke(@RequestBody CollectionMember member) {
		return changeCollectorService.revoke(member);
    }
	
    @PreAuthorize("hasRole('ROLE_COLLECTION_HEAD') or hasRole('ROLE_COLLECTION_SPV')")
	@RequestMapping(value="/api/collection/changecollector/assignBaru", method=RequestMethod.POST)
    public @ResponseBody ServiceResult assignBaru(@RequestBody CollectionMember member) {
		return changeCollectorService.assignBaru(member);
    }
	
}
