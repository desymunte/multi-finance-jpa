package com.collection.jpa.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.collection.jpa.dto.DeskCallTeamMemberDTO;
import com.collection.jpa.dto.MemberCriteria;
import com.collection.jpa.entity.DeskcallMember;
import com.collection.jpa.service.DeskcallMemberImpl;
import com.collection.jpa.util.Page;
import com.collection.jpa.util.ServiceResult;

@RestController
@EnableResourceServer
public class DeskcallMemberController {

	@Autowired
	private DeskcallMemberImpl deskcallMemberService;

    @PreAuthorize("hasRole('ROLE_SUPERADMIN') or hasRole('ROLE_DESKCALL_HEAD') or hasRole('ROLE_DESKCALL_SPV')")
	@RequestMapping(value="/api/collection/dcall/member/page", method=RequestMethod.POST)
	public @ResponseBody Page<DeskCallTeamMemberDTO> getPage(@RequestBody MemberCriteria criteria, @PageableDefault(value=5, sort={"id"}) Pageable pageable) {
		Page<DeskCallTeamMemberDTO> results = deskcallMemberService.page(criteria, pageable);
		return results;
	}
    
    @PreAuthorize("hasRole('ROLE_DESKCALL_HEAD') or hasRole('ROLE_DESKCALL_SPV')")
	@RequestMapping(value="/api/collection/dcall/member/delete", method=RequestMethod.POST)
    public @ResponseBody ServiceResult delete(@RequestBody DeskcallMember data) {
		return deskcallMemberService.delete(data);
    }
	
    @PreAuthorize("hasRole('ROLE_DESKCALL_HEAD') or hasRole('ROLE_DESKCALL_SPV')")
	@RequestMapping(value="/api/collection/dcall/member/insert", method=RequestMethod.POST)
    public @ResponseBody ServiceResult insert(@RequestBody DeskcallMember data) {
		return deskcallMemberService.insert(data);
    }
	
}
