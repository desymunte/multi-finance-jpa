package com.collection.jpa.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.collection.jpa.dto.RemedialMemberDTO;
import com.collection.jpa.dto.MemberCriteria;
import com.collection.jpa.entity.RemedialMember;
import com.collection.jpa.service.RemedialMemberImpl;
import com.collection.jpa.util.Page;
import com.collection.jpa.util.ServiceResult;

@RestController
@EnableResourceServer
public class RemedialMemberController {

	@Autowired
	private RemedialMemberImpl remedialMemberService;

    @PreAuthorize("hasRole('ROLE_SUPERADMIN') or hasRole('ROLE_REMEDIAL_HEAD') or hasRole('ROLE_REMEDIAL_SPV')")
	@RequestMapping(value="/api/collection/rem/member/page", method=RequestMethod.POST)
	public @ResponseBody Page<RemedialMemberDTO> getPage(@RequestBody MemberCriteria criteria, @PageableDefault(value=5, sort={"id"}) Pageable pageable) {
		Page<RemedialMemberDTO> results = remedialMemberService.page(criteria, pageable);
		return results;
	}
	
    @PreAuthorize("hasRole('ROLE_REMEDIAL_HEAD') or hasRole('ROLE_REMEDIAL_SPV')")
	@RequestMapping(value="/api/collection/rem/member/insert", method=RequestMethod.POST)
    public @ResponseBody ServiceResult insert(@RequestBody RemedialMember data) {
		return remedialMemberService.insert(data);
    }
    
    @PreAuthorize("hasRole('ROLE_REMEDIAL_HEAD') or hasRole('ROLE_REMEDIAL_SPV')")
	@RequestMapping(value="/api/collection/rem/member/delete", method=RequestMethod.POST)
    public @ResponseBody ServiceResult delete(@RequestBody RemedialMember data) {
		return remedialMemberService.delete(data);
    }
	
}
