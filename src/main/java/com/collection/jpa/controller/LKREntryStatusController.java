package com.collection.jpa.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.collection.jpa.entity.LKREntryStatus;
import com.collection.jpa.service.LKREntryStatusImpl;

@RestController
@EnableResourceServer
public class LKREntryStatusController {

	@Autowired
	private LKREntryStatusImpl lkrEntryStatusService;


	@RequestMapping(value="/api/collection/lkrentry/list", method=RequestMethod.POST)
	public @ResponseBody List<LKREntryStatus> getStatusList() {
		long start = System.currentTimeMillis();
		List<LKREntryStatus> results = lkrEntryStatusService.listStatus();
		long timing = System.currentTimeMillis() - start;
		System.out.println(timing);
		
		return results;
	}
	
}
