package com.collection.jpa.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.collection.jpa.entity.Role;
import com.collection.jpa.service.RoleImpl;

@RestController
@EnableResourceServer
public class RoleController {
	
	private final Logger log = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private RoleImpl roleService;


	@RequestMapping(value="/api/collection/role/list", method=RequestMethod.POST)
    public @ResponseBody List<Role> list() {
		long start = System.currentTimeMillis();
		List<Role> results = roleService.listRole();
		long timing = System.currentTimeMillis() - start;
		System.out.println(timing);
		
		return results;
    }

}
