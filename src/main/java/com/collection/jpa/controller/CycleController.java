package com.collection.jpa.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.collection.jpa.dto.CycleCriteria;
import com.collection.jpa.dto.CycleDTO;
import com.collection.jpa.entity.Cycle;
import com.collection.jpa.service.CycleImpl;
import com.collection.jpa.util.Page;
import com.collection.jpa.util.ServiceResult;

@RestController
@EnableResourceServer
public class CycleController {

	@Autowired
	private CycleImpl cycleService;

	@RequestMapping(value="/api/collection/cycle/page", method=RequestMethod.POST)
	public @ResponseBody Page<CycleDTO> getPage(@RequestBody CycleCriteria criteria, @PageableDefault(value=5, sort={"id"}) Pageable pageable) {
		long start = System.currentTimeMillis();
		Page<CycleDTO> results = cycleService.page(criteria, pageable);
		long timing = System.currentTimeMillis() - start;
		System.out.println(timing);

		return results;
	}
	
	@RequestMapping(value="/api/collection/cycle/content", method=RequestMethod.POST)
	public @ResponseBody Cycle getContent(@RequestBody CycleCriteria criteria) {
		long start = System.currentTimeMillis();
		Cycle results = cycleService.content(criteria);
		long timing = System.currentTimeMillis() - start;
		System.out.println(timing);

		return results;
	}
	
    @PreAuthorize("hasRole('ROLE_SUPERADMIN')")
	@RequestMapping(value="/api/collection/cycle/update", method=RequestMethod.POST)
	public @ResponseBody ServiceResult update(@RequestBody Cycle data) {
		ServiceResult results = cycleService.update(data);
		return results;
	}
	
    @PreAuthorize("hasRole('ROLE_SUPERADMIN')")
	@RequestMapping(value="/api/collection/cycle/insert", method=RequestMethod.POST)
	public @ResponseBody ServiceResult insert(@RequestBody Cycle data) {
		ServiceResult results = cycleService.insert(data);
		return results;
	}
	
    @PreAuthorize("hasRole('ROLE_SUPERADMIN')")
	@RequestMapping(value="/api/collection/cycle/delete", method=RequestMethod.POST)
	public @ResponseBody ServiceResult delete(@RequestBody Cycle data) {
		ServiceResult results = cycleService.delete(data);
		return results;
	}
	
	@RequestMapping(value="/api/collection/cycle/coll/list", method=RequestMethod.POST)
	public @ResponseBody List<CycleDTO> getCollectionList() {
		List<CycleDTO> results = cycleService.listCollection();
		return results;
	}
	
	@RequestMapping(value="/api/collection/cycle/rem/list", method=RequestMethod.POST)
	public @ResponseBody List<CycleDTO> getRemedialList() {
		List<CycleDTO> results = cycleService.listRemedial();
		return results;
	}
	
	@RequestMapping(value="/api/collection/cycle/dcall/list", method=RequestMethod.POST)
	public @ResponseBody List<CycleDTO> getDeskcallList() {
		List<CycleDTO> results = cycleService.listDeskcall();
		return results;
	}
	
}
