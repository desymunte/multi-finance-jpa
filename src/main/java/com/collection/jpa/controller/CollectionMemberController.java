package com.collection.jpa.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.collection.jpa.dto.CollectionMemberDTO;
import com.collection.jpa.dto.MemberCriteria;
import com.collection.jpa.entity.CollectionMember;
import com.collection.jpa.service.CollectionMemberImpl;
import com.collection.jpa.util.Page;
import com.collection.jpa.util.ServiceResult;

@RestController
@EnableResourceServer
public class CollectionMemberController {

	@Autowired
	private CollectionMemberImpl collectionMemberService;

    @PreAuthorize("hasRole('ROLE_SUPERADMIN') or hasRole('ROLE_COLLECTION_HEAD') or hasRole('ROLE_COLLECTION_SPV')")
	@RequestMapping(value="/api/collection/coll/member/page", method=RequestMethod.POST)
	public @ResponseBody Page<CollectionMemberDTO> getPage(@RequestBody MemberCriteria criteria, @PageableDefault(value=5, sort={"id"}) Pageable pageable) {
		Page<CollectionMemberDTO> results = collectionMemberService.page(criteria, pageable);
		return results;
	}
	
    @PreAuthorize("hasRole('ROLE_COLLECTION_HEAD') or hasRole('ROLE_COLLECTION_SPV')")
	@RequestMapping(value="/api/collection/coll/member/insert", method=RequestMethod.POST)
    public @ResponseBody ServiceResult insert(@RequestBody CollectionMember data) {
		return collectionMemberService.insert(data);
    }
	
}
