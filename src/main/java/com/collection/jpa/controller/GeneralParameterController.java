package com.collection.jpa.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.collection.jpa.entity.GeneralParameter;
import com.collection.jpa.service.GeneralParameterImpl;
import com.collection.jpa.util.ServiceResult;

@RestController
@EnableResourceServer
public class GeneralParameterController {

	@Autowired
	private GeneralParameterImpl genparamService;

	@RequestMapping(value="/api/collection/genparam/content", method=RequestMethod.POST)
	public @ResponseBody GeneralParameter getContent() {
		GeneralParameter results = genparamService.content();
		return results;
	}
	
    @PreAuthorize("hasRole('ROLE_SUPERADMIN')")
	@RequestMapping(value="/api/collection/genparam/update", method=RequestMethod.POST)
	public @ResponseBody ServiceResult update(@RequestBody GeneralParameter data) {
		ServiceResult results = genparamService.update(data);
		return results;
	}
	
}
