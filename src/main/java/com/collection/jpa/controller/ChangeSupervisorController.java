package com.collection.jpa.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.collection.jpa.dto.TeamCriteria;
import com.collection.jpa.dto.TeamDTO;
import com.collection.jpa.service.CollectionTeamImpl;
import com.collection.jpa.service.DeskcallTeamImpl;
import com.collection.jpa.service.RemedialTeamImpl;
import com.collection.jpa.util.Page;

@RestController
@EnableResourceServer
public class ChangeSupervisorController {

	@Autowired
	private CollectionTeamImpl collectionTeamService;

	@Autowired
	private RemedialTeamImpl remedialTeamService;

	@Autowired
	private DeskcallTeamImpl deskcallTeamService;


	@PreAuthorize("hasRole('ROLE_COLLECTION_SPV') or hasRole('ROLE_COLLECTION_HEAD') or hasRole('ROLE_REMEDIAL_SPV') or hasRole('ROLE_REMEDIAL_HEAD') or hasRole('ROLE_DESKCALL_SPV') or hasRole('ROLE_DESKCALL_HEAD')")
	@RequestMapping(value="/api/collection/changesupervisor/page", method=RequestMethod.POST)
	public @ResponseBody Page<TeamDTO> getPage(@RequestBody TeamCriteria criteria, @PageableDefault(value=5, sort={"id"}) Pageable pageable) {
    	Page<TeamDTO> results = new Page<TeamDTO>();
    	if (criteria.getCycleType() == null) {
    		return results;
    	}

    	switch (criteria.getCycleType()) {
		//collection
		case "C":
			results = collectionTeamService.page(criteria, pageable);
			break;

		//remedial
		case "R": //
			results = remedialTeamService.page(criteria, pageable);
			break;

		//deskcall
		case "D":
			results = deskcallTeamService.page(criteria, pageable);
			break;
		}

		return results;
	}
	
}
