package com.collection.jpa.controller;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.collection.jpa.dto.CycleCriteria;
import com.collection.jpa.dto.MemberCriteria;
import com.collection.jpa.dto.ZipDTO;
import com.collection.jpa.service.ZipcodeImpl;

@RestController
@EnableResourceServer
public class ZipcodeController {

	@Autowired
	private ZipcodeImpl zipcodeService;

	@RequestMapping(value="/api/collection/zipcode/coll/list/available", method=RequestMethod.POST)
	public @ResponseBody List<ZipDTO> getCollAvailableList(@RequestBody CycleCriteria criteria) {
		List<ZipDTO> results = zipcodeService.listCollection(criteria);
		return results;
	}
		
	@RequestMapping(value="/api/collection/zipcode/rem/list/available", method=RequestMethod.POST)
	public @ResponseBody List<ZipDTO> getRemAvailableList(@RequestBody CycleCriteria criteria) {
		List<ZipDTO> results = zipcodeService.listRemedial(criteria);
		return results;
	}
		
	@RequestMapping(value="/api/collection/zipcode/coll/member/list", method=RequestMethod.POST)
	public @ResponseBody List<ZipDTO> getCollMemberList(@RequestBody MemberCriteria criteria) {
		List<ZipDTO> results = zipcodeService.listCollectionMember(criteria);
		return results;
	}
		
}
