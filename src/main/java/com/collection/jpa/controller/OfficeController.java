package com.collection.jpa.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.collection.jpa.dto.OfficeCriteria;
import com.collection.jpa.entity.Office;
import com.collection.jpa.service.OfficeImpl;
import com.collection.jpa.util.Page;

@RestController
@EnableResourceServer
public class OfficeController {

	@Autowired
	private OfficeImpl officeService;

	@RequestMapping(value="/api/collection/office/page", method=RequestMethod.POST)
	public @ResponseBody Page<Office> getPage(@RequestBody OfficeCriteria criteria, @PageableDefault(value=5, sort={"id"}) Pageable pageable) {
		Page<Office> results = officeService.page(criteria, pageable);
		return results;
	}
	
}
