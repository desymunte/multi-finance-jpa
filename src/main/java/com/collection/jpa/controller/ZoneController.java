package com.collection.jpa.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.collection.jpa.dto.ZoneCriteria;
import com.collection.jpa.entity.Zone;
import com.collection.jpa.service.ZoneImpl;
import com.collection.jpa.util.Page;

@RestController
@EnableResourceServer
public class ZoneController {

	@Autowired
	private ZoneImpl zoneService;
	
	@RequestMapping(value="/api/zone/page", method=RequestMethod.POST)
	public @ResponseBody Page<Zone> getZoneByParams(@RequestBody ZoneCriteria criteria, @PageableDefault(value=5, sort={"id"}) Pageable pageable) {
		long start = System.currentTimeMillis();
		Page<Zone> results = zoneService.findAll(criteria, pageable);
		long timing = System.currentTimeMillis() - start;
		System.out.println(timing);
		
		return results;
	}
	
}
