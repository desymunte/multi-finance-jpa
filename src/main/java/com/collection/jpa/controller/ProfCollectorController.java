package com.collection.jpa.controller;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.collection.jpa.CustomUserDetails;
import com.collection.jpa.dao.GeneralParameterRepository;
import com.collection.jpa.dao.OfficeRepository;
import com.collection.jpa.dto.ProfCollMemberDTO;
import com.collection.jpa.dto.ProfCollMouPrintDTO;
import com.collection.jpa.dto.ProfCollectorCriteria;
import com.collection.jpa.dto.ProfCollectorDTO;
import com.collection.jpa.entity.GeneralParameter;
import com.collection.jpa.entity.Office;
import com.collection.jpa.entity.ProfCollector;
import com.collection.jpa.service.ProfCollectorImpl;
import com.collection.jpa.util.Page;
import com.collection.jpa.util.ServiceResult;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.xml.JRXmlLoader;

@RestController
@EnableResourceServer
public class ProfCollectorController {
	
	private final Logger log = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private ProfCollectorImpl profCollectorService;
	
	@Autowired
	private GeneralParameterRepository paramrepository;
	
	@Autowired
	private OfficeRepository officerepository;
	

	@PreAuthorize("hasRole('ROLE_REMEDIAL_HEAD')")
	@RequestMapping(value="/api/collection/profcoll/team/page", method=RequestMethod.POST)
	public @ResponseBody Page<ProfCollectorDTO> getPage(@RequestBody ProfCollectorCriteria criteria, @PageableDefault(value=5, sort={"id"}) Pageable pageable) {
		long start = System.currentTimeMillis();
		Page<ProfCollectorDTO> results = profCollectorService.page(criteria, pageable);
		long timing = System.currentTimeMillis() - start;
		System.out.println(timing);
		
		return results;
	}
	
    @PreAuthorize("hasRole('ROLE_REMEDIAL_HEAD')")
	@RequestMapping(value="/api/collection/profcoll/team/setup", method=RequestMethod.POST)
    public @ResponseBody ServiceResult setup(@RequestBody ProfCollector data) {
		return profCollectorService.setup(data);
    }

    @PreAuthorize("hasRole('ROLE_REMEDIAL_HEAD')")
	@RequestMapping(value="/api/collection/profcoll/mou/create", method=RequestMethod.POST)
    public @ResponseBody ServiceResult mou(@RequestBody ProfCollector data) {
		return profCollectorService.update(data);
    }
	
    @PreAuthorize("hasRole('ROLE_REMEDIAL_HEAD')")
	@RequestMapping(value="/api/collection/profcoll/mou/print", method=RequestMethod.POST)
	public @ResponseBody void print(HttpServletResponse response, @RequestBody ProfCollector criteria) {
        CustomUserDetails user = (CustomUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        List<ProfCollMouPrintDTO> data = new ArrayList<ProfCollMouPrintDTO>();
		ProfCollMouPrintDTO dataSingle = profCollectorService.print(criteria);
		data.add(dataSingle);

		try
		{
			InputStream jasperStream = this.getClass().getResourceAsStream("/report/MOU.jrxml");
			JasperDesign design = JRXmlLoader.load(jasperStream);
			JasperReport report = JasperCompileManager.compileReport(design);
			
			Map<String, Object> parameterMap = new HashMap<String, Object>();
			JRDataSource jRDataSource = new JRBeanCollectionDataSource(data);

    		GeneralParameter params = paramrepository.findOne(1);
    		Office office = officerepository.findOne(user.getOfficeId());
			parameterMap.put("company", params.getCompany_name());
			parameterMap.put("office", office.getName());
			parameterMap.put("address", office.getAddress());
			parameterMap.put("datasource", jRDataSource);
			
			JasperPrint jasperPrint = JasperFillManager.fillReport(report, parameterMap, jRDataSource);
			response.setContentType("application/x-pdf");
			response.setHeader("Content-Disposition", "attachment: filename=\"" + dataSingle.getMouNo() + ".pdf\"");
			response.setHeader("Content-Filename", dataSingle.getMouNo() + ".pdf");
			
			final OutputStream outputStream = response.getOutputStream();
			JasperExportManager.exportReportToPdfStream(jasperPrint, outputStream);

		} catch (JRException ex)
		{
			System.out.println(ex);
			log.info("tidak bisa membaca file jrxml");
		} catch (IOException ex) 
		{
			log.info("Tidak bisa mengambil outputstream dari response");
		}
	}

	@RequestMapping(value="/api/collection/profcoll/active/list", method=RequestMethod.POST)
    public @ResponseBody List<ProfCollMemberDTO> list(@RequestBody ProfCollectorCriteria criteria) {
		return profCollectorService.listActiveProfColl(criteria);
    }
	
}
