package com.collection.jpa.controller;

import java.security.Principal;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.collection.jpa.CustomUserDetails;
import com.collection.jpa.dto.UserPageDTO;
import com.collection.jpa.entity.User;
import com.collection.jpa.service.UserImpl;
import com.collection.jpa.util.Page;
import com.collection.jpa.util.ServiceResult;

@RestController
@EnableResourceServer
public class UserController {
	
	private final Logger log = LoggerFactory.getLogger(this.getClass());
	
//	private UserDetailsService user;
	
	@Autowired
	private UserImpl userService;

	@RequestMapping(value="/api/collection/user", method=RequestMethod.GET)
    public @ResponseBody CustomUserDetails user(Principal principal) {
//		log.info(principal);
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		System.out.println(authentication.getDetails());
		return (CustomUserDetails) authentication.getDetails();
    }
	
	@RequestMapping(value="/api/collection/user1", method=RequestMethod.POST)
    public @ResponseBody CustomUserDetails user1(Principal principal) {
//		log.info(principal);
		Object userDetails = SecurityContextHolder.getContext().getAuthentication().getDetails();
        if (userDetails instanceof UserDetails) {
            return ((CustomUserDetails)userDetails);
        }
//        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
//        Object cud = user.loadUserByUsername("admin");

//        return (CustomUserDetails) cud;
        
        CustomUserDetails user1 = (CustomUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        CustomUserDetails user2 = user1;
        user2.setPassword("*****");
//        return (CustomUserDetails) user;
        return user2;
    }
	

//	@PreAuthorize("hasRole('ROLE_SUPER_ADMIN')")
	@RequestMapping(value="/api/collection/user/page", method=RequestMethod.POST)
	public @ResponseBody Page<UserPageDTO> getPage(@PageableDefault(value=5, sort={"id"}) Pageable pageable) {
		long start = System.currentTimeMillis();
		Page<UserPageDTO> results = userService.page(pageable);
		long timing = System.currentTimeMillis() - start;
		System.out.println(timing);
		
		return results;
	}
	
	@RequestMapping(value="/api/collection/user/create", method=RequestMethod.POST)
    public @ResponseBody ServiceResult create(@RequestBody User data) {
		return userService.create(data);
    }
	
	@RequestMapping(value="/api/collection/user/update", method=RequestMethod.POST)
    public @ResponseBody ServiceResult update(@RequestBody User data) {
		return userService.update(data);
    }
	
	@RequestMapping(value="/api/collection/user/delete", method=RequestMethod.POST)
    public @ResponseBody ServiceResult delete(@RequestBody User data) {
		return userService.delete(data);
    }
	
}
