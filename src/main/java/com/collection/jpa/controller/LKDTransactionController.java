package com.collection.jpa.controller;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.collection.jpa.CustomUserDetails;
import com.collection.jpa.dao.GeneralParameterRepository;
import com.collection.jpa.dto.LkdCriteria;
import com.collection.jpa.dto.LkdDataDTO;
import com.collection.jpa.dto.LkdPrintDTO;
import com.collection.jpa.dto.LkdHeaderDTO;
import com.collection.jpa.entity.GeneralParameter;
import com.collection.jpa.entity.LKDEntryStatus;
import com.collection.jpa.entity.LKDTransaction;
import com.collection.jpa.service.LKDTransactionImpl;
import com.collection.jpa.util.ServiceResult;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.xml.JRXmlLoader;


@RestController
@EnableResourceServer
public class LKDTransactionController {
	
	private final Logger log = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private LKDTransactionImpl lkdTrxService;

	@Autowired
	private GeneralParameterRepository paramrepository;

	
	@PreAuthorize("hasRole('ROLE_DESKCALL_SPV')")
	@RequestMapping(value="/api/collection/lkd/print", method=RequestMethod.POST)
	public @ResponseBody void print(HttpServletResponse response) {
        CustomUserDetails user = (CustomUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if (!user.getJobCode().equals("DESKSPV")) {
			System.out.println("Not authorized user");
			log.info("Not authorized user");
        	return;
    	}

    	List<LkdPrintDTO> data = lkdTrxService.listPrint();
		if (data == null) {
			System.out.println("No LKD to print");
			log.info("No LKD to print");
			return;
		}

		LkdHeaderDTO header = lkdTrxService.headerPrint();
		if (header == null) {
			System.out.println("Cannot create header");
			log.info("Cannot create header");
			return;
		}

		try
		{
			InputStream jasperStream = this.getClass().getResourceAsStream("/report/LKD.jrxml");
			JasperDesign design = JRXmlLoader.load(jasperStream);
			JasperReport report = JasperCompileManager.compileReport(design);
			
			Map<String, Object> parameterMap = new HashMap<String, Object>();
			JRDataSource jRDataSource = new JRBeanCollectionDataSource(data);
			
	    	GeneralParameter params = paramrepository.findOne(1);
//			String subReportDir = params.getReport_dir();
//            parameterMap.put("SUBREPORT_DIR", subReportDir);
			parameterMap.put("datasource", jRDataSource);
			parameterMap.put("printNo", header.getPrintNo());
			parameterMap.put("printDate", header.getPrintDate());
			parameterMap.put("teamId", header.getTeamId());
			parameterMap.put("teamName", header.getTeamName());
//			parameterMap.put("employeeId", header.getEmployeeId());
//			parameterMap.put("employeeRegNo", header.getEmployeeRegNo());
//			parameterMap.put("employeeName", header.getEmployeeName());

			JasperPrint jasperPrint = JasperFillManager.fillReport(report, parameterMap, jRDataSource);
			response.setContentType("application/x-pdf");
			response.setHeader("Content-Disposition", "attachment: filename=\"" + header.getPrintNo() + ".pdf\"");
			response.setHeader("Content-Filename", header.getPrintNo() + ".pdf");
			
			final OutputStream outputStream = response.getOutputStream();
			JasperExportManager.exportReportToPdfStream(jasperPrint, outputStream);

			lkdTrxService.setPrinted(header, data);	
		} catch (JRException ex)
		{
			System.out.println(ex);
			log.info("Cannot read jrxml file");
		} catch (IOException ex) 
		{
			log.info("Cannot get outputstream from response");
		}
	}
    
/*    @PreAuthorize("hasRole('ROLE_DESKCALL_SPV')")
	@RequestMapping(value="/api/collection/lkd/print", method=RequestMethod.POST)
	public @ResponseBody void print(HttpServletResponse response) {
		List<LkdPrintDTO> data = lkdTrxService.listPrint();
		if (data == null) {
			log.info("No LKD to print");
			return;
		}
		LkdHeaderDTO header = lkdTrxService.headerPrint();

		try
		{
			InputStream jasperStream = this.getClass().getResourceAsStream("/report/LKD.jrxml");
			JasperDesign design = JRXmlLoader.load(jasperStream);
			JasperReport report = JasperCompileManager.compileReport(design);
			
			Map<String, Object> parameterMap = new HashMap<String, Object>();

			JRDataSource jRDataSource = new JRBeanCollectionDataSource(data);
			
			parameterMap.put("datasource", jRDataSource);
			parameterMap.put("printNo", header.getPrintNo());
			parameterMap.put("printDate", header.getPrintDate());
			parameterMap.put("teamId", header.getTeamId());
			parameterMap.put("teamName", header.getTeamName());
			
			parameterMap.put("employeeId", "");
			parameterMap.put("employeeRegNo", "");
			parameterMap.put("employeeName", "");

			JasperReport subReport = JasperCompileManager.compileReport(this.getClass().getResourceAsStream("/report/LKD_subreport.jrxml"));
            parameterMap.put("subReport", subReport);
            
			JasperPrint jasperPrint = JasperFillManager.fillReport(report, parameterMap, jRDataSource);
			response.setContentType("application/x-pdf");
			response.setHeader("Content-Disposition", "inline: filename=test.pdf");
			//response.setHeader("Content-Disposition", "inline: filename=" + header.getPrintNo() + ".pdf");
			
			final OutputStream outputStream = response.getOutputStream();
			JasperExportManager.exportReportToPdfStream(jasperPrint, outputStream);

			lkdTrxService.setPrinted(header, data);	
		} catch (JRException excp)
		{
			System.out.println(excp);
			log.info("Cannot read jrxml file");
			return;
		} catch (IOException excp) 
		{
			log.info("Cannot get outputstream from response");
			return;
		}

		log.info("LKD printed successfully");
		return;
	}
*/
	@RequestMapping(value="/api/collection/lkd/list", method=RequestMethod.POST)
	public @ResponseBody List<LkdPrintDTO> getPrintList() {
		long start = System.currentTimeMillis();
		List<LkdPrintDTO> results = lkdTrxService.listPrint();
		long timing = System.currentTimeMillis() - start;
		System.out.println(timing);
		
		return results;
	}
	
//	@RequestMapping(value="/api/collection/lkd/header", method=RequestMethod.POST)
//	public @ResponseBody LkdHeaderDTO getPrintHeader() {
//		long start = System.currentTimeMillis();
//		LkdHeaderDTO results = lkdTrxService.headerPrint();
//		long timing = System.currentTimeMillis() - start;
//		System.out.println(timing);
//		
//		return results;
//	}
	
    @PreAuthorize("hasRole('ROLE_DESKCALL_ADMIN')")
	@RequestMapping(value="/api/collection/lkd/entry/contract", method=RequestMethod.POST)
	public @ResponseBody LkdDataDTO getContract(@RequestBody LkdCriteria criteria) {
		long start = System.currentTimeMillis();
		LkdDataDTO results = lkdTrxService.contract(criteria);
		long timing = System.currentTimeMillis() - start;
		System.out.println(timing);
		
		return results;
	}
	
    @PreAuthorize("hasRole('ROLE_DESKCALL_ADMIN')")
	@RequestMapping(value="/api/collection/lkd/entry", method=RequestMethod.POST)
	public @ResponseBody ServiceResult entry(@RequestBody LKDTransaction data) {
		long start = System.currentTimeMillis();
		ServiceResult results = lkdTrxService.entry(data);
		long timing = System.currentTimeMillis() - start;
		System.out.println(timing);
		
		return results;
	}
	
    @PreAuthorize("hasRole('ROLE_DESKCALL_ADMIN')")
	@RequestMapping(value="/api/collection/lkdentry/list", method=RequestMethod.POST)
	public @ResponseBody List<LKDEntryStatus> getStatusList() {
		long start = System.currentTimeMillis();
		List<LKDEntryStatus> results = lkdTrxService.listStatus();
		long timing = System.currentTimeMillis() - start;
		System.out.println(timing);
		
		return results;
	}
	
}
