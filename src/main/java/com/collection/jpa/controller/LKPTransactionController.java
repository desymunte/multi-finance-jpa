package com.collection.jpa.controller;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.collection.jpa.CustomUserDetails;
import com.collection.jpa.dao.GeneralParameterRepository;
import com.collection.jpa.dto.LkpTeamPageDTO;
import com.collection.jpa.dto.LkpCriteria;
import com.collection.jpa.dto.LkpHeaderDTO;
import com.collection.jpa.dto.LkpPageDTO;
import com.collection.jpa.dto.LkpPrintDTO;
import com.collection.jpa.dto.LkpPrintPageDTO;
import com.collection.jpa.dto.LkpCyclePageDTO;
import com.collection.jpa.dto.PrintCriteria;
import com.collection.jpa.dto.ReclusteringDTO;
import com.collection.jpa.dto.SpecialDelegationDTO;
import com.collection.jpa.dto.StaffCriteria;
import com.collection.jpa.entity.GeneralParameter;
import com.collection.jpa.entity.LKPTransaction;
import com.collection.jpa.entity.PrintActivity;
import com.collection.jpa.service.LKPTransactionImpl;
import com.collection.jpa.util.FPrint;
import com.collection.jpa.util.Page;
import com.collection.jpa.util.ServiceResult;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.xml.JRXmlLoader;


@RestController
@EnableResourceServer
public class LKPTransactionController {
	
	private final Logger log = LoggerFactory.getLogger(this.getClass());

	Date currentDate = new Date();

	@Autowired
	private LKPTransactionImpl lkpTrxService;

	@Autowired
	private GeneralParameterRepository paramrepository;
	
	@Autowired
	private FPrint fprint;


    @PreAuthorize("hasRole('ROLE_SUPERADMIN') or hasRole('ROLE_COLLECTION_HEAD') or hasRole('ROLE_COLLECTION_SPV')")
	@RequestMapping(value="/api/collection/lkp/manualassignment/page", method=RequestMethod.POST)
	public @ResponseBody Page<LkpCyclePageDTO> getManualAssignmentPage(@PageableDefault(value=5, sort={"id"}) Pageable pageable) {
		long start = System.currentTimeMillis();
		Page<LkpCyclePageDTO> results = lkpTrxService.pageManualAssignment(pageable);
		long timing = System.currentTimeMillis() - start;
		System.out.println(timing);
		
		return results;
	}
	
    @PreAuthorize("hasRole('ROLE_SUPERADMIN') or hasRole('ROLE_COLLECTION_HEAD') or hasRole('ROLE_COLLECTION_SPV')")
	@RequestMapping(value="/api/collection/lkp/clustering/page", method=RequestMethod.POST)
	public @ResponseBody Page<LkpTeamPageDTO> getClusteringPage(@PageableDefault(value=5, sort={"id"}) Pageable pageable) {
		long start = System.currentTimeMillis();
		Page<LkpTeamPageDTO> results = lkpTrxService.pageClustering(pageable);
		long timing = System.currentTimeMillis() - start;
		System.out.println(timing);
		
		return results;
	}
	
    @PreAuthorize("hasRole('ROLE_COLLECTION_SPV')")
	@RequestMapping(value="/api/collection/lkp/assign", method=RequestMethod.POST)
	public @ResponseBody ServiceResult assign(@RequestBody LKPTransaction persisted) {
		long start = System.currentTimeMillis();
		ServiceResult results = lkpTrxService.assign(persisted);
		long timing = System.currentTimeMillis() - start;
		System.out.println(timing);
		
		return results;
	}

    @PreAuthorize("hasRole('ROLE_COLLECTION_SPV')")
	@RequestMapping(value="/api/collection/lkp/print", method=RequestMethod.POST)
	public @ResponseBody void print(HttpServletResponse response, @RequestBody StaffCriteria criteria) {
        CustomUserDetails user = (CustomUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if (!user.getJobCode().equals("COLLSPV")) {
			System.out.println("Not authorized user");
			log.info("Not authorized user");
        	return;
    	}

    	List<LkpPrintDTO> data = lkpTrxService.listPrint(criteria);
		if (data == null) {
			System.out.println("No Contract to print");
			log.info("No Contract to print");
			return;
		}

		LkpHeaderDTO header = lkpTrxService.headerPrint(criteria);
		if (header == null) {
			System.out.println("Cannot create header, invalid Collector");
			log.info("Cannot create header, invalid Collector");
			return;
		}

		String printNo = fprint.getNextPrintNo("LKP", String.format("%5s", header.getEmployeeRegNo()));
		header.setPrintNo(printNo);
		header.setPrintDate(currentDate);

		try
		{
			InputStream jasperStream = this.getClass().getResourceAsStream("/report/LKP.jrxml");
			JasperDesign design = JRXmlLoader.load(jasperStream);
			JasperReport report = JasperCompileManager.compileReport(design);
			
			Map<String, Object> parameterMap = new HashMap<String, Object>();
			JRDataSource jRDataSource = new JRBeanCollectionDataSource(data);
			
	    	GeneralParameter params = paramrepository.findOne(1);
			String subReportDir = params.getReport_dir();
            parameterMap.put("SUBREPORT_DIR", subReportDir);
			parameterMap.put("datasource", jRDataSource);
			parameterMap.put("printNo", header.getPrintNo());
			parameterMap.put("printDate", header.getPrintDate());
			parameterMap.put("teamId", header.getTeamId());
			parameterMap.put("teamName", header.getTeamName());
			parameterMap.put("employeeId", header.getEmployeeId());
			parameterMap.put("employeeRegNo", header.getEmployeeRegNo());
			parameterMap.put("employeeName", header.getEmployeeName());

			JasperPrint jasperPrint = JasperFillManager.fillReport(report, parameterMap, jRDataSource);
			response.setContentType("application/x-pdf");
			response.setHeader("Content-Disposition", "attachment: filename=\"" + header.getPrintNo() + ".pdf\"");
			response.setHeader("Content-Filename", header.getPrintNo() + ".pdf");
			
			final OutputStream outputStream = response.getOutputStream();
			JasperExportManager.exportReportToPdfStream(jasperPrint, outputStream);

			lkpTrxService.setPrinted(header, data);	
		} catch (JRException ex)
		{
			System.out.println(ex);
			log.info("Cannot read jrxml file");
		} catch (IOException ex) 
		{
			log.info("Cannot get outputstream from response");
		}
	}

    @PreAuthorize("hasRole('ROLE_SUPERADMIN') or hasRole('ROLE_COLLECTION_HEAD') or hasRole('ROLE_COLLECTION_SPV')")
	@RequestMapping(value="/api/collection/lkp/reprint", method=RequestMethod.POST)
	public @ResponseBody void reprint(HttpServletResponse response, @RequestBody PrintCriteria criteria) {
		try
		{
			List<LkpPrintDTO> data = lkpTrxService.listReprint(criteria);

			if (data == null) {
				System.out.println("Cannot reprint, no data");
				log.info("Cannot reprint, no data");
				return;
			}

			StaffCriteria staffcriteria = new StaffCriteria();
			staffcriteria.setStaffId(criteria.getStaffId());
			LkpHeaderDTO header = lkpTrxService.headerPrint(staffcriteria);
			if (header == null) {
				System.out.println("Cannot create header, invalid Collector");
				log.info("Cannot create header, invalid Collector");
				return;
			}

			header.setPrintNo(data.get(0).getPrintNo());
			header.setPrintDate(data.get(0).getPrintDate());

			InputStream jasperStream = this.getClass().getResourceAsStream("/report/LKP_reprint.jrxml");
			JasperDesign design = JRXmlLoader.load(jasperStream);
			JasperReport report = JasperCompileManager.compileReport(design);
			
			Map<String, Object> parameterMap = new HashMap<String, Object>();
			JRDataSource jRDataSource = new JRBeanCollectionDataSource(data);
			
			GeneralParameter params = paramrepository.findOne(1);
			String subReportDir = params.getReport_dir();
			parameterMap.put("SUBREPORT_DIR", subReportDir);
			parameterMap.put("datasource", jRDataSource);
			parameterMap.put("printNo", header.getPrintNo());
			parameterMap.put("printDate", header.getPrintDate());
			parameterMap.put("teamId", header.getTeamId());
			parameterMap.put("teamName", header.getTeamName());
			parameterMap.put("employeeId", header.getEmployeeId());
			parameterMap.put("employeeRegNo", header.getEmployeeRegNo());
			parameterMap.put("employeeName", header.getEmployeeName());
			
			JasperPrint jasperPrint = JasperFillManager.fillReport(report, parameterMap, jRDataSource);
			response.setContentType("application/x-pdf");
			response.setHeader("Content-Disposition", "attachment: filename=\"" + header.getPrintNo() + ".pdf\"");
			response.setHeader("Content-Filename", header.getPrintNo() + ".pdf");
			
			final OutputStream outputStream = response.getOutputStream();
			JasperExportManager.exportReportToPdfStream(jasperPrint, outputStream);

		} catch (JRException ex)
		{
			log.info("tidak bisa membaca file jrxml");
		} catch (IOException ex) 
		{
			log.info("Tidak bisa mengambil outputstream dari response");
		}
	}

    @PreAuthorize("hasRole('ROLE_SUPERADMIN') or hasRole('ROLE_COLLECTION_HEAD') or hasRole('ROLE_COLLECTION_SPV')")
	@RequestMapping(value="/api/collection/lkp/printed/page", method=RequestMethod.POST)
	public @ResponseBody Page<PrintActivity> getReprintPage(@RequestBody StaffCriteria criteria, @PageableDefault(value=5, sort={"id"}) Pageable pageable) {
		long start = System.currentTimeMillis();
		Page<PrintActivity> results = lkpTrxService.pageReprint(criteria, pageable);
		long timing = System.currentTimeMillis() - start;
		System.out.println(timing);
		
		return results;
	}
	
    @PreAuthorize("hasRole('ROLE_SUPERADMIN') or hasRole('ROLE_COLLECTION_HEAD') or hasRole('ROLE_COLLECTION_SPV')")
	@RequestMapping(value="/api/collection/lkp/detail/page", method=RequestMethod.POST)
	public @ResponseBody Page<LkpPrintPageDTO> getDetailPage(@RequestBody PrintCriteria criteria, @PageableDefault(value=5, sort={"id"}) Pageable pageable) {
		long start = System.currentTimeMillis();
		Page<LkpPrintPageDTO> results = lkpTrxService.pageReprintDetail(criteria, pageable);
		long timing = System.currentTimeMillis() - start;
		System.out.println(timing);
		
		return results;
	}
	
	
/*    @PreAuthorize("hasRole('ROLE_SUPERADMIN') or hasRole('ROLE_COLLECTION_HEAD') or hasRole('ROLE_COLLECTION_ADMIN')")
	@RequestMapping(value="/api/collection/lkp/entry/contract/page", method=RequestMethod.POST)
	public @ResponseBody Page<LkpDataDTO> getContractPage(@RequestBody LkpCriteria criteria, @PageableDefault(value=5, sort={"id"}) Pageable pageable) {
		long start = System.currentTimeMillis();
		Page<LkpDataDTO> results = lkpTrxService.pageEntry(criteria, pageable);
		long timing = System.currentTimeMillis() - start;
		System.out.println(timing);
		
		return results;
	}
*/	
    @PreAuthorize("hasRole('ROLE_SUPERADMIN') or hasRole('ROLE_COLLECTION_HEAD') or hasRole('ROLE_COLLECTION_ADMIN')")
	@RequestMapping(value="/api/collection/lkp/entry/contract", method=RequestMethod.POST)
	public @ResponseBody LkpPageDTO getContract(@RequestBody LkpCriteria criteria) {
		long start = System.currentTimeMillis();
		LkpPageDTO results = lkpTrxService.contract(criteria);
		long timing = System.currentTimeMillis() - start;
		System.out.println(timing);
		
		return results;
	}
	
    @PreAuthorize("hasRole('ROLE_COLLECTION_ADMIN')")
	@RequestMapping(value="/api/collection/lkp/entry", method=RequestMethod.POST)
	public @ResponseBody ServiceResult entry(@RequestBody LKPTransaction persisted) {
		long start = System.currentTimeMillis();
		ServiceResult results = lkpTrxService.entry(persisted);
		long timing = System.currentTimeMillis() - start;
		System.out.println(timing);
		
		return results;
	}
	
    @PreAuthorize("hasRole('ROLE_SUPERADMIN') or hasRole('ROLE_COLLECTION_HEAD') or hasRole('ROLE_COLLECTION_SPV')")
	@RequestMapping(value="/api/collection/lkp/reclustering/page", method=RequestMethod.POST)
	public @ResponseBody Page<LkpPrintPageDTO> getReclusteringPage(@RequestBody StaffCriteria criteria, @PageableDefault(value=5, sort={"id"}) Pageable pageable) {
		long start = System.currentTimeMillis();
		Page<LkpPrintPageDTO> results = lkpTrxService.pageReclustering(criteria, pageable);
		long timing = System.currentTimeMillis() - start;
		System.out.println(timing);
		
		return results;
	}

    @PreAuthorize("hasRole('ROLE_COLLECTION_SPV')")
	@RequestMapping(value="/api/collection/lkp/recluster", method=RequestMethod.POST)
	public @ResponseBody ServiceResult recluster(@RequestBody ReclusteringDTO data) {
		long start = System.currentTimeMillis();
		ServiceResult results = lkpTrxService.recluster(data);
		long timing = System.currentTimeMillis() - start;
		System.out.println(timing);
		
		return results;
	}
	
    @PreAuthorize("hasRole('ROLE_SUPERADMIN') or hasRole('ROLE_COLLECTION_HEAD') or hasRole('ROLE_COLLECTION_SPV')")
	@RequestMapping(value="/api/collection/lkp/specialdelegation/page", method=RequestMethod.POST)
	public @ResponseBody Page<LkpPageDTO> getSpecialDelegationPage(@PageableDefault(value=5, sort={"id"}) Pageable pageable) {
		long start = System.currentTimeMillis();
		Page<LkpPageDTO> results = lkpTrxService.pageSpecialDelegation(pageable);
		long timing = System.currentTimeMillis() - start;
		System.out.println(timing);
		
		return results;
	}

    @PreAuthorize("hasRole('ROLE_COLLECTION_SPV')")
	@RequestMapping(value="/api/collection/lkp/delegate", method=RequestMethod.POST)
	public @ResponseBody ServiceResult delegate(@RequestBody SpecialDelegationDTO data) {
		long start = System.currentTimeMillis();
		ServiceResult results = lkpTrxService.delegate(data);
		long timing = System.currentTimeMillis() - start;
		System.out.println(timing);
		
		return results;
	}

	@RequestMapping(value="/api/collection/lkp/list", method=RequestMethod.POST)
	public @ResponseBody List<LkpPrintDTO> getPrintList(@RequestBody StaffCriteria criteria) {
		long start = System.currentTimeMillis();
		List<LkpPrintDTO> results = lkpTrxService.listPrint(criteria);
		long timing = System.currentTimeMillis() - start;
		System.out.println(timing);
		
		return results;
	}

}
