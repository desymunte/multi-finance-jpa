package com.collection.jpa.controller;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.collection.jpa.CustomUserDetails;
import com.collection.jpa.dao.GeneralParameterRepository;
import com.collection.jpa.dto.SkpcCriteria;
import com.collection.jpa.dto.SkpcPageDTO;
import com.collection.jpa.dto.SkpcPrintDTO;
import com.collection.jpa.entity.GeneralParameter;
import com.collection.jpa.entity.SKPCTransaction;
import com.collection.jpa.service.SKPCTransactionImpl;
import com.collection.jpa.util.Page;
import com.collection.jpa.util.ServiceResult;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.xml.JRXmlLoader;


@RestController
@EnableResourceServer
public class SKPCTransactionController {
	
	private final Logger log = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private SKPCTransactionImpl skpcTrxService;
	
	@Autowired
	private GeneralParameterRepository paramrepository;
	
    @PreAuthorize("hasRole('ROLE_SUPERADMIN') or hasRole('ROLE_REMEDIAL_HEAD') or hasRole('ROLE_REMEDIAL_SPV')")
	@RequestMapping(value="/api/collection/skpc/page", method=RequestMethod.POST)
	public @ResponseBody Page<SkpcPageDTO> getSKPCPage(@RequestBody SkpcCriteria criteria, @PageableDefault(value=5, sort={"id"}) Pageable pageable) {
		long start = System.currentTimeMillis();
		Page<SkpcPageDTO> results = skpcTrxService.page(criteria, pageable);
		long timing = System.currentTimeMillis() - start;
		System.out.println(timing);
		
		return results;
	}
	
    @PreAuthorize("hasRole('ROLE_REMEDIAL_SPV')")
	@RequestMapping(value="/api/collection/skpc/insert", method=RequestMethod.POST)
	public @ResponseBody ServiceResult insert(@RequestBody SkpcCriteria criteria) {
		long start = System.currentTimeMillis();
		ServiceResult results = skpcTrxService.insert(criteria);
		long timing = System.currentTimeMillis() - start;
		System.out.println(timing);
		
		return results;
	}
	
    @PreAuthorize("hasRole('ROLE_REMEDIAL_SPV')")
	@RequestMapping(value="/api/collection/skpc/print", method=RequestMethod.POST)
	public @ResponseBody void print(HttpServletResponse response, @RequestBody SkpcCriteria criteria) {
        CustomUserDetails user = (CustomUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if (!user.getJobCode().equals("REMSPV")) {
			System.out.println("Not authorized user");
			log.info("Not authorized user");
        	return;
    	}

		List<SkpcPrintDTO> data = new ArrayList<SkpcPrintDTO>();
		SkpcPrintDTO dataSingle = skpcTrxService.getContent(criteria);
		data.add(dataSingle);

		try
		{
			InputStream jasperStream = this.getClass().getResourceAsStream("/report/SKPC.jrxml");
			JasperDesign design = JRXmlLoader.load(jasperStream);
			JasperReport report = JasperCompileManager.compileReport(design);
			
			Map<String, Object> parameterMap = new HashMap<String, Object>();
			JRDataSource jRDataSource = new JRBeanCollectionDataSource(data);

    		GeneralParameter params = paramrepository.findOne(1);
			parameterMap.put("company", params.getCompany_name());
			parameterMap.put("datasource", jRDataSource);
			
			JasperPrint jasperPrint = JasperFillManager.fillReport(report, parameterMap, jRDataSource);
			response.setContentType("application/x-pdf");
			response.setHeader("Content-Disposition", "attachment: filename=\"" + dataSingle.getSkpcPrintNo() + ".pdf\"");
			response.setHeader("Content-Filename", dataSingle.getSkpcPrintNo() + ".pdf");
			
			final OutputStream outputStream = response.getOutputStream();
			JasperExportManager.exportReportToPdfStream(jasperPrint, outputStream);

		} catch (JRException ex)
		{
			System.out.println(ex);
			log.info("tidak bisa membaca file jrxml");
		} catch (IOException ex) 
		{
			log.info("Tidak bisa mengambil outputstream dari response");
		}
	}
	
    @PreAuthorize("hasRole('ROLE_SUPERADMIN') or hasRole('ROLE_REMEDIAL_HEAD') or hasRole('ROLE_REMEDIAL_SPV')")
	@RequestMapping(value="/api/collection/skpc/entry/contract", method=RequestMethod.POST)
	public @ResponseBody SkpcPageDTO getSkpcContract(@RequestBody SkpcCriteria criteria) {
		long start = System.currentTimeMillis();
		SkpcPageDTO results = skpcTrxService.getSkpcContract(criteria);
		long timing = System.currentTimeMillis() - start;
		System.out.println(timing);
		
		return results;
	}
	
	@RequestMapping(value="/api/collection/skpc/enterresult", method=RequestMethod.POST)
	public @ResponseBody ServiceResult enterResult(@RequestBody SKPCTransaction data) {
		long start = System.currentTimeMillis();
		ServiceResult results = skpcTrxService.updateSKPCEntry(data);
		long timing = System.currentTimeMillis() - start;
		System.out.println(timing);
		
		return results;
	}
	
}
