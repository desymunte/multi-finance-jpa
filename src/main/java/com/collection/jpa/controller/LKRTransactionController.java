package com.collection.jpa.controller;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.collection.jpa.CustomUserDetails;
import com.collection.jpa.dao.GeneralParameterRepository;
import com.collection.jpa.dto.LkrTeamPageDTO;
import com.collection.jpa.dto.LkrCriteria;
import com.collection.jpa.dto.LkrHeaderDTO;
import com.collection.jpa.dto.LkrPageDTO;
import com.collection.jpa.dto.LkrPrintDTO;
import com.collection.jpa.dto.LkrPrintPageDTO;
import com.collection.jpa.dto.LkrCyclePageDTO;
import com.collection.jpa.dto.PrintCriteria;
import com.collection.jpa.dto.ReclusteringDTO;
import com.collection.jpa.dto.SpecialDelegationDTO;
import com.collection.jpa.dto.StaffCriteria;
import com.collection.jpa.entity.GeneralParameter;
import com.collection.jpa.entity.LKRTransaction;
import com.collection.jpa.entity.PrintActivity;
import com.collection.jpa.service.LKRTransactionImpl;
import com.collection.jpa.util.FPrint;
import com.collection.jpa.util.Page;
import com.collection.jpa.util.ServiceResult;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.xml.JRXmlLoader;


@RestController
@EnableResourceServer
public class LKRTransactionController {
	
	private final Logger log = LoggerFactory.getLogger(this.getClass());

	Date currentDate = new Date();

	@Autowired
	private LKRTransactionImpl lkrTrxService;

	@Autowired
	private GeneralParameterRepository paramrepository;
	
	@Autowired
	private FPrint fprint;


    @PreAuthorize("hasRole('ROLE_SUPERADMIN') or hasRole('ROLE_REMEDIAL_HEAD') or hasRole('ROLE_REMEDIAL_SPV')")
	@RequestMapping(value="/api/collection/lkr/manualassignment/page", method=RequestMethod.POST)
	public @ResponseBody Page<LkrCyclePageDTO> getManualAssignmentPage(@PageableDefault(value=5, sort={"id"}) Pageable pageable) {
		long start = System.currentTimeMillis();
		Page<LkrCyclePageDTO> results = lkrTrxService.pageManualAssignment(pageable);
		long timing = System.currentTimeMillis() - start;
		System.out.println(timing);
		
		return results;
	}
	
    @PreAuthorize("hasRole('ROLE_SUPERADMIN') or hasRole('ROLE_REMEDIAL_HEAD') or hasRole('ROLE_REMEDIAL_SPV')")
	@RequestMapping(value="/api/collection/lkr/clustering/page", method=RequestMethod.POST)
	public @ResponseBody Page<LkrTeamPageDTO> getClusteringPage(@PageableDefault(value=5, sort={"id"}) Pageable pageable) {
		long start = System.currentTimeMillis();
		Page<LkrTeamPageDTO> results = lkrTrxService.pageClustering(pageable);
		long timing = System.currentTimeMillis() - start;
		System.out.println(timing);
		
		return results;
	}
	
    @PreAuthorize("hasRole('ROLE_REMEDIAL_SPV')")
	@RequestMapping(value="/api/collection/lkr/assign", method=RequestMethod.POST)
	public @ResponseBody ServiceResult assign(@RequestBody LKRTransaction persisted) {
		long start = System.currentTimeMillis();
		ServiceResult results = lkrTrxService.assign(persisted);
		long timing = System.currentTimeMillis() - start;
		System.out.println(timing);
		
		return results;
	}

    @PreAuthorize("hasRole('ROLE_REMEDIAL_SPV')")
	@RequestMapping(value="/api/collection/lkr/print", method=RequestMethod.POST)
	public @ResponseBody void print(HttpServletResponse response, @RequestBody StaffCriteria criteria) {
        CustomUserDetails user = (CustomUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if (!user.getJobCode().equals("REMSPV")) {
			System.out.println("Not authorized user");
			log.info("Not authorized user");
        	return;
    	}

    	List<LkrPrintDTO> data = lkrTrxService.listPrint(criteria);
		if (data == null) {
			System.out.println("No Contract to print");
			log.info("No Contract to print");
			return;
		}

		LkrHeaderDTO header = lkrTrxService.headerPrint(criteria);
		if (header == null) {
			System.out.println("Cannot create header, invalid Collector");
			log.info("Cannot create header, invalid Collector");
			return;
		}

		String printNo = fprint.getNextPrintNo("LKR", String.format("%5s", header.getEmployeeRegNo()));
		header.setPrintNo(printNo);
		header.setPrintDate(currentDate);

		try
		{
			InputStream jasperStream = this.getClass().getResourceAsStream("/report/LKR.jrxml");
			JasperDesign design = JRXmlLoader.load(jasperStream);
			JasperReport report = JasperCompileManager.compileReport(design);
			
			Map<String, Object> parameterMap = new HashMap<String, Object>();
			JRDataSource jRDataSource = new JRBeanCollectionDataSource(data);
			
	    GeneralParameter params = paramrepository.findOne(1);
			String subReportDir = params.getReport_dir();
      parameterMap.put("SUBREPORT_DIR", subReportDir);
			parameterMap.put("datasource", jRDataSource);
			parameterMap.put("printNo", header.getPrintNo());
			parameterMap.put("printDate", header.getPrintDate());
			parameterMap.put("teamId", header.getTeamId());
			parameterMap.put("teamName", header.getTeamName());
			parameterMap.put("employeeId", header.getEmployeeId());
			parameterMap.put("employeeRegNo", header.getEmployeeRegNo());
			parameterMap.put("employeeName", header.getEmployeeName());

			JasperPrint jasperPrint = JasperFillManager.fillReport(report, parameterMap, jRDataSource);
			response.setContentType("application/x-pdf");
			response.setHeader("Content-Disposition", "attachment: filename=\"" + header.getPrintNo() + ".pdf\"");
			response.setHeader("Content-Filename", header.getPrintNo() + ".pdf");
			
			final OutputStream outputStream = response.getOutputStream();
			JasperExportManager.exportReportToPdfStream(jasperPrint, outputStream);

			lkrTrxService.setPrinted(header, data);	
		} catch (JRException ex)
		{
			System.out.println(ex);
			log.info("Cannot read jrxml file");
		} catch (IOException ex) 
		{
			log.info("Cannot get outputstream from response");
		}
	}

    @PreAuthorize("hasRole('ROLE_SUPERADMIN') or hasRole('ROLE_REMEDIAL_HEAD') or hasRole('ROLE_REMEDIAL_SPV')")
	@RequestMapping(value="/api/collection/lkr/reprint", method=RequestMethod.POST)
	public @ResponseBody void reprint(HttpServletResponse response, @RequestBody PrintCriteria criteria) {
		try
		{
			List<LkrPrintDTO> data = lkrTrxService.listReprint(criteria);

			if (data == null) {
				System.out.println("Cannot reprint, no data");
				log.info("Cannot reprint, no data");
				return;
			}

			StaffCriteria staffcriteria = new StaffCriteria();
			staffcriteria.setStaffId(criteria.getStaffId());
			LkrHeaderDTO header = lkrTrxService.headerPrint(staffcriteria);
			if (header == null) {
				System.out.println("Cannot create header, invalid Collector");
				log.info("Cannot create header, invalid Collector");
				return;
			}

			header.setPrintNo(data.get(0).getPrintNo());
			header.setPrintDate(data.get(0).getPrintDate());

			InputStream jasperStream = this.getClass().getResourceAsStream("/report/LKR_reprint.jrxml");
			JasperDesign design = JRXmlLoader.load(jasperStream);
			JasperReport report = JasperCompileManager.compileReport(design);
			
			Map<String, Object> parameterMap = new HashMap<String, Object>();
			JRDataSource jRDataSource = new JRBeanCollectionDataSource(data);
			
			GeneralParameter params = paramrepository.findOne(1);
			String subReportDir = params.getReport_dir();
			parameterMap.put("SUBREPORT_DIR", subReportDir);
			parameterMap.put("datasource", jRDataSource);
			parameterMap.put("printNo", header.getPrintNo());
			parameterMap.put("printDate", header.getPrintDate());
			parameterMap.put("teamId", header.getTeamId());
			parameterMap.put("teamName", header.getTeamName());
			parameterMap.put("employeeId", header.getEmployeeId());
			parameterMap.put("employeeRegNo", header.getEmployeeRegNo());
			parameterMap.put("employeeName", header.getEmployeeName());
			
			JasperPrint jasperPrint = JasperFillManager.fillReport(report, parameterMap, jRDataSource);
			response.setContentType("application/x-pdf");
			response.setHeader("Content-Disposition", "attachment: filename=\"" + header.getPrintNo() + ".pdf\"");
			response.setHeader("Content-Filename", header.getPrintNo() + ".pdf");
			
			final OutputStream outputStream = response.getOutputStream();
			JasperExportManager.exportReportToPdfStream(jasperPrint, outputStream);

		} catch (JRException ex)
		{
			log.info("tidak bisa membaca file jrxml");
		} catch (IOException ex) 
		{
			log.info("Tidak bisa mengambil outputstream dari response");
		}
	}

    @PreAuthorize("hasRole('ROLE_SUPERADMIN') or hasRole('ROLE_REMEDIAL_HEAD') or hasRole('ROLE_REMEDIAL_SPV')")
	@RequestMapping(value="/api/collection/lkr/printed/page", method=RequestMethod.POST)
	public @ResponseBody Page<PrintActivity> getReprintPage(@RequestBody StaffCriteria criteria, @PageableDefault(value=5, sort={"id"}) Pageable pageable) {
		long start = System.currentTimeMillis();
		Page<PrintActivity> results = lkrTrxService.pageReprint(criteria, pageable);
		long timing = System.currentTimeMillis() - start;
		System.out.println(timing);
		
		return results;
	}
	
    @PreAuthorize("hasRole('ROLE_SUPERADMIN') or hasRole('ROLE_REMEDIAL_HEAD') or hasRole('ROLE_REMEDIAL_SPV')")
	@RequestMapping(value="/api/collection/lkr/detail/page", method=RequestMethod.POST)
	public @ResponseBody Page<LkrPrintPageDTO> getDetailPage(@RequestBody PrintCriteria criteria, @PageableDefault(value=5, sort={"id"}) Pageable pageable) {
		long start = System.currentTimeMillis();
		Page<LkrPrintPageDTO> results = lkrTrxService.pageReprintDetail(criteria, pageable);
		long timing = System.currentTimeMillis() - start;
		System.out.println(timing);
		
		return results;
	}
	
	
/*    @PreAuthorize("hasRole('ROLE_SUPERADMIN') or hasRole('ROLE_REMEDIAL_HEAD') or hasRole('ROLE_REMEDIAL_ADMIN')")
	@RequestMapping(value="/api/collection/lkr/entry/contract/page", method=RequestMethod.POST)
	public @ResponseBody Page<LkrDataDTO> getContractPage(@RequestBody LkrCriteria criteria, @PageableDefault(value=5, sort={"id"}) Pageable pageable) {
		long start = System.currentTimeMillis();
		Page<LkrDataDTO> results = lkrTrxService.pageEntry(criteria, pageable);
		long timing = System.currentTimeMillis() - start;
		System.out.println(timing);
		
		return results;
	}
*/	
    @PreAuthorize("hasRole('ROLE_SUPERADMIN') or hasRole('ROLE_REMEDIAL_HEAD') or hasRole('ROLE_REMEDIAL_ADMIN')")
	@RequestMapping(value="/api/collection/lkr/entry/contract", method=RequestMethod.POST)
	public @ResponseBody LkrPageDTO getContract(@RequestBody LkrCriteria criteria) {
		long start = System.currentTimeMillis();
		LkrPageDTO results = lkrTrxService.contract(criteria);
		long timing = System.currentTimeMillis() - start;
		System.out.println(timing);
		
		return results;
	}
	
    @PreAuthorize("hasRole('ROLE_REMEDIAL_ADMIN')")
	@RequestMapping(value="/api/collection/lkr/entry", method=RequestMethod.POST)
	public @ResponseBody ServiceResult entry(@RequestBody LKRTransaction persisted) {
		long start = System.currentTimeMillis();
		ServiceResult results = lkrTrxService.entry(persisted);
		long timing = System.currentTimeMillis() - start;
		System.out.println(timing);
		
		return results;
	}
	
    @PreAuthorize("hasRole('ROLE_SUPERADMIN') or hasRole('ROLE_REMEDIAL_HEAD') or hasRole('ROLE_REMEDIAL_SPV')")
	@RequestMapping(value="/api/collection/lkr/reclustering/page", method=RequestMethod.POST)
	public @ResponseBody Page<LkrPrintPageDTO> getReclusteringPage(@RequestBody StaffCriteria criteria, @PageableDefault(value=5, sort={"id"}) Pageable pageable) {
		long start = System.currentTimeMillis();
		Page<LkrPrintPageDTO> results = lkrTrxService.pageReclustering(criteria, pageable);
		long timing = System.currentTimeMillis() - start;
		System.out.println(timing);
		
		return results;
	}

    @PreAuthorize("hasRole('ROLE_REMEDIAL_SPV')")
	@RequestMapping(value="/api/collection/lkr/recluster", method=RequestMethod.POST)
	public @ResponseBody ServiceResult recluster(@RequestBody ReclusteringDTO data) {
		long start = System.currentTimeMillis();
		ServiceResult results = lkrTrxService.recluster(data);
		long timing = System.currentTimeMillis() - start;
		System.out.println(timing);
		
		return results;
	}
	
    @PreAuthorize("hasRole('ROLE_SUPERADMIN') or hasRole('ROLE_REMEDIAL_HEAD') or hasRole('ROLE_REMEDIAL_SPV')")
	@RequestMapping(value="/api/collection/lkr/specialdelegation/page", method=RequestMethod.POST)
	public @ResponseBody Page<LkrPageDTO> getSpecialDelegationPage(@PageableDefault(value=5, sort={"id"}) Pageable pageable) {
		long start = System.currentTimeMillis();
		Page<LkrPageDTO> results = lkrTrxService.pageSpecialDelegation(pageable);
		long timing = System.currentTimeMillis() - start;
		System.out.println(timing);
		
		return results;
	}

    @PreAuthorize("hasRole('ROLE_REMEDIAL_SPV')")
	@RequestMapping(value="/api/collection/lkr/delegate", method=RequestMethod.POST)
	public @ResponseBody ServiceResult delegate(@RequestBody SpecialDelegationDTO data) {
		long start = System.currentTimeMillis();
		ServiceResult results = lkrTrxService.delegate(data);
		long timing = System.currentTimeMillis() - start;
		System.out.println(timing);
		
		return results;
	}

	@RequestMapping(value="/api/collection/lkr/list", method=RequestMethod.POST)
	public @ResponseBody List<LkrPrintDTO> getPrintList(@RequestBody StaffCriteria criteria) {
		long start = System.currentTimeMillis();
		List<LkrPrintDTO> results = lkrTrxService.listPrint(criteria);
		long timing = System.currentTimeMillis() - start;
		System.out.println(timing);
		
		return results;
	}

}
