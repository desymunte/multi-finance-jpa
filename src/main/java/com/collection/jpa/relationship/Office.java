package com.collection.jpa.relationship;
//package com.collection.data.relationship;
//
//import java.io.Serializable;
//import javax.persistence.Column;
//import javax.persistence.Entity;
//import javax.persistence.GeneratedValue;
//import javax.persistence.GenerationType;
//import javax.persistence.Id;
//import javax.persistence.Table;
//import javax.persistence.UniqueConstraint;
//
//@Entity
//@Table(name = "office", schema="param", 
//	uniqueConstraints = {@UniqueConstraint(columnNames={"code"})}
//)
//public class Office implements Serializable {
// 
//    private static final long serialVersionUID = 1L;
//        
//    @Id
//	@GeneratedValue(strategy = GenerationType.IDENTITY)
//    @Column(name="id", nullable = false)  
//    private Integer id;       
//    
////    @OneToMany(mappedBy="office", orphanRemoval = true)
////    private Set<Zone> zone;
//    
////    @OneToMany(fetch = FetchType.LAZY, cascade=CascadeType.ALL, mappedBy="office", orphanRemoval=true)
////    private Set<Zone> zones;
//    
//    @Column(name = "code", nullable = false)    
//    private String code;      
//
//    @Column(name="name")
//	private String name;
//	
//    @Column(name="address")
//	private String address;
//
//    @Column(name="address_area_code")
//	private String addressAreaCode;
//
//    @Column(name="phone")
//	private String phone;
//
//    @Column(name="type")
//	private String type;
//
//    @Column(name="pic_name")
//	private String picName;
//
//    @Column(name="pic_address")
//	private String picAddress;
//
//    @Column(name="pic_address_area_code")
//	private String picAddressAreaCode;
//
//    @Column(name="pic_phone")
//	private String picPhone;
//
//    @Column(name="pic_email")
//	private String picEmail;
//
//	public Integer getId() {
//		return id;
//	}
//
//	public void setId(Integer id) {
//		this.id = id;
//	}
//
//	public String getCode() {
//		return code;
//	}
//
//	public void setCode(String code) {
//		this.code = code;
//	}
//
//	public String getName() {
//		return name;
//	}
//
//	public void setName(String name) {
//		this.name = name;
//	}
//
//	public String getAddress() {
//		return address;
//	}
//
//	public void setAddress(String address) {
//		this.address = address;
//	}
//
//	public String getAddressAreaCode() {
//		return addressAreaCode;
//	}
//
//	public void setAddressAreaCode(String addressAreaCode) {
//		this.addressAreaCode = addressAreaCode;
//	}
//
//	public String getPhone() {
//		return phone;
//	}
//
//	public void setPhone(String phone) {
//		this.phone = phone;
//	}
//
//	public String getType() {
//		return type;
//	}
//
//	public void setType(String type) {
//		this.type = type;
//	}
//
//	public String getPicName() {
//		return picName;
//	}
//
//	public void setPicName(String picName) {
//		this.picName = picName;
//	}
//
//	public String getPicAddress() {
//		return picAddress;
//	}
//
//	public void setPicAddress(String picAddress) {
//		this.picAddress = picAddress;
//	}
//
//	public String getPicAddressAreaCode() {
//		return picAddressAreaCode;
//	}
//
//	public void setPicAddressAreaCode(String picAddressAreaCode) {
//		this.picAddressAreaCode = picAddressAreaCode;
//	}
//
//	public String getPicPhone() {
//		return picPhone;
//	}
//
//	public void setPicPhone(String picPhone) {
//		this.picPhone = picPhone;
//	}
//
//	public String getPicEmail() {
//		return picEmail;
//	}
//
//	public void setPicEmail(String picEmail) {
//		this.picEmail = picEmail;
//	}
//
//	public static long getSerialversionuid() {
//		return serialVersionUID;
//	}
//
//	public Office(Integer id, String code, String name, String address, String addressAreaCode, String phone,
//			String type, String picName, String picAddress, String picAddressAreaCode, String picPhone,
//			String picEmail) {
//		super();
//		this.id = id;
//		this.code = code;
//		this.name = name;
//		this.address = address;
//		this.addressAreaCode = addressAreaCode;
//		this.phone = phone;
//		this.type = type;
//		this.picName = picName;
//		this.picAddress = picAddress;
//		this.picAddressAreaCode = picAddressAreaCode;
//		this.picPhone = picPhone;
//		this.picEmail = picEmail;
//	}
//
//	public Office() {
//		super();
//		// TODO Auto-generated constructor stub
//	}
//
//	@Override
//	public String toString() {
//		return "Office [id=" + id + ", code=" + code + ", name=" + name + ", address=" + address + ", addressAreaCode="
//				+ addressAreaCode + ", phone=" + phone + ", type=" + type + ", picName=" + picName + ", picAddress="
//				+ picAddress + ", picAddressAreaCode=" + picAddressAreaCode + ", picPhone=" + picPhone + ", picEmail="
//				+ picEmail + "]";
//	}
//}
