package com.collection.jpa.relationship;
//package com.collection.data.relationship;
//import javax.persistence.Table;
//import javax.persistence.UniqueConstraint;
//
//import javax.persistence.Column;
//import javax.persistence.Entity;
//import javax.persistence.FetchType;
//import javax.persistence.Id;
//import javax.persistence.JoinColumn;
//import javax.persistence.ManyToOne;
//
//
//@Entity
//@Table(name = "employee", schema="param", 
//	uniqueConstraints = {@UniqueConstraint(columnNames={"reg_no"})}
//)
////@JsonIgnoreProperties(ignoreUnknown = true)
//public class Employee {
//        
//    @Id
//    @Column(name = "id", nullable = false)  
//    private Integer id;    
//    
//    @ManyToOne(targetEntity=com.collection.data.relationship.Office.class, fetch = FetchType.LAZY, optional=true) 
//    @JoinColumn(name="office_id", nullable=false, updatable=false)
//    private Office office;    
//	
//	@Column(name="reg_no", nullable=false)
//	private String regNo;
//
//    @Column(name="name")
//	private String name;
//
//	public Integer getId() {
//		return id;
//	}
//
//	public void setId(Integer id) {
//		this.id = id;
//	}
//
//	public Office getOffice() {
//		return office;
//	}
//
//	public void setOffice(Office office) {
//		this.office = office;
//	}
//
//	public String getRegNo() {
//		return regNo;
//	}
//
//	public void setRegNo(String regNo) {
//		this.regNo = regNo;
//	}
//
//	public String getName() {
//		return name;
//	}
//
//	public void setName(String name) {
//		this.name = name;
//	}
//}
