@echo off

set PG_HOME="C:\Program Files\PostgreSQL\pg96\bin"
set MF_HOME=D:\Projects\Multifinance\Design\Batch
set DB_NAME=MFCollection
set DB_USER=postgres
set DB_PASS=postgres

set /p rundate=Enter running date (YYYYMMDD):
%PG_HOME%\psql -U%DB_USER% -d%DB_NAME% -c"SELECT spNightrun('%rundate%')"
