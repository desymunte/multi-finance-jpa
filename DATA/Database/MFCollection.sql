--
-- PostgreSQL database dump
--

-- Dumped from database version 10.3
-- Dumped by pg_dump version 10.3

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: auth; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA auth;


ALTER SCHEMA auth OWNER TO postgres;

--
-- Name: hist; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA hist;


ALTER SCHEMA hist OWNER TO postgres;

--
-- Name: param; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA param;


ALTER SCHEMA param OWNER TO postgres;

--
-- Name: staging; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA staging;


ALTER SCHEMA staging OWNER TO postgres;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


--
-- Name: fnget_cyclecode_bydate(character varying, date, date); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.fnget_cyclecode_bydate(p_type character varying, p_rundate date, p_duedate date) RETURNS character varying
    LANGUAGE plpgsql
    AS $$
DECLARE
  strCycleCode param.cycle.code%TYPE;
BEGIN
  SELECT code INTO strCycleCode
    FROM param.cycle
   WHERE type = p_type
     AND (p_rundate - p_duedate) BETWEEN ovdstart AND ovdend;
  
  RETURN strCycleCode;
END; $$;


ALTER FUNCTION public.fnget_cyclecode_bydate(p_type character varying, p_rundate date, p_duedate date) OWNER TO postgres;

--
-- Name: fnget_cyclecode_byteam(integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.fnget_cyclecode_byteam(p_teamid integer) RETURNS character varying
    LANGUAGE plpgsql
    AS $$
DECLARE
  strCycleCode param.cycle.code%TYPE;
BEGIN
  SELECT c.code INTO strCycleCode
    FROM param.coll t
    LEFT JOIN param.cycle c ON c.id = t.cycle_id
   WHERE t.id = p_teamid;
  
  RETURN strCycleCode;
END; $$;


ALTER FUNCTION public.fnget_cyclecode_byteam(p_teamid integer) OWNER TO postgres;

--
-- Name: fnget_cycleid_byovd(integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.fnget_cycleid_byovd(p_overdue integer) RETURNS integer
    LANGUAGE plpgsql
    AS $$
DECLARE
  intCycleId param.cycle.id%TYPE;
BEGIN
  SELECT id INTO intCycleId
    FROM param.cycle
   WHERE p_overdue BETWEEN ovdstart AND ovdend;
  
  RETURN intCycleId;
END; $$;


ALTER FUNCTION public.fnget_cycleid_byovd(p_overdue integer) OWNER TO postgres;

--
-- Name: fnget_nextseq(character varying); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.fnget_nextseq(p_print_no character varying) RETURNS character varying
    LANGUAGE plpgsql
    AS $$
DECLARE
  lastSeq integer;

BEGIN
  SELECT MAX(RIGHT(printno, 2)) INTO lastSeq FROM print
   WHERE printno LIKE p_print_no || '%';

  IF lastSeq IS NULL THEN
    lastSeq := -1;
  END IF;

  RETURN (p_print_no || LPAD((lastSeq + 1)::text, 2, '0'));
END; $$;


ALTER FUNCTION public.fnget_nextseq(p_print_no character varying) OWNER TO postgres;

--
-- Name: fnget_numassignment_collector(integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.fnget_numassignment_collector(p_employee_id integer) RETURNS integer
    LANGUAGE plpgsql
    AS $$
DECLARE
  numAssignment integer;

BEGIN
  SELECT COUNT(1) INTO numAssignment FROM lkptrx
   WHERE COALESCE(asgnd_to, 0) = p_employee_id
     AND entry_cd IS NULL;

  RETURN numAssignment;
END; $$;


ALTER FUNCTION public.fnget_numassignment_collector(p_employee_id integer) OWNER TO postgres;

--
-- Name: fnget_numassignment_profcollector(integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.fnget_numassignment_profcollector(p_profcoll_id integer) RETURNS integer
    LANGUAGE plpgsql
    AS $$
DECLARE
  numAssignment integer;

BEGIN
  SELECT COUNT(1) INTO numAssignment FROM skpctrx
   WHERE COALESCE(profcoll_id, 0) = p_profcoll_id
     AND status IN (1, 3);

  RETURN numAssignment;
END; $$;


ALTER FUNCTION public.fnget_numassignment_profcollector(p_profcoll_id integer) OWNER TO postgres;

--
-- Name: fnget_numcollectionteam(integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.fnget_numcollectionteam(p_cycle_id integer) RETURNS integer
    LANGUAGE plpgsql
    AS $$
DECLARE
  numTeam integer;

BEGIN
  SELECT COUNT(1) INTO numTeam FROM param.coll
   WHERE cycle_id = p_cycle_id;

  RETURN numTeam;
END; $$;


ALTER FUNCTION public.fnget_numcollectionteam(p_cycle_id integer) OWNER TO postgres;

--
-- Name: fnget_numdeskcallteam(integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.fnget_numdeskcallteam(p_cycle_id integer) RETURNS integer
    LANGUAGE plpgsql
    AS $$
DECLARE
  numTeam integer;

BEGIN
  SELECT COUNT(1) INTO numTeam FROM param.dcall
   WHERE cycle_id = p_cycle_id;

  RETURN numTeam;
END; $$;


ALTER FUNCTION public.fnget_numdeskcallteam(p_cycle_id integer) OWNER TO postgres;

--
-- Name: fnget_numremedialteam(integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.fnget_numremedialteam(p_cycle_id integer) RETURNS integer
    LANGUAGE plpgsql
    AS $$
DECLARE
  numTeam integer;

BEGIN
  SELECT COUNT(1) INTO numTeam FROM param.rmdl
   WHERE cycle_id = p_cycle_id;

  RETURN numTeam;
END; $$;


ALTER FUNCTION public.fnget_numremedialteam(p_cycle_id integer) OWNER TO postgres;

--
-- Name: fnhas_maxloadreached(integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.fnhas_maxloadreached(p_employee_id integer) RETURNS boolean
    LANGUAGE plpgsql
    AS $$
DECLARE
  numLoad integer;

BEGIN
  SELECT COALESCE(dload, 0) INTO numLoad FROM param.coll_empl
   WHERE empl_id = p_employee_id;

  RETURN (numLoad <= fnGet_NumAssignment_Collector(p_employee_id));
END; $$;


ALTER FUNCTION public.fnhas_maxloadreached(p_employee_id integer) OWNER TO postgres;

--
-- Name: fnhas_pcmaxloadreached(integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.fnhas_pcmaxloadreached(p_profcoll_id integer) RETURNS boolean
    LANGUAGE plpgsql
    AS $$
DECLARE
  numLoad integer;

BEGIN
  SELECT COALESCE(skpc_max_outstanding_load, 0) INTO numLoad FROM param.genparam;

  RETURN (numLoad <= fnGet_NumAssignment_ProfCollector(p_profcoll_id));
END; $$;


ALTER FUNCTION public.fnhas_pcmaxloadreached(p_profcoll_id integer) OWNER TO postgres;

--
-- Name: fnis_assignedtopc(character varying); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.fnis_assignedtopc(p_contract_no character varying) RETURNS boolean
    LANGUAGE plpgsql
    AS $$
BEGIN
  RETURN EXISTS(SELECT 1 FROM skpctrx WHERE cntrno = p_contract_no);
END; $$;


ALTER FUNCTION public.fnis_assignedtopc(p_contract_no character varying) OWNER TO postgres;

--
-- Name: fnreplacekeyword(text, character varying, character varying); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.fnreplacekeyword(p_strin text, p_keyword character varying, p_strsub character varying) RETURNS text
    LANGUAGE plpgsql
    AS $$
DECLARE
  strOut text;
BEGIN
  CASE
    WHEN p_keyword = '<rundate>' THEN strOut := REPLACE(p_strIn, p_keyword, 'TO_DATE(''' || p_strSub || ''', ''YYYYMMDD'')');
    ELSE strOut := REPLACE(p_strIn, p_keyword, p_strSub);
  END CASE;

  RETURN strOut;
END; $$;


ALTER FUNCTION public.fnreplacekeyword(p_strin text, p_keyword character varying, p_strsub character varying) OWNER TO postgres;

--
-- Name: fnretrieve_collector(character varying, character varying, character varying, character varying, character varying); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.fnretrieve_collector(p_office_code character varying, p_cycle_code character varying, p_businessunit_code character varying, p_zipcode character varying, p_subzipcode character varying) RETURNS TABLE(team_id integer, spv_id integer, empl_id integer, load integer)
    LANGUAGE plpgsql
    AS $$
BEGIN
  RETURN QUERY SELECT
    t.id,
    t.spv_id,
    m.empl_id,
    m.dload
  FROM param.coll t
  LEFT OUTER JOIN param.coll_empl m ON m.coll_id = t.id
  LEFT OUTER JOIN param.coll_zip a ON a.coll_empl_id = m.id
  LEFT OUTER JOIN param.coll_bizu cbu ON cbu.coll_id = t.id
  LEFT JOIN param.offi o ON t.off_id = o.id
  LEFT JOIN param.cycle c ON t.cycle_id = c.id
  LEFT JOIN param.zip z ON z.id = a.zip_id
  LEFT JOIN param.bizu bu ON bu.id = cbu.bizu_id
  WHERE o.code = p_office_code
    AND c.code = p_cycle_code
    AND COALESCE(z.zipcd, '') = COALESCE(p_zipcode, '')
    AND COALESCE(z.subzipcd, '') = COALESCE(p_subzipcode, '')
    AND COALESCE(bu.code, p_businessunit_code) = p_businessunit_code;
END; $$;


ALTER FUNCTION public.fnretrieve_collector(p_office_code character varying, p_cycle_code character varying, p_businessunit_code character varying, p_zipcode character varying, p_subzipcode character varying) OWNER TO postgres;

--
-- Name: fnretrieve_deskcall(character varying); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.fnretrieve_deskcall(p_office_code character varying) RETURNS TABLE(team_id integer, spv_id integer)
    LANGUAGE plpgsql
    AS $$
BEGIN
  RETURN QUERY SELECT
    t.id,
    t.spv_id
  FROM param.dcall t
  LEFT JOIN param.offi o ON t.off_id = o.id
  WHERE o.code = p_office_code;
END; $$;


ALTER FUNCTION public.fnretrieve_deskcall(p_office_code character varying) OWNER TO postgres;

--
-- Name: fnretrieve_deskcall(character varying, character varying); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.fnretrieve_deskcall(p_office_code character varying, p_cycle_code character varying) RETURNS TABLE(team_id integer, spv_id integer)
    LANGUAGE plpgsql
    AS $$
BEGIN
  RETURN QUERY SELECT
    t.id,
    t.spv_id
  FROM param.dcall t
  LEFT JOIN param.offi o ON t.off_id = o.id
  LEFT JOIN param.cycle c ON t.cycle_id = c.id
  WHERE o.code = p_office_code
    AND c.code = p_cycle_code;
END; $$;


ALTER FUNCTION public.fnretrieve_deskcall(p_office_code character varying, p_cycle_code character varying) OWNER TO postgres;

--
-- Name: spcreatebucket(character varying); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.spcreatebucket(p_rundate character varying, OUT retval integer, OUT retstr text) RETURNS record
    LANGUAGE plpgsql
    AS $$
DECLARE
  dateRun date := to_date(p_rundate, 'YYYYMMDD');
  intOverdueStart integer;

  firstCollected boolean;
  isSimilar boolean;

  dateLastDue date;
  intInstallmentNo smallint;
  intOverdue integer;

  rec_armaster RECORD;
  cur_armaster CURSOR FOR SELECT * FROM staging.vw_ARMaster;

  rec_ardetail RECORD;
  cur_ardetail CURSOR (intId integer) FOR 
    SELECT * FROM staging.vw_ARDetail
     WHERE ID = intId
       AND Due_Date + intOverdueStart <= dateRun
     ORDER BY Installment_No DESC;

BEGIN
  SELECT MIN(ovdstart) INTO intOverdueStart FROM param.Cycle;

  retval := 0;
  retstr := 'spCreateBucket (rundate: ' || p_rundate || ')';

  OPEN cur_armaster;

  LOOP
    FETCH cur_armaster INTO rec_armaster;
    EXIT WHEN NOT FOUND;

    OPEN cur_ardetail(rec_armaster.id);
 
    isSimilar := false;
    firstCollected := true;
    dateLastDue := dateRun;
    LOOP
      FETCH cur_ardetail INTO rec_ardetail;
      EXIT WHEN NOT FOUND OR isSimilar;

      IF COALESCE(rec_ardetail.Principal_Amnt, 0) > COALESCE(rec_ardetail.Principal_Amnt_Paid, 0) OR
         COALESCE(rec_ardetail.Interest_Amnt, 0) > COALESCE(rec_ardetail.Interest_Amnt_Paid, 0) OR
         COALESCE(rec_ardetail.penalty_amnt, 0) > COALESCE(rec_ardetail.penalty_amnt_paid, 0) THEN  -- 20180124: Biaya penalti dimasukkan

        IF firstCollected THEN
          INSERT INTO Bucket (
            cntrno,
            cntrdate,
            officd,
            topinstl,
            outsdpriamnt,
            outsdintamnt,
            minstl,
            custname,
            custaddr,
            custbilladdr,
            zipcd,
            subzipcd,
            phone1,
            phone2,
            bizu,
            srcid,
            leastduedate,
            leastinstlno,
            lastduedate,
            lastinstlno,
            ovd
          )
          VALUES (
            rec_armaster.Contract_No,
            rec_armaster.Contract_Date,
            rec_armaster.Office_Code,
            CAST(rec_armaster.TOP_Installment AS INTEGER),
            COALESCE(rec_armaster.Outstanding_Principal_Amnt, 0),
            COALESCE(rec_armaster.Outstanding_Interest_Amnt, 0),
            COALESCE(rec_armaster.Installment_Monthly, 0),
            rec_armaster.Cust_Name,
            rec_armaster.Cust_Address,
            rec_armaster.Cust_BillingAddress,
            rec_armaster.Zipcode,
            rec_armaster.Subzipcode,
            rec_armaster.Phone1,
            rec_armaster.Phone2,
            rec_armaster.BusinessUnit,
            rec_armaster.ID,
            rec_ardetail.Due_Date,
            rec_ardetail.Installment_No,
            rec_ardetail.Due_Date,
            rec_ardetail.Installment_No,
            dateRun - rec_ardetail.Due_Date
          );

          INSERT INTO Unit (
            cntrno,
            brand,
            model,
            color,
            frameno,
            engineno,
            polno,
            stnk,
            addinfo
          )
          VALUES (
            rec_armaster.Contract_No,
            rec_armaster.brand,
            rec_armaster.model,
            rec_armaster.color,
            rec_armaster.nomor_rangka,
            rec_armaster.nomor_mesin,
            rec_armaster.nomor_polisi,
            rec_armaster.stnk_an,
            rec_armaster.additional_info
          );
          
          firstCollected := false;
        END IF;

        INSERT INTO BucketDetail (
          instlno,
          priamnt,
          intamnt,
          duedate,
          ovd,
          priamntpaid,
          intamntpaid,
          paydate,
          pnltamnt,
          pnltamntpaid,
          pnltpaydate,
          collfee,
          collfeepaid,
          collfeepaydate,
          cntrno
        )
        VALUES (
          rec_ardetail.Installment_No,
          COALESCE(rec_ardetail.Principal_Amnt, 0),
          COALESCE(rec_ardetail.Interest_Amnt, 0),
          rec_ardetail.Due_Date,
          dateRun - rec_ardetail.Due_Date,
          COALESCE(rec_ardetail.Principal_Amnt_Paid, 0),
          COALESCE(rec_ardetail.Interest_Amnt_Paid, 0),
          rec_ardetail.Payment_Date,
          COALESCE(rec_ardetail.Penalty_Amnt, 0),
          COALESCE(rec_ardetail.Penalty_Amnt_Paid, 0),
          rec_ardetail.Penalty_Payment_Date,
          COALESCE(rec_ardetail.CollectionFee, 0),
          COALESCE(rec_ardetail.CollectionFee_Paid, 0),
          rec_ardetail.CollectionFee_Payment_Date,
          rec_armaster.Contract_No
        );
        
        IF dateLastDue > rec_ardetail.Due_Date THEN
          dateLastDue := rec_ardetail.Due_Date;
          intInstallmentNo := rec_ardetail.Installment_No;
          intOverdue := dateRun - rec_ardetail.Due_Date;
        END IF;

      ELSE
        isSimilar := true;
      END IF;
    END LOOP;
    CLOSE cur_ardetail;

    IF NOT firstCollected THEN
      UPDATE Bucket
         SET leastduedate = dateLastDue,
             leastinstlno = intInstallmentNo,
             ovd = intOverdue
       WHERE cntrno = rec_armaster.Contract_No;
    END IF;
  END LOOP;
  CLOSE cur_armaster;
 
END; $$;


ALTER FUNCTION public.spcreatebucket(p_rundate character varying, OUT retval integer, OUT retstr text) OWNER TO postgres;

--
-- Name: spcreatecollectionbucket(character varying); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.spcreatecollectionbucket(p_rundate character varying, OUT retval integer, OUT retstr text) RETURNS record
    LANGUAGE plpgsql
    AS $$
DECLARE
  intOverdueStart smallint;
  intOverdueEnd smallint;
  sql text;

BEGIN
  retval := 0;
  retstr := 'spCreateCollectionBucket (rundate: ' || p_rundate || ')';

  SELECT MIN(ovdstart), MAX(ovdend) INTO intOverdueStart, intOverdueEnd FROM param.Cycle WHERE type = 'C';

  IF EXISTS(SELECT 1 FROM pg_matviews WHERE matviewname = 'vw_coll_bucket') THEN
    DROP MATERIALIZED VIEW vw_coll_bucket;
  END IF;

  sql := 'CREATE MATERIALIZED VIEW vw_coll_bucket AS ' ||
         'SELECT b.*, lkp.id, rundate, team_id, spv_id, asgnd_to, print_id, entry_cd entry_status, cmnt_id, cmntdate comment_date ' ||
         'FROM Bucket b ' ||
         'LEFT OUTER JOIN lkptrx lkp ON lkp.cntrno = b.cntrno ';
  sql := sql || 'WHERE NOT fnIs_AssignedToPC(b.cntrno) ';
  sql := sql || FORMAT('AND ovd BETWEEN %s AND %s', intOverdueStart, intOverdueEnd);
  --sql := sql || FORMAT('AND (TO_DATE(''%s'', ''YYYYMMDD'') - leastduedate) BETWEEN %s AND %s', p_rundate, intOverdueStart, intOverdueEnd);

  EXECUTE sql;
END; $$;


ALTER FUNCTION public.spcreatecollectionbucket(p_rundate character varying, OUT retval integer, OUT retstr text) OWNER TO postgres;

--
-- Name: spcreatedeskcallbucket(character varying); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.spcreatedeskcallbucket(p_rundate character varying, OUT retval integer, OUT retstr text) RETURNS record
    LANGUAGE plpgsql
    AS $$
DECLARE
  intOverdueStart smallint;
  intOverdueEnd smallint;
  sql text;

BEGIN
  retval := 0;
  retstr := 'spCreateDeskcallBucket (rundate: ' || p_rundate || ')';

  SELECT MIN(ovdstart), MAX(ovdend) INTO intOverdueStart, intOverdueEnd FROM param.Cycle WHERE type = 'D';

  IF EXISTS(SELECT 1 FROM pg_matviews WHERE matviewname = 'vw_deskcall_bucket') THEN
    DROP MATERIALIZED VIEW vw_deskcall_bucket;
  END IF;

  sql := 'CREATE MATERIALIZED VIEW vw_deskcall_bucket AS ' ||
         'SELECT b.*, rundate, team_id, spv_id, print_id, entry_cd entry_status ' ||
         'FROM Bucket b ' ||
         'LEFT OUTER JOIN lkdtrx lkd ON lkd.cntrno = b.cntrno ';
  sql := sql || 'WHERE NOT fnIs_AssignedToPC(b.cntrno) ';
  sql := sql || FORMAT('AND (TO_DATE(''%s'', ''YYYYMMDD'') - leastduedate) BETWEEN %s AND %s', p_rundate, intOverdueStart, intOverdueEnd);
  EXECUTE sql;
END; $$;


ALTER FUNCTION public.spcreatedeskcallbucket(p_rundate character varying, OUT retval integer, OUT retstr text) OWNER TO postgres;

--
-- Name: spcreatelkddaily(character varying); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.spcreatelkddaily(p_rundate character varying, OUT retval integer, OUT retstr text) RETURNS record
    LANGUAGE plpgsql
    AS $$
DECLARE
  dateRun date := to_date(p_rundate, 'YYYYMMDD');
  strCycleCode character varying;
  intOverdueStart smallint;
  intOverdueEnd smallint;
  intTeamId integer;
  intSpvId integer;

  rec_bucket RECORD;
  cur_bucket REFCURSOR;

BEGIN
  retval := 0;
  retstr := 'spCreateLKDDaily (rundate: ' || p_rundate || ')';

  SELECT MIN(ovdstart), MAX(ovdend) INTO intOverdueStart, intOverdueEnd FROM param.Cycle WHERE type = 'D';

  PERFORM spCreateDeskcallBucket(p_rundate);

  OPEN cur_bucket FOR
    SELECT * FROM vw_Deskcall_Bucket
     WHERE rundate IS NULL;

  LOOP
    FETCH cur_bucket INTO rec_bucket;
    EXIT WHEN NOT FOUND;

    strCycleCode := fnGet_CycleCode_ByDate('D', dateRun, rec_bucket.leastduedate);
    IF strCycleCode IS NOT NULL THEN

      SELECT team_id, spv_id
        FROM fnRetrieve_Deskcall(rec_bucket.officd, strCycleCode)
        INTO intTeamId, intSpvId;

      PERFORM spLKD_Transaction_Insert(rec_bucket.cntrno, dateRun::timestamp, intTeamId, intSpvId);

    END IF;
    
  END LOOP;
  CLOSE cur_bucket;
END; $$;


ALTER FUNCTION public.spcreatelkddaily(p_rundate character varying, OUT retval integer, OUT retstr text) OWNER TO postgres;

--
-- Name: spcreatelkpbysqlstmt(character varying, text); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.spcreatelkpbysqlstmt(p_rundate character varying, p_sqlstmt text, OUT retval integer, OUT retstr text) RETURNS record
    LANGUAGE plpgsql
    AS $$
DECLARE
  dateRun date := TO_DATE(p_rundate, 'YYYYMMDD');
  strCycleCode character varying;
  intTeamId integer;
  intEmployeeId integer;
  intAssignedTo integer;
  intSpvId integer;
  numLoad integer;
  
  rec_bucket RECORD;
  cur_bucket REFCURSOR;

BEGIN
  retval := 0;
  retstr := 'spCreateLKPbySQLStmt (SQL: ' || p_rundate || ' - ' || p_sqlstmt || ')';

  OPEN cur_bucket FOR EXECUTE p_sqlstmt;
  LOOP
    FETCH cur_bucket INTO rec_bucket;
    EXIT WHEN NOT FOUND;

    strCycleCode := fnGet_CycleCode_ByDate('C', dateRun, rec_bucket.leastduedate);
    IF strCycleCode IS NOT NULL THEN

      --IF strCycleCode <> fnGet_CycleCode_ByTeam(rec_bucket.team_id) THEN
        -- move to next cycle
        PERFORM spLKP_Transaction_Delete(rec_bucket.cntrno);
      --END IF;

      SELECT team_id, spv_id, empl_id, load
        FROM fnRetrieve_Collector(rec_bucket.officd, strCycleCode, rec_bucket.bizu, rec_bucket.zipcd, rec_bucket.subzipcd)
        INTO intTeamId, intSpvId, intEmployeeId, numLoad;

      intAssignedTo := intEmployeeId;
      IF intEmployeeId IS NOT NULL THEN
        IF numLoad <= fnGet_NumAssignment_Collector(intEmployeeId) THEN
          intAssignedTo := NULL;
        END IF;
      END IF;

      --PERFORM spLKP_Transaction_Delete(rec_bucket.cntrno);
      PERFORM spLKP_Transaction_Insert(rec_bucket.cntrno, dateRun::timestamp, intTeamId, intSpvId, intAssignedTo, intEmployeeId);
    END IF;

  END LOOP;
  CLOSE cur_bucket;

END; $$;


ALTER FUNCTION public.spcreatelkpbysqlstmt(p_rundate character varying, p_sqlstmt text, OUT retval integer, OUT retstr text) OWNER TO postgres;

--
-- Name: spcreatelkpdaily(character varying); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.spcreatelkpdaily(p_rundate character varying, OUT retval integer, OUT retstr text) RETURNS record
    LANGUAGE plpgsql
    AS $$
DECLARE
  intOverdueStart smallint;
  intOverdueEnd smallint;
  txtSQL text;
  txtSubSQL text;
  txtDefaultSQL text;

  rec_priority RECORD;
  cur_priority REFCURSOR;

BEGIN
  retval := 0;
  retstr := 'spCreateLKPDaily (rundate: ' || p_rundate || ')';

  SELECT MIN(ovdstart), MAX(ovdend) INTO intOverdueStart, intOverdueEnd FROM param.Cycle WHERE type = 'C';
  txtSubSQL := FORMAT('AND ovd BETWEEN %s AND %s ', intOverdueStart, intOverdueEnd);
  --txtSubSQL := FORMAT('AND (TO_DATE(''%s'', ''YYYYMMDD'') - leastduedate) BETWEEN %s AND %s ', p_rundate, intOverdueStart, intOverdueEnd);

  txtDefaultSQL := 'SELECT * FROM vw_coll_bucket ';
  txtDefaultSQL := txtDefaultSQL || FORMAT('WHERE COALESCE(rundate, TO_DATE(''20000101'', ''YYYYMMDD'')) < TO_DATE(''%s'', ''YYYYMMDD'') ', p_rundate);
  txtDefaultSQL := txtDefaultSQL || 'AND NOT fnIs_AssignedToPC(cntrno) ';
  txtDefaultSQL := txtDefaultSQL || txtSubSQL;
  txtDefaultSQL := txtDefaultSQL || 'AND <SQLText> ';
  --txtDefaultSQL := txtDefaultSQL || 'ORDER BY officd';

  OPEN cur_priority FOR
    SELECT script FROM param.LKPPrio
    ORDER BY type, seqno;

  LOOP
    FETCH cur_priority INTO rec_priority;
    EXIT WHEN NOT FOUND;

    PERFORM spCreateCollectionBucket(p_rundate);
    
    txtSubSQL := rec_priority.script;
    txtSubSQL := fnReplaceKeyword(txtSubSQL, '<rundate>', p_rundate);
    txtSQL := fnReplaceKeyword(txtDefaultSQL, '<SQLText>', txtSubSQL);
    PERFORM spCreateLKPbySQLStmt(p_rundate, txtSQL);
  END LOOP;
  CLOSE cur_priority;
END; $$;


ALTER FUNCTION public.spcreatelkpdaily(p_rundate character varying, OUT retval integer, OUT retstr text) OWNER TO postgres;

--
-- Name: spcreatelkrbysqlstmt(character varying, text); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.spcreatelkrbysqlstmt(p_rundate character varying, p_sqlstmt text, OUT retval integer, OUT retstr text) RETURNS record
    LANGUAGE plpgsql
    AS $$


DECLARE


  dateRun date := TO_DATE(p_rundate, 'YYYYMMDD');


  strCycleCode character varying;


  intTeamId integer;


  intEmployeeId integer;


  intAssignedTo integer;


  intSpvId integer;


  numLoad integer;


  


  rec_bucket RECORD;


  cur_bucket REFCURSOR;





BEGIN


  retval := 0;


  retstr := 'spCreateLKRbySQLStmt (SQL: ' || p_rundate || ' - ' || p_sqlstmt || ')';





  OPEN cur_bucket FOR EXECUTE p_sqlstmt;


  LOOP


    FETCH cur_bucket INTO rec_bucket;


    EXIT WHEN NOT FOUND;





    strCycleCode := fnGet_CycleCode_ByDate('R', dateRun, rec_bucket.leastduedate);


    IF strCycleCode IS NOT NULL THEN





      --IF strCycleCode <> fnGet_CycleCode_ByTeam(rec_bucket.team_id) THEN


        -- move to next cycle


        PERFORM spLKR_Transaction_Delete(rec_bucket.cntrno);


      --END IF;





      SELECT team_id, spv_id, empl_id, load


        FROM fnRetrieve_Collector(rec_bucket.officd, strCycleCode, rec_bucket.bizu, rec_bucket.zipcd, rec_bucket.subzipcd)


        INTO intTeamId, intSpvId, intEmployeeId, numLoad;





      intAssignedTo := intEmployeeId;


      IF intEmployeeId IS NOT NULL THEN


        IF numLoad <= fnGet_NumAssignment_Collector(intEmployeeId) THEN


          intAssignedTo := NULL;


        END IF;


      END IF;





      --PERFORM spLKR_Transaction_Delete(rec_bucket.cntrno);


      PERFORM spLKR_Transaction_Insert(rec_bucket.cntrno, dateRun::timestamp, intTeamId, intSpvId, intAssignedTo, intEmployeeId);


    END IF;





  END LOOP;


  CLOSE cur_bucket;





END; $$;


ALTER FUNCTION public.spcreatelkrbysqlstmt(p_rundate character varying, p_sqlstmt text, OUT retval integer, OUT retstr text) OWNER TO postgres;

--
-- Name: spcreatelkrdaily(character varying); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.spcreatelkrdaily(p_rundate character varying, OUT retval integer, OUT retstr text) RETURNS record
    LANGUAGE plpgsql
    AS $$


DECLARE


  intOverdueStart smallint;


  intOverdueEnd smallint;


  txtSQL text;


  txtSubSQL text;


  txtDefaultSQL text;





  rec_priority RECORD;


  cur_priority REFCURSOR;





BEGIN


  retval := 0;


  retstr := 'spCreateLKRDaily (rundate: ' || p_rundate || ')';





  SELECT MIN(ovdstart), MAX(ovdend) INTO intOverdueStart, intOverdueEnd FROM param.Cycle WHERE type = 'R';


  txtSubSQL := FORMAT('AND ovd BETWEEN %s AND %s ', intOverdueStart, intOverdueEnd);


  --txtSubSQL := FORMAT('AND (TO_DATE(''%s'', ''YYYYMMDD'') - leastduedate) BETWEEN %s AND %s ', p_rundate, intOverdueStart, intOverdueEnd);





  txtDefaultSQL := 'SELECT * FROM vw_rem_bucket ';


  txtDefaultSQL := txtDefaultSQL || FORMAT('WHERE COALESCE(rundate, TO_DATE(''20000101'', ''YYYYMMDD'')) < TO_DATE(''%s'', ''YYYYMMDD'') ', p_rundate);


  txtDefaultSQL := txtDefaultSQL || 'AND NOT fnIs_AssignedToPC(cntrno) ';


  txtDefaultSQL := txtDefaultSQL || txtSubSQL;


  txtDefaultSQL := txtDefaultSQL || 'AND <SQLText> ';


  --txtDefaultSQL := txtDefaultSQL || 'ORDER BY officd';





  OPEN cur_priority FOR


    SELECT script FROM param.LKPPrio


    ORDER BY type, seqno;





  LOOP


    FETCH cur_priority INTO rec_priority;


    EXIT WHEN NOT FOUND;





    PERFORM spCreateRemedialBucket(p_rundate);


    


    txtSubSQL := rec_priority.script;


    txtSubSQL := fnReplaceKeyword(txtSubSQL, '<rundate>', p_rundate);


    txtSQL := fnReplaceKeyword(txtDefaultSQL, '<SQLText>', txtSubSQL);


    PERFORM spCreateLKRbySQLStmt(p_rundate, txtSQL);


  END LOOP;


  CLOSE cur_priority;


END; $$;


ALTER FUNCTION public.spcreatelkrdaily(p_rundate character varying, OUT retval integer, OUT retstr text) OWNER TO postgres;

--
-- Name: spcreateremedialbucket(character varying); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.spcreateremedialbucket(p_rundate character varying, OUT retval integer, OUT retstr text) RETURNS record
    LANGUAGE plpgsql
    AS $$


DECLARE


  intOverdueStart smallint;


  intOverdueEnd smallint;


  sql text;





BEGIN


  retval := 0;


  retstr := 'spCreateRemedialBucket (rundate: ' || p_rundate || ')';





  SELECT MIN(ovdstart), MAX(ovdend) INTO intOverdueStart, intOverdueEnd FROM param.Cycle WHERE type = 'R';





  IF EXISTS(SELECT 1 FROM pg_matviews WHERE matviewname = 'vw_rem_bucket') THEN


    DROP MATERIALIZED VIEW vw_rem_bucket;


  END IF;





  sql := 'CREATE MATERIALIZED VIEW vw_rem_bucket AS ' ||


         'SELECT b.*, lkr.id, rundate, team_id, spv_id, asgnd_to, print_id, entry_cd entry_status, cmnt_id, cmntdate comment_date ' ||


         'FROM Bucket b ' ||


         'LEFT OUTER JOIN lkrtrx lkp ON lkr.cntrno = b.cntrno ';


  sql := sql || 'WHERE NOT fnIs_AssignedToPC(b.cntrno) ';


  sql := sql || FORMAT('AND ovd BETWEEN %s AND %s', intOverdueStart, intOverdueEnd);


  --sql := sql || FORMAT('AND (TO_DATE(''%s'', ''YYYYMMDD'') - leastduedate) BETWEEN %s AND %s', p_rundate, intOverdueStart, intOverdueEnd);





  EXECUTE sql;


END; $$;


ALTER FUNCTION public.spcreateremedialbucket(p_rundate character varying, OUT retval integer, OUT retstr text) OWNER TO postgres;

--
-- Name: splkd_transaction_insert(character varying, timestamp without time zone, integer, integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.splkd_transaction_insert(p_contract_no character varying, p_run_date timestamp without time zone, p_team_id integer, p_spv_id integer, OUT retval integer, OUT retstr text) RETURNS record
    LANGUAGE plpgsql
    AS $$
BEGIN
  retval := 0;
  retstr := 'spLKD_Transaction_Insert (Contract No: ' || p_contract_no || ' - ' || p_run_date || ', ' || p_team_id || ', ' || p_spv_id || ')';

  INSERT INTO lkdtrx (
    cntrno,
    rundate,
    team_id,
    spv_id
  )
  VALUES (
    p_contract_no,
    p_run_date,
    p_team_id,
    p_spv_id
  );

END; $$;


ALTER FUNCTION public.splkd_transaction_insert(p_contract_no character varying, p_run_date timestamp without time zone, p_team_id integer, p_spv_id integer, OUT retval integer, OUT retstr text) OWNER TO postgres;

--
-- Name: splkp_transaction_delete(character varying); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.splkp_transaction_delete(p_contract_no character varying, OUT retval integer, OUT retstr text) RETURNS record
    LANGUAGE plpgsql
    AS $$
BEGIN
  retval := 0;
  retstr := 'spLKP_Transaction_Delete (Contract No: ' || p_contract_no || ')';

  INSERT INTO hist.lkptrx(cntrno, rundate, team_id, spv_id, asgnd_to, owner_id, print_id, entry_cd, cmnt_id, cmntdate, cmnttext)
  SELECT cntrno, rundate, team_id, spv_id, asgnd_to, owner_id, print_id, entry_cd, cmnt_id, cmntdate, cmnttext FROM lkptrx WHERE cntrno = p_contract_no;
  
  DELETE FROM lkptrx WHERE cntrno = p_contract_no;
END; $$;


ALTER FUNCTION public.splkp_transaction_delete(p_contract_no character varying, OUT retval integer, OUT retstr text) OWNER TO postgres;

--
-- Name: splkp_transaction_insert(character varying, timestamp without time zone, integer, integer, integer, integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.splkp_transaction_insert(p_contract_no character varying, p_run_date timestamp without time zone, p_team_id integer, p_spv_id integer, p_assigned_to integer, p_owner_id integer, OUT retval integer, OUT retstr text) RETURNS record
    LANGUAGE plpgsql
    AS $$
BEGIN
  retval := 0;
  retstr := 'spLKP_Transaction_Insert (Contract No: ' || p_contract_no || ' - ' || p_run_date || ', ' || p_team_id || ', ' || p_spv_id || ', ' || p_assigned_to || ', ' || p_owner_id || ')';

  INSERT INTO lkptrx (
    cntrno,
    rundate,
    team_id,
    spv_id,
    asgnd_to,
    owner_id
  )
  VALUES (
    p_contract_no,
    p_run_date,
    p_team_id,
    p_spv_id,
    p_assigned_to,
    p_owner_id
  );

END; $$;


ALTER FUNCTION public.splkp_transaction_insert(p_contract_no character varying, p_run_date timestamp without time zone, p_team_id integer, p_spv_id integer, p_assigned_to integer, p_owner_id integer, OUT retval integer, OUT retstr text) OWNER TO postgres;

--
-- Name: splkr_transaction_delete(character varying); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.splkr_transaction_delete(p_contract_no character varying, OUT retval integer, OUT retstr text) RETURNS record
    LANGUAGE plpgsql
    AS $$


BEGIN


  retval := 0;


  retstr := 'spLKR_Transaction_Delete (Contract No: ' || p_contract_no || ')';





  INSERT INTO hist.lkrtrx(cntrno, rundate, team_id, spv_id, asgnd_to, owner_id, print_id, entry_cd, cmnt_id, cmntdate, cmnttext)


  SELECT cntrno, rundate, team_id, spv_id, asgnd_to, owner_id, print_id, entry_cd, cmnt_id, cmntdate, cmnttext FROM lkptrx WHERE cntrno = p_contract_no;


  


  DELETE FROM lkrtrx WHERE cntrno = p_contract_no;


END; $$;


ALTER FUNCTION public.splkr_transaction_delete(p_contract_no character varying, OUT retval integer, OUT retstr text) OWNER TO postgres;

--
-- Name: splkr_transaction_insert(character varying, timestamp without time zone, integer, integer, integer, integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.splkr_transaction_insert(p_contract_no character varying, p_run_date timestamp without time zone, p_team_id integer, p_spv_id integer, p_assigned_to integer, p_owner_id integer, OUT retval integer, OUT retstr text) RETURNS record
    LANGUAGE plpgsql
    AS $$


BEGIN


  retval := 0;


  retstr := 'spLKR_Transaction_Insert (Contract No: ' || p_contract_no || ' - ' || p_run_date || ', ' || p_team_id || ', ' || p_spv_id || ', ' || p_assigned_to || ', ' || p_owner_id || ')';





  INSERT INTO lkrtrx (


    cntrno,


    rundate,


    team_id,


    spv_id,


    asgnd_to,


    owner_id


  )


  VALUES (


    p_contract_no,


    p_run_date,


    p_team_id,


    p_spv_id,


    p_assigned_to,


    p_owner_id


  );





END; $$;


ALTER FUNCTION public.splkr_transaction_insert(p_contract_no character varying, p_run_date timestamp without time zone, p_team_id integer, p_spv_id integer, p_assigned_to integer, p_owner_id integer, OUT retval integer, OUT retstr text) OWNER TO postgres;

--
-- Name: spnightrun(character varying); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.spnightrun(p_rundate character varying, OUT retval integer, OUT retstr text) RETURNS record
    LANGUAGE plpgsql
    AS $$
DECLARE
  dateRun date := to_date(p_rundate, 'YYYYMMDD');
  day integer := date_part('day', dateRun);
  lkpmaxentrydays integer := 0;
  skpcvalidityperiod integer := 0;
  skpcextendedvalidityperiod integer := 0;
  skpcmaxidledays integer := 0;

BEGIN
  retval := 0;
  retstr := 'spNightrun (rundate: ' || p_rundate || ')';

  SELECT lkp_max_entry_days, skpc_validity_period, skpc_extended_validity_period, skpc_max_idle_days
    INTO lkpmaxentrydays, skpcvalidityperiod, skpcextendedvalidityperiod, skpcmaxidledays
    FROM param.genparam
   WHERE id = 1;


-- UPDATE UNVISITED LKPEntry STATUS

  -- update LKPEntry UNV = unvisited for not filled LKP
  UPDATE public.lkptrx
     SET entry_cd = 'UNV'
   WHERE entry_cd IS NULL
     AND print_id IS NOT NULL
     AND (dateRun::date - rundate::date) >= lkpmaxentrydays;


-- UPDATE Professional Collector MOU STATUS

  UPDATE param.prof SET status = 0 WHERE dateRun < moustart;
  UPDATE param.prof SET status = 1 WHERE dateRun BETWEEN moustart AND mouend;
  UPDATE param.prof SET status = 2 WHERE dateRun > mouend;


-- UPDATE SKPC STATUS

  UPDATE public.skpctrx
     SET status = 3
   WHERE seqno = 0
     AND (dateRun::date - rundate::date) >= skpcvalidityperiod
     AND (dateRun::date - rundate::date) < (skpcvalidityperiod + skpcmaxidledays);

  UPDATE public.skpctrx
     SET status = 2
   WHERE seqno = 0
     AND (dateRun::date - rundate::date) >= (skpcvalidityperiod + skpcmaxidledays);

  UPDATE public.skpctrx
     SET status = 3
   WHERE seqno > 0
     AND (dateRun::date - rundate::date) >= skpcextendedvalidityperiod
     AND (dateRun::date - rundate::date) < (skpcextendedvalidityperiod + skpcmaxidledays);

  UPDATE public.skpctrx
     SET status = 2
   WHERE seqno > 0
     AND (dateRun::date - rundate::date) >= (skpcextendedvalidityperiod + skpcmaxidledays);


-- PROCESS THE BEGINNING OF THE MONTH

  IF (day = 1) THEN
    INSERT into hist.lkptrx(id, cntrno, rundate, team_id, spv_id, asgnd_to, owner_id, print_id, entry_cd, cmnt_id, cmntdate, cmnttext)
    SELECT id, cntrno, rundate, team_id, spv_id, asgnd_to, owner_id, print_id, entry_cd, cmnt_id, cmntdate, cmnttext FROM public.lkptrx;
    
    TRUNCATE TABLE public.bucket CASCADE;

    PERFORM spCreateBucket(p_rundate);

  --ELSE
    --PERFORM spUpdateBucket(p_rundate);
  END IF;
  
  UPDATE public.bucket SET cycle_id = fnGet_CycleId_ByOvd(ovd);
  PERFORM spCreateLKPDaily(p_rundate);

END; $$;


ALTER FUNCTION public.spnightrun(p_rundate character varying, OUT retval integer, OUT retstr text) OWNER TO postgres;

--
-- Name: spupdatebucket(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.spupdatebucket(OUT retval integer, OUT retstr text) RETURNS record
    LANGUAGE plpgsql
    AS $$
BEGIN
  UPDATE bucket SET
    cycle_id = fnGet_CycleId_ByOvd(ovd);
END; $$;


ALTER FUNCTION public.spupdatebucket(OUT retval integer, OUT retstr text) OWNER TO postgres;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: role; Type: TABLE; Schema: auth; Owner: postgres
--

CREATE TABLE auth.role (
    id integer NOT NULL,
    name character varying(255) NOT NULL
);


ALTER TABLE auth.role OWNER TO postgres;

--
-- Name: role_id_seq; Type: SEQUENCE; Schema: auth; Owner: postgres
--

CREATE SEQUENCE auth.role_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE auth.role_id_seq OWNER TO postgres;

--
-- Name: role_id_seq; Type: SEQUENCE OWNED BY; Schema: auth; Owner: postgres
--

ALTER SEQUENCE auth.role_id_seq OWNED BY auth.role.id;


--
-- Name: user; Type: TABLE; Schema: auth; Owner: postgres
--

CREATE TABLE auth."user" (
    id integer NOT NULL,
    pwd character varying(20) NOT NULL,
    uname character varying(20) NOT NULL,
    empl_id integer
);


ALTER TABLE auth."user" OWNER TO postgres;

--
-- Name: user_id_seq; Type: SEQUENCE; Schema: auth; Owner: postgres
--

CREATE SEQUENCE auth.user_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE auth.user_id_seq OWNER TO postgres;

--
-- Name: user_id_seq; Type: SEQUENCE OWNED BY; Schema: auth; Owner: postgres
--

ALTER SEQUENCE auth.user_id_seq OWNED BY auth."user".id;


--
-- Name: user_roles; Type: TABLE; Schema: auth; Owner: postgres
--

CREATE TABLE auth.user_roles (
    user_id integer NOT NULL,
    role_id integer NOT NULL
);


ALTER TABLE auth.user_roles OWNER TO postgres;

--
-- Name: lkptrx; Type: TABLE; Schema: hist; Owner: postgres
--

CREATE TABLE hist.lkptrx (
    id integer NOT NULL,
    asgnd_to integer,
    cmntdate date,
    cmnt_id integer,
    owner_id integer,
    rundate timestamp without time zone NOT NULL,
    cmnttext character varying(255),
    spv_id integer,
    team_id integer,
    cntrno character varying(12) NOT NULL,
    entry_cd character varying(3),
    print_id integer
);


ALTER TABLE hist.lkptrx OWNER TO postgres;

--
-- Name: lkptrx_id_seq; Type: SEQUENCE; Schema: hist; Owner: postgres
--

CREATE SEQUENCE hist.lkptrx_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE hist.lkptrx_id_seq OWNER TO postgres;

--
-- Name: lkptrx_id_seq; Type: SEQUENCE OWNED BY; Schema: hist; Owner: postgres
--

ALTER SEQUENCE hist.lkptrx_id_seq OWNED BY hist.lkptrx.id;


--
-- Name: lkrtrx; Type: TABLE; Schema: hist; Owner: postgres
--

CREATE TABLE hist.lkrtrx (
    id integer NOT NULL,
    asgnd_to integer,
    cmntdate date,
    cmnt_id integer,
    owner_id integer,
    rundate timestamp without time zone NOT NULL,
    cmnttext character varying(255),
    spv_id integer,
    team_id integer,
    cntrno character varying(12) NOT NULL,
    entry_cd character varying(3),
    print_id integer
);


ALTER TABLE hist.lkrtrx OWNER TO postgres;

--
-- Name: lkrtrx_id_seq; Type: SEQUENCE; Schema: hist; Owner: postgres
--

CREATE SEQUENCE hist.lkrtrx_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE hist.lkrtrx_id_seq OWNER TO postgres;

--
-- Name: lkrtrx_id_seq; Type: SEQUENCE OWNED BY; Schema: hist; Owner: postgres
--

ALTER SEQUENCE hist.lkrtrx_id_seq OWNED BY hist.lkrtrx.id;


--
-- Name: mou; Type: TABLE; Schema: hist; Owner: postgres
--

CREATE TABLE hist.mou (
    mouno character varying(20) NOT NULL,
    mouend date NOT NULL,
    status integer NOT NULL,
    moudate date NOT NULL,
    perf double precision NOT NULL,
    moustart date NOT NULL,
    prof_id integer NOT NULL
);


ALTER TABLE hist.mou OWNER TO postgres;

--
-- Name: area_zip; Type: TABLE; Schema: param; Owner: postgres
--

CREATE TABLE param.area_zip (
    id integer NOT NULL,
    coll_empl_id integer NOT NULL,
    zip_id integer NOT NULL
);


ALTER TABLE param.area_zip OWNER TO postgres;

--
-- Name: area_zip_id_seq; Type: SEQUENCE; Schema: param; Owner: postgres
--

CREATE SEQUENCE param.area_zip_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE param.area_zip_id_seq OWNER TO postgres;

--
-- Name: area_zip_id_seq; Type: SEQUENCE OWNED BY; Schema: param; Owner: postgres
--

ALTER SEQUENCE param.area_zip_id_seq OWNED BY param.area_zip.id;


--
-- Name: bizu; Type: TABLE; Schema: param; Owner: postgres
--

CREATE TABLE param.bizu (
    id integer NOT NULL,
    code character varying(255) NOT NULL,
    name character varying(255) NOT NULL
);


ALTER TABLE param.bizu OWNER TO postgres;

--
-- Name: bizu_id_seq; Type: SEQUENCE; Schema: param; Owner: postgres
--

CREATE SEQUENCE param.bizu_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE param.bizu_id_seq OWNER TO postgres;

--
-- Name: bizu_id_seq; Type: SEQUENCE OWNED BY; Schema: param; Owner: postgres
--

ALTER SEQUENCE param.bizu_id_seq OWNED BY param.bizu.id;


--
-- Name: coll; Type: TABLE; Schema: param; Owner: postgres
--

CREATE TABLE param.coll (
    id integer NOT NULL,
    name character varying(50) NOT NULL,
    cycle_id integer NOT NULL,
    spv_id integer,
    off_id integer NOT NULL
);


ALTER TABLE param.coll OWNER TO postgres;

--
-- Name: coll_bizu; Type: TABLE; Schema: param; Owner: postgres
--

CREATE TABLE param.coll_bizu (
    id integer NOT NULL,
    bizu_id integer NOT NULL,
    coll_id integer NOT NULL
);


ALTER TABLE param.coll_bizu OWNER TO postgres;

--
-- Name: coll_bizu_id_seq; Type: SEQUENCE; Schema: param; Owner: postgres
--

CREATE SEQUENCE param.coll_bizu_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE param.coll_bizu_id_seq OWNER TO postgres;

--
-- Name: coll_bizu_id_seq; Type: SEQUENCE OWNED BY; Schema: param; Owner: postgres
--

ALTER SEQUENCE param.coll_bizu_id_seq OWNED BY param.coll_bizu.id;


--
-- Name: coll_drmt; Type: TABLE; Schema: param; Owner: postgres
--

CREATE TABLE param.coll_drmt (
    id integer NOT NULL,
    datein timestamp without time zone NOT NULL,
    dateout timestamp without time zone,
    empl_id integer NOT NULL,
    next_member_id integer,
    prev_member_id integer NOT NULL,
    dload integer NOT NULL,
    mload integer NOT NULL
);


ALTER TABLE param.coll_drmt OWNER TO postgres;

--
-- Name: coll_drmt_id_seq; Type: SEQUENCE; Schema: param; Owner: postgres
--

CREATE SEQUENCE param.coll_drmt_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE param.coll_drmt_id_seq OWNER TO postgres;

--
-- Name: coll_drmt_id_seq; Type: SEQUENCE OWNED BY; Schema: param; Owner: postgres
--

ALTER SEQUENCE param.coll_drmt_id_seq OWNED BY param.coll_drmt.id;


--
-- Name: coll_empl; Type: TABLE; Schema: param; Owner: postgres
--

CREATE TABLE param.coll_empl (
    id integer NOT NULL,
    dload integer NOT NULL,
    mload integer NOT NULL,
    coll_id integer NOT NULL,
    empl_id integer
);


ALTER TABLE param.coll_empl OWNER TO postgres;

--
-- Name: coll_empl_id_seq; Type: SEQUENCE; Schema: param; Owner: postgres
--

CREATE SEQUENCE param.coll_empl_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE param.coll_empl_id_seq OWNER TO postgres;

--
-- Name: coll_empl_id_seq; Type: SEQUENCE OWNED BY; Schema: param; Owner: postgres
--

ALTER SEQUENCE param.coll_empl_id_seq OWNED BY param.coll_empl.id;


--
-- Name: coll_id_seq; Type: SEQUENCE; Schema: param; Owner: postgres
--

CREATE SEQUENCE param.coll_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE param.coll_id_seq OWNER TO postgres;

--
-- Name: coll_id_seq; Type: SEQUENCE OWNED BY; Schema: param; Owner: postgres
--

ALTER SEQUENCE param.coll_id_seq OWNED BY param.coll.id;


--
-- Name: coll_zip; Type: TABLE; Schema: param; Owner: postgres
--

CREATE TABLE param.coll_zip (
    id integer NOT NULL,
    coll_empl_id integer NOT NULL,
    zip_id integer NOT NULL
);


ALTER TABLE param.coll_zip OWNER TO postgres;

--
-- Name: coll_zip_id_seq; Type: SEQUENCE; Schema: param; Owner: postgres
--

CREATE SEQUENCE param.coll_zip_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE param.coll_zip_id_seq OWNER TO postgres;

--
-- Name: coll_zip_id_seq; Type: SEQUENCE OWNED BY; Schema: param; Owner: postgres
--

ALTER SEQUENCE param.coll_zip_id_seq OWNED BY param.coll_zip.id;


--
-- Name: cycle; Type: TABLE; Schema: param; Owner: postgres
--

CREATE TABLE param.cycle (
    id integer NOT NULL,
    code character varying(3) NOT NULL,
    descr character varying(255),
    name character varying(30) NOT NULL,
    ovdend integer NOT NULL,
    ovdstart integer NOT NULL,
    type character varying(1) NOT NULL
);


ALTER TABLE param.cycle OWNER TO postgres;

--
-- Name: cycle_id_seq; Type: SEQUENCE; Schema: param; Owner: postgres
--

CREATE SEQUENCE param.cycle_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE param.cycle_id_seq OWNER TO postgres;

--
-- Name: cycle_id_seq; Type: SEQUENCE OWNED BY; Schema: param; Owner: postgres
--

ALTER SEQUENCE param.cycle_id_seq OWNED BY param.cycle.id;


--
-- Name: cycletype; Type: TABLE; Schema: param; Owner: postgres
--

CREATE TABLE param.cycletype (
    type character varying(1) NOT NULL,
    descr character varying(10) NOT NULL
);


ALTER TABLE param.cycletype OWNER TO postgres;

--
-- Name: dcall; Type: TABLE; Schema: param; Owner: postgres
--

CREATE TABLE param.dcall (
    id integer NOT NULL,
    name character varying(50) NOT NULL,
    cycle_id integer NOT NULL,
    off_id integer NOT NULL,
    spv_id integer
);


ALTER TABLE param.dcall OWNER TO postgres;

--
-- Name: dcall_empl; Type: TABLE; Schema: param; Owner: postgres
--

CREATE TABLE param.dcall_empl (
    id integer NOT NULL,
    dload integer NOT NULL,
    mload integer NOT NULL,
    dcall_id integer NOT NULL,
    empl_id integer
);


ALTER TABLE param.dcall_empl OWNER TO postgres;

--
-- Name: dcall_empl_id_seq; Type: SEQUENCE; Schema: param; Owner: postgres
--

CREATE SEQUENCE param.dcall_empl_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE param.dcall_empl_id_seq OWNER TO postgres;

--
-- Name: dcall_empl_id_seq; Type: SEQUENCE OWNED BY; Schema: param; Owner: postgres
--

ALTER SEQUENCE param.dcall_empl_id_seq OWNED BY param.dcall_empl.id;


--
-- Name: dcall_id_seq; Type: SEQUENCE; Schema: param; Owner: postgres
--

CREATE SEQUENCE param.dcall_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE param.dcall_id_seq OWNER TO postgres;

--
-- Name: dcall_id_seq; Type: SEQUENCE OWNED BY; Schema: param; Owner: postgres
--

ALTER SEQUENCE param.dcall_id_seq OWNED BY param.dcall.id;


--
-- Name: empl; Type: TABLE; Schema: param; Owner: postgres
--

CREATE TABLE param.empl (
    id integer NOT NULL,
    name character varying(50) NOT NULL,
    regno character varying(5) NOT NULL,
    off_id integer NOT NULL,
    job_cd character varying(7) NOT NULL
);


ALTER TABLE param.empl OWNER TO postgres;

--
-- Name: empl_id_seq; Type: SEQUENCE; Schema: param; Owner: postgres
--

CREATE SEQUENCE param.empl_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE param.empl_id_seq OWNER TO postgres;

--
-- Name: empl_id_seq; Type: SEQUENCE OWNED BY; Schema: param; Owner: postgres
--

ALTER SEQUENCE param.empl_id_seq OWNED BY param.empl.id;


--
-- Name: genparam; Type: TABLE; Schema: param; Owner: postgres
--

CREATE TABLE param.genparam (
    id integer NOT NULL,
    lkp_max_entry_days integer NOT NULL,
    skpc_extended_validity_period integer NOT NULL,
    skpc_max_extend integer NOT NULL,
    skpc_max_idle_days integer NOT NULL,
    skpc_max_outstanding_load integer NOT NULL,
    skpc_validity_period integer NOT NULL,
    company_name character varying(50) NOT NULL,
    company_address character varying(100) NOT NULL,
    mou_min_performance double precision NOT NULL,
    report_dir character varying(256) NOT NULL
);


ALTER TABLE param.genparam OWNER TO postgres;

--
-- Name: genparam_id_seq; Type: SEQUENCE; Schema: param; Owner: postgres
--

CREATE SEQUENCE param.genparam_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE param.genparam_id_seq OWNER TO postgres;

--
-- Name: genparam_id_seq; Type: SEQUENCE OWNED BY; Schema: param; Owner: postgres
--

ALTER SEQUENCE param.genparam_id_seq OWNED BY param.genparam.id;


--
-- Name: idcard; Type: TABLE; Schema: param; Owner: postgres
--

CREATE TABLE param.idcard (
    id integer NOT NULL,
    name character varying(10) NOT NULL
);


ALTER TABLE param.idcard OWNER TO postgres;

--
-- Name: idcard_id_seq; Type: SEQUENCE; Schema: param; Owner: postgres
--

CREATE SEQUENCE param.idcard_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE param.idcard_id_seq OWNER TO postgres;

--
-- Name: idcard_id_seq; Type: SEQUENCE OWNED BY; Schema: param; Owner: postgres
--

ALTER SEQUENCE param.idcard_id_seq OWNED BY param.idcard.id;


--
-- Name: job; Type: TABLE; Schema: param; Owner: postgres
--

CREATE TABLE param.job (
    code character varying(7) NOT NULL,
    level integer NOT NULL,
    name character varying(50)
);


ALTER TABLE param.job OWNER TO postgres;

--
-- Name: lkdentry; Type: TABLE; Schema: param; Owner: postgres
--

CREATE TABLE param.lkdentry (
    code character varying(3) NOT NULL,
    descr character varying(256)
);


ALTER TABLE param.lkdentry OWNER TO postgres;

--
-- Name: lkpcmnt; Type: TABLE; Schema: param; Owner: postgres
--

CREATE TABLE param.lkpcmnt (
    id integer NOT NULL,
    code character varying(3) NOT NULL,
    descr character varying(256),
    date boolean NOT NULL,
    reason boolean NOT NULL,
    name character varying(30) NOT NULL
);


ALTER TABLE param.lkpcmnt OWNER TO postgres;

--
-- Name: lkpcmnt_id_seq; Type: SEQUENCE; Schema: param; Owner: postgres
--

CREATE SEQUENCE param.lkpcmnt_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE param.lkpcmnt_id_seq OWNER TO postgres;

--
-- Name: lkpcmnt_id_seq; Type: SEQUENCE OWNED BY; Schema: param; Owner: postgres
--

ALTER SEQUENCE param.lkpcmnt_id_seq OWNED BY param.lkpcmnt.id;


--
-- Name: lkpentry; Type: TABLE; Schema: param; Owner: postgres
--

CREATE TABLE param.lkpentry (
    code character varying(3) NOT NULL,
    descr character varying(256),
    date boolean NOT NULL,
    reason boolean NOT NULL,
    name character varying(30) NOT NULL
);


ALTER TABLE param.lkpentry OWNER TO postgres;

--
-- Name: lkpprio; Type: TABLE; Schema: param; Owner: postgres
--

CREATE TABLE param.lkpprio (
    id integer NOT NULL,
    code character varying(3) NOT NULL,
    descr character varying(256),
    script text NOT NULL,
    seqno integer,
    type character varying(1) NOT NULL
);


ALTER TABLE param.lkpprio OWNER TO postgres;

--
-- Name: lkpprio_id_seq; Type: SEQUENCE; Schema: param; Owner: postgres
--

CREATE SEQUENCE param.lkpprio_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE param.lkpprio_id_seq OWNER TO postgres;

--
-- Name: lkpprio_id_seq; Type: SEQUENCE OWNED BY; Schema: param; Owner: postgres
--

ALTER SEQUENCE param.lkpprio_id_seq OWNED BY param.lkpprio.id;


--
-- Name: lkrentry; Type: TABLE; Schema: param; Owner: postgres
--

CREATE TABLE param.lkrentry (
    code character varying(3) NOT NULL,
    name character varying(30) NOT NULL,
    descr character varying(256)
);


ALTER TABLE param.lkrentry OWNER TO postgres;

--
-- Name: offi; Type: TABLE; Schema: param; Owner: postgres
--

CREATE TABLE param.offi (
    id integer NOT NULL,
    addr character varying(100),
    code character varying(5) NOT NULL,
    name character varying(100) NOT NULL,
    phone character varying(20),
    pic_addr character varying(100),
    pic_email character varying(50),
    pic_name character varying(50),
    pic_phone character varying(20),
    type character varying(1),
    skpc_fee double precision NOT NULL
);


ALTER TABLE param.offi OWNER TO postgres;

--
-- Name: offi_id_seq; Type: SEQUENCE; Schema: param; Owner: postgres
--

CREATE SEQUENCE param.offi_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE param.offi_id_seq OWNER TO postgres;

--
-- Name: offi_id_seq; Type: SEQUENCE OWNED BY; Schema: param; Owner: postgres
--

ALTER SEQUENCE param.offi_id_seq OWNED BY param.offi.id;


--
-- Name: prof; Type: TABLE; Schema: param; Owner: postgres
--

CREATE TABLE param.prof (
    id integer NOT NULL,
    addr character varying(100),
    email character varying(50),
    idno character varying(20) NOT NULL,
    moudate date,
    mouend date,
    mouno character varying(20),
    moustart date,
    name character varying(50) NOT NULL,
    perf double precision,
    phone character varying(20),
    type integer NOT NULL,
    status integer,
    off_id integer NOT NULL,
    code character varying(5) NOT NULL
);


ALTER TABLE param.prof OWNER TO postgres;

--
-- Name: prof_id_seq; Type: SEQUENCE; Schema: param; Owner: postgres
--

CREATE SEQUENCE param.prof_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE param.prof_id_seq OWNER TO postgres;

--
-- Name: prof_id_seq; Type: SEQUENCE OWNED BY; Schema: param; Owner: postgres
--

ALTER SEQUENCE param.prof_id_seq OWNED BY param.prof.id;


--
-- Name: prof_staf; Type: TABLE; Schema: param; Owner: postgres
--

CREATE TABLE param.prof_staf (
    id integer NOT NULL,
    address character varying(100),
    email character varying(50),
    idcard_no character varying(20) NOT NULL,
    name character varying(50) NOT NULL,
    phone character varying(20),
    prof_id integer NOT NULL
);


ALTER TABLE param.prof_staf OWNER TO postgres;

--
-- Name: prof_staf_id_seq; Type: SEQUENCE; Schema: param; Owner: postgres
--

CREATE SEQUENCE param.prof_staf_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE param.prof_staf_id_seq OWNER TO postgres;

--
-- Name: prof_staf_id_seq; Type: SEQUENCE OWNED BY; Schema: param; Owner: postgres
--

ALTER SEQUENCE param.prof_staf_id_seq OWNED BY param.prof_staf.id;


--
-- Name: rem; Type: TABLE; Schema: param; Owner: postgres
--

CREATE TABLE param.rem (
    id integer NOT NULL,
    name character varying(50) NOT NULL,
    cycle_id integer NOT NULL,
    off_id integer NOT NULL,
    spv_id integer
);


ALTER TABLE param.rem OWNER TO postgres;

--
-- Name: rem_empl; Type: TABLE; Schema: param; Owner: postgres
--

CREATE TABLE param.rem_empl (
    id integer NOT NULL,
    dload integer NOT NULL,
    mload integer NOT NULL,
    empl_id integer,
    rem_id integer NOT NULL
);


ALTER TABLE param.rem_empl OWNER TO postgres;

--
-- Name: rem_empl_id_seq; Type: SEQUENCE; Schema: param; Owner: postgres
--

CREATE SEQUENCE param.rem_empl_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE param.rem_empl_id_seq OWNER TO postgres;

--
-- Name: rem_empl_id_seq; Type: SEQUENCE OWNED BY; Schema: param; Owner: postgres
--

ALTER SEQUENCE param.rem_empl_id_seq OWNED BY param.rem_empl.id;


--
-- Name: rem_id_seq; Type: SEQUENCE; Schema: param; Owner: postgres
--

CREATE SEQUENCE param.rem_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE param.rem_id_seq OWNER TO postgres;

--
-- Name: rem_id_seq; Type: SEQUENCE OWNED BY; Schema: param; Owner: postgres
--

ALTER SEQUENCE param.rem_id_seq OWNED BY param.rem.id;


--
-- Name: rem_zip; Type: TABLE; Schema: param; Owner: postgres
--

CREATE TABLE param.rem_zip (
    id integer NOT NULL,
    rem_empl_id integer NOT NULL,
    zip_id integer NOT NULL
);


ALTER TABLE param.rem_zip OWNER TO postgres;

--
-- Name: rem_zip_id_seq; Type: SEQUENCE; Schema: param; Owner: postgres
--

CREATE SEQUENCE param.rem_zip_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE param.rem_zip_id_seq OWNER TO postgres;

--
-- Name: rem_zip_id_seq; Type: SEQUENCE OWNED BY; Schema: param; Owner: postgres
--

ALTER SEQUENCE param.rem_zip_id_seq OWNED BY param.rem_zip.id;


--
-- Name: rmdl_zone; Type: TABLE; Schema: param; Owner: postgres
--

CREATE TABLE param.rmdl_zone (
    id integer NOT NULL,
    rmdl_empl_id integer NOT NULL,
    zone_id integer NOT NULL
);


ALTER TABLE param.rmdl_zone OWNER TO postgres;

--
-- Name: rmdl_zone_id_seq; Type: SEQUENCE; Schema: param; Owner: postgres
--

CREATE SEQUENCE param.rmdl_zone_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE param.rmdl_zone_id_seq OWNER TO postgres;

--
-- Name: rmdl_zone_id_seq; Type: SEQUENCE OWNED BY; Schema: param; Owner: postgres
--

ALTER SEQUENCE param.rmdl_zone_id_seq OWNED BY param.rmdl_zone.id;


--
-- Name: stts; Type: TABLE; Schema: param; Owner: postgres
--

CREATE TABLE param.stts (
    id integer NOT NULL,
    descr character varying(255)
);


ALTER TABLE param.stts OWNER TO postgres;

--
-- Name: workingarea; Type: TABLE; Schema: param; Owner: postgres
--

CREATE TABLE param.workingarea (
    member_id integer NOT NULL,
    zipcode_id integer NOT NULL
);


ALTER TABLE param.workingarea OWNER TO postgres;

--
-- Name: zip; Type: TABLE; Schema: param; Owner: postgres
--

CREATE TABLE param.zip (
    id integer NOT NULL,
    name character varying(50),
    subzipcd character varying(2) NOT NULL,
    zipcd character varying(5) NOT NULL,
    off_id integer,
    zone_id integer
);


ALTER TABLE param.zip OWNER TO postgres;

--
-- Name: zip_id_seq; Type: SEQUENCE; Schema: param; Owner: postgres
--

CREATE SEQUENCE param.zip_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE param.zip_id_seq OWNER TO postgres;

--
-- Name: zip_id_seq; Type: SEQUENCE OWNED BY; Schema: param; Owner: postgres
--

ALTER SEQUENCE param.zip_id_seq OWNED BY param.zip.id;


--
-- Name: zone; Type: TABLE; Schema: param; Owner: postgres
--

CREATE TABLE param.zone (
    id integer NOT NULL,
    code character varying(2) NOT NULL,
    name character varying(50),
    off_id integer NOT NULL
);


ALTER TABLE param.zone OWNER TO postgres;

--
-- Name: zone_id_seq; Type: SEQUENCE; Schema: param; Owner: postgres
--

CREATE SEQUENCE param.zone_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE param.zone_id_seq OWNER TO postgres;

--
-- Name: zone_id_seq; Type: SEQUENCE OWNED BY; Schema: param; Owner: postgres
--

ALTER SEQUENCE param.zone_id_seq OWNED BY param.zone.id;


--
-- Name: bucket; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.bucket (
    cntrno character varying(12) NOT NULL,
    subzipcd character varying(2),
    bizu character varying(2),
    cntrdate date NOT NULL,
    custaddr character varying(100),
    custbilladdr character varying(100),
    custname character varying(50),
    lastduedate date,
    lastinstlno integer,
    leastduedate date,
    leastinstlno integer,
    minstl double precision,
    officd character varying(5) NOT NULL,
    outsdintamnt double precision,
    outsdpriamnt double precision,
    ovd integer,
    phone1 character varying(100),
    phone2 character varying(100),
    srcid integer,
    topinstl integer NOT NULL,
    zipcd character varying(5) NOT NULL,
    cycle_id integer
);


ALTER TABLE public.bucket OWNER TO postgres;

--
-- Name: bucketdetail; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.bucketdetail (
    id integer NOT NULL,
    collfee double precision,
    collfeepaid double precision,
    collfeepaydate date,
    duedate date NOT NULL,
    instlno integer NOT NULL,
    intamnt double precision,
    intamntpaid double precision,
    ovd integer NOT NULL,
    paydate date,
    pnltamnt double precision,
    pnltamntpaid double precision,
    pnltpaydate date,
    priamnt double precision NOT NULL,
    priamntpaid double precision,
    cntrno character varying(12) NOT NULL
);


ALTER TABLE public.bucketdetail OWNER TO postgres;

--
-- Name: bucketdetail_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.bucketdetail_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.bucketdetail_id_seq OWNER TO postgres;

--
-- Name: bucketdetail_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.bucketdetail_id_seq OWNED BY public.bucketdetail.id;


--
-- Name: delegbucket; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.delegbucket (
    id integer NOT NULL,
    subzipcd character varying(2),
    bizu character varying(2),
    cntrdate date NOT NULL,
    cntrno character varying(12) NOT NULL,
    custaddr character varying(100),
    custbilladdr character varying(100),
    custname character varying(50),
    lastduedate date,
    lastinstlno integer,
    leastduedate date,
    leastinstlno integer,
    memono character varying(12) NOT NULL,
    minstl double precision,
    officd character varying(5) NOT NULL,
    outsdintamnt double precision,
    outsdpriamnt double precision,
    phone1 character varying(100),
    phone2 character varying(100),
    memodate date NOT NULL,
    topinstl integer NOT NULL,
    zipcd character varying(5) NOT NULL,
    rundate date NOT NULL
);


ALTER TABLE public.delegbucket OWNER TO postgres;

--
-- Name: delegbucket_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.delegbucket_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.delegbucket_id_seq OWNER TO postgres;

--
-- Name: delegbucket_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.delegbucket_id_seq OWNED BY public.delegbucket.id;


--
-- Name: lkdtrx; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.lkdtrx (
    id integer NOT NULL,
    rundate timestamp without time zone NOT NULL,
    spv_id integer,
    team_id integer,
    cntrno character varying(12) NOT NULL,
    entry_cd character varying(3),
    print_id integer
);


ALTER TABLE public.lkdtrx OWNER TO postgres;

--
-- Name: lkdtrx_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.lkdtrx_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.lkdtrx_id_seq OWNER TO postgres;

--
-- Name: lkdtrx_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.lkdtrx_id_seq OWNED BY public.lkdtrx.id;


--
-- Name: lkptrx; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.lkptrx (
    id integer NOT NULL,
    asgnd_to integer,
    cmntdate date,
    cmnt_id integer,
    owner_id integer,
    rundate timestamp without time zone NOT NULL,
    cmnttext character varying(255),
    spv_id integer,
    team_id integer,
    cntrno character varying(12) NOT NULL,
    entry_cd character varying(3),
    print_id integer
);


ALTER TABLE public.lkptrx OWNER TO postgres;

--
-- Name: lkptrx_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.lkptrx_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.lkptrx_id_seq OWNER TO postgres;

--
-- Name: lkptrx_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.lkptrx_id_seq OWNED BY public.lkptrx.id;


--
-- Name: lkrtrx; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.lkrtrx (
    id integer NOT NULL,
    asgnd_to integer,
    cmntdate date,
    cmnt_id integer,
    owner_id integer,
    rundate timestamp without time zone NOT NULL,
    cmnttext character varying(255),
    spv_id integer,
    team_id integer,
    cntrno character varying(12) NOT NULL,
    entry_cd character varying(3),
    print_id integer
);


ALTER TABLE public.lkrtrx OWNER TO postgres;

--
-- Name: lkrtrx_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.lkrtrx_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.lkrtrx_id_seq OWNER TO postgres;

--
-- Name: lkrtrx_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.lkrtrx_id_seq OWNED BY public.lkrtrx.id;


--
-- Name: print; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.print (
    id integer NOT NULL,
    empl_id integer,
    printdate timestamp without time zone,
    printno character varying(20) NOT NULL,
    team_id integer,
    type character varying(1) NOT NULL
);


ALTER TABLE public.print OWNER TO postgres;

--
-- Name: print_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.print_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.print_id_seq OWNER TO postgres;

--
-- Name: print_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.print_id_seq OWNED BY public.print.id;


--
-- Name: skpctrx; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.skpctrx (
    id integer NOT NULL,
    asgnd_to integer,
    mouno character varying(20),
    profcoll_id integer,
    refno character varying(20),
    result integer,
    rundate timestamp without time zone NOT NULL,
    cntrno character varying(12) NOT NULL,
    print_id integer,
    status integer NOT NULL,
    seqno integer
);


ALTER TABLE public.skpctrx OWNER TO postgres;

--
-- Name: skpctrx_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.skpctrx_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.skpctrx_id_seq OWNER TO postgres;

--
-- Name: skpctrx_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.skpctrx_id_seq OWNED BY public.skpctrx.id;


--
-- Name: skpctrxdet; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.skpctrxdet (
    id integer NOT NULL,
    enddate date NOT NULL,
    seqno integer,
    startdate date NOT NULL,
    skpctrx_id integer NOT NULL
);


ALTER TABLE public.skpctrxdet OWNER TO postgres;

--
-- Name: skpctrxdet_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.skpctrxdet_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.skpctrxdet_id_seq OWNER TO postgres;

--
-- Name: skpctrxdet_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.skpctrxdet_id_seq OWNED BY public.skpctrxdet.id;


--
-- Name: unit; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.unit (
    id integer NOT NULL,
    addinfo text,
    brand character varying(50),
    color character varying(50),
    engineno character varying(50),
    frameno character varying(50),
    model character varying(50),
    polno character varying(50),
    stnk character varying(50),
    cntrno character varying(12) NOT NULL
);


ALTER TABLE public.unit OWNER TO postgres;

--
-- Name: unit_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.unit_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.unit_id_seq OWNER TO postgres;

--
-- Name: unit_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.unit_id_seq OWNED BY public.unit.id;


--
-- Name: vw_coll_bucket; Type: MATERIALIZED VIEW; Schema: public; Owner: postgres
--

CREATE MATERIALIZED VIEW public.vw_coll_bucket AS
 SELECT b.cntrno,
    b.subzipcd,
    b.bizu,
    b.cntrdate,
    b.custaddr,
    b.custbilladdr,
    b.custname,
    b.lastduedate,
    b.lastinstlno,
    b.leastduedate,
    b.leastinstlno,
    b.minstl,
    b.officd,
    b.outsdintamnt,
    b.outsdpriamnt,
    b.ovd,
    b.phone1,
    b.phone2,
    b.srcid,
    b.topinstl,
    b.zipcd,
    b.cycle_id,
    lkp.id,
    lkp.rundate,
    lkp.team_id,
    lkp.spv_id,
    lkp.asgnd_to,
    lkp.print_id,
    lkp.entry_cd AS entry_status,
    lkp.cmnt_id,
    lkp.cmntdate AS comment_date
   FROM (public.bucket b
     LEFT JOIN public.lkptrx lkp ON (((lkp.cntrno)::text = (b.cntrno)::text)))
  WHERE ((NOT public.fnis_assignedtopc(b.cntrno)) AND ((b.ovd >= 4) AND (b.ovd <= 90)))
  WITH NO DATA;


ALTER TABLE public.vw_coll_bucket OWNER TO postgres;

--
-- Name: vw_deskcall_bucket; Type: MATERIALIZED VIEW; Schema: public; Owner: postgres
--

CREATE MATERIALIZED VIEW public.vw_deskcall_bucket AS
 SELECT b.cntrno,
    b.subzipcd,
    b.bizu,
    b.cntrdate,
    b.custaddr,
    b.custbilladdr,
    b.custname,
    b.lastduedate,
    b.lastinstlno,
    b.leastduedate,
    b.leastinstlno,
    b.minstl,
    b.officd,
    b.outsdintamnt,
    b.outsdpriamnt,
    b.ovd,
    b.phone1,
    b.phone2,
    b.srcid,
    b.topinstl,
    b.zipcd,
    lkd.rundate,
    lkd.team_id,
    lkd.spv_id,
    lkd.print_id,
    lkd.entry_cd AS entry_status
   FROM (public.bucket b
     LEFT JOIN public.lkdtrx lkd ON (((lkd.cntrno)::text = (b.cntrno)::text)))
  WHERE ((NOT public.fnis_assignedtopc(b.cntrno)) AND (((to_date('20170901'::text, 'YYYYMMDD'::text) - b.leastduedate) >= '-3'::integer) AND ((to_date('20170901'::text, 'YYYYMMDD'::text) - b.leastduedate) <= 3)))
  WITH NO DATA;


ALTER TABLE public.vw_deskcall_bucket OWNER TO postgres;

--
-- Name: vw_skpc_latest; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW public.vw_skpc_latest AS
 SELECT DISTINCT ON (skpctrxdet.skpctrx_id) skpctrxdet.id,
    skpctrxdet.skpctrx_id,
    skpctrxdet.seqno,
    skpctrxdet.startdate,
    skpctrxdet.enddate
   FROM public.skpctrxdet
  ORDER BY skpctrxdet.skpctrx_id, skpctrxdet.seqno DESC;


ALTER TABLE public.vw_skpc_latest OWNER TO postgres;

--
-- Name: account_receivable; Type: TABLE; Schema: staging; Owner: postgres
--

CREATE TABLE staging.account_receivable (
    id integer NOT NULL,
    bussines_unit_name character varying(100),
    office_code character varying(5),
    contract_date date,
    no_contract character varying(12),
    name_customer character varying(100),
    debt_principal numeric(20,2),
    total_interest numeric(20,2),
    month_lenght character varying(2),
    percentage_eff numeric(4,2),
    percentage_flat numeric(4,2),
    monthly_installment numeric(20,2),
    customer_address character varying(100),
    billing_address character varying(100),
    version integer,
    zipcode character varying(5),
    sub_zipcode character varying(2),
    no_phone1 character varying(20),
    no_phone2 character varying(20),
    created_date date,
    created_by character varying(20),
    modified_date date,
    modified_by character varying(20)
);


ALTER TABLE staging.account_receivable OWNER TO postgres;

--
-- Name: account_receivable_id_seq; Type: SEQUENCE; Schema: staging; Owner: postgres
--

CREATE SEQUENCE staging.account_receivable_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE staging.account_receivable_id_seq OWNER TO postgres;

--
-- Name: account_receivable_id_seq; Type: SEQUENCE OWNED BY; Schema: staging; Owner: postgres
--

ALTER SEQUENCE staging.account_receivable_id_seq OWNED BY staging.account_receivable.id;


--
-- Name: detail_account_receivable; Type: TABLE; Schema: staging; Owner: postgres
--

CREATE TABLE staging.detail_account_receivable (
    account_receivable_id integer NOT NULL,
    installment_to integer NOT NULL,
    due_date_to date,
    principal_to numeric(20,2),
    interest_installment_to numeric(20,2),
    principal_payments_to numeric(20,2),
    pay_interest numeric(20,2),
    installment_payday_to date,
    penalty_value numeric(20,2),
    pay_penalty_to numeric(20,2),
    penalty_paying_date date,
    bill_charge_value_to numeric(20,2),
    pay_bill_rate_to numeric(20,2),
    bill_charge_date_to date
);


ALTER TABLE staging.detail_account_receivable OWNER TO postgres;

--
-- Name: vw_ardetail; Type: MATERIALIZED VIEW; Schema: staging; Owner: postgres
--

CREATE MATERIALIZED VIEW staging.vw_ardetail AS
 SELECT detail_account_receivable.account_receivable_id AS id,
    detail_account_receivable.installment_to AS installment_no,
    detail_account_receivable.due_date_to AS due_date,
    detail_account_receivable.principal_to AS principal_amnt,
    detail_account_receivable.interest_installment_to AS interest_amnt,
    detail_account_receivable.principal_payments_to AS principal_amnt_paid,
    detail_account_receivable.pay_interest AS interest_amnt_paid,
    detail_account_receivable.installment_payday_to AS payment_date,
    detail_account_receivable.penalty_value AS penalty_amnt,
    detail_account_receivable.pay_penalty_to AS penalty_amnt_paid,
    detail_account_receivable.penalty_paying_date AS penalty_payment_date,
    detail_account_receivable.bill_charge_value_to AS collectionfee,
    detail_account_receivable.pay_bill_rate_to AS collectionfee_paid,
    detail_account_receivable.bill_charge_date_to AS collectionfee_payment_date
   FROM staging.detail_account_receivable
  WITH NO DATA;


ALTER TABLE staging.vw_ardetail OWNER TO postgres;

--
-- Name: vw_arversion; Type: MATERIALIZED VIEW; Schema: staging; Owner: postgres
--

CREATE MATERIALIZED VIEW staging.vw_arversion AS
 SELECT account_receivable.no_contract,
    max(account_receivable.version) AS version
   FROM staging.account_receivable
  GROUP BY account_receivable.no_contract
  WITH NO DATA;


ALTER TABLE staging.vw_arversion OWNER TO postgres;

--
-- Name: vw_armaster; Type: MATERIALIZED VIEW; Schema: staging; Owner: postgres
--

CREATE MATERIALIZED VIEW staging.vw_armaster AS
 SELECT ar1.id,
    ar1.bussines_unit_name AS businessunit,
    ar1.office_code,
    ar1.contract_date,
    ar1.no_contract AS contract_no,
    ar2.version,
    ar1.name_customer AS cust_name,
    ar1.debt_principal AS outstanding_principal_amnt,
    ar1.total_interest AS outstanding_interest_amnt,
    ar1.month_lenght AS top_installment,
    ar1.percentage_eff AS effective_rate,
    ar1.percentage_flat AS flat_rate,
    ar1.monthly_installment AS installment_monthly,
    ar1.customer_address AS cust_address,
    ar1.billing_address AS cust_billingaddress,
    ar1.zipcode,
    ar1.sub_zipcode AS subzipcode,
    ar1.no_phone1 AS phone1,
    ar1.no_phone2 AS phone2,
    ' '::text AS brand,
    ' '::text AS model,
    ' '::text AS color,
    ' '::text AS nomor_rangka,
    ' '::text AS nomor_mesin,
    ' '::text AS nomor_polisi,
    ' '::text AS stnk_an,
    ' '::text AS additional_info
   FROM (staging.account_receivable ar1
     JOIN staging.vw_arversion ar2 ON ((((ar1.no_contract)::text = (ar2.no_contract)::text) AND (ar1.version = ar2.version))))
  WITH NO DATA;


ALTER TABLE staging.vw_armaster OWNER TO postgres;

--
-- Name: role id; Type: DEFAULT; Schema: auth; Owner: postgres
--

ALTER TABLE ONLY auth.role ALTER COLUMN id SET DEFAULT nextval('auth.role_id_seq'::regclass);


--
-- Name: user id; Type: DEFAULT; Schema: auth; Owner: postgres
--

ALTER TABLE ONLY auth."user" ALTER COLUMN id SET DEFAULT nextval('auth.user_id_seq'::regclass);


--
-- Name: lkptrx id; Type: DEFAULT; Schema: hist; Owner: postgres
--

ALTER TABLE ONLY hist.lkptrx ALTER COLUMN id SET DEFAULT nextval('hist.lkptrx_id_seq'::regclass);


--
-- Name: lkrtrx id; Type: DEFAULT; Schema: hist; Owner: postgres
--

ALTER TABLE ONLY hist.lkrtrx ALTER COLUMN id SET DEFAULT nextval('hist.lkrtrx_id_seq'::regclass);


--
-- Name: area_zip id; Type: DEFAULT; Schema: param; Owner: postgres
--

ALTER TABLE ONLY param.area_zip ALTER COLUMN id SET DEFAULT nextval('param.area_zip_id_seq'::regclass);


--
-- Name: bizu id; Type: DEFAULT; Schema: param; Owner: postgres
--

ALTER TABLE ONLY param.bizu ALTER COLUMN id SET DEFAULT nextval('param.bizu_id_seq'::regclass);


--
-- Name: coll id; Type: DEFAULT; Schema: param; Owner: postgres
--

ALTER TABLE ONLY param.coll ALTER COLUMN id SET DEFAULT nextval('param.coll_id_seq'::regclass);


--
-- Name: coll_bizu id; Type: DEFAULT; Schema: param; Owner: postgres
--

ALTER TABLE ONLY param.coll_bizu ALTER COLUMN id SET DEFAULT nextval('param.coll_bizu_id_seq'::regclass);


--
-- Name: coll_drmt id; Type: DEFAULT; Schema: param; Owner: postgres
--

ALTER TABLE ONLY param.coll_drmt ALTER COLUMN id SET DEFAULT nextval('param.coll_drmt_id_seq'::regclass);


--
-- Name: coll_empl id; Type: DEFAULT; Schema: param; Owner: postgres
--

ALTER TABLE ONLY param.coll_empl ALTER COLUMN id SET DEFAULT nextval('param.coll_empl_id_seq'::regclass);


--
-- Name: coll_zip id; Type: DEFAULT; Schema: param; Owner: postgres
--

ALTER TABLE ONLY param.coll_zip ALTER COLUMN id SET DEFAULT nextval('param.coll_zip_id_seq'::regclass);


--
-- Name: cycle id; Type: DEFAULT; Schema: param; Owner: postgres
--

ALTER TABLE ONLY param.cycle ALTER COLUMN id SET DEFAULT nextval('param.cycle_id_seq'::regclass);


--
-- Name: dcall id; Type: DEFAULT; Schema: param; Owner: postgres
--

ALTER TABLE ONLY param.dcall ALTER COLUMN id SET DEFAULT nextval('param.dcall_id_seq'::regclass);


--
-- Name: dcall_empl id; Type: DEFAULT; Schema: param; Owner: postgres
--

ALTER TABLE ONLY param.dcall_empl ALTER COLUMN id SET DEFAULT nextval('param.dcall_empl_id_seq'::regclass);


--
-- Name: empl id; Type: DEFAULT; Schema: param; Owner: postgres
--

ALTER TABLE ONLY param.empl ALTER COLUMN id SET DEFAULT nextval('param.empl_id_seq'::regclass);


--
-- Name: genparam id; Type: DEFAULT; Schema: param; Owner: postgres
--

ALTER TABLE ONLY param.genparam ALTER COLUMN id SET DEFAULT nextval('param.genparam_id_seq'::regclass);


--
-- Name: idcard id; Type: DEFAULT; Schema: param; Owner: postgres
--

ALTER TABLE ONLY param.idcard ALTER COLUMN id SET DEFAULT nextval('param.idcard_id_seq'::regclass);


--
-- Name: lkpcmnt id; Type: DEFAULT; Schema: param; Owner: postgres
--

ALTER TABLE ONLY param.lkpcmnt ALTER COLUMN id SET DEFAULT nextval('param.lkpcmnt_id_seq'::regclass);


--
-- Name: lkpprio id; Type: DEFAULT; Schema: param; Owner: postgres
--

ALTER TABLE ONLY param.lkpprio ALTER COLUMN id SET DEFAULT nextval('param.lkpprio_id_seq'::regclass);


--
-- Name: offi id; Type: DEFAULT; Schema: param; Owner: postgres
--

ALTER TABLE ONLY param.offi ALTER COLUMN id SET DEFAULT nextval('param.offi_id_seq'::regclass);


--
-- Name: prof id; Type: DEFAULT; Schema: param; Owner: postgres
--

ALTER TABLE ONLY param.prof ALTER COLUMN id SET DEFAULT nextval('param.prof_id_seq'::regclass);


--
-- Name: prof_staf id; Type: DEFAULT; Schema: param; Owner: postgres
--

ALTER TABLE ONLY param.prof_staf ALTER COLUMN id SET DEFAULT nextval('param.prof_staf_id_seq'::regclass);


--
-- Name: rem id; Type: DEFAULT; Schema: param; Owner: postgres
--

ALTER TABLE ONLY param.rem ALTER COLUMN id SET DEFAULT nextval('param.rem_id_seq'::regclass);


--
-- Name: rem_empl id; Type: DEFAULT; Schema: param; Owner: postgres
--

ALTER TABLE ONLY param.rem_empl ALTER COLUMN id SET DEFAULT nextval('param.rem_empl_id_seq'::regclass);


--
-- Name: rem_zip id; Type: DEFAULT; Schema: param; Owner: postgres
--

ALTER TABLE ONLY param.rem_zip ALTER COLUMN id SET DEFAULT nextval('param.rem_zip_id_seq'::regclass);


--
-- Name: rmdl_zone id; Type: DEFAULT; Schema: param; Owner: postgres
--

ALTER TABLE ONLY param.rmdl_zone ALTER COLUMN id SET DEFAULT nextval('param.rmdl_zone_id_seq'::regclass);


--
-- Name: zip id; Type: DEFAULT; Schema: param; Owner: postgres
--

ALTER TABLE ONLY param.zip ALTER COLUMN id SET DEFAULT nextval('param.zip_id_seq'::regclass);


--
-- Name: zone id; Type: DEFAULT; Schema: param; Owner: postgres
--

ALTER TABLE ONLY param.zone ALTER COLUMN id SET DEFAULT nextval('param.zone_id_seq'::regclass);


--
-- Name: bucketdetail id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.bucketdetail ALTER COLUMN id SET DEFAULT nextval('public.bucketdetail_id_seq'::regclass);


--
-- Name: delegbucket id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.delegbucket ALTER COLUMN id SET DEFAULT nextval('public.delegbucket_id_seq'::regclass);


--
-- Name: lkdtrx id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.lkdtrx ALTER COLUMN id SET DEFAULT nextval('public.lkdtrx_id_seq'::regclass);


--
-- Name: lkptrx id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.lkptrx ALTER COLUMN id SET DEFAULT nextval('public.lkptrx_id_seq'::regclass);


--
-- Name: lkrtrx id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.lkrtrx ALTER COLUMN id SET DEFAULT nextval('public.lkrtrx_id_seq'::regclass);


--
-- Name: print id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.print ALTER COLUMN id SET DEFAULT nextval('public.print_id_seq'::regclass);


--
-- Name: skpctrx id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.skpctrx ALTER COLUMN id SET DEFAULT nextval('public.skpctrx_id_seq'::regclass);


--
-- Name: skpctrxdet id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.skpctrxdet ALTER COLUMN id SET DEFAULT nextval('public.skpctrxdet_id_seq'::regclass);


--
-- Name: unit id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.unit ALTER COLUMN id SET DEFAULT nextval('public.unit_id_seq'::regclass);


--
-- Name: account_receivable id; Type: DEFAULT; Schema: staging; Owner: postgres
--

ALTER TABLE ONLY staging.account_receivable ALTER COLUMN id SET DEFAULT nextval('staging.account_receivable_id_seq'::regclass);


--
-- Data for Name: role; Type: TABLE DATA; Schema: auth; Owner: postgres
--

COPY auth.role (id, name) FROM stdin;
1	ROLE_SUPERADMIN
3	ROLE_COLLECTION_ADMIN
2	ROLE_COLLECTION_SPV
9	ROLE_DESKCALL_ADMIN
8	ROLE_DESKCALL_SPV
6	ROLE_REMEDIAL_ADMIN
5	ROLE_REMEDIAL_SPV
4	ROLE_COLLECTION_HEAD
7	ROLE_REMEDIAL_HEAD
10	ROLE_DESKCALL_HEAD
\.


--
-- Data for Name: user; Type: TABLE DATA; Schema: auth; Owner: postgres
--

COPY auth."user" (id, pwd, uname, empl_id) FROM stdin;
4	pwd	deskcall_spv	15
8	pwd	remedial_head	26
1	admin	admin	12
10	admin	rem_admin	31
9	admin	coll_adm	10
11	admin	desk_admin	32
5	pwd	remedial_spv	19
13	dead1	deskcall_head	33
14	head1	collection_head	34
2	pwd	collection_spv	11
\.


--
-- Data for Name: user_roles; Type: TABLE DATA; Schema: auth; Owner: postgres
--

COPY auth.user_roles (user_id, role_id) FROM stdin;
1	1
1	3
1	2
1	9
1	10
1	8
1	7
1	6
1	5
1	4
4	8
4	9
5	5
8	7
10	6
9	3
11	9
13	10
14	4
2	2
\.


--
-- Data for Name: lkptrx; Type: TABLE DATA; Schema: hist; Owner: postgres
--

COPY hist.lkptrx (id, asgnd_to, cmntdate, cmnt_id, owner_id, rundate, cmnttext, spv_id, team_id, cntrno, entry_cd, print_id) FROM stdin;
17	4	\N	\N	4	2017-10-01 00:00:00	\N	11	29	150100000117	\N	\N
18	2	\N	\N	\N	2017-10-01 00:00:00	\N	9	24	150160000117	\N	\N
\.


--
-- Data for Name: lkrtrx; Type: TABLE DATA; Schema: hist; Owner: postgres
--

COPY hist.lkrtrx (id, asgnd_to, cmntdate, cmnt_id, owner_id, rundate, cmnttext, spv_id, team_id, cntrno, entry_cd, print_id) FROM stdin;
\.


--
-- Data for Name: mou; Type: TABLE DATA; Schema: hist; Owner: postgres
--

COPY hist.mou (mouno, mouend, status, moudate, perf, moustart, prof_id) FROM stdin;
\.


--
-- Data for Name: area_zip; Type: TABLE DATA; Schema: param; Owner: postgres
--

COPY param.area_zip (id, coll_empl_id, zip_id) FROM stdin;
\.


--
-- Data for Name: bizu; Type: TABLE DATA; Schema: param; Owner: postgres
--

COPY param.bizu (id, code, name) FROM stdin;
1	NC	New Car
2	NM	New Motorcycle
3	UC	Used Car
4	UM	Used Motorcyle
5	NG	New Gadget
\.


--
-- Data for Name: coll; Type: TABLE DATA; Schema: param; Owner: postgres
--

COPY param.coll (id, name, cycle_id, spv_id, off_id) FROM stdin;
24	TEST 2	1	9	2
31	Collection Team Cycle 2	3	25	2
33	TEAM BARU	1	11	2
34	TEAM1	1	20	2
\.


--
-- Data for Name: coll_bizu; Type: TABLE DATA; Schema: param; Owner: postgres
--

COPY param.coll_bizu (id, bizu_id, coll_id) FROM stdin;
\.


--
-- Data for Name: coll_drmt; Type: TABLE DATA; Schema: param; Owner: postgres
--

COPY param.coll_drmt (id, datein, dateout, empl_id, next_member_id, prev_member_id, dload, mload) FROM stdin;
1	2018-04-13 16:34:42.146	2018-04-13 16:43:02.264	1	24	24	0	0
3	2018-04-13 16:44:35.083	2018-04-13 16:45:10.088	1	24	24	0	0
10	2018-05-24 15:46:25.792	2018-05-24 15:47:10.834	5	29	29	0	0
12	2018-06-11 16:42:21.376	2018-06-11 16:45:28.034	30	38	38	0	0
11	2018-06-11 16:39:19.037	2018-06-11 16:47:10.071	1	24	24	0	0
13	2018-06-11 16:47:14.507	2018-06-11 16:54:29.167	7	31	31	0	0
\.


--
-- Data for Name: coll_empl; Type: TABLE DATA; Schema: param; Owner: postgres
--

COPY param.coll_empl (id, dload, mload, coll_id, empl_id) FROM stdin;
25	12	100	24	2
30	15	100	31	6
29	15	100	31	5
39	3	50	33	3
40	4	50	33	4
41	3	60	34	8
42	3	60	34	29
38	12	100	31	30
24	15	100	24	1
31	14	100	24	7
\.


--
-- Data for Name: coll_zip; Type: TABLE DATA; Schema: param; Owner: postgres
--

COPY param.coll_zip (id, coll_empl_id, zip_id) FROM stdin;
1	29	3
2	29	4
3	29	5
4	29	1
5	30	9
6	30	8
7	30	6
8	30	7
12	38	6
13	39	1
14	39	2
15	40	3
16	40	4
17	41	5
18	41	6
19	42	7
20	42	8
\.


--
-- Data for Name: cycle; Type: TABLE DATA; Schema: param; Owner: postgres
--

COPY param.cycle (id, code, descr, name, ovdend, ovdstart, type) FROM stdin;
1	C0	Contract overdue 4-30 days	Collector C0	30	4	C
2	C1	Contract overdue 31-60 days	Collector C1	60	31	C
3	C2	Contract overdue 61-90 days	Collector C2	90	61	C
5	RMD	Contract overdue 91-120 days	Remedial	120	91	R
4	DSC	Contract (pre-)overdue 3 days	Desk Call	3	-3	D
8	PRO	Contract overdue more than 120 days	Proffesional Collector	999	121	R
\.


--
-- Data for Name: cycletype; Type: TABLE DATA; Schema: param; Owner: postgres
--

COPY param.cycletype (type, descr) FROM stdin;
C	Collection
D	Desk Call
R	Remedial
\.


--
-- Data for Name: dcall; Type: TABLE DATA; Schema: param; Owner: postgres
--

COPY param.dcall (id, name, cycle_id, off_id, spv_id) FROM stdin;
4	TEAM DESKCALL	4	2	15
\.


--
-- Data for Name: dcall_empl; Type: TABLE DATA; Schema: param; Owner: postgres
--

COPY param.dcall_empl (id, dload, mload, dcall_id, empl_id) FROM stdin;
14	2	60	4	13
16	2	12	4	14
\.


--
-- Data for Name: empl; Type: TABLE DATA; Schema: param; Owner: postgres
--

COPY param.empl (id, name, regno, off_id, job_cd) FROM stdin;
2	Collector 2	00002	2	COLL
3	Collector 3	00003	2	COLL
4	Collector 4	00004	2	COLL
5	Collector 5	00005	2	COLL
6	Collector 6	00006	2	COLL
7	Collector 7	00007	2	COLL
8	Collector 8	00008	2	COLL
1	Collector 1	00001	2	COLL
9	Supervisor A	SPV_A	2	COLLSPV
11	Supervisor C	SPV_C	2	COLLSPV
13	Desk Call 1	DESK1	2	DESK
14	Desk Call 2	DESK2	2	DESK
15	Supervisor Desk Call	SPVDC	2	DESKSPV
19	Remedial SPV	REMSP	2	REMSPV
16	Remedial 1	REM01	2	REM
17	Remedial 2	REM02	2	REM
18	Remedial 3	REM03	2	REM
20	Supervisor E	SPV_E	2	COLLSPV
21	Deskcall 3	DESK3	2	DESK
22	Deskcall 4	DESK4	2	DESK
23	SPV Deskcall 2	SPVD2	2	DESKSPV
24	Remedial SPV 2	SPVR2	2	REMSPV
25	Supervisor F	SPV_F	2	COLLSPV
26	Remedial head	REMHD	2	REMHD
27	Remedial 4	REM04	2	REM
28	Remedial 5	REM05	2	REM
29	Collector 9	00009	2	COLL
30	Collector 10	00010	2	COLL
31	Remedial Admin	REMAD	2	REMADM
10	Supervisor B	SPV_B	2	COLLADM
32	Deskcall Admin	DESKA	2	DESKADM
33	Deskcall Head	DESKH	2	DESKHD
34	Collection Head	COLLH	2	COLLHD
35	Remedial 6	REM06	2	REM
12	Supervisor D	SPV_D	2	REMHD
36	Supervisor G	SPV_G	2	COLLSPV
37	Supervisor 1	SPV_1	2	COLLSPV
38	Collector 11	coll0	2	COLL
39	Collector 12	00012	2	COLL
40	Supervisor Deskcall 2	002SP	2	DESKSPV
\.


--
-- Data for Name: genparam; Type: TABLE DATA; Schema: param; Owner: postgres
--

COPY param.genparam (id, lkp_max_entry_days, skpc_extended_validity_period, skpc_max_extend, skpc_max_idle_days, skpc_max_outstanding_load, skpc_validity_period, company_name, company_address, mou_min_performance, report_dir) FROM stdin;
1	1	7	2	3	15	14	PT. Serasi Raya Autofinance	Jl. Raya No. 1	60	C:\\\\Users\\\\admin\\\\Desktop\\\\MultiFinance\\\\MFCollectionJPAServer\\\\src\\\\main\\\\resources\\\\report\\\\
\.


--
-- Data for Name: idcard; Type: TABLE DATA; Schema: param; Owner: postgres
--

COPY param.idcard (id, name) FROM stdin;
\.


--
-- Data for Name: job; Type: TABLE DATA; Schema: param; Owner: postgres
--

COPY param.job (code, level, name) FROM stdin;
COLL	0	Collector
COLLSPV	1	Collection Supervisor
COLLHD	2	Collection Head
REM	0	Remedial Collector
REMSPV	1	Remedial Supervisor
REMHD	2	Remedial Head
DESK	0	Deskcall
DESKSPV	1	Deskcall Supervisor
DESKHD	2	Deskcall Head
COLLADM	1	Collection Admin
REMADM	1	Remedial Admin
DESKADM	1	Deskcall Admin
\.


--
-- Data for Name: lkdentry; Type: TABLE DATA; Schema: param; Owner: postgres
--

COPY param.lkdentry (code, descr) FROM stdin;
21	Unable To Contact - Tidak diangkat
22	Unable To Contact - Tidak aktif
23	Unable To Contact - Reject to customer
33	Bad Phone - Belum terpasang
32	Bad Phone - Salah nomor telpon
31	Bad Phone - Salah sambung
11	Titip pesan bila yang menerima bukan konsumen sendiri - Anggota keluarga
12	Titip pesan bila yang menerima bukan konsumen sendiri - Teman rumah
13	Titip pesan bila yang menerima bukan konsumen sendiri - Teman kantor
\.


--
-- Data for Name: lkpcmnt; Type: TABLE DATA; Schema: param; Owner: postgres
--

COPY param.lkpcmnt (id, code, descr, date, reason, name) FROM stdin;
\.


--
-- Data for Name: lkpentry; Type: TABLE DATA; Schema: param; Owner: postgres
--

COPY param.lkpentry (code, descr, date, reason, name) FROM stdin;
BCO	Bad Collection adalah parameter yang dipilih untuk konsumen yang tidak bisa dikunjungi karena alamat collection yang tidak jelas atau salah. Dibutuhkan penjelasan kenapa konsumen ini termasuk bad collection.	f	t	Bad Collection
COL	Collected adalah parameter yang digunakan jika konsumen berhasil ditagih (pembayaran melalui Collector).	f	f	Collected
NEW	New Assignment adalah status LKP baru, sebagai penugasan terhadap Collector.	f	f	New Assignment
PCU	Pick Up adalah parameter yang digunakan jika dilakukan penarikan atas barang jaminan. Dibutuhkan alasan kenapa dilakukan penarikan serta tanggal dilakukan penarikan.	t	t	Pick Up
PRE	Promise to Terminate adalah janji untuk melunasi angsuran sampai closed. Dibutuhkan alasan dari konsumen dan tanggal kapan akan dilakukan pelunasan.	t	t	Promise to Terminate
PTC	Promise to be Collected adalah janji untuk melakukan pembayaran melalui Collector. Dibutuhkan alasan konsumen membuat janji dan tanggal kapan akan melakukan pembayaran.	t	t	Promise to be Collected
PTD	Pay Today Directly adalah parameter yang digunakan jika konsumen menyatakan akan membayar hari ini langsung ke Kasir ataupun ke Payment Point.	f	f	Pay Today Directly
PTP	Promise to Pay adalah janji untuk melakukan pembayaran melalui Kasir ataupun Payment Point. Dibutuhkan alasan konsumen membuat janji dan tanggal kapan akan melakukan pembayaran.	t	t	Promise to Pay
UNV	Unvisited adalah parameter yang digunakan untuk konsumen yang tidak dikunjungi. Dibutuhkan tanggal kapan akan dikunjungi oleh Collector.	t	f	Unvisited
\.


--
-- Data for Name: lkpprio; Type: TABLE DATA; Schema: param; Owner: postgres
--

COPY param.lkpprio (id, code, descr, script, seqno, type) FROM stdin;
1	N01	Kontrak baru masuk bucket	rundate IS NULL	1	N
4	N04	Kontrak dengan status ('PTP', 'PTC') yang sesuai promise date-nya	entry_status in ('PTP', 'PTC') AND comment_date = <rundate>	4	N
2	N02	Kontrak pindah cycle team	(fnGet_CycleCode_ByTeam(team_id) <> fnGet_CycleCode_ByDate('C', <rundate>, leastduedate)) AND NOT (entry_status IN ('PTP', 'PTC'))	2	N
5	P01	\N	FALSE	1	P
3	N03	Kontrak belum diprint	print_id IS NULL	3	N
\.


--
-- Data for Name: lkrentry; Type: TABLE DATA; Schema: param; Owner: postgres
--

COPY param.lkrentry (code, name, descr) FROM stdin;
P10	Pindah Alamat	Pindah alamat diketahui
M42	Motor Hilang	Motor hilang klaim ditolak
K47	Kontrak Fiktif	Kontrak fiktif
B49	Bencana Alam	Bencana alam
P30	Pindah Alamat	Pindah alamat tidak diketahui
B11	Belum Terkunjungi	Belum terkunjungi
J11	Janji Bayar	Janji bayar
A3	Atas Nama	Atas nama
M20	Musibah	Musibah
S21	Sakit	Sakit
B23	Bangkrut/PHK	Bangkrut/PHK
I24	Itikad Buruk	Itikad buruk
M25	Meninggal	Meninggal
C26	Dipenjara	Customer dipenjara
A17	Alamat Tdk Jelas	Alamat tidak jelas
D28	Dispute	Customer sudah bayar tetapi belum diterima
P40	Pindah Tangan	Pindah tangan / gadai
M41	Dicuri	Motor dicuri / asuransi
R45	Raib, Keluarga Ada	Customer melarikan diri, keluarga ada
R46	Raib	Customer melarikan diri
A48	Alamat Fiktif	Alamat fiktif
M50	Motor BB	Motor sebagai barang bukti
C99	Collected	Collected
P90	Pick Up	Pick up
O10	Orang Hilang	Orang hilang
\.


--
-- Data for Name: offi; Type: TABLE DATA; Schema: param; Owner: postgres
--

COPY param.offi (id, addr, code, name, phone, pic_addr, pic_email, pic_name, pic_phone, type, skpc_fee) FROM stdin;
1	\N	10100	JAKARTA	\N	\N	\N	\N	\N	\N	750000
2	Jl. Margonda Depok	10500	DEPOK	\N	Jl. Margonda Depok	\N	Nama Kepala Cabang Depok	\N	\N	750000
\.


--
-- Data for Name: prof; Type: TABLE DATA; Schema: param; Owner: postgres
--

COPY param.prof (id, addr, email, idno, moudate, mouend, mouno, moustart, name, perf, phone, type, status, off_id, code) FROM stdin;
5	Jl. Mata Elang	sfsfs	12345	2018-05-22	2018-05-22	10500180409MUMATAE	2018-05-22	MATA ELANG	0	12345	1	1	2	MATAE
12	JL KAKAP	Abc@yaoo.com	15125316437547	2018-06-06	2018-07-31	10500180606MUXXX  	2018-06-06	PROF COLL BARU	0	087284215	0	1	2	XXX
\.


--
-- Data for Name: prof_staf; Type: TABLE DATA; Schema: param; Owner: postgres
--

COPY param.prof_staf (id, address, email, idcard_no, name, phone, prof_id) FROM stdin;
1	sfsdfsgq	fsfsd	KTP 1	Profcoll 1	dsdgsg	5
2	asfsaf	fsgdg	KTP 2	Profcoll 2	fsdfsg	5
9	JL KAKAP	Abc@yaoo.com	15125316437547	PROF COLL BARU	087284215	12
\.


--
-- Data for Name: rem; Type: TABLE DATA; Schema: param; Owner: postgres
--

COPY param.rem (id, name, cycle_id, off_id, spv_id) FROM stdin;
2	TEAM REMEDIAL	5	2	19
\.


--
-- Data for Name: rem_empl; Type: TABLE DATA; Schema: param; Owner: postgres
--

COPY param.rem_empl (id, dload, mload, empl_id, rem_id) FROM stdin;
25	7	30	16	2
\.


--
-- Data for Name: rem_zip; Type: TABLE DATA; Schema: param; Owner: postgres
--

COPY param.rem_zip (id, rem_empl_id, zip_id) FROM stdin;
30	25	3
31	25	4
\.


--
-- Data for Name: rmdl_zone; Type: TABLE DATA; Schema: param; Owner: postgres
--

COPY param.rmdl_zone (id, rmdl_empl_id, zone_id) FROM stdin;
\.


--
-- Data for Name: stts; Type: TABLE DATA; Schema: param; Owner: postgres
--

COPY param.stts (id, descr) FROM stdin;
0	inactive
1	active
3	idle
4	close
5	freeze
2	not active / expired
\.


--
-- Data for Name: workingarea; Type: TABLE DATA; Schema: param; Owner: postgres
--

COPY param.workingarea (member_id, zipcode_id) FROM stdin;
\.


--
-- Data for Name: zip; Type: TABLE DATA; Schema: param; Owner: postgres
--

COPY param.zip (id, name, subzipcd, zipcd, off_id, zone_id) FROM stdin;
1	DEPOK UTARA		19456	2	\N
2	DEPOK SELATAN		86253	2	\N
3	\N		13456	2	\N
4	\N	01	13456	2	\N
5	\N		15431	2	\N
6	\N	01	18456	2	\N
7	\N		31662	2	\N
8	\N		12389	2	\N
9	\N		11456	2	\N
10	\N		13900	2	\N
11	\N		14580	2	\N
12	\N	01	13356	2	\N
13	\N		14456	2	\N
14	\N		13440	2	\N
15	\N	01	13440	2	\N
16	\N		95995	2	\N
17	\N		22765	2	\N
18	\N		13442	2	\N
19	\N		12009	2	\N
20	\N		14980	2	\N
21	\N		13568	2	\N
22	\N		13340	2	\N
23	\N	01	12760	2	\N
24	\N		13455	2	\N
25	\N	01	19956	2	\N
26	\N	01	89456	2	\N
27	\N		17980	2	\N
28	\N	01	15456	2	\N
29	\N		19556	2	\N
30	\N		19756	2	\N
31	\N		17456	2	\N
32	\N		12980	2	\N
33	\N		13450	2	\N
34	\N		14345	2	\N
35	\N		14330	2	\N
36	\N		14340	2	\N
37	\N		13222	2	\N
38	\N		13662	2	\N
39	\N		12990	2	\N
40	\N		59456	2	\N
41	\N		13278	2	\N
42	\N	01	17466	2	\N
43	\N		11466	2	\N
44	\N		18980	2	\N
45	\N		16980	2	\N
46	KEL. DEPOK	01	16431	1	\N
\.


--
-- Data for Name: zone; Type: TABLE DATA; Schema: param; Owner: postgres
--

COPY param.zone (id, code, name, off_id) FROM stdin;
\.


--
-- Data for Name: bucket; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.bucket (cntrno, subzipcd, bizu, cntrdate, custaddr, custbilladdr, custname, lastduedate, lastinstlno, leastduedate, leastinstlno, minstl, officd, outsdintamnt, outsdpriamnt, ovd, phone1, phone2, srcid, topinstl, zipcd, cycle_id) FROM stdin;
150110000117	\N	NC	2017-08-12	Jl.Markatim 2	Jl.Markatim	Andi	2017-09-12	1	2017-09-12	1	41898333	10500	98370000	655800000	19	\N	\N	12	18	13450	1
150120000117	\N	NM	2017-01-19	Jl.awan	Jl.awan	Yanuar	2017-09-19	8	2017-02-19	1	11925833	10500	1301000	130100000	224	\N	\N	13	12	13455	8
150040000117	\N	NM	2017-07-29	Jl.Kepriben Raya 3	Jl.Kepriben Raya 3	Saipul	2017-10-01	3	2017-08-01	1	2260000	10500	9040000	45200000	61	\N	\N	5	24	95995	3
150030000117	\N	NC	2017-07-25	Jl.Pendek	Jl.Pendek	Yoko	2017-10-01	3	2017-08-01	1	11925833	10500	1301000	130100000	61	\N	\N	4	12	14330	3
150070000117	\N	NC	2017-08-10	Jl.Kita 3	jl.Dia 5	Abdul	2017-09-10	3	2017-07-10	1	8950833	10500	21015000	140100000	83	\N	\N	8	18	31662	3
150160000117	\N	NC	2017-08-05	Jl.Sentosa2	Jl.Sentosa2	Dedi	2017-09-05	1	2017-09-05	1	63483842	10500	69255100	692551000	26	\N	\N	17	12	13662	1
150050000117	\N	NM	2017-08-02	Jl.Nusuk Duri 4	Jl.Nusuk Duri 4	Titis	2017-10-01	2	2017-09-01	1	1682083	10500	1835000	18350000	30	\N	\N	6	12	22765	1
150010000117	\N	NC	2017-04-17	Jl.Mundur1	Jl.Maju1	Yadi	2017-09-13	5	2017-05-13	1	8035000	10500	32140000	160700000	141	\N	\N	2	24	14345	8
150140000117	\N	NM	2017-07-18	Jl.Kakatua	Jl.Kakatua	Atun	2017-09-18	2	2017-08-18	1	1169167	10500	2745000	18300000	44	\N	\N	15	18	13440	2
150100000117	\N	UM	2017-07-17	Jl.Taman Tekno 1	Jl.Taman Tekno 1	Dicky	2017-09-17	2	2017-08-17	1	1078917	10500	1177000	11770000	45	\N	\N	11	12	15431	2
150150000117	\N	NM	2017-08-04	Jl.Anuan	Jl.Anuan	Andre	2017-10-04	2	2017-09-04	1	1150508	10500	1255100	12551000	27	\N	\N	16	12	13442	1
150180000117	\N	UC	2017-08-27	Jl.Slamet	Jl.Slamet	Sukirman	2017-09-27	1	2017-09-27	1	7038717	10500	7678600	76786000	4	\N	\N	19	12	13568	1
105200000117	\N	NC	2017-05-13	Jl.Jalan	Jl.Jalan1	Yanto	2017-09-13	4	2017-06-13	1	7342500	10500	8010000	80100000	110	\N	\N	1	12	14340	5
150090000117	\N	UC	2017-04-15	Jl.Antutu 2	Jl.Antutu 2	Adul	2017-10-01	6	2017-05-01	1	3210000	10500	12840000	64200000	153	\N	\N	10	24	13340	8
150020000117	\N	NC	2017-07-21	Jl.Merdeka1	Jl.Maerdeka1	Yanuar	2017-10-01	3	2017-08-01	1	8950833	10500	21015000	140100000	61	\N	\N	3	18	31662	3
150080000117	\N	NG	2017-07-20	Jl.Kereta	Jl.Tut-Tut-tut	Yuni	2017-10-01	3	2017-08-01	1	987250	10500	1077000	10770000	61	\N	\N	9	12	31662	3
150060000117	\N	NC	2017-08-06	Jl.Nubruk Pohon 2	Jl.Ayam Mie 4	Amar	2017-09-06	1	2017-09-06	1	7342500	10500	8010000	80100000	25	\N	\N	7	12	86253	1
150130000117	\N	NG	2017-08-14	Jl.Ananda	Jl.Ananda	Alek	2017-09-14	1	2017-09-14	1	1425508	10500	1555100	15551000	17	\N	\N	14	12	12389	1
150190000117	\N	UM	2017-08-24	Jl.Anget	Jl.Anget	Kjelberg	2017-09-24	1	2017-09-24	1	671000	10500	732000	7320000	7	\N	\N	20	12	13456	1
\.


--
-- Data for Name: bucketdetail; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.bucketdetail (id, collfee, collfeepaid, collfeepaydate, duedate, instlno, intamnt, intamntpaid, ovd, paydate, pnltamnt, pnltamntpaid, pnltpaydate, priamnt, priamntpaid, cntrno) FROM stdin;
146	6000	500	\N	2017-09-14	2	25000	4000	10	\N	5000	3000	\N	1187775	2000	150130000117
133	43000	9000	\N	2017-09-01	5	841121	4000	30	\N	5000	3000	\N	2368879	6000	150090000117
132	32000	8000	\N	2017-10-01	6	778942	5000	0	\N	3000	500	\N	2431058	4000	150090000117
136	41000	8000	\N	2017-06-01	2	916838	7000	122	\N	32000	9000	\N	2293162	4000	150090000117
137	10000	5000	\N	2017-05-01	1	981468	7000	153	\N	4000	3000	\N	2228532	6000	150090000117
143	25000	3000	\N	2017-08-01	1	164648	3000	61	\N	9000	8000	\N	822602	4000	150080000117
118	23000	4000	\N	2017-07-13	3	2283698	4000	80	\N	7000	4000	\N	5751302	5000	150010000117
126	7000	3000	\N	2017-09-04	1	191876	3000	27	\N	5000	500	\N	958633	5000	150150000117
125	9000	7000	\N	2017-10-04	2	171504	4400	-3	\N	5000	4000	\N	979005	5000	150150000117
124	8000	3000	\N	2017-08-17	1	179936	6000	45	\N	8000	6000	\N	898981	9000	150100000117
131	12000	7000	\N	2017-06-13	1	1224542	7000	110	\N	4000	400	\N	6117958	8000	105200000117
130	34000	5000	\N	2017-07-13	2	1094529	7000	80	\N	45000	5000	\N	6247971	7000	105200000117
127	23000	5000	\N	2017-09-27	1	1173879	2000	4	\N	30000	4000	\N	5864838	13000	150180000117
115	45000	5000	\N	2017-09-01	1	280529	3000	30	\N	4000	1000	\N	1401555	10000	150050000117
114	34000	5000	\N	2017-10-01	2	250744	7000	0	\N	6000	4000	\N	1431339	3000	150050000117
119	34000	6000	\N	2017-06-13	2	2294952	6000	110	\N	35000	5000	\N	5740048	4999	150010000117
120	21000	9000	\N	2017-05-13	1	2456729	8000	141	\N	23000	5000	\N	5578271	7000	150010000117
129	40000	5000	\N	2017-08-13	3	1035496	8000	49	\N	5000	4000	\N	6307004	9000	105200000117
121	45000	5000	\N	2017-09-18	2	257581	6000	13	\N	45000	8000	\N	911585	8000	150140000117
142	53000	3000	\N	2017-09-01	2	147167	3000	30	\N	8800	6700	\N	840083	5000	150080000117
140	32000	3000	\N	2017-08-01	1	2141803	4000	61	\N	7000	4000	\N	6809031	6000	150020000117
141	56000	4000	\N	2017-10-01	3	139230	4000	0	\N	8000	4000	\N	848020	6000	150080000117
138	24000	4000	\N	2017-10-01	3	1931018	5000	0	\N	35000	9000	\N	7019815	9000	150020000117
139	22000	2000	\N	2017-09-01	2	1971976	5000	30	\N	45000	5000	\N	6978857	8000	150020000117
134	65000	7000	\N	2017-08-01	4	848919	5000	61	\N	5000	3000	\N	2361081	5999	150090000117
135	80000	6000	\N	2017-07-01	3	912342	6000	92	\N	30000	5000	\N	2297658	4300	150090000117
145	55000	5000	\N	2017-09-14	1	237739	2000	17	\N	66000	4400	\N	1187770	2000	150130000117
128	55000	5000	\N	2017-09-13	4	908784	6000	18	\N	7000	6000	\N	6433716	8000	105200000117
144	8000	5000	\N	2017-09-06	1	1224542	2000	25	\N	7700	5500	\N	6117958	3000	150060000117
116	34000	5000	\N	2017-09-13	5	2105423	4000	18	\N	9000	6000	\N	5929577	10000	150010000117
105	20000	5000	\N	2017-09-01	2	645500	4000	30	\N	4500	600	\N	1614500	4000	150040000117
104	20000	4000	\N	2017-10-01	3	642334	3000	0	\N	5500	3000	\N	1617666	7000	150040000117
107	40000	4000	\N	2017-10-01	3	1681873	7000	0	\N	6000	5500	\N	10243960	3500	150030000117
106	30000	5000	\N	2017-08-01	1	691003	5000	61	\N	32000	8000	\N	1568997	3000	150040000117
109	15000	5000	\N	2017-08-01	1	1988926	6000	61	\N	47000	35000	\N	9936907	5000	150030000117
108	12000	3000	\N	2017-09-01	2	1777755	8000	30	\N	4000	1000	\N	10148078	4000	150030000117
111	23000	3000	\N	2017-08-10	2	1971976	5000	52	\N	14000	5000	\N	6978857	8000	150070000117
110	13000	6000	\N	2017-09-10	3	1931018	7000	21	\N	7000	5000	\N	7019815	8000	150070000117
113	43000	3000	\N	2017-09-05	1	10587492	8000	26	\N	4000	3000	\N	52896350	4000	150160000117
112	50000	5000	\N	2017-07-10	1	2141803	9000	83	\N	32000	19000	\N	6809031	6000	150070000117
95	10000	4000	\N	2017-09-12	1	10025655	800	19	\N	5000	3000	\N	31872679	8000	150110000117
94	10000	5000	\N	2017-09-24	1	111906	700	7	\N	8000	5000	\N	559094	9000	150190000117
97	10000	4000	\N	2017-08-19	7	1039567	69990	43	\N	6000	5000	\N	10886266	7000	150120000117
96	10000	4000	\N	2017-09-19	8	844976	7000	12	\N	4000	2000	\N	11080858	9000	150120000117
99	20000	5000	\N	2017-06-19	5	1662319	6500	104	\N	7000	5000	\N	10560319	8000	150120000117
98	20000	5000	\N	2017-07-19	6	1365514	7000	74	\N	4000	3000	\N	10760603	7000	150120000117
101	20000	6000	\N	2017-04-19	3	1681873	4400	165	\N	44000	31000	\N	10243960	4000	150120000117
100	30000	5000	\N	2017-05-19	4	1476065	5600	135	\N	40000	35000	\N	10449768	6000	150120000117
103	40000	10000	\N	2017-02-19	1	1988926	3200	224	\N	4500	600	\N	9936907	9000	150120000117
102	20000	7000	\N	2017-03-19	2	1777755	4500	196	\N	8000	3000	\N	10148078	6600	150120000117
117	18000	8000	\N	2017-08-13	4	2124943	3000	49	\N	45000	6000	\N	5910057	7000	150010000117
122	23000	5000	\N	2017-08-18	1	279764	6000	44	\N	56000	43000	\N	889402	5500	150140000117
123	12000	6000	\N	2017-09-17	2	160832	7000	14	\N	53000	9000	\N	918085	8000	150100000117
\.


--
-- Data for Name: delegbucket; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.delegbucket (id, subzipcd, bizu, cntrdate, cntrno, custaddr, custbilladdr, custname, lastduedate, lastinstlno, leastduedate, leastinstlno, memono, minstl, officd, outsdintamnt, outsdpriamnt, phone1, phone2, memodate, topinstl, zipcd, rundate) FROM stdin;
1	\N	NC	2017-05-13	105200000117	Jl.Jalan	Jl.Jalan1	Yanto	2017-08-13	3	2017-06-13	1	Memo	7342500	10500	8010000	80100000	\N	\N	2018-02-06	12	14340	2018-02-06
2	\N	NC	2017-07-25	150030000117	Jl.Pendek	Jl.Pendek	Yoko	2017-09-01	2	2017-08-01	1	Memo	11925833	10500	1301000	130100000	\N	\N	2018-02-05	12	14330	2018-02-06
3	\N	NC	2017-08-10	150070000117	Jl.Kita 3	jl.Dia 5	Abdul	2017-08-10	2	2017-07-10	1	12345	8950833	10500	21015000	140100000	\N	\N	1970-01-01	18	31662	2018-02-08
4	\N	NM	2017-07-29	150040000117	Jl.Kepriben Raya 3	Jl.Kepriben Raya 3	Saipul	2017-09-01	2	2017-08-01	1	12345	2260000	10500	9040000	45200000	\N	\N	1970-01-01	24	95995	2018-02-08
5	\N	NG	2017-07-20	150080000117	Jl.Kereta	Jl.Tut-Tut-tut	Yuni	2017-09-01	2	2017-08-01	1	12345	987250	10500	1077000	10770000	\N	\N	1970-01-01	12	31662	2018-02-09
6	\N	NM	2017-07-18	150140000117	Jl.Kakatua	Jl.Kakatua	Atun	2017-08-18	1	2017-08-18	1	12345	1169167	10500	2745000	18300000	\N	\N	1970-01-01	18	13440	2018-02-12
7	\N	NC	2017-07-21	150020000117	Jl.Merdeka1	Jl.Maerdeka1	Yanuar	2017-09-01	2	2017-08-01	1	12345	8950833	10500	21015000	140100000	\N	\N	1970-01-01	18	31662	2018-02-12
8	\N	UM	2017-07-17	150100000117	Jl.Taman Tekno 1	Jl.Taman Tekno 1	Dicky	2017-08-17	1	2017-08-17	1	12345	1078917	10500	1177000	11770000	\N	\N	1970-01-01	12	15431	2018-02-13
9	\N	NC	2017-05-13	105200000117	Jl.Jalan	Jl.Jalan1	Yanto	2017-08-13	3	2017-06-13	1	12345	7342500	10500	8010000	80100000	\N	\N	1970-01-01	12	14340	2018-02-13
10	\N	NC	2017-07-25	150030000117	Jl.Pendek	Jl.Pendek	Yoko	2017-09-01	2	2017-08-01	1	12345	11925833	10500	1301000	130100000	\N	\N	1970-01-01	12	14330	2018-02-13
11	\N	NC	2017-08-10	150070000117	Jl.Kita 3	jl.Dia 5	Abdul	2017-08-10	2	2017-07-10	1	12345	8950833	10500	21015000	140100000	\N	\N	1970-01-01	18	31662	2018-02-14
12	\N	NM	2017-07-29	150040000117	Jl.Kepriben Raya 3	Jl.Kepriben Raya 3	Saipul	2017-09-01	2	2017-08-01	1	12345	2260000	10500	9040000	45200000	\N	\N	1970-01-01	24	95995	2018-02-14
13	\N	NG	2017-07-20	150080000117	Jl.Kereta	Jl.Tut-Tut-tut	Yuni	2017-09-01	2	2017-08-01	1	12345	987250	10500	1077000	10770000	\N	\N	1970-01-01	12	31662	2018-02-14
14	\N	NM	2017-07-18	150140000117	Jl.Kakatua	Jl.Kakatua	Atun	2017-08-18	1	2017-08-18	1	12345	1169167	10500	2745000	18300000	\N	\N	1970-01-01	18	13440	2018-02-20
15	\N	NC	2017-07-21	150020000117	Jl.Merdeka1	Jl.Maerdeka1	Yanuar	2017-09-01	2	2017-08-01	1	12345	8950833	10500	21015000	140100000	\N	\N	1970-01-01	18	31662	2018-02-20
16	\N	NC	2017-05-13	105200000117	Jl.Jalan	Jl.Jalan1	Yanto	2017-08-13	3	2017-06-13	1	12345	7342500	10500	8010000	80100000	\N	\N	1970-01-01	12	14340	2018-03-01
17	\N	UM	2017-07-17	150100000117	Jl.Taman Tekno 1	Jl.Taman Tekno 1	Dicky	2017-09-17	2	2017-08-17	1	12345	1078917	10500	1177000	11770000	\N	\N	2018-04-08	12	15431	2018-04-09
18	\N	NC	2017-08-05	150160000117	Jl.Sentosa2	Jl.Sentosa2	Dedi	2017-09-05	1	2017-09-05	1	12345	63483842	10500	69255100	692551000	\N	\N	2018-04-09	12	13662	2018-04-09
19	\N	NC	2017-04-17	150010000117	Jl.Mundur1	Jl.Maju1	Yadi	2017-09-13	5	2017-05-13	1	123	8035000	10500	32140000	160700000	\N	\N	2018-05-25	24	14345	2018-05-25
\.


--
-- Data for Name: lkdtrx; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.lkdtrx (id, rundate, spv_id, team_id, cntrno, entry_cd, print_id) FROM stdin;
6	2017-09-01 00:00:00	15	4	150050000117	\N	\N
5	2017-09-01 00:00:00	15	4	150150000117	\N	\N
\.


--
-- Data for Name: lkptrx; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.lkptrx (id, asgnd_to, cmntdate, cmnt_id, owner_id, rundate, cmnttext, spv_id, team_id, cntrno, entry_cd, print_id) FROM stdin;
43	\N	\N	\N	\N	2017-10-01 00:00:00	\N	\N	\N	150030000117	\N	\N
44	\N	\N	\N	\N	2017-10-01 00:00:00	\N	\N	\N	150070000117	\N	\N
47	\N	\N	\N	\N	2017-10-01 00:00:00	\N	\N	\N	150080000117	\N	\N
50	\N	\N	\N	\N	2017-10-01 00:00:00	\N	\N	\N	150140000117	\N	\N
51	\N	\N	\N	\N	2017-10-01 00:00:00	\N	\N	\N	150020000117	\N	\N
45	\N	\N	\N	\N	2017-10-01 00:00:00	\N	\N	\N	150040000117	\N	\N
46	\N	\N	\N	\N	2017-10-01 00:00:00	\N	9	24	150150000117	\N	\N
41	2	\N	\N	\N	2017-10-01 00:00:00	\N	9	24	150180000117	\N	\N
52	1	\N	\N	\N	2017-10-01 00:00:00	\N	9	24	150110000117	NEW	\N
55	1	\N	\N	\N	2018-04-10 13:18:07.479	\N	9	24	150110000117	COL	\N
54	2	\N	\N	\N	2017-10-01 00:00:00	\N	9	24	150190000117	\N	\N
49	1	2018-05-22	\N	\N	2017-10-01 00:00:00	\N	9	24	150050000117	UNV	\N
42	1	2018-06-30	\N	1	2017-10-01 00:00:00	janji bayar	9	24	150060000117	PTP	\N
48	1	\N	\N	\N	2017-10-01 00:00:00	\N	9	24	150130000117	\N	45
\.


--
-- Data for Name: lkrtrx; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.lkrtrx (id, asgnd_to, cmntdate, cmnt_id, owner_id, rundate, cmnttext, spv_id, team_id, cntrno, entry_cd, print_id) FROM stdin;
2	16	\N	\N	\N	2017-10-01 00:00:00	\N	19	2	105200000117	\N	\N
1	16	\N	\N	\N	2017-10-01 00:00:00	\N	19	2	150010000117	\N	\N
\.


--
-- Data for Name: print; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.print (id, empl_id, printdate, printno, team_id, type) FROM stdin;
31	2	2018-04-10 00:00:00	10500180410CO0000200	24	C
25	1	2018-04-10 05:04:01.455	10500180406CO0000100	24	C
27	1	2018-04-10 10:21:44.005	10500180409CO0000100	24	C
32	1	2018-04-11 11:06:16.237	10500180411CO0000100	24	C
33	1	2018-04-11 12:58:00.096	10500180411CO0000101	24	C
41	1	2018-04-12 13:02:01.387	10500180412PC0000100	5	P
42	1	2018-04-13 11:04:16.515	10500180413CO0000100	24	C
43	\N	2018-05-28 10:51:50.043	10500180528DC0000100	1	D
44	1	2018-06-06 09:49:09.521	10500180606CO0000100	24	C
45	1	2018-06-11 15:28:44.333	10500180611CO0000100	24	C
46	16	2018-06-11 16:01:53.601	10500180611RMREM0100	2	R
47	16	2018-06-11 20:11:10.039	10500180611RMREM0101	2	R
48	1	2018-06-11 20:11:10.039	10500180611PC0000100	5	P
\.


--
-- Data for Name: skpctrx; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.skpctrx (id, asgnd_to, mouno, profcoll_id, refno, result, rundate, cntrno, print_id, status, seqno) FROM stdin;
3	1	10500180409MUMATAE	5	\N	\N	2018-05-22 13:18:22.306	150160000117	\N	1	\N
2	1	10500180409MUMATAE	5	\N	\N	2018-05-22 13:03:49.949	150100000117	\N	1	1
4	\N	\N	\N	\N	\N	2018-05-25 11:25:07.599	150010000117	\N	0	\N
\.


--
-- Data for Name: skpctrxdet; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.skpctrxdet (id, enddate, seqno, startdate, skpctrx_id) FROM stdin;
2	2018-05-25	\N	2018-05-22	3
1	2018-05-21	0	2018-05-15	2
15	2018-05-28	1	2018-05-22	2
\.


--
-- Data for Name: unit; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.unit (id, addinfo, brand, color, engineno, frameno, model, polno, stnk, cntrno) FROM stdin;
54	Tidak ada	Honda	Hitam	D 11 SS	1234	Matic	B 4455 SS	2222222235346	150090000117
57	Tidak ada	Suzuki	Merah	D 11 SS	1234	Sport	B 6666 JL	078078078	150060000117
56	Tidak ada	Suzuki	Merah	D 11 SS	1234	Sport	BK 3322 GG	4574575	150080000117
58	Tidak ada	Yamaha	Putih	D 11 SS	1234	N Max	B 6699 KP	3734734743	150130000117
41	Tidak ada	Suzuki	Hitam	D 11 SS	1234	Bebek	B 9090 L	3523532523	150110000117
40	Tidak ada	Yamaha	Hitam	D 11 SS	1234	N Max	B 8765 K	25151531532	150190000117
43	Tidak ada	Honda	Hitam	D 11 SS	1234	Bebek	BK 9865 HH	151251333	150040000117
42	Tidak ada	 Suzuki	Hitam	D 11 SS	1234	Bebek	B 7654 KL	15125125	150120000117
45	Tidak ada	Honda	Merah	D 11 SS	1234	Matic	B 8744 KO	15135345465	150070000117
44	Tidak ada	Honda	Merah	D 11 SS	1234	Bebek	B 8760 OL	1512535432643	150030000117
47	Tidak ada	Honda	Putih	D 11 SS	1234	Matic	BL 1234 RW	4634757547	150050000117
46	Tidak ada	Yamaha	Hitam	D 11 SS	1234	N Max	B 6784 RT	56856856856	150160000117
49	Tidak ada	Suzuki	Silver	D 11 SS	1234	Matic	BU 4532 WW	458458458	150140000117
48	Tidak ada	Yamaha	Hitam	D 11 SS	1234	Matic	BK 7812 HU	7575754	150010000117
51	Tidak ada	Suzuki	Silver	D 11 SS	1234	Sport	BL 5643 HL	45845845	150150000117
50	Tidak ada	Suzuki	Biru	D 11 SS	1234	Bebek	B 4444 XX	585485484	150100000117
53	Tidak ada	Yamaha	Merah	D 11 SS	1234	N Max	B 5090 KK	22222222222222222	105200000117
52	Tidak ada	Yamaha	Biru	D 11 SS	1234	N Max	BK 8021 YY	458548458	150180000117
\.


--
-- Data for Name: account_receivable; Type: TABLE DATA; Schema: staging; Owner: postgres
--

COPY staging.account_receivable (id, bussines_unit_name, office_code, contract_date, no_contract, name_customer, debt_principal, total_interest, month_lenght, percentage_eff, percentage_flat, monthly_installment, customer_address, billing_address, version, zipcode, sub_zipcode, no_phone1, no_phone2, created_date, created_by, modified_date, modified_by) FROM stdin;
30	NC	10500	2017-08-09	150290000117	Arif	459651100.00	45965110.00	12	18.00	10.00	42134684.00	Jl.Buntu	Jl.Buntu	1	19456	\N	\N	\N	\N	\N	\N	\N
7	NC	10500	2017-08-06	150060000117	Amar	80100000.00	8010000.00	12	18.00	10.00	7342500.00	Jl.Nubruk Pohon 2	Jl.Ayam Mie 4	1	86253	\N	\N	\N	\N	\N	\N	\N
8	NC	10500	2017-08-10	150070000117	Abdul	140100000.00	21015000.00	18	18.00	10.00	8950833.00	Jl.Kita 3	jl.Dia 5	1	31662	\N	\N	\N	\N	\N	\N	\N
20	UM	10500	2017-08-24	150190000117	Kjelberg	7320000.00	732000.00	12	18.00	10.00	671000.00	Jl.Anget	Jl.Anget	1	13456	\N	\N	\N	\N	\N	\N	\N
11	UM	10500	2017-07-17	150100000117	Dicky	11770000.00	1177000.00	12	18.00	10.00	1078917.00	Jl.Taman Tekno 1	Jl.Taman Tekno 1	1	15431	\N	\N	\N	\N	\N	\N	\N
29	NG	10500	2017-08-02	150280000117	Nanda	4550000.00	455000.00	12	18.00	10.00	417083.00	Jl.Mulu	Jl.Mulu	1	18456	01	\N	\N	\N	\N	\N	\N
9	NG	10500	2017-07-20	150080000117	Yuni	10770000.00	1077000.00	12	18.00	10.00	987250.00	Jl.Kereta	Jl.Tut-Tut-tut	1	31662	\N	\N	\N	\N	\N	\N	\N
14	NG	10500	2017-08-14	150130000117	Alek	15551000.00	1555100.00	12	18.00	10.00	1425508.00	Jl.Ananda	Jl.Ananda	1	12389	\N	\N	\N	\N	\N	\N	\N
40	NG	10500	2017-10-19	150390000117	Rasya	9770000.00	977000.00	12	18.00	10.00	895583.00	Jl.Rusak	Jl.Rusak	1	11456	\N	\N	\N	\N	\N	\N	\N
50	NG	10500	2017-08-02	150490000117	Reynaldi	4550000.00	455000.00	12	18.00	10.00	417083.00	Jl.Bagged	Jl.Bagged	1	13900	\N	\N	\N	\N	\N	\N	\N
44	NG	10500	2017-10-19	150430000117	Felix	9770000.00	977000.00	12	18.00	10.00	895583.00	Jl.Gondrong	Jl.Gondrong	1	14580	\N	\N	\N	\N	\N	\N	\N
25	NM	10500	2017-08-05	150240000117	Vijay	100850000.00	10085000.00	12	18.00	10.00	9244583.00	Jl.Salju	Jl.Salju	1	13356	01	\N	\N	\N	\N	\N	\N
26	NM	10500	2017-08-08	150250000117	Surya	22650000.00	2265000.00	12	18.00	10.00	2076250.00	Jl.Petir	Jl.Petir	1	14456	\N	\N	\N	\N	\N	\N	\N
31	NM	10500	2017-10-11	150300000117	Patrick	21350000.00	2135000.00	12	18.00	10.00	1957083.00	Jl.Mekar	Jl.Mekar	1	13440	01	\N	\N	\N	\N	\N	\N
13	NM	10500	2017-01-19	150120000117	Yanuar	130100000.00	1301000.00	12	18.00	10.00	11925833.00	Jl.awan	Jl.awan	1	13455	\N	\N	\N	\N	\N	\N	\N
15	NM	10500	2017-07-18	150140000117	Atun	18300000.00	2745000.00	18	18.00	10.00	1169167.00	Jl.Kakatua	Jl.Kakatua	1	13440	\N	\N	\N	\N	\N	\N	\N
5	NM	10500	2017-07-29	150040000117	Saipul	45200000.00	9040000.00	24	18.00	10.00	2260000.00	Jl.Kepriben Raya 3	Jl.Kepriben Raya 3	1	95995	\N	\N	\N	\N	\N	\N	\N
6	NM	10500	2017-08-02	150050000117	Titis	18350000.00	1835000.00	12	18.00	10.00	1682083.00	Jl.Nusuk Duri 4	Jl.Nusuk Duri 4	1	22765	\N	\N	\N	\N	\N	\N	\N
16	NM	10500	2017-08-04	150150000117	Andre	12551000.00	1255100.00	12	18.00	10.00	1150508.00	Jl.Anuan	Jl.Anuan	1	13442	\N	\N	\N	\N	\N	\N	\N
32	NM	10500	2018-01-12	150310000117	Sandy	19350000.00	1935000.00	12	18.00	10.00	1773750.00	Jl.Beo	Jl.Beo	1	12009	\N	\N	\N	\N	\N	\N	\N
46	NM	10500	2017-10-11	150450000117	Wilshere	21350000.00	2135000.00	12	18.00	10.00	1957083.00	Jl.Lurus	Jl.Lurus	1	14980	\N	\N	\N	\N	\N	\N	\N
49	NM	10500	2017-08-09	150480000117	Ronaldo	100850000.00	10085000.00	12	18.00	10.00	9244583.00	Jl.Chamber	Jl.Chamber	1	14980	\N	\N	\N	\N	\N	\N	\N
19	UC	10500	2017-08-27	150180000117	Sukirman	76786000.00	7678600.00	12	18.00	10.00	7038717.00	Jl.Slamet	Jl.Slamet	1	13568	\N	\N	\N	\N	\N	\N	\N
10	UC	10500	2017-04-15	150090000117	Adul	64200000.00	12840000.00	24	18.00	10.00	3210000.00	Jl.Antutu 2	Jl.Antutu 2	1	13340	\N	\N	\N	\N	\N	\N	\N
23	UC	10500	2017-09-22	150220000117	Obama	143350000.00	14335000.00	12	18.00	10.00	13140417.00	Jl.Adem	Jl.Adem	1	12760	01	\N	\N	\N	\N	\N	\N
24	UC	10500	2017-08-04	150230000117	Kim Jong Un	101350000.00	10135000.00	12	18.00	10.00	9290417.00	Jl.Hujan	Jl.Hujan	1	13455	\N	\N	\N	\N	\N	\N	\N
37	UC	10500	2017-06-16	150360000117	Beth	8350000.00	835000.00	12	18.00	10.00	765417.00	Jl.Siput	Jl.Siput	1	19956	01	\N	\N	\N	\N	\N	\N
39	UC	10500	2017-09-09	150380000117	Anandita	373350000.00	37335000.00	12	18.00	10.00	34223750.00	Jl.Second	Jl.Second	1	89456	01	\N	\N	\N	\N	\N	\N
43	UC	10500	2017-09-09	150420000117	Ian	373350000.00	37335000.00	12	18.00	10.00	34223750.00	Jl.Kresek	Jl.Kresek	1	17980	\N	\N	\N	\N	\N	\N	\N
27	UM	10500	2017-08-04	150260000117	Tommy	8350000.00	835000.00	12	18.00	10.00	765417.00	Jl.Gledek	Jl.Gledek	1	15456	01	\N	\N	\N	\N	\N	\N
35	UM	10500	2017-04-14	150340000117	Steph	50350000.00	5035000.00	12	18.00	10.00	4615417.00	Jl.Kuda	Jl.Kuda	1	19556	\N	\N	\N	\N	\N	\N	\N
36	UM	10500	2017-05-15	150350000117	Rudolf	10350000.00	1035000.00	12	18.00	10.00	948750.00	Jl.Lumping	Jl.Lumping	1	19756	\N	\N	\N	\N	\N	\N	\N
28	UM	10500	2017-08-03	150270000117	Rifki	9850000.00	985000.00	12	18.00	10.00	902917.00	Jl.Muan	Jl.Muan	1	17456	\N	\N	\N	\N	\N	\N	\N
48	NC	10500	2017-07-17	150470000117	Diego	463350000.00	46335000.00	12	18.00	10.00	42473750.00	Jl.Spring	Jl.Spring	1	12980	\N	\N	\N	\N	\N	\N	\N
12	NC	10500	2017-08-12	150110000117	Andi	655800000.00	98370000.00	18	18.00	10.00	41898333.00	Jl.Markatim 2	Jl.Markatim	1	13450	\N	\N	\N	\N	\N	\N	\N
2	NC	10500	2017-04-17	150010000117	Yadi	160700000.00	32140000.00	24	18.00	10.00	8035000.00	Jl.Mundur1	Jl.Maju1	1	14345	\N	\N	\N	\N	\N	\N	\N
3	NC	10500	2017-07-21	150020000117	Yanuar	140100000.00	21015000.00	18	18.00	10.00	8950833.00	Jl.Merdeka1	Jl.Maerdeka1	1	31662	\N	\N	\N	\N	\N	\N	\N
4	NC	10500	2017-07-25	150030000117	Yoko	130100000.00	1301000.00	12	18.00	10.00	11925833.00	Jl.Pendek	Jl.Pendek	1	14330	\N	\N	\N	\N	\N	\N	\N
21	NC	10500	2017-09-28	150200000117	Kintan	158350000.00	15835000.00	12	18.00	10.00	14515417.00	Jl.Sans	Jl.Sans	1	13456	01	\N	\N	\N	\N	\N	\N
1	NC	10500	2017-05-13	105200000117	Yanto	80100000.00	8010000.00	12	18.00	10.00	7342500.00	Jl.Jalan	Jl.Jalan1	1	14340	\N	\N	\N	\N	\N	\N	\N
22	NC	10500	2017-10-01	150210000117	Paul	238350000.00	23835000.00	12	18.00	10.00	21848750.00	Jl.Panas	Jl.Panas	1	13222	\N	\N	\N	\N	\N	\N	\N
17	NC	10500	2017-08-05	150160000117	Dedi	692551000.00	69255100.00	12	18.00	10.00	63483842.00	Jl.Sentosa2	Jl.Sentosa2	1	13662	\N	\N	\N	\N	\N	\N	\N
18	NC	10500	2018-08-22	150170000117	Pharrel	751351000.00	75135100.00	12	18.00	10.00	68873842.00	Jl.Akan	Jl.Akan	1	12990	\N	\N	\N	\N	\N	\N	\N
38	NC	10500	2017-07-17	150370000117	Seelah	463350000.00	46335000.00	12	18.00	10.00	42473750.00	Jl.Seafood	Jl.Seafood	1	59456	\N	\N	\N	\N	\N	\N	\N
33	NC	10500	2017-02-13	150320000117	Larry	186350000.00	18635000.00	12	18.00	10.00	17082083.00	Jl.Anjas	Jl.Anjas	1	13278	\N	\N	\N	\N	\N	\N	\N
34	NC	10500	2017-03-17	150330000117	Gary	516350000.00	51635000.00	12	18.00	10.00	47332083.00	Jl.Kurir	Jl.Kurir	1	17466	01	\N	\N	\N	\N	\N	\N
42	NC	10500	2017-12-13	150410000117	Anthony	457192000.00	45719200.00	12	18.00	10.00	41909267.00	Jl.Biasa	Jl.Biasa	1	16980	\N	\N	\N	\N	\N	\N	\N
41	NC	10500	2017-11-20	150400000117	John	457192000.00	45719200.00	12	18.00	10.00	41909267.00	Jl.Bagus	Jl.Bagus	1	11466	\N	\N	\N	\N	\N	\N	\N
47	NC	10500	2017-03-17	150460000117	Messi	459651100.00	45965110.00	12	18.00	10.00	42134684.00	Jl.Straight	Jl.Straight	1	18980	\N	\N	\N	\N	\N	\N	\N
45	NM	10500	2017-11-11	150440000117	Jack	21350000.00	2135000.00	12	18.00	10.00	1957083.00	Jl.Ikal	Jl.Ikal	1	16980	\N	\N	\N	\N	\N	\N	\N
\.


--
-- Data for Name: detail_account_receivable; Type: TABLE DATA; Schema: staging; Owner: postgres
--

COPY staging.detail_account_receivable (account_receivable_id, installment_to, due_date_to, principal_to, interest_installment_to, principal_payments_to, pay_interest, installment_payday_to, penalty_value, pay_penalty_to, penalty_paying_date, bill_charge_value_to, pay_bill_rate_to, bill_charge_date_to) FROM stdin;
1	1	2017-06-13	6117958.00	1224542.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
5	1	2017-08-01	1568997.00	691003.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
5	2	2017-09-01	1614500.00	645500.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
1	2	2017-07-13	6247971.00	1094529.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
1	3	2017-08-13	6307004.00	1035496.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
5	3	2017-10-01	1617666.00	642334.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
1	4	2017-09-13	6433716.00	908784.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
5	4	2017-11-01	1662319.00	597681.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
4	9	2018-04-01	11222092.00	703741.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
1	5	2017-10-13	6501780.00	840720.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
1	6	2017-11-13	6625090.00	717410.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
1	7	2017-12-13	6702459.00	640041.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
1	8	2018-01-13	6822265.00	520235.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
4	10	2018-05-01	11410819.00	515014.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
4	11	2018-06-01	11568097.00	357737.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
1	9	2018-02-13	6909221.00	433279.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
1	10	2018-03-13	7025416.00	317084.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
1	11	2018-04-13	7122249.00	220251.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
1	12	2018-05-13	7284872.00	57628.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
2	1	2017-05-13	5578271.00	2456729.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
2	3	2017-07-13	5751302.00	2283698.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
2	4	2017-08-13	5910057.00	2124943.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
2	5	2017-09-13	5929577.00	2105423.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
2	6	2017-10-13	6085219.00	1949781.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
2	7	2017-11-13	6113255.00	1921745.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
2	8	2017-12-13	6265690.00	1769310.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
2	9	2018-01-13	6302501.00	1732499.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
2	10	2018-02-13	6451630.00	1583370.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
2	11	2018-03-13	6497481.00	1537519.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
2	12	2018-04-13	6643206.00	1391794.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
2	13	2018-05-13	6698372.00	1336628.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
2	14	2018-06-13	6840588.00	1194412.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
2	15	2018-07-13	6905351.00	1129649.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
2	16	2018-08-13	7043953.00	991047.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
2	17	2018-09-13	7118604.00	916396.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
2	18	2018-10-13	7253481.00	781519.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
2	19	2018-11-13	7338319.00	696681.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
2	20	2018-12-13	7469360.00	565640.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
2	21	2019-01-13	7579865.00	455135.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
2	22	2019-02-13	7680573.00	354427.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
2	23	2019-03-13	7797991.00	237009.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
2	24	2019-04-13	7705306.00	329694.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
3	1	2017-08-01	6809031.00	2141803.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
3	2	2017-09-01	6978857.00	1971976.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
3	3	2017-10-01	7019815.00	1931018.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
3	4	2017-11-01	7185961.00	1764872.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
3	5	2017-12-01	7236989.00	1713845.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
3	6	2018-01-01	7399342.00	1551492.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
3	7	2018-02-01	7460744.00	1490089.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
3	8	2018-03-01	7619189.00	1331644.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
3	9	2018-04-01	7691281.00	1259552.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
3	10	2018-05-01	7845701.00	1105133.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
3	11	2018-06-01	7928805.00	1022028.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
3	12	2018-07-01	8079077.00	871757.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
3	13	2018-08-01	8173528.00	777305.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
3	14	2018-09-01	8319526.00	631307.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
3	15	2018-10-01	8425669.00	525164.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
3	16	2018-11-01	8567263.00	383570.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
3	17	2018-12-01	8685451.00	265382.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
3	18	2019-01-01	8673770.00	277063.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
2	2	2017-06-13	5740048.00	2294952.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
4	1	2017-08-01	9936907.00	1988926.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
4	12	2018-07-01	11832233.00	93600.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
5	13	2018-08-01	1884047.00	375953.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
5	5	2017-12-01	1667809.00	592191.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
5	6	2018-01-01	1711586.00	548414.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
5	14	2018-09-01	1924048.00	335952.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
5	8	2018-03-01	1762347.00	497653.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
4	2	2017-09-01	10148078.00	1777755.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
4	3	2017-10-01	10243960.00	1681873.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
4	4	2017-11-01	10449768.00	1476065.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
4	5	2017-12-01	10560319.00	1365514.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
4	6	2018-01-01	10760603.00	1165231.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
4	7	2018-02-01	10886266.00	1039567.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
4	8	2018-03-01	11080858.00	844976.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
11	1	2017-08-17	898981.00	179936.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
6	6	2018-02-01	1517733.00	164350.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
6	11	2018-07-01	1631626.00	50457.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
6	12	2018-08-01	1668881.00	13202.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
7	2	2017-10-06	6247971.00	1094529.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
7	8	2018-04-06	6822265.00	520235.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
5	7	2018-02-01	1719472.00	540528.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
5	9	2018-04-01	1772701.00	487299.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
5	10	2018-05-01	1814646.00	445354.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
5	11	2018-06-01	1827543.00	432457.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
5	12	2018-07-01	1868531.00	391469.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
5	15	2018-10-01	1942264.00	317736.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
5	16	2018-11-01	1981249.00	278751.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
5	17	2018-12-01	2002246.00	257754.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
5	18	2019-01-01	2040183.00	219817.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
5	19	2019-02-01	2064045.00	195955.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
5	20	2019-03-01	2100903.00	159097.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
5	21	2019-04-01	2131985.00	128015.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
5	22	2019-05-01	2160310.00	99690.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
5	23	2019-06-01	2193337.00	66663.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
5	24	2019-07-01	2167267.00	92733.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
6	1	2017-09-01	1401555.00	280529.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
6	2	2017-10-01	1431339.00	250744.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
6	3	2017-11-01	1444863.00	237220.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
6	4	2017-12-01	1473891.00	208192.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
6	5	2018-01-01	1489484.00	192599.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
6	7	2018-03-01	1535457.00	146626.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
6	8	2018-04-01	1562903.00	119180.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
6	9	2018-05-01	1582824.00	99259.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
6	10	2018-06-01	1609443.00	72640.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
7	10	2018-06-06	7025416.00	317084.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
8	3	2017-09-10	7019815.00	1931018.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
7	3	2017-11-06	6307004.00	1035496.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
7	4	2017-12-06	6433716.00	908784.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
7	6	2018-02-06	6625090.00	717410.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
7	7	2018-03-06	6702459.00	640041.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
7	11	2018-07-06	7122249.00	220251.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
8	2	2017-08-10	6978857.00	1971976.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
8	5	2017-11-10	7236989.00	1713845.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
7	1	2017-09-06	6117958.00	1224542.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
10	3	2017-07-01	2297658.00	912342.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
7	5	2018-01-06	6501780.00	840720.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
10	4	2017-08-01	2361081.00	848919.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
7	9	2018-05-06	6909221.00	433279.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
7	12	2018-08-06	8079077.00	57628.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
10	5	2017-09-01	2368879.00	841121.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
8	4	2017-10-10	7185961.00	1764872.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
10	6	2017-10-01	2431058.00	778942.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
8	6	2017-12-10	7399342.00	1551492.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
10	7	2017-11-01	2442259.00	767741.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
8	7	2018-01-10	7460744.00	1490089.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
8	8	2018-02-10	7619189.00	1331644.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
10	8	2017-12-01	2503157.00	706843.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
8	9	2018-03-10	7691281.00	1259552.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
10	9	2018-01-01	2517863.00	692137.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
8	10	2018-04-10	7845701.00	1105133.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
10	10	2018-02-02	2577440.00	632560.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
8	11	2018-05-10	7928805.00	1022028.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
10	11	2018-03-01	2595758.00	614242.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
8	12	2018-06-10	8079077.00	871757.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
10	12	2018-04-01	2653975.00	556025.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
8	13	2018-07-10	8173528.00	777305.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
10	13	2018-05-01	2676014.00	533986.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
8	14	2018-08-10	8319526.00	631307.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
10	14	2018-06-01	2732830.00	477170.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
8	15	2018-09-10	8425669.00	525164.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
10	15	2018-07-01	2758703.00	451297.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
8	16	2018-10-10	8567263.00	383570.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
10	16	2018-08-01	2814074.00	395926.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
8	17	2018-11-10	8685451.00	265382.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
10	17	2018-09-01	2843898.00	366102.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
8	18	2018-12-10	8673770.00	277063.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
9	1	2017-08-01	822602.00	164648.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
9	2	2017-09-01	840083.00	147167.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
10	18	2018-10-01	2897781.00	312219.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
10	19	2018-11-01	2931675.00	278325.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
9	3	2017-10-01	848020.00	139230.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
9	4	2017-11-01	865058.00	122192.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
9	5	2017-12-01	874209.00	113041.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
10	20	2018-12-01	2984025.00	225975.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
9	6	2018-01-01	890789.00	96461.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
10	21	2019-01-01	3028173.00	181827.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
10	22	2019-02-01	3068405.00	141595.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
9	7	2018-02-01	901192.00	86058.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
9	8	2018-03-01	917301.00	69949.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
9	9	2018-04-01	928993.00	58257.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
9	10	2018-05-01	944616.00	42634.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
9	11	2018-06-01	957636.00	29614.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
9	12	2018-07-01	979502.00	7748.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
10	1	2017-05-01	2228532.00	981468.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
12	11	2018-07-12	37114279.00	4784054.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
12	12	2018-08-12	37817691.00	4080642.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
12	13	2018-09-12	38259814.00	3638519.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
12	14	2018-10-12	38943222.00	2955112.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
12	15	2018-11-12	39440069.00	2458264.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
12	16	2018-12-12	40102865.00	1795469.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
12	17	2019-01-12	40656095.00	1242238.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
10	23	2019-03-01	3115314.00	94686.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
12	18	2019-02-12	40601417.00	1296916.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
10	2	2017-06-01	2293162.00	916838.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
10	24	2019-04-01	3078286.00	131714.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
13	6	2017-07-19	10760603.00	1365514.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
13	11	2017-12-19	11568097.00	50457.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
13	7	2017-08-19	10886266.00	1039567.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
11	2	2017-09-17	918085.00	160832.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
11	3	2017-10-17	926759.00	152157.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
11	4	2017-11-17	945379.00	133538.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
11	5	2017-12-17	955380.00	123537.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
11	6	2018-01-17	973500.00	105417.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
11	7	2018-02-17	984868.00	94048.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
11	8	2018-03-17	1002473.00	76444.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
11	9	2018-04-17	1015250.00	63667.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
13	8	2017-09-19	11080858.00	844976.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
11	10	2018-05-17	1032324.00	46593.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
13	12	2018-01-19	11832233.00	93600.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
11	11	2018-06-17	1046553.00	32364.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
11	12	2018-07-17	1070449.00	8468.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
12	1	2017-09-12	31872679.00	10025655.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
12	2	2017-10-12	32667628.00	9230706.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
12	3	2017-11-12	32859350.00	9038984.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
12	4	2017-12-12	33637068.00	8261265.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
12	5	2018-01-12	33875925.00	8022408.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
12	6	2018-02-12	34635890.00	7262443.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
12	7	2018-03-12	34923311.00	6975022.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
12	8	2018-04-12	35664985.00	6233348.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
12	9	2018-05-12	36002442.00	5895892.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
12	10	2018-06-12	36725271.00	5173063.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
13	1	2017-02-19	9936907.00	1988926.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
13	9	2017-10-19	11222092.00	703741.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
13	2	2017-03-19	10148078.00	1777755.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
14	2	2017-10-14	1213011.00	212497.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
13	10	2017-11-19	11410819.00	515014.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
13	3	2017-04-19	10243960.00	1681873.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
13	4	2017-05-19	10449768.00	1476065.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
13	5	2017-06-19	10560319.00	1662319.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
14	4	2017-12-14	1249073.00	176436.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
14	1	2017-09-14	1187770.00	237739.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
14	5	2018-01-14	1262287.00	163221.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
14	6	2018-02-14	1286227.00	139281.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
14	7	2018-03-14	1301248.00	124261.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
14	8	2018-04-14	1324507.00	101001.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
14	9	2018-05-14	1341389.00	84119.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
14	10	2018-06-14	1363948.00	61560.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
14	11	2018-07-14	1382748.00	42761.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
14	12	2018-08-14	1414320.00	11188.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
14	3	2017-11-14	1224472.00	201036.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
15	4	2017-11-18	938637.00	230529.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
15	6	2018-01-18	966509.00	202657.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
15	7	2018-02-18	974530.00	194637.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
15	10	2018-05-18	1024813.00	144354.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
15	1	2017-08-18	889402.00	279764.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
17	9	2018-05-05	59737673.00	3746169.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
17	10	2018-06-05	60742307.00	2741535.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
17	11	2018-07-05	61579531.00	1904311.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
17	12	2018-08-05	62985586.00	498255.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
19	11	2018-07-27	6827578.00	211139.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
19	12	2018-08-27	6983473.00	55244.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
15	2	2017-09-18	911585.00	257581.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
15	3	2017-10-18	916935.00	252231.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
15	5	2017-12-18	945303.00	223864.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
15	8	2018-03-18	995226.00	173941.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
15	9	2018-04-18	1004643.00	164524.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
15	11	2018-06-18	1035668.00	133498.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
15	12	2018-07-18	1055297.00	113870.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
15	13	2018-08-18	1067634.00	101532.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
15	14	2018-09-18	1086705.00	82462.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
15	15	2018-10-18	1100569.00	68597.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
15	16	2018-11-18	1119064.00	50102.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
15	17	2018-12-18	1134502.00	34664.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
15	18	2019-01-18	1132976.00	36190.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
18	1	2018-09-22	57387435.00	11486407.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
18	2	2018-10-22	58606983.00	10266858.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
18	3	2018-11-22	59160719.00	9713123.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
18	4	2018-12-22	60349300.00	8524542.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
18	5	2019-01-22	60987749.00	7886093.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
16	1	2017-09-04	958633.00	191876.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
16	2	2017-10-04	979005.00	171504.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
16	3	2017-11-04	988255.00	162254.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
16	4	2017-12-04	1008109.00	142399.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
16	5	2018-01-04	1018774.00	131734.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
16	6	2018-02-04	1038096.00	112412.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
16	7	2018-03-04	1050219.00	100289.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
18	6	2019-02-22	62144424.00	6729418.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
16	8	2018-04-04	1068992.00	81516.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
16	9	2018-05-04	1082617.00	67891.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
16	10	2018-06-04	1100824.00	49684.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
16	11	2018-07-04	1115997.00	34512.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
16	12	2018-08-04	1141479.00	9030.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
18	7	2019-03-22	62870153.00	6003688.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
18	8	2019-04-22	63993954.00	4879887.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
18	9	2019-05-22	64809610.00	4064232.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
18	10	2019-06-22	65899541.00	2974300.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
18	11	2019-07-22	66807848.00	2065993.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
18	12	2019-08-22	68333283.00	540559.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
20	1	2017-09-24	559094.00	111906.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
20	2	2017-10-24	570976.00	100024.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
20	3	2017-11-24	576370.00	94630.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
17	1	2017-09-05	52896350.00	10587492.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
17	2	2017-10-05	54020458.00	9463384.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
17	3	2017-11-05	54530859.00	8952983.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
17	4	2017-12-05	55626422.00	7857419.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
17	5	2018-01-05	56214907.00	7268935.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
17	6	2018-02-05	57281061.00	6202780.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
17	7	2018-03-05	57949996.00	5533846.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
17	8	2018-04-05	58985850.00	4497992.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
20	4	2017-12-24	587950.00	83050.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
20	5	2018-01-24	594170.00	76830.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
20	6	2018-02-24	605439.00	65561.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
20	7	2018-03-24	612509.00	58491.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
20	8	2018-04-24	623458.00	47542.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
20	9	2018-05-24	631404.00	39596.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
20	10	2018-06-24	642023.00	28977.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
20	11	2018-07-24	650872.00	20128.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
20	12	2018-08-24	665734.00	5266.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
19	1	2017-09-27	5864838.00	1173879.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
19	2	2017-10-27	5989472.00	1049245.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
19	3	2017-11-27	6046062.00	992654.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
19	4	2017-12-27	6167532.00	871185.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
19	5	2018-01-27	6232780.00	805937.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
19	6	2018-02-27	6350989.00	687728.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
19	7	2018-03-27	6425156.00	613560.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
19	8	2018-04-27	6540006.00	498711.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
19	9	2018-05-27	6623363.00	415353.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
19	10	2018-06-27	6734751.00	303965.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
8	1	2017-07-10	6809031.00	2141803.00	\N	\N	\N	\N	\N	\N	\N	\N	\N
\.


--
-- Name: role_id_seq; Type: SEQUENCE SET; Schema: auth; Owner: postgres
--

SELECT pg_catalog.setval('auth.role_id_seq', 4, true);


--
-- Name: user_id_seq; Type: SEQUENCE SET; Schema: auth; Owner: postgres
--

SELECT pg_catalog.setval('auth.user_id_seq', 15, true);


--
-- Name: lkptrx_id_seq; Type: SEQUENCE SET; Schema: hist; Owner: postgres
--

SELECT pg_catalog.setval('hist.lkptrx_id_seq', 18, true);


--
-- Name: lkrtrx_id_seq; Type: SEQUENCE SET; Schema: hist; Owner: postgres
--

SELECT pg_catalog.setval('hist.lkrtrx_id_seq', 1, false);


--
-- Name: area_zip_id_seq; Type: SEQUENCE SET; Schema: param; Owner: postgres
--

SELECT pg_catalog.setval('param.area_zip_id_seq', 1, false);


--
-- Name: bizu_id_seq; Type: SEQUENCE SET; Schema: param; Owner: postgres
--

SELECT pg_catalog.setval('param.bizu_id_seq', 5, true);


--
-- Name: coll_bizu_id_seq; Type: SEQUENCE SET; Schema: param; Owner: postgres
--

SELECT pg_catalog.setval('param.coll_bizu_id_seq', 1, false);


--
-- Name: coll_drmt_id_seq; Type: SEQUENCE SET; Schema: param; Owner: postgres
--

SELECT pg_catalog.setval('param.coll_drmt_id_seq', 13, true);


--
-- Name: coll_empl_id_seq; Type: SEQUENCE SET; Schema: param; Owner: postgres
--

SELECT pg_catalog.setval('param.coll_empl_id_seq', 42, true);


--
-- Name: coll_id_seq; Type: SEQUENCE SET; Schema: param; Owner: postgres
--

SELECT pg_catalog.setval('param.coll_id_seq', 34, true);


--
-- Name: coll_zip_id_seq; Type: SEQUENCE SET; Schema: param; Owner: postgres
--

SELECT pg_catalog.setval('param.coll_zip_id_seq', 20, true);


--
-- Name: cycle_id_seq; Type: SEQUENCE SET; Schema: param; Owner: postgres
--

SELECT pg_catalog.setval('param.cycle_id_seq', 9, true);


--
-- Name: dcall_empl_id_seq; Type: SEQUENCE SET; Schema: param; Owner: postgres
--

SELECT pg_catalog.setval('param.dcall_empl_id_seq', 16, true);


--
-- Name: dcall_id_seq; Type: SEQUENCE SET; Schema: param; Owner: postgres
--

SELECT pg_catalog.setval('param.dcall_id_seq', 4, true);


--
-- Name: empl_id_seq; Type: SEQUENCE SET; Schema: param; Owner: postgres
--

SELECT pg_catalog.setval('param.empl_id_seq', 25, true);


--
-- Name: genparam_id_seq; Type: SEQUENCE SET; Schema: param; Owner: postgres
--

SELECT pg_catalog.setval('param.genparam_id_seq', 6, true);


--
-- Name: idcard_id_seq; Type: SEQUENCE SET; Schema: param; Owner: postgres
--

SELECT pg_catalog.setval('param.idcard_id_seq', 1, false);


--
-- Name: lkpcmnt_id_seq; Type: SEQUENCE SET; Schema: param; Owner: postgres
--

SELECT pg_catalog.setval('param.lkpcmnt_id_seq', 1, false);


--
-- Name: lkpprio_id_seq; Type: SEQUENCE SET; Schema: param; Owner: postgres
--

SELECT pg_catalog.setval('param.lkpprio_id_seq', 1, false);


--
-- Name: offi_id_seq; Type: SEQUENCE SET; Schema: param; Owner: postgres
--

SELECT pg_catalog.setval('param.offi_id_seq', 2, true);


--
-- Name: prof_id_seq; Type: SEQUENCE SET; Schema: param; Owner: postgres
--

SELECT pg_catalog.setval('param.prof_id_seq', 12, true);


--
-- Name: prof_staf_id_seq; Type: SEQUENCE SET; Schema: param; Owner: postgres
--

SELECT pg_catalog.setval('param.prof_staf_id_seq', 9, true);


--
-- Name: rem_empl_id_seq; Type: SEQUENCE SET; Schema: param; Owner: postgres
--

SELECT pg_catalog.setval('param.rem_empl_id_seq', 25, true);


--
-- Name: rem_id_seq; Type: SEQUENCE SET; Schema: param; Owner: postgres
--

SELECT pg_catalog.setval('param.rem_id_seq', 5, true);


--
-- Name: rem_zip_id_seq; Type: SEQUENCE SET; Schema: param; Owner: postgres
--

SELECT pg_catalog.setval('param.rem_zip_id_seq', 31, true);


--
-- Name: rmdl_zone_id_seq; Type: SEQUENCE SET; Schema: param; Owner: postgres
--

SELECT pg_catalog.setval('param.rmdl_zone_id_seq', 1, false);


--
-- Name: zip_id_seq; Type: SEQUENCE SET; Schema: param; Owner: postgres
--

SELECT pg_catalog.setval('param.zip_id_seq', 48, true);


--
-- Name: zone_id_seq; Type: SEQUENCE SET; Schema: param; Owner: postgres
--

SELECT pg_catalog.setval('param.zone_id_seq', 1, false);


--
-- Name: bucketdetail_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.bucketdetail_id_seq', 145, true);


--
-- Name: delegbucket_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.delegbucket_id_seq', 19, true);


--
-- Name: lkdtrx_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.lkdtrx_id_seq', 6, true);


--
-- Name: lkptrx_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.lkptrx_id_seq', 55, true);


--
-- Name: lkrtrx_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.lkrtrx_id_seq', 1, false);


--
-- Name: print_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.print_id_seq', 47, true);


--
-- Name: skpctrx_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.skpctrx_id_seq', 4, true);


--
-- Name: skpctrxdet_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.skpctrxdet_id_seq', 15, true);


--
-- Name: unit_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.unit_id_seq', 58, true);


--
-- Name: account_receivable_id_seq; Type: SEQUENCE SET; Schema: staging; Owner: postgres
--

SELECT pg_catalog.setval('staging.account_receivable_id_seq', 1, false);


--
-- Name: role role_pkey; Type: CONSTRAINT; Schema: auth; Owner: postgres
--

ALTER TABLE ONLY auth.role
    ADD CONSTRAINT role_pkey PRIMARY KEY (id);


--
-- Name: user user_pkey; Type: CONSTRAINT; Schema: auth; Owner: postgres
--

ALTER TABLE ONLY auth."user"
    ADD CONSTRAINT user_pkey PRIMARY KEY (id);


--
-- Name: user user_ukey; Type: CONSTRAINT; Schema: auth; Owner: postgres
--

ALTER TABLE ONLY auth."user"
    ADD CONSTRAINT user_ukey UNIQUE (uname);


--
-- Name: lkptrx lkptrx_pkey; Type: CONSTRAINT; Schema: hist; Owner: postgres
--

ALTER TABLE ONLY hist.lkptrx
    ADD CONSTRAINT lkptrx_pkey PRIMARY KEY (id);


--
-- Name: lkrtrx lkrtrx_pkey; Type: CONSTRAINT; Schema: hist; Owner: postgres
--

ALTER TABLE ONLY hist.lkrtrx
    ADD CONSTRAINT lkrtrx_pkey PRIMARY KEY (id);


--
-- Name: mou mou_pkey; Type: CONSTRAINT; Schema: hist; Owner: postgres
--

ALTER TABLE ONLY hist.mou
    ADD CONSTRAINT mou_pkey PRIMARY KEY (mouno);


--
-- Name: area_zip area_zip_pkey; Type: CONSTRAINT; Schema: param; Owner: postgres
--

ALTER TABLE ONLY param.area_zip
    ADD CONSTRAINT area_zip_pkey PRIMARY KEY (id);


--
-- Name: bizu bizu_pkey; Type: CONSTRAINT; Schema: param; Owner: postgres
--

ALTER TABLE ONLY param.bizu
    ADD CONSTRAINT bizu_pkey PRIMARY KEY (id);


--
-- Name: bizu bizu_ukey; Type: CONSTRAINT; Schema: param; Owner: postgres
--

ALTER TABLE ONLY param.bizu
    ADD CONSTRAINT bizu_ukey UNIQUE (code);


--
-- Name: coll_bizu coll_bizu_pkey; Type: CONSTRAINT; Schema: param; Owner: postgres
--

ALTER TABLE ONLY param.coll_bizu
    ADD CONSTRAINT coll_bizu_pkey PRIMARY KEY (id);


--
-- Name: coll_drmt coll_drmt_pkey; Type: CONSTRAINT; Schema: param; Owner: postgres
--

ALTER TABLE ONLY param.coll_drmt
    ADD CONSTRAINT coll_drmt_pkey PRIMARY KEY (id);


--
-- Name: coll_empl coll_empl_pkey; Type: CONSTRAINT; Schema: param; Owner: postgres
--

ALTER TABLE ONLY param.coll_empl
    ADD CONSTRAINT coll_empl_pkey PRIMARY KEY (id);


--
-- Name: coll_empl coll_empl_ukey; Type: CONSTRAINT; Schema: param; Owner: postgres
--

ALTER TABLE ONLY param.coll_empl
    ADD CONSTRAINT coll_empl_ukey UNIQUE (empl_id);


--
-- Name: coll coll_pkey; Type: CONSTRAINT; Schema: param; Owner: postgres
--

ALTER TABLE ONLY param.coll
    ADD CONSTRAINT coll_pkey PRIMARY KEY (id);


--
-- Name: coll_zip coll_zip_pkey; Type: CONSTRAINT; Schema: param; Owner: postgres
--

ALTER TABLE ONLY param.coll_zip
    ADD CONSTRAINT coll_zip_pkey PRIMARY KEY (id);


--
-- Name: cycle cycle_pkey; Type: CONSTRAINT; Schema: param; Owner: postgres
--

ALTER TABLE ONLY param.cycle
    ADD CONSTRAINT cycle_pkey PRIMARY KEY (id);


--
-- Name: cycle cycle_ukey; Type: CONSTRAINT; Schema: param; Owner: postgres
--

ALTER TABLE ONLY param.cycle
    ADD CONSTRAINT cycle_ukey UNIQUE (code);


--
-- Name: cycletype cycletype_pkey; Type: CONSTRAINT; Schema: param; Owner: postgres
--

ALTER TABLE ONLY param.cycletype
    ADD CONSTRAINT cycletype_pkey PRIMARY KEY (type);


--
-- Name: dcall_empl dcall_empl_pkey; Type: CONSTRAINT; Schema: param; Owner: postgres
--

ALTER TABLE ONLY param.dcall_empl
    ADD CONSTRAINT dcall_empl_pkey PRIMARY KEY (id);


--
-- Name: dcall_empl dcall_empl_ukey; Type: CONSTRAINT; Schema: param; Owner: postgres
--

ALTER TABLE ONLY param.dcall_empl
    ADD CONSTRAINT dcall_empl_ukey UNIQUE (empl_id);


--
-- Name: dcall dcall_pkey; Type: CONSTRAINT; Schema: param; Owner: postgres
--

ALTER TABLE ONLY param.dcall
    ADD CONSTRAINT dcall_pkey PRIMARY KEY (id);


--
-- Name: empl empl_pkey; Type: CONSTRAINT; Schema: param; Owner: postgres
--

ALTER TABLE ONLY param.empl
    ADD CONSTRAINT empl_pkey PRIMARY KEY (id);


--
-- Name: empl empl_ukey; Type: CONSTRAINT; Schema: param; Owner: postgres
--

ALTER TABLE ONLY param.empl
    ADD CONSTRAINT empl_ukey UNIQUE (regno);


--
-- Name: genparam genparam_pkey; Type: CONSTRAINT; Schema: param; Owner: postgres
--

ALTER TABLE ONLY param.genparam
    ADD CONSTRAINT genparam_pkey PRIMARY KEY (id);


--
-- Name: idcard idcard_pkey; Type: CONSTRAINT; Schema: param; Owner: postgres
--

ALTER TABLE ONLY param.idcard
    ADD CONSTRAINT idcard_pkey PRIMARY KEY (id);


--
-- Name: job job_pkey; Type: CONSTRAINT; Schema: param; Owner: postgres
--

ALTER TABLE ONLY param.job
    ADD CONSTRAINT job_pkey PRIMARY KEY (code);


--
-- Name: lkdentry lkdentry_pkey; Type: CONSTRAINT; Schema: param; Owner: postgres
--

ALTER TABLE ONLY param.lkdentry
    ADD CONSTRAINT lkdentry_pkey PRIMARY KEY (code);


--
-- Name: lkpcmnt lkpcmnt_pkey; Type: CONSTRAINT; Schema: param; Owner: postgres
--

ALTER TABLE ONLY param.lkpcmnt
    ADD CONSTRAINT lkpcmnt_pkey PRIMARY KEY (id);


--
-- Name: lkpentry lkpentry_pkey; Type: CONSTRAINT; Schema: param; Owner: postgres
--

ALTER TABLE ONLY param.lkpentry
    ADD CONSTRAINT lkpentry_pkey PRIMARY KEY (code);


--
-- Name: lkpprio lkpprio_pkey; Type: CONSTRAINT; Schema: param; Owner: postgres
--

ALTER TABLE ONLY param.lkpprio
    ADD CONSTRAINT lkpprio_pkey PRIMARY KEY (id);


--
-- Name: lkpprio lkpprio_seqno_ukey; Type: CONSTRAINT; Schema: param; Owner: postgres
--

ALTER TABLE ONLY param.lkpprio
    ADD CONSTRAINT lkpprio_seqno_ukey UNIQUE (type, seqno);


--
-- Name: lkpprio lkpprio_ukey; Type: CONSTRAINT; Schema: param; Owner: postgres
--

ALTER TABLE ONLY param.lkpprio
    ADD CONSTRAINT lkpprio_ukey UNIQUE (code);


--
-- Name: lkrentry lkrentry_pkey; Type: CONSTRAINT; Schema: param; Owner: postgres
--

ALTER TABLE ONLY param.lkrentry
    ADD CONSTRAINT lkrentry_pkey PRIMARY KEY (code);


--
-- Name: lkpprio lkrprio_seqno_ukey; Type: CONSTRAINT; Schema: param; Owner: postgres
--

ALTER TABLE ONLY param.lkpprio
    ADD CONSTRAINT lkrprio_seqno_ukey UNIQUE (type, seqno);


--
-- Name: lkpprio lkrprio_ukey; Type: CONSTRAINT; Schema: param; Owner: postgres
--

ALTER TABLE ONLY param.lkpprio
    ADD CONSTRAINT lkrprio_ukey UNIQUE (code);


--
-- Name: offi off_ukey; Type: CONSTRAINT; Schema: param; Owner: postgres
--

ALTER TABLE ONLY param.offi
    ADD CONSTRAINT off_ukey UNIQUE (code);


--
-- Name: offi offi_pkey; Type: CONSTRAINT; Schema: param; Owner: postgres
--

ALTER TABLE ONLY param.offi
    ADD CONSTRAINT offi_pkey PRIMARY KEY (id);


--
-- Name: prof prof_pkey; Type: CONSTRAINT; Schema: param; Owner: postgres
--

ALTER TABLE ONLY param.prof
    ADD CONSTRAINT prof_pkey PRIMARY KEY (id);


--
-- Name: prof_staf prof_staf_pkey; Type: CONSTRAINT; Schema: param; Owner: postgres
--

ALTER TABLE ONLY param.prof_staf
    ADD CONSTRAINT prof_staf_pkey PRIMARY KEY (id);


--
-- Name: rem_empl rem_empl_pkey; Type: CONSTRAINT; Schema: param; Owner: postgres
--

ALTER TABLE ONLY param.rem_empl
    ADD CONSTRAINT rem_empl_pkey PRIMARY KEY (id);


--
-- Name: rem_empl rem_empl_ukey; Type: CONSTRAINT; Schema: param; Owner: postgres
--

ALTER TABLE ONLY param.rem_empl
    ADD CONSTRAINT rem_empl_ukey UNIQUE (empl_id);


--
-- Name: rem rem_pkey; Type: CONSTRAINT; Schema: param; Owner: postgres
--

ALTER TABLE ONLY param.rem
    ADD CONSTRAINT rem_pkey PRIMARY KEY (id);


--
-- Name: rem_zip rem_zip_pkey; Type: CONSTRAINT; Schema: param; Owner: postgres
--

ALTER TABLE ONLY param.rem_zip
    ADD CONSTRAINT rem_zip_pkey PRIMARY KEY (id);


--
-- Name: rmdl_zone rmdl_zone_pkey; Type: CONSTRAINT; Schema: param; Owner: postgres
--

ALTER TABLE ONLY param.rmdl_zone
    ADD CONSTRAINT rmdl_zone_pkey PRIMARY KEY (id);


--
-- Name: rmdl_zone rmdl_zone_ukey; Type: CONSTRAINT; Schema: param; Owner: postgres
--

ALTER TABLE ONLY param.rmdl_zone
    ADD CONSTRAINT rmdl_zone_ukey UNIQUE (zone_id);


--
-- Name: stts status_ukey; Type: CONSTRAINT; Schema: param; Owner: postgres
--

ALTER TABLE ONLY param.stts
    ADD CONSTRAINT status_ukey UNIQUE (descr);


--
-- Name: stts stts_pkey; Type: CONSTRAINT; Schema: param; Owner: postgres
--

ALTER TABLE ONLY param.stts
    ADD CONSTRAINT stts_pkey PRIMARY KEY (id);


--
-- Name: zip zip_pkey; Type: CONSTRAINT; Schema: param; Owner: postgres
--

ALTER TABLE ONLY param.zip
    ADD CONSTRAINT zip_pkey PRIMARY KEY (id);


--
-- Name: zip zip_ukey; Type: CONSTRAINT; Schema: param; Owner: postgres
--

ALTER TABLE ONLY param.zip
    ADD CONSTRAINT zip_ukey UNIQUE (zipcd, subzipcd);


--
-- Name: zone zone_pkey; Type: CONSTRAINT; Schema: param; Owner: postgres
--

ALTER TABLE ONLY param.zone
    ADD CONSTRAINT zone_pkey PRIMARY KEY (id);


--
-- Name: zone zone_ukey; Type: CONSTRAINT; Schema: param; Owner: postgres
--

ALTER TABLE ONLY param.zone
    ADD CONSTRAINT zone_ukey UNIQUE (off_id, code);


--
-- Name: bucket bucket_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.bucket
    ADD CONSTRAINT bucket_pkey PRIMARY KEY (cntrno);


--
-- Name: bucketdetail bucketdetail_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.bucketdetail
    ADD CONSTRAINT bucketdetail_pkey PRIMARY KEY (id);


--
-- Name: delegbucket delegbucket_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.delegbucket
    ADD CONSTRAINT delegbucket_pkey PRIMARY KEY (id);


--
-- Name: lkdtrx lkdtrx_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.lkdtrx
    ADD CONSTRAINT lkdtrx_pkey PRIMARY KEY (id);


--
-- Name: lkptrx lkptrx_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.lkptrx
    ADD CONSTRAINT lkptrx_pkey PRIMARY KEY (id);


--
-- Name: lkrtrx lkrtrx_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.lkrtrx
    ADD CONSTRAINT lkrtrx_pkey PRIMARY KEY (id);


--
-- Name: print print_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.print
    ADD CONSTRAINT print_pkey PRIMARY KEY (id);


--
-- Name: skpctrx skpctrx_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.skpctrx
    ADD CONSTRAINT skpctrx_pkey PRIMARY KEY (id);


--
-- Name: skpctrxdet skpctrxdet_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.skpctrxdet
    ADD CONSTRAINT skpctrxdet_pkey PRIMARY KEY (id);


--
-- Name: unit unit_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.unit
    ADD CONSTRAINT unit_pkey PRIMARY KEY (id);


--
-- Name: account_receivable account_receivable_pkey; Type: CONSTRAINT; Schema: staging; Owner: postgres
--

ALTER TABLE ONLY staging.account_receivable
    ADD CONSTRAINT account_receivable_pkey PRIMARY KEY (id);


--
-- Name: detail_account_receivable detail_pkey; Type: CONSTRAINT; Schema: staging; Owner: postgres
--

ALTER TABLE ONLY staging.detail_account_receivable
    ADD CONSTRAINT detail_pkey PRIMARY KEY (account_receivable_id, installment_to);


--
-- Name: user_roles fk55itppkw3i07do3h7qoclqd4k; Type: FK CONSTRAINT; Schema: auth; Owner: postgres
--

ALTER TABLE ONLY auth.user_roles
    ADD CONSTRAINT fk55itppkw3i07do3h7qoclqd4k FOREIGN KEY (user_id) REFERENCES auth."user"(id);


--
-- Name: user_roles fkrhfovtciq1l558cw6udg0h0d3; Type: FK CONSTRAINT; Schema: auth; Owner: postgres
--

ALTER TABLE ONLY auth.user_roles
    ADD CONSTRAINT fkrhfovtciq1l558cw6udg0h0d3 FOREIGN KEY (role_id) REFERENCES auth.role(id);


--
-- Name: user user_empl_fkey; Type: FK CONSTRAINT; Schema: auth; Owner: postgres
--

ALTER TABLE ONLY auth."user"
    ADD CONSTRAINT user_empl_fkey FOREIGN KEY (empl_id) REFERENCES param.empl(id);


--
-- Name: lkptrx lkphist_bucket_fkey; Type: FK CONSTRAINT; Schema: hist; Owner: postgres
--

ALTER TABLE ONLY hist.lkptrx
    ADD CONSTRAINT lkphist_bucket_fkey FOREIGN KEY (cntrno) REFERENCES public.bucket(cntrno);


--
-- Name: lkptrx lkphist_lkpentry_fkey; Type: FK CONSTRAINT; Schema: hist; Owner: postgres
--

ALTER TABLE ONLY hist.lkptrx
    ADD CONSTRAINT lkphist_lkpentry_fkey FOREIGN KEY (entry_cd) REFERENCES param.lkpentry(code);


--
-- Name: lkptrx lkphist_print_fkey; Type: FK CONSTRAINT; Schema: hist; Owner: postgres
--

ALTER TABLE ONLY hist.lkptrx
    ADD CONSTRAINT lkphist_print_fkey FOREIGN KEY (print_id) REFERENCES public.print(id);


--
-- Name: lkrtrx lkrhist_bucket_fkey; Type: FK CONSTRAINT; Schema: hist; Owner: postgres
--

ALTER TABLE ONLY hist.lkrtrx
    ADD CONSTRAINT lkrhist_bucket_fkey FOREIGN KEY (cntrno) REFERENCES public.bucket(cntrno);


--
-- Name: lkrtrx lkrhist_lkrentry_fkey; Type: FK CONSTRAINT; Schema: hist; Owner: postgres
--

ALTER TABLE ONLY hist.lkrtrx
    ADD CONSTRAINT lkrhist_lkrentry_fkey FOREIGN KEY (entry_cd) REFERENCES param.lkrentry(code);


--
-- Name: lkrtrx lkrhist_print_fkey; Type: FK CONSTRAINT; Schema: hist; Owner: postgres
--

ALTER TABLE ONLY hist.lkrtrx
    ADD CONSTRAINT lkrhist_print_fkey FOREIGN KEY (print_id) REFERENCES public.print(id);


--
-- Name: mou profmou_prof_fkey; Type: FK CONSTRAINT; Schema: hist; Owner: postgres
--

ALTER TABLE ONLY hist.mou
    ADD CONSTRAINT profmou_prof_fkey FOREIGN KEY (prof_id) REFERENCES param.prof(id);


--
-- Name: coll coll_cycle_fkey; Type: FK CONSTRAINT; Schema: param; Owner: postgres
--

ALTER TABLE ONLY param.coll
    ADD CONSTRAINT coll_cycle_fkey FOREIGN KEY (cycle_id) REFERENCES param.cycle(id);


--
-- Name: coll coll_empl_fkey; Type: FK CONSTRAINT; Schema: param; Owner: postgres
--

ALTER TABLE ONLY param.coll
    ADD CONSTRAINT coll_empl_fkey FOREIGN KEY (spv_id) REFERENCES param.empl(id);


--
-- Name: coll coll_off_fkey; Type: FK CONSTRAINT; Schema: param; Owner: postgres
--

ALTER TABLE ONLY param.coll
    ADD CONSTRAINT coll_off_fkey FOREIGN KEY (off_id) REFERENCES param.offi(id);


--
-- Name: coll_bizu collbizu_bizu_fkey; Type: FK CONSTRAINT; Schema: param; Owner: postgres
--

ALTER TABLE ONLY param.coll_bizu
    ADD CONSTRAINT collbizu_bizu_fkey FOREIGN KEY (bizu_id) REFERENCES param.bizu(id);


--
-- Name: coll_bizu collbizu_coll_fkey; Type: FK CONSTRAINT; Schema: param; Owner: postgres
--

ALTER TABLE ONLY param.coll_bizu
    ADD CONSTRAINT collbizu_coll_fkey FOREIGN KEY (coll_id) REFERENCES param.coll(id);


--
-- Name: coll_drmt colldrmt_empl_fkey; Type: FK CONSTRAINT; Schema: param; Owner: postgres
--

ALTER TABLE ONLY param.coll_drmt
    ADD CONSTRAINT colldrmt_empl_fkey FOREIGN KEY (empl_id) REFERENCES param.empl(id);


--
-- Name: coll_drmt colldrmt_next_fkey; Type: FK CONSTRAINT; Schema: param; Owner: postgres
--

ALTER TABLE ONLY param.coll_drmt
    ADD CONSTRAINT colldrmt_next_fkey FOREIGN KEY (next_member_id) REFERENCES param.coll_empl(id);


--
-- Name: coll_drmt colldrmt_prev_fkey; Type: FK CONSTRAINT; Schema: param; Owner: postgres
--

ALTER TABLE ONLY param.coll_drmt
    ADD CONSTRAINT colldrmt_prev_fkey FOREIGN KEY (prev_member_id) REFERENCES param.coll_empl(id);


--
-- Name: coll_empl collempl_coll_fkey; Type: FK CONSTRAINT; Schema: param; Owner: postgres
--

ALTER TABLE ONLY param.coll_empl
    ADD CONSTRAINT collempl_coll_fkey FOREIGN KEY (coll_id) REFERENCES param.coll(id);


--
-- Name: coll_empl collempl_empl_fkey; Type: FK CONSTRAINT; Schema: param; Owner: postgres
--

ALTER TABLE ONLY param.coll_empl
    ADD CONSTRAINT collempl_empl_fkey FOREIGN KEY (empl_id) REFERENCES param.empl(id);


--
-- Name: area_zip collzip_collempl_fkey; Type: FK CONSTRAINT; Schema: param; Owner: postgres
--

ALTER TABLE ONLY param.area_zip
    ADD CONSTRAINT collzip_collempl_fkey FOREIGN KEY (coll_empl_id) REFERENCES param.coll_empl(id);


--
-- Name: coll_zip collzip_collempl_fkey; Type: FK CONSTRAINT; Schema: param; Owner: postgres
--

ALTER TABLE ONLY param.coll_zip
    ADD CONSTRAINT collzip_collempl_fkey FOREIGN KEY (coll_empl_id) REFERENCES param.coll_empl(id);


--
-- Name: area_zip collzip_zip_fkey; Type: FK CONSTRAINT; Schema: param; Owner: postgres
--

ALTER TABLE ONLY param.area_zip
    ADD CONSTRAINT collzip_zip_fkey FOREIGN KEY (zip_id) REFERENCES param.zip(id);


--
-- Name: coll_zip collzip_zip_fkey; Type: FK CONSTRAINT; Schema: param; Owner: postgres
--

ALTER TABLE ONLY param.coll_zip
    ADD CONSTRAINT collzip_zip_fkey FOREIGN KEY (zip_id) REFERENCES param.zip(id);


--
-- Name: cycle cycle_type_fkey; Type: FK CONSTRAINT; Schema: param; Owner: postgres
--

ALTER TABLE ONLY param.cycle
    ADD CONSTRAINT cycle_type_fkey FOREIGN KEY (type) REFERENCES param.cycletype(type);


--
-- Name: dcall dcall_cycle_fkey; Type: FK CONSTRAINT; Schema: param; Owner: postgres
--

ALTER TABLE ONLY param.dcall
    ADD CONSTRAINT dcall_cycle_fkey FOREIGN KEY (cycle_id) REFERENCES param.cycle(id);


--
-- Name: dcall dcall_empl_fkey; Type: FK CONSTRAINT; Schema: param; Owner: postgres
--

ALTER TABLE ONLY param.dcall
    ADD CONSTRAINT dcall_empl_fkey FOREIGN KEY (spv_id) REFERENCES param.empl(id);


--
-- Name: dcall dcall_off_fkey; Type: FK CONSTRAINT; Schema: param; Owner: postgres
--

ALTER TABLE ONLY param.dcall
    ADD CONSTRAINT dcall_off_fkey FOREIGN KEY (off_id) REFERENCES param.offi(id);


--
-- Name: dcall_empl dcallempl_dcall_fkey; Type: FK CONSTRAINT; Schema: param; Owner: postgres
--

ALTER TABLE ONLY param.dcall_empl
    ADD CONSTRAINT dcallempl_dcall_fkey FOREIGN KEY (dcall_id) REFERENCES param.dcall(id);


--
-- Name: dcall_empl dcallempl_empl_fkey; Type: FK CONSTRAINT; Schema: param; Owner: postgres
--

ALTER TABLE ONLY param.dcall_empl
    ADD CONSTRAINT dcallempl_empl_fkey FOREIGN KEY (empl_id) REFERENCES param.empl(id);


--
-- Name: empl empl_job_fkey; Type: FK CONSTRAINT; Schema: param; Owner: postgres
--

ALTER TABLE ONLY param.empl
    ADD CONSTRAINT empl_job_fkey FOREIGN KEY (job_cd) REFERENCES param.job(code);


--
-- Name: empl empl_off_fkey; Type: FK CONSTRAINT; Schema: param; Owner: postgres
--

ALTER TABLE ONLY param.empl
    ADD CONSTRAINT empl_off_fkey FOREIGN KEY (off_id) REFERENCES param.offi(id);


--
-- Name: workingarea fk26cyjbhiofo149wcj74q3fhh4; Type: FK CONSTRAINT; Schema: param; Owner: postgres
--

ALTER TABLE ONLY param.workingarea
    ADD CONSTRAINT fk26cyjbhiofo149wcj74q3fhh4 FOREIGN KEY (zipcode_id) REFERENCES param.zip(id);


--
-- Name: workingarea fkf4syd53rl6a54jt5b4ej3bo57; Type: FK CONSTRAINT; Schema: param; Owner: postgres
--

ALTER TABLE ONLY param.workingarea
    ADD CONSTRAINT fkf4syd53rl6a54jt5b4ej3bo57 FOREIGN KEY (member_id) REFERENCES param.coll_empl(id);


--
-- Name: workingarea fkp17gytypxpc7cqv0hgv90w980; Type: FK CONSTRAINT; Schema: param; Owner: postgres
--

ALTER TABLE ONLY param.workingarea
    ADD CONSTRAINT fkp17gytypxpc7cqv0hgv90w980 FOREIGN KEY (member_id) REFERENCES param.rem_empl(id);


--
-- Name: prof prof_off_fkey; Type: FK CONSTRAINT; Schema: param; Owner: postgres
--

ALTER TABLE ONLY param.prof
    ADD CONSTRAINT prof_off_fkey FOREIGN KEY (off_id) REFERENCES param.offi(id);


--
-- Name: prof prof_stts_fkey; Type: FK CONSTRAINT; Schema: param; Owner: postgres
--

ALTER TABLE ONLY param.prof
    ADD CONSTRAINT prof_stts_fkey FOREIGN KEY (status) REFERENCES param.stts(id);


--
-- Name: prof_staf profstaf_prof_fkey; Type: FK CONSTRAINT; Schema: param; Owner: postgres
--

ALTER TABLE ONLY param.prof_staf
    ADD CONSTRAINT profstaf_prof_fkey FOREIGN KEY (prof_id) REFERENCES param.prof(id);


--
-- Name: rem rem_cycle_fkey; Type: FK CONSTRAINT; Schema: param; Owner: postgres
--

ALTER TABLE ONLY param.rem
    ADD CONSTRAINT rem_cycle_fkey FOREIGN KEY (cycle_id) REFERENCES param.cycle(id);


--
-- Name: rem rem_empl_fkey; Type: FK CONSTRAINT; Schema: param; Owner: postgres
--

ALTER TABLE ONLY param.rem
    ADD CONSTRAINT rem_empl_fkey FOREIGN KEY (spv_id) REFERENCES param.empl(id);


--
-- Name: rem rem_off_fkey; Type: FK CONSTRAINT; Schema: param; Owner: postgres
--

ALTER TABLE ONLY param.rem
    ADD CONSTRAINT rem_off_fkey FOREIGN KEY (off_id) REFERENCES param.offi(id);


--
-- Name: rem_empl remempl_empl_fkey; Type: FK CONSTRAINT; Schema: param; Owner: postgres
--

ALTER TABLE ONLY param.rem_empl
    ADD CONSTRAINT remempl_empl_fkey FOREIGN KEY (empl_id) REFERENCES param.empl(id);


--
-- Name: rem_empl remempl_rem_fkey; Type: FK CONSTRAINT; Schema: param; Owner: postgres
--

ALTER TABLE ONLY param.rem_empl
    ADD CONSTRAINT remempl_rem_fkey FOREIGN KEY (rem_id) REFERENCES param.rem(id);


--
-- Name: rem_zip remzip_remempl_fkey; Type: FK CONSTRAINT; Schema: param; Owner: postgres
--

ALTER TABLE ONLY param.rem_zip
    ADD CONSTRAINT remzip_remempl_fkey FOREIGN KEY (rem_empl_id) REFERENCES param.rem_empl(id);


--
-- Name: rem_zip remzip_zip_fkey; Type: FK CONSTRAINT; Schema: param; Owner: postgres
--

ALTER TABLE ONLY param.rem_zip
    ADD CONSTRAINT remzip_zip_fkey FOREIGN KEY (zip_id) REFERENCES param.zip(id);


--
-- Name: rmdl_zone rmdlzone_rmdlempl_fkey; Type: FK CONSTRAINT; Schema: param; Owner: postgres
--

ALTER TABLE ONLY param.rmdl_zone
    ADD CONSTRAINT rmdlzone_rmdlempl_fkey FOREIGN KEY (rmdl_empl_id) REFERENCES param.rem_empl(id);


--
-- Name: rmdl_zone rmdlzone_zone_fkey; Type: FK CONSTRAINT; Schema: param; Owner: postgres
--

ALTER TABLE ONLY param.rmdl_zone
    ADD CONSTRAINT rmdlzone_zone_fkey FOREIGN KEY (zone_id) REFERENCES param.zone(id);


--
-- Name: zip zip_off_fkey; Type: FK CONSTRAINT; Schema: param; Owner: postgres
--

ALTER TABLE ONLY param.zip
    ADD CONSTRAINT zip_off_fkey FOREIGN KEY (off_id) REFERENCES param.offi(id);


--
-- Name: zip zip_zone_fkey; Type: FK CONSTRAINT; Schema: param; Owner: postgres
--

ALTER TABLE ONLY param.zip
    ADD CONSTRAINT zip_zone_fkey FOREIGN KEY (zone_id) REFERENCES param.zone(id);


--
-- Name: zone zone_off_fkey; Type: FK CONSTRAINT; Schema: param; Owner: postgres
--

ALTER TABLE ONLY param.zone
    ADD CONSTRAINT zone_off_fkey FOREIGN KEY (off_id) REFERENCES param.offi(id);


--
-- Name: bucket bucket_cycle_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.bucket
    ADD CONSTRAINT bucket_cycle_fkey FOREIGN KEY (cycle_id) REFERENCES param.cycle(id);


--
-- Name: bucketdetail detail_bucket_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.bucketdetail
    ADD CONSTRAINT detail_bucket_fkey FOREIGN KEY (cntrno) REFERENCES public.bucket(cntrno);


--
-- Name: lkdtrx lkdtrx_bucket_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.lkdtrx
    ADD CONSTRAINT lkdtrx_bucket_fkey FOREIGN KEY (cntrno) REFERENCES public.bucket(cntrno);


--
-- Name: lkdtrx lkdtrx_lkdentry_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.lkdtrx
    ADD CONSTRAINT lkdtrx_lkdentry_fkey FOREIGN KEY (entry_cd) REFERENCES param.lkdentry(code);


--
-- Name: lkdtrx lkdtrx_print_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.lkdtrx
    ADD CONSTRAINT lkdtrx_print_fkey FOREIGN KEY (print_id) REFERENCES public.print(id);


--
-- Name: lkptrx lkptrx_bucket_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.lkptrx
    ADD CONSTRAINT lkptrx_bucket_fkey FOREIGN KEY (cntrno) REFERENCES public.bucket(cntrno);


--
-- Name: skpctrx lkptrx_bucket_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.skpctrx
    ADD CONSTRAINT lkptrx_bucket_fkey FOREIGN KEY (cntrno) REFERENCES public.bucket(cntrno);


--
-- Name: lkdtrx lkptrx_bucket_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.lkdtrx
    ADD CONSTRAINT lkptrx_bucket_fkey FOREIGN KEY (cntrno) REFERENCES public.bucket(cntrno);


--
-- Name: lkptrx lkptrx_coll_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.lkptrx
    ADD CONSTRAINT lkptrx_coll_fkey FOREIGN KEY (team_id) REFERENCES param.coll(id);


--
-- Name: lkptrx lkptrx_empl_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.lkptrx
    ADD CONSTRAINT lkptrx_empl_fkey FOREIGN KEY (asgnd_to) REFERENCES param.empl(id);


--
-- Name: lkptrx lkptrx_lkpentry_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.lkptrx
    ADD CONSTRAINT lkptrx_lkpentry_fkey FOREIGN KEY (entry_cd) REFERENCES param.lkpentry(code);


--
-- Name: lkptrx lkptrx_print_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.lkptrx
    ADD CONSTRAINT lkptrx_print_fkey FOREIGN KEY (print_id) REFERENCES public.print(id);


--
-- Name: lkptrx lkptrxspv_empl_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.lkptrx
    ADD CONSTRAINT lkptrxspv_empl_fkey FOREIGN KEY (spv_id) REFERENCES param.empl(id);


--
-- Name: lkrtrx lkrtrx_bucket_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.lkrtrx
    ADD CONSTRAINT lkrtrx_bucket_fkey FOREIGN KEY (cntrno) REFERENCES public.bucket(cntrno);


--
-- Name: lkrtrx lkrtrx_coll_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.lkrtrx
    ADD CONSTRAINT lkrtrx_coll_fkey FOREIGN KEY (team_id) REFERENCES param.rem(id);


--
-- Name: lkrtrx lkrtrx_empl_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.lkrtrx
    ADD CONSTRAINT lkrtrx_empl_fkey FOREIGN KEY (asgnd_to) REFERENCES param.empl(id);


--
-- Name: lkrtrx lkrtrx_lkpentry_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.lkrtrx
    ADD CONSTRAINT lkrtrx_lkpentry_fkey FOREIGN KEY (entry_cd) REFERENCES param.lkpentry(code);


--
-- Name: lkrtrx lkrtrx_lkrentry_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.lkrtrx
    ADD CONSTRAINT lkrtrx_lkrentry_fkey FOREIGN KEY (entry_cd) REFERENCES param.lkrentry(code);


--
-- Name: lkrtrx lkrtrx_print_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.lkrtrx
    ADD CONSTRAINT lkrtrx_print_fkey FOREIGN KEY (print_id) REFERENCES public.print(id);


--
-- Name: lkrtrx lkrtrxspv_empl_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.lkrtrx
    ADD CONSTRAINT lkrtrxspv_empl_fkey FOREIGN KEY (spv_id) REFERENCES param.empl(id);


--
-- Name: skpctrx skpctrx_print_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.skpctrx
    ADD CONSTRAINT skpctrx_print_fkey FOREIGN KEY (print_id) REFERENCES public.print(id);


--
-- Name: skpctrx skpctrx_status_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.skpctrx
    ADD CONSTRAINT skpctrx_status_fkey FOREIGN KEY (status) REFERENCES param.stts(id);


--
-- Name: skpctrxdet skpctrxdet_skpctrx_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.skpctrxdet
    ADD CONSTRAINT skpctrxdet_skpctrx_fkey FOREIGN KEY (skpctrx_id) REFERENCES public.skpctrx(id);


--
-- Name: unit unit_bucket_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.unit
    ADD CONSTRAINT unit_bucket_fkey FOREIGN KEY (cntrno) REFERENCES public.bucket(cntrno);


--
-- Name: vw_ardetail; Type: MATERIALIZED VIEW DATA; Schema: staging; Owner: postgres
--

REFRESH MATERIALIZED VIEW staging.vw_ardetail;


--
-- Name: vw_arversion; Type: MATERIALIZED VIEW DATA; Schema: staging; Owner: postgres
--

REFRESH MATERIALIZED VIEW staging.vw_arversion;


--
-- Name: vw_armaster; Type: MATERIALIZED VIEW DATA; Schema: staging; Owner: postgres
--

REFRESH MATERIALIZED VIEW staging.vw_armaster;


--
-- PostgreSQL database dump complete
--

